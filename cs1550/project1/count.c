#include <stdio.h>
#include <unistd.h>

void write_to_parent(int pipefd[2], int countData[26])
{
    close(pipefd[0]);
    
    if (write(pipefd[1], countData, sizeof(int) * 26) != sizeof(int) * 26)
    {
        fprintf(stderr, "Error while writing count data to a pipe.\n");
        exit(-1);
    }
    
    close(pipefd[1]);
}

int main(int argc, char **argv)
{
    int countData[26];
    int index;
    int pipefd[2];
    
    for (index = 0; index < 26; index++)
    {
        countData[index] = 0;
    }
    
    pipefd[0] = (int)argv[0][0];
    pipefd[1] = (int)argv[0][1];
    
    countData[argv[1][0] - 97]++;
    countData[argv[1][1] - 97]++;

    write_to_parent(pipefd, countData);
    return 0;
}
