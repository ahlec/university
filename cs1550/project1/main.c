#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

struct processData
{
    int length;
    char* characters;
};
typedef struct processData ProcessData;

ProcessData read_from_parent(int pipefd[2])
{
    ProcessData data;
    int dataLength;
    
    close(pipefd[1]);
    
    /* get data length */
    if (read(pipefd[0], &dataLength, sizeof(int)) != sizeof(int))
    {
        fprintf(stderr, "Error while reading data length from a pipe.\n");
        exit(-1);
    }
    data.length = dataLength;
    data.characters = (char *)malloc(sizeof(char) * dataLength);
    
    /* get data */
    if (read(pipefd[0], data.characters, sizeof(char) * dataLength) != sizeof(char) * dataLength)
    {
        fprintf(stderr, "Error while reading data from a pipe.\n");
        exit(-1);
    }
    
    close(pipefd[0]);
    
    return data;    
}
void read_from_child(int pipefd[2], int *countData)
{
    close(pipefd[1]);
    
    if (read(pipefd[0], countData, sizeof(int) * 26) != sizeof(int) * 26)
    {
        fprintf(stderr, "Error while reading count data to a pipe.\n");
        exit(-1);
    }
    
    close(pipefd[0]);
}

void write_to_child(int pipefd[2], char *data, int offset, int length)
{
    int index;
    
    close(pipefd[0]);
    
    /* write data length */
    if (write(pipefd[1], &length, sizeof(int)) != sizeof(int))
    {
        fprintf(stderr, "Error while writing data length to a pipe.\n");
        exit(-1);
        return;
    }
    
    /* write data */
    if (write(pipefd[1], &data[offset], sizeof(char) * length) != sizeof(char) * length)
    {
        fprintf(stderr, "Error while writing data to a pipe.\n");
        exit(-1);
        return;
    }
    
    close(pipefd[1]);
}
void write_to_parent(int pipefd[2], int countData[26])
{
    close(pipefd[0]);
    
    if (write(pipefd[1], countData, sizeof(int) * 26) != sizeof(int) * 26)
    {
        fprintf(stderr, "Error while writing count data to a pipe.\n");
        exit(-1);
    }
    
    close(pipefd[1]);
}

void printCharacterCounts(int countData[26])
{
    int index;
    for (index = 0; index < 26; index++)
    {
        if (countData[index] <= 0)
        {
            continue;
        }
        printf("[%d] %c: %d\n", getpid(), index + 97, countData[index]);
    }
}

int accumulateChildCount(int child1_pipe[2], int child2_pipe[2], int *count)
{
    char child1_returned = 0;
    char child2_returned = 0;
    int child1_count[26];
    int child2_count[26];
    int max_child_pid;
    fd_set rfds;
    int index;
    int select_return = 0;
    
    close(child1_pipe[1]);
    close(child2_pipe[1]);
    while (1)
    {
        FD_ZERO(&rfds);
        max_child_pid = -1;
        if (!child1_returned)
        {
            FD_SET(child1_pipe[0], &rfds);
            max_child_pid = child1_pipe[0];
        }
        if (!child2_returned)
        {
            FD_SET(child2_pipe[0], &rfds);
            if (child2_pipe[0] > max_child_pid)
            {
                max_child_pid = child2_pipe[0];
            }
        }
        
        if (max_child_pid == -1)
        {
            break;
        }
        select_return = select(max_child_pid + 1, &rfds, NULL, NULL, NULL);
        if (select_return == -1)
        {
            fprintf(stderr, "Error returned when using select().\n");
            return -1;
        }
        if (select_return == 1)
        {
            if (!child1_returned && FD_ISSET(child1_pipe[0], &rfds))
            {
                if (read(child1_pipe[0], child1_count, sizeof(int) * 26) != sizeof(int) * 26)
                {
                    fprintf(stderr, "1Error while reading count data from child1 pipe.\n");
                    return -1;
                }
                close(child1_pipe[0]);
                child1_returned = 1;
            }
            if (!child2_returned && FD_ISSET(child2_pipe[0], &rfds))
            {
                if (read(child2_pipe[0], child2_count, sizeof(int) * 26) != sizeof(int) * 26)
                {
                    fprintf(stderr, "2Error while reading count data from child2 pipe.\n");
                    return -1;
                }
                close(child2_pipe[0]);
                child2_returned = 1;
            }
        }
        else if (select_return == 2)
        {
            if (read(child1_pipe[0], child1_count, sizeof(int) * 26) != sizeof(int) * 26)
            {
                fprintf(stderr, "3Error while reading count data from child1 pipe.\n");
                return -1;
            }
            close(child1_pipe[0]);
            if (read(child2_pipe[0], child2_count, sizeof(int) * 26) != sizeof(int) * 26)
            {
                fprintf(stderr, "4Error while reading count data from child2 pipe.\n");
                return -1;
            }
            close(child2_pipe[0]);
            child1_returned = 1;
            child2_returned = 1;
        }
    }
    
    for (index = 0; index < 26; index++)
    {
        count[index] = child1_count[index] + child2_count[index];
    }
    
    return 0;
}

int recurseProcesses(int pipefd[2], int return_pipefd[2])
{
    int child_pipefd[4][2];
    int child_pid;
    int child_pid2;
    ProcessData processData;
    int index;
    int countData[26];
    char pipeChar[2];
    
    processData = read_from_parent(pipefd);
    
    if (processData.length > 2)
    {
        if (pipe(child_pipefd[0]) == -1)
        {
            fprintf(stderr, "Could not establish the pipes.\n");
            return -1;
        }
        if (pipe(child_pipefd[1]) == -1)
        {
            fprintf(stderr, "Could not establish the pipes.\n");
            return -1;
        }
        
        if (pipe(child_pipefd[2]) == -1)
        {
            fprintf(stderr, "Could not establish the pipes.\n");
            return -1;
        }
        if (pipe(child_pipefd[3]) == -1)
        {
            fprintf(stderr, "Could not establish the pipes.\n");
            return -1;
        }
        
        child_pid = fork();
        if (child_pid == -1)
        {
            fprintf(stderr, "Failed to fork.\n");
            return -2;
        }
        else if (child_pid == 0)
        {
            recurseProcesses(child_pipefd[0], child_pipefd[1]);
            return 0;
        }
        
        child_pid2 = fork();
        if (child_pid2 == -1)
        {
            fprintf(stderr, "Failed to fork.\n");
            return -2;
        }
        else if (child_pid2 == 0)
        {
            recurseProcesses(child_pipefd[2], child_pipefd[3]);
            return 0;
        }
        
        write_to_child(child_pipefd[0], processData.characters, 0, processData.length / 2);
        write_to_child(child_pipefd[2], processData.characters, processData.length / 2, processData.length / 2);
        
        if (accumulateChildCount(child_pipefd[1], child_pipefd[3], countData) == -1)
        {
            fprintf(stderr, "Error when accumulating child count in process #%d.\n", getpid());
            exit (-1);
        }
        
        write_to_parent(return_pipefd, countData);
        return 0;
    }
    else
    {
        pipeChar[0] = (char)return_pipefd[0];
        pipeChar[1] = (char)return_pipefd[1];
        execl("./count", pipeChar, processData.characters, (char *)NULL);
    }
}

int main(int argc, char** argv)
{
    int pipefd[4][2];
    int arrayLength;
    int tempLength;
    char *characters;
    int pid;
    int pid2;
    int characterCount[26];
    
    // Determine if array length is proper
    arrayLength = argc - 1;
    if (arrayLength <= 2)
    {
        fprintf(stderr, "Invalid number of arguments. Number of characters provided must be greater than 2.\n");
        return 1;
    }
    tempLength = arrayLength;
    while (((tempLength & 1) == 0) && tempLength > 0)
    {
        tempLength >>= 1;
    }
    if (tempLength != 1)
    {
        fprintf(stderr, "Invalid number of arguments. Must provide 2^k number of characters, where k > 1.\n");
        return 1;
    }
    characters = (char *)malloc(sizeof(char) * arrayLength);
    for (tempLength = 0; tempLength < arrayLength; tempLength++)
    {
        characters[tempLength] = argv[tempLength + 1][0];
        if (characters[tempLength] < 97 || characters[tempLength] > 122)
        {
            if (characters[tempLength] <= 90 && characters[tempLength] >= 65)
            {
                characters[tempLength] += 32;
            }
            else
            {
                fprintf(stderr, "Invalid character. '%c' is not a character that can be counted in this program.\n", characters[tempLength]);
                return 1;
            }
        }
    }
    // Finish determining if length is proper
    
    if (pipe(pipefd[0]) == -1)
    {
        fprintf(stderr, "Could not establish the pipes.\n");
        return 1;
    }
    if (pipe(pipefd[1]) == -1)
    {
        fprintf(stderr, "Could not establish the pipes.\n");
        return 1;
    }
    if (pipe(pipefd[2]) == -1)
    {
        fprintf(stderr, "Could not establish the pipes.\n");
        return 1;
    }
    if (pipe(pipefd[3]) == -1)
    {
        fprintf(stderr, "Could not establish the pipes.\n");
        return 1;
    }
    
    pid = fork();
    if (pid == -1)
    {
        fprintf(stderr, "Failed to fork\n");
        return -2;
    }
    else if (pid == 0)
    {
        recurseProcesses(pipefd[0], pipefd[1]);
        return 0;
    }
    
    pid2 = fork();
    if (pid2 == -1)
    {
        fprintf(stderr, "Failed to fork\n");
        return -2;
    }
    else if (pid2 == 0)
    {
        recurseProcesses(pipefd[2], pipefd[3]);
        return 0;
    }
    
    write_to_child(pipefd[0], characters, 0, arrayLength / 2);
    write_to_child(pipefd[2], characters, arrayLength / 2, arrayLength / 2);
    
    if (accumulateChildCount(pipefd[1], pipefd[3], characterCount) == -1)
    {
        fprintf(stderr, "Error accumulating the character counts in the root process.\n");
        exit(-1);
    }
    
    printf("Root process is pid #%d.\n", getpid());
    printCharacterCounts(characterCount);
}
