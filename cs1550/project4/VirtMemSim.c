#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BIT_MASK_16 65535
#define BIT_MASK_8 255
#define PAGE_TABLE_P_BIT_MASK 65536
#define PHYSICAL_MEMORY_SIZE 65536
#define FRAME_SIZE 256
#define TLB_SIZE 16

int tlbLookup(unsigned int* tlb, int pageNumber)
{
	int index;
	int tlbEntry;

	for (index = 0; index < TLB_SIZE; ++index)
	{
		tlbEntry = tlb[index];
		if (tlbEntry == -1)
		{
			return -1;
		}

		if ((tlbEntry >> 16) == pageNumber)
		{
			return (tlbEntry & BIT_MASK_16);
		}
	}

	return -1;
}

void updateTlb(unsigned int* tlb, int pageNumber, int frame)
{
	int index;
	int nextTlbEntry;
	int tempNextTlbEntry;
	int done = 0;
	int newTlbEntry = (pageNumber << 16) + frame;

	nextTlbEntry = newTlbEntry;
	for (index = 0; index < TLB_SIZE; ++index)
	{
		done = (tlb[index] == newTlbEntry);
		tempNextTlbEntry = tlb[index];
		tlb[index] = nextTlbEntry;
		if (done)
		{
			break;
		}
		nextTlbEntry = tempNextTlbEntry;
	}
}

int pageFault(unsigned int* pageTable, int pageNumber,
	FILE* backingStore, char* physicalMemory, int* framePointer)
{
	fseek(backingStore, pageNumber * FRAME_SIZE, SEEK_SET);
	if (fread(&physicalMemory[*framePointer], sizeof(char), FRAME_SIZE,
		backingStore) != FRAME_SIZE)
	{
		return -1;
	}

	pageTable[pageNumber] = PAGE_TABLE_P_BIT_MASK;
	pageTable[pageNumber] |= *framePointer;
	
	*framePointer += FRAME_SIZE;

	return 0;
}

int main(int argc, char** argv)
{
	char physicalMemory[PHYSICAL_MEMORY_SIZE];
	FILE* backingStore;
	FILE* addressFile;
	char addressReadBuffer[7];
	unsigned int tlb[TLB_SIZE];
	unsigned int pageTable[255];
	int index;
	int address;
	int pageNumber;
	int offset;
	int frame;
	int framePointer;
	int physicalAddress;
	FILE* myCorrect;
	int numberAddressesRead = 0;
	int numberPageFaults = 0;
	int numberTlbHits = 0;

	// Process command-line arguments, open files
	if (argc != 3)
	{
		printf("Invalid number of arguments. Must provide two (2) "
			"command line arguments (backing-store file and "
			"address file.\n");
		return 1;
	}
	backingStore = fopen(argv[1], "rb");
	if (backingStore == NULL)
	{
		printf("Invalid first parameter. Please provide an existant"
			" file for the backing store.\n");
		return 1;
	}
	addressFile = fopen(argv[2], "rb");
	if (addressFile == NULL)
	{
		fclose(backingStore);
		printf("Invalid second parameter. Please provide an existant"
			" file for the address file.\n");
		return 1;
	}

	// Clear everything, set up pointer
	for (index = 0; index < 255; ++index)
	{
		pageTable[index] = 0;
	}
	for (index = 0; index < TLB_SIZE; ++index)
	{
		tlb[index] = -1;
	}
	framePointer = 0;

	// Open our output file
	myCorrect = fopen("myCorrect.txt", "w");
	if (myCorrect == NULL)
	{
		fclose(addressFile);
		fclose(backingStore);
		printf("Could not open myCorrect.txt. Aborting.\n");
		return 3;
	}

	// Work the magic!
	while (!feof(addressFile))
	{
		if (fgets(addressReadBuffer, 7, addressFile) == NULL)
		{
			continue;
		}
		++numberAddressesRead;

		address = atoi(addressReadBuffer) & BIT_MASK_16;
		pageNumber = address >> 8;
		offset = address & BIT_MASK_8;

		frame = tlbLookup(tlb, pageNumber);
		if (frame >= 0)
		{
			++numberTlbHits;
		}	


		if (frame < 0)
		{
			if ((pageTable[pageNumber] &
				PAGE_TABLE_P_BIT_MASK) == 0)
			{
				if (pageFault(pageTable, pageNumber,
					backingStore, physicalMemory,
					&framePointer))
				{
					printf("Could not load from backing "
						"store. Aborting.\n");
					break;
				}
				++numberPageFaults;
			}
			frame = (pageTable[pageNumber] & BIT_MASK_16);
		}

		physicalAddress = frame + offset;
		updateTlb(tlb, pageNumber, frame);

		fprintf(myCorrect, "Virtual address: %d Physical address: "
			"%d Value: %d\n", address, physicalAddress,
			physicalMemory[physicalAddress]);
	}

	// Write out our stats
	fprintf(myCorrect, "Number of Translated Addresses = %d\n",
		numberAddressesRead);
	fprintf(myCorrect, "Page Faults = %d\n", numberPageFaults);
	fprintf(myCorrect, "Page Fault Rate = %.3f\n",
		numberPageFaults / (float)numberAddressesRead);
	fprintf(myCorrect, "TLB Hits = %d\n", numberTlbHits);
	fprintf(myCorrect, "TLB Hit Rate = %.3f\n",
		numberTlbHits / (float)numberAddressesRead);

	// finish up
	fclose(myCorrect);
	fclose(addressFile);
	fclose(backingStore);
	backingStore = 0;
	return 0;
}
