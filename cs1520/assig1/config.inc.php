<?php
if (!defined ("ASSIG1"))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

// CONFIG PARAMETERS
//    * These are optional config parameters that enable or disable functionality
//      for this assignment. The values provided below are the defaults, and the recommended
//      values for each setting; however, the program has been tested and verified with
//      the non-default values (so long as "data type" is preserved. 
define ("DELETE_USERS_WITHOUT_HOURS", true);
define ("FORCE_OWNERSHIP_TO_OVERRIDE_WITH_NEW", true);

// -------------------------
//   DO NOT MODIFY BELOW THIS LINE
// -------------------------

@session_start();
define ("DATE_FORMAT", "l<b\\r />j/m/y<b\\r />h:i a");
define ("CHECK_SYMBOL", "&#x2713;");
define ("YEAR_IN_MILLISECONDS", 365 * 24 * 60 * 60);
define ("USERNAME_CLAIM_SESSION", "assig1_jbd19_usernameclaims");
define ("PROCESS_RESULTS_MESSAGE", "assig1_jbd19_procResultsMsg");
define ("PROCESS_RESULTS_STATUS", "assig1_jbd19_procResultsStatus");
date_default_timezone_set("America/New_York");

// Defining username claim functions
function hasOwnershipOf($userName)
{
  if (!isset($_COOKIE[USERNAME_CLAIM_SESSION]))
  {
    return false;
  }
  return in_array($userName, $_COOKIE[USERNAME_CLAIM_SESSION]);
}
function getUsernameClaims()
{
  if (!isset($_COOKIE[USERNAME_CLAIM_SESSION]))
  {
    return array();
  }
  return $_COOKIE[USERNAME_CLAIM_SESSION];
}
function commitUsernameClaims($usernameClaims)
{
  $uniqueClaims = array_unique($usernameClaims);
  $originalClaimLength = count(getUsernameClaims());
  $currentIndex = 0;
  foreach ($uniqueClaims as $claim)
  {
    setcookie(USERNAME_CLAIM_SESSION . "[" . $currentIndex . "]", $claim, time() + YEAR_IN_MILLISECONDS);
    ++$currentIndex;
  }
  while ($currentIndex < $originalClaimLength)
  {
    setcookie(USERNAME_CLAIM_SESSION . "[" . $currentIndex . "]", "DELETEME", time() - 3600);
    ++$currentIndex;
  }
}

// Defining multibyte functions if they don't exist
if (!function_exists("mb_strpos"))
{
  function mb_strpos($haystack, $needle, $offset = 0)
  {
    return strpos($haystack, $needle, $offset);
  }
}

if (!function_exists("mb_substr"))
{
  function mb_substr($str, $start, $length = 0)
  {
    if (func_num_args() <= 2)
    {
      return substr($str, $start);
    }
    else
    {
      return substr($str, $start, $length);
    }
  }
}

if (!function_exists("mb_strtolower"))
{
  function mb_strtolower($str)
  {
    return strtolower($str);
  }
}

if (!function_exists("mb_strlen"))
{
  function mb_strlen($str)
  {
    return strlen($str);
  }
}
?>
