<?php
define ("ASSIG1", "Alec Jacob Deitloff");
require_once ("config.inc.php");

// Read in the schedule
$schedule = array();
$scheduleTotals = array();
$scheduleTxt = fopen("schedule.txt", "r");
while ($line = fgets($scheduleTxt))
{
  $carot = mb_strpos($line, '^');
  $date = mb_substr($line, 0, $carot);
  $rawHours = explode("|", mb_substr($line, $carot + 1));
  foreach ($rawHours as $hour)
  {
    $schedule[] = strtotime($date . " " . $hour);
    $scheduleTotals[] = 0;
  }
}
fclose($scheduleTxt);

// Read in users
$users = array();
if (file_exists("users.txt"))
{
  $usersTxt = fopen("users.txt", "r");
  while (!flock($usersTxt, LOCK_SH))
  {
  }

  while ($row = fgets($usersTxt))
  {
    if (mb_strlen($row) == 0)
    {
      continue;
    }
    $carot = mb_strpos($row, '^');
    $name = mb_substr($row, 0, $carot);
    $rawIndeces = trim(mb_substr($row, $carot + 1));
    $indeces = array();
    if (mb_strlen($rawIndeces) > 0)
    {
      $indeces = array_map("intval", explode("|", $rawIndeces));
    }
    $users[$name] = $indeces;
  }

  flock($usersTxt, LOCK_UN);
  fclose($usersTxt);
}

// Total the schedule
foreach ($users as $name => $indeces)
{
  foreach ($indeces as $index)
  {
    ++$scheduleTotals[intval($index)];
  }
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Assignment 1 - Alec Jacob Deitloff</title>
    <link rel="stylesheet" href="style.css" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="script.js"></script>
  </head>
  <body>
    <?php
      if (isset($_SESSION[PROCESS_RESULTS_STATUS]))
      {
        echo "    <div class=\"", ($_SESSION[PROCESS_RESULTS_STATUS] ? "success" : "error"),
             "\">";
        if (isset($_SESSION[PROCESS_RESULTS_MESSAGE]))
        {
          echo $_SESSION[PROCESS_RESULTS_MESSAGE];
          unset ($_SESSION[PROCESS_RESULTS_MESSAGE]);
        }
        else if ($_SESSION[PROCESS_RESULTS_STATUS])
        {
          echo "Your changes have been successfully saved.";
        }
        else
        {
          echo "An unspecified error occurred while processing your results.";
        }
        echo "</div>\n";
        unset($_SESSION[PROCESS_RESULTS_STATUS]);
      }
    ?>
    <table>
      <tr>
        <th>User</th>
        <th>Action</th>
        <?php
          foreach ($schedule as $time)
          {
            echo "        <th>", date(DATE_FORMAT, $time), "</th>\n";
          }
        ?>
      </tr>
      <?php
        foreach ($users as $name => $indeces)
        {
          echo "      <tr class=\"user\">\n";
          echo "        <td class=\"name\">", $name, "<input type=\"hidden\" name=\"usersName\" value=\"",
               $name, "\" /></td>\n";
          echo "        <td class=\"action\">";
          if (hasOwnershipOf($name))
          {
            echo "<input type=\"button\" class=\"action\" disabled=\"disabled\" data-defaulttext=\"Edit\" value=\"Edit\" />";
          }
          echo "</td>\n";
          for ($index = 0; $index < count($schedule); ++$index)
          {
            echo "        <td>\n";
            if (in_array($index, $indeces))
            {
              echo "          <span class=\"check\">", CHECK_SYMBOL, "</span>\n";
            }
            echo "          <input type=\"checkbox\" name=\"indeces[]\" value=\"", $index, "\" data-isdefaultchecked=\"",
                 (in_array($index, $indeces) ? "true\" checked=\"checked\"" : "false\"") . " />\n"; // <-- wow that was a hack u>_> 
            echo "</td>\n";
          }
          echo "      </tr>\n";
        }
      ?>
      <tr id="newRow" class="user newRow" data-cancelonnoticks="true">
        <td class="name"><input type="text" name="usersName" /></td>
        <td><input type="button" class="action" disabled="disabled" data-defaulttext="New" value="New" /></td>
        <?php
          for ($index = 0; $index < count($schedule); ++$index)
          {
            echo "        <td><input type=\"checkbox\" name=\"indeces[]\" data-isdefaultchecked=\"false\" value=\"", $index,
                 "\" /></td>\n";
          }
        ?>
      <tr class="totals">
        <td colspan="2">TOTAL:</td>
        <?php
          foreach ($scheduleTotals as $total)
          {
            echo "        <td>", $total, "</td>\n";
          }
        ?>
      </tr>
    </table>
    <form action="./process.php" id="submitForm" method="POST">
    </form>
    <hr />
    <?php
    require_once ("rules.inc.php");
    ?>
  </body>
</html>
