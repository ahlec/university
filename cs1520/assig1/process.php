<?php
define ("ASSIG1", "Alec Jacob Deitloff");
require_once ("config.inc.php");

// Utility function
function abortProcess($message = null, $success = false)
{
  if ($message != null && mb_strlen($message) > 0)
  {
    $_SESSION[PROCESS_RESULTS_MESSAGE] = $message;
  }
  $_SESSION[PROCESS_RESULTS_STATUS] = $success;
  header ("Location: index.php");
  exit();
}

// Only allow if we are the result of a form submission
if (!isset($_POST["process"]))
{
  abortProcess("You may only access this page through the form.");
}

// Read in the schedule
$schedule = array();
$scheduleTotals = array();
$scheduleTxt = fopen("schedule.txt", "r");
while ($line = fgets($scheduleTxt))
{
  $carot = mb_strpos($line, '^');
  $date = mb_substr($line, 0, $carot);
  $rawHours = explode("|", mb_substr($line, $carot + 1));
  foreach ($rawHours as $hour)
  {
    $schedule[] = strtotime($date . " " . $hour);
    $scheduleTotals[] = 0;
  }
}
fclose($scheduleTxt);

// Read in users
$users = array();
if (file_exists("users.txt"))
{
  $usersTxt = fopen("users.txt", "r");
  while (!flock($usersTxt, LOCK_SH))
  {
  }

  while ($row = fgets($usersTxt))
  {
    if (mb_strlen($row) == 0)
    {
      continue;
    }
    $carot = mb_strpos($row, '^');
    $name = mb_substr($row, 0, $carot);
    $rawIndeces = trim(mb_substr($row, $carot + 1));
    $indeces = array();
    if (mb_strlen($rawIndeces) > 0)
    {
      $indeces = array_map("intval", explode("|", $rawIndeces));
    }
    $users[$name] = $indeces;
  }

  flock($usersTxt, LOCK_UN);
  fclose($usersTxt);
}

// Ensure we are in a valid process
$process = mb_strtolower($_POST["process"]);
if ($process != "new" && $process != "edit")
{
  abortProcess("The specified action was not understood.");
}

// Process username
if (!isset($_POST["usersName"]) || mb_strlen($_POST["usersName"]) == 0 || ctype_space($_POST["usersName"]))
{
  abortProcess("A username must be provided in order to save results.");
}

$username = $_POST["usersName"];
if (mb_strpos($username, '^') !== false)
{
  abortProcess("A username may not contain the carot ('^') symbol.");
}

if (preg_match("/(\\n|\\r)/", $username) == 1)
{
  abortProcess("A username may not span multiple lines.");
}

if ($process == "new" && FORCE_OWNERSHIP_TO_OVERRIDE_WITH_NEW && isset($users[$username]) &&
    !hasOwnershipOf($username))
{
  abortProcess("The desired username already exists, but you are not the original registering account.");
}
else if ($process == "edit")
{
  if (!hasOwnershipOf($username))
  {
    abortProcess("Unable to save changes, as you do not have ownership of the specified username.");
  }
  else if (!array_key_exists($username, $users))
  {
    abortProcess("The specified username does not exist.");
  }
}

// Process indeces
$editIndeces = array();
if (isset($_POST["indeces"]) && is_array($_POST["indeces"]) && count($_POST["indeces"]) > 0)
{
  foreach ($_POST["indeces"] as $newIndex)
  {
    if (!ctype_digit($newIndex) || $newIndex < 0 || $newIndex >= count($schedule))
    {
      abortProcess("One or more desired hours do not exist within the schedule.");
    }
    $editIndeces[] = intval($newIndex);
  }
}

if ($process == "new" && count($editIndeces) == 0)
{
  abortProcess("In order to create a new entry, please select one or more hours.");
}

// Perform the appropriate processing
$usernameClaims = getUsernameClaims();
if ($process == "edit" && DELETE_USERS_WITHOUT_HOURS && count($editIndeces) == 0)
{
  unset($users[$username]);
  $usernameClaims = array_diff($usernameClaims, array($username));
}
else
{
  $users[$username] = $editIndeces;
  $usernameClaims[] = $username;
}
commitUsernameClaims($usernameClaims);

// Write the new data back to users.txt
$usersTxt = fopen("users.txt", "w");
while (!flock($usersTxt, LOCK_EX))
{
}

$isFirstOutput = true;
foreach ($users as $user => $hours)
{
  fwrite($usersTxt, trim($user) . "^" . implode("|", $hours) . "\n");
}

flock($usersTxt, LOCK_UN);
fclose($usersTxt);

abortProcess(null, true);
?>
