<?php
if (!defined ("ASSIG1"))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

echo "    <ul class=\"rules\">\n";

function outputRule($label, $text = "", $configParam = false)
{
  echo "      <li><span class=\"label\">", $label, "</span>";
  if (func_num_args() > 1)
  {
    echo " ", $text;
  }
  if (func_num_args() == 3)
  {
    echo " <span class=\"configParam\">(This can be changed by modifying the '",
         $configParam, "' config parameter)</span>";
  }
  echo "</li>\n";
}

outputRule("New users must schedule at least once.", "In order to create a new user, at least one hour must be " .
           "selected; you will be unable to submit your registration without selecting at least one hour.");
outputRule("User names may not contain the carot ('^') symbol.");
outputRule("User names must contain one or more non-whitespace characters.");
outputRule("Unsaved changes may be cancelled by pressing the 'Cancel' button.", "The 'Cancel' button will only " .
           "appear while there are no unsaved changes, or while the data is not complete. To cancel at any other " .
           "time, you may undo your changes to retrieve the 'Cancel' button, or you may press the ESC key.");

// DELETE_USERS_WITHOUT_HOURS
if (DELETE_USERS_WITHOUT_HOURS)
{
  outputRule("Users without hours are deleted.", "Any users who edit their hours and remove all " .
             "of their scheduled hours will be removed from the table.", "DELETE_USERS_WITHOUT_HOURS");
}

// FORCE_OWNERSHIP_TO_OVERRIDE_WITH_NEW
if (FORCE_OWNERSHIP_TO_OVERRIDE_WITH_NEW)
{
  outputRule("You must have registered the original user name in order to create a new user with the same name.",
             "User names are unique, and attempting to register a 'new' user account with the same user name " .
             "as one that already exists will require you to have registered the original user name. Note that " .
             "this process is analagous to modifying the existing entry.", "FORCE_OWNERSHIP_TO_OVERRIDE_WITH_NEW");
}

// Cleanup
echo "</ul>\n";
?>
