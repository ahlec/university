<?php
require_once ("config.inc.php");
$database = openDatabase();
$user = User::getCurrent();
if ($user != null)
{
	header("Location: index.php");
	exit();
}

$failedLogin = false;
if (isset($_POST["email"]) && isset($_POST["password"]))
{
	$user = User::login($_POST["email"], $_POST["password"]);
	if ($user != null)
	{
		header("Location: index.php");
		exit();
	}
	$failedLogin = true;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<?php
			if ($failedLogin)
			{
				echo "<h1>TRY AGAIN.</h1>\n";
			}
		?>
		<form method="POST" action="./login.php">
			<label for="email">Email:</label>
			<input type="email" id="email" name="email" />
			<label for="password">Password:</label>
			<input type="password" id="password" name="password" />
			<input type="submit" value="Login" />
		</form>
		<a href="./forgotPassword.php">Forgot password?</a>
		|
		<a href="./newUser.php">New user</a>
	</body>
</html>