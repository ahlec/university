<?php
require_once ("../config.inc.php");
$database = openDatabase();

if (!isset($_GET["victory"]) || !($_GET["victory"] == "true" || $_GET["victory"] == "false"))
{
	exit(json_encode(array("success" => "false", "message" => "State of round must be provided in a valid manner.")));
}
$isWon = ($_GET["victory"] == "true");

$currentUser = User::getCurrent();
if ($currentUser->recordRound($_GET["victory"] == "true"))
{
	exit(json_encode(array("success" => true, "nRoundsPlayed" => $currentUser->getRoundsPlayed(),
		"nRoundsWon" => $currentUser->getRoundsWon())));
}

exit(json_encode(array("success" => true, "message" => "Unable to update the database.")));
?>