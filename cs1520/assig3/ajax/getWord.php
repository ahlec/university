<?php
require_once ("../config.inc.php");
$database = openDatabase();

$numberWords = current($database->query("SELECT count(*) FROM assig3_words")->fetch_array());

$word = null;
$baseQuery = "SELECT word FROM assig3_words";
if (isset($_GET["previousWord"]))
{
	$baseQuery .= " WHERE word<>'" . $database->escape_string($_GET["previousWord"]) . "'";
}
$baseQuery .= " LIMIT 1 OFFSET ";

while ($word == null)
{
	$randomOffset = mt_rand(0, $numberWords - 1);
	$wordResults = $database->query($baseQuery . $randomOffset);
	if ($wordResults != null && $wordResults->num_rows > 0)
	{
		$word = current($wordResults->fetch_array());
	}
}

exit(json_encode(array("success" => true, "word" => $word)));
?>