<?php
require_once ("./config.inc.php");
$user = User::getCurrent();
if ($user != null)
{
	$user->logout();
}
header ("Location: login.php");
?>