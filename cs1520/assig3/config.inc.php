<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

define("MIN_PASSWORD_LENGTH", 6);
define("PASSWORD_ALPHABET", "abcdefghijklmnopqrstuvwxyz0123456789!(){}[]");
// -------------------------
//   DO NOT MODIFY BELOW THIS LINE
// -------------------------
define ("URL_ROOT", "http://cs1520.cs.pitt.edu/~jbd19/assig3");
define ("DOCUMENT_ROOT", __DIR__);
define ("DATE_FORMAT", "l<b\\r />j/m/y<b\\r />h:i a");
date_default_timezone_set("America/New_York");

// Sessions
if (!isset($_SESSION))
{
	@session_start();
}

// Database
function openDatabase()
{
	return new mysqli("localhost", "DeitloffJ", "dada.pray", "DeitloffJ");
}

// Library
require_once (DOCUMENT_ROOT . "/lib/mbstring.inc.php");
require_once (DOCUMENT_ROOT . "/lib/user.inc.php");

// HTML
function outputHead()
{
	echo "\t\t\t<title>Lingo! - Assignment 3 (jbd19)</title>\n";
	echo "\t\t\t<base href=\"", URL_ROOT, "/\" />\n";
	echo "\t\t\t<link href=\"http://fonts.googleapis.com/css?family=Overlock+SC\" rel=\"stylesheet\" type=\"text/css\" />\n";
	echo "\t\t\t<link href=\"http://fonts.googleapis.com/css?family=Della+Respira\" rel=\"stylesheet\" type=\"text/css\">\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/script.js\"></script>\n";
	echo "\t\t\t<link rel=\"stylesheet\" href=\"./style/style.css\" />\n";
	
	$scriptName = basename($_SERVER["SCRIPT_NAME"], ".php");
	if (file_exists(DOCUMENT_ROOT . "/script/" . $scriptName . ".js"))
	{
		echo "\t\t\t<script type=\"text/javascript\" src=\"./script/" . $scriptName . ".js\"></script>\n";
	}
}

function outputUserPanel($user)
{
	echo "\t<div class=\"userPanel\">\n";
	echo "\t\t<span class=\"name\">", $user->getName(), "</span>\n";
	echo "\t\t|\n";
	echo "\t\t<";
	if (basename($_SERVER["SCRIPT_NAME"], ".php") == "index")
	{
		echo "span";
	}
	else
	{
		echo "a href=\"./index.php\"";
	}
	echo " class=\"stats\" id=\"stats\">", $user->getRoundsPlayed(),
		" round", ($user->getRoundsPlayed() != 1 ? "s" : ""), " played (",
		$user->getRoundsWon(), " won)</";
	if (basename($_SERVER["SCRIPT_NAME"], ".php") == "index")
	{
		echo "span";
	}
	else
	{
		echo "a";
	}
	echo ">\n";
	echo "\t\t|\n";
	if (basename($_SERVER["SCRIPT_NAME"], ".php") == "changePassword")
	{
		echo "\t\t<span>Change password</span>\n";
	}
	else
	{
		echo "\t\t<a href=\"./changePassword.php\">Change password</a>\n";
	}
	echo "\t\t|\n";
	echo "\t\t<a href=\"./logout.php\">Logout</a>\n";
	echo "\t</div>\n";
}
?>
