function ajax(url)
{
	var AJAX,
		isIssued = false,
		successCallbacks = Array();
		
	// Create the Ajax request
	try
	{
		AJAX = new XMLHttpRequest();
	}
	catch(e)
	{
		try
		{
			AJAX = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e)
		{
			try
			{
				AJAX = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e)
			{
				alert("Your browser does not support AJAX.");
				return false;
			}
		}
	}
	
	// Create the failure callback
	function ajaxFailure(message) {
	}
	
	// Prepare the Ajax request
	AJAX.onreadystatechange = function () {
		var responseData;
		
		if (AJAX.readyState == 4) {
			if (AJAX.status == 200) {
				try {
					responseData = JSON.parse(AJAX.responseText);
				}
				catch (e) {
					ajaxFailure(e);
					return;
				}
				
				if (!responseData.success) {
					ajaxFailure(responseData);
					return;
				}
				
				for (var index = 0; index < successCallbacks.length; ++index) {
					successCallbacks[index].call(null, responseData);
				}
			}
			else {
				ajaxFailure(AJAX.statusText);
				return;
			}
		}
	};
	AJAX.open("GET", url, true);
	
	var publicFunctions = {
		success: function (callback) {
			if (typeof callback == "function") {
				successCallbacks.push(callback);
			}
			
			return publicFunctions;
		},
		
		issue: function () {
			if (isIssued) {
				return publicFunctions;
			}
			
			AJAX.send(null);
			isIssued = true;
			return publicFunctions;
		}
	};
	
	return publicFunctions;
}