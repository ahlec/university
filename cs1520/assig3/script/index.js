function getWord(successCallback, previousWord) {
	var url = "./ajax/getWord.php";
	
	if (typeof previousWord != "undefined") {
		url += "?previousWord=" + encodeURIComponent(previousWord);
	}
	
	ajax(url).success(function (data) {
		successCallback.call(null, data.word);
	}).issue();
}

function startGuess() {
	var table = document.getElementById("lingoTable"),
		countdown = 16;
	if (!table) {
		return;
	}
	
	var decrementCountdown = function () {
		if (countdown > 0) {
			--countdown;
		}
		
		document.getElementById("countdown").innerHTML = countdown;
		table.timeout = setTimeout(decrementCountdown, 1000);
	};
	decrementCountdown();
	
	table.expireTimeout = setTimeout("submitGuess('')", 15000);
	document.getElementById("roundGuess").focus();
}

function putGuessIntoRow(guess, rowNo) {
	var tr = document.getElementById("lingoTable-tr" + rowNo),
		cells = tr.getElementsByTagName("td"),
		word = document.getElementById("lingoTable").word,
		runningTotal = {},
		wordTotals = {};
		
	for (var index = 0; index < 5; ++index) {
		wordTotals[word[index]] = (wordTotals.hasOwnProperty(word[index]) ?
			wordTotals[word[index]] + 1 : 1);
	}
	
	guess = guess.toLowerCase();
	for (var index = 0; index < 5; ++index) {
		if (guess.length <= index) {
			break;
		}
		
		cells[index].innerHTML = guess[index];
		if (guess[index] == word[index]) {
			cells[index].className = "red";
			runningTotal[guess[index]] = (runningTotal.hasOwnProperty(guess[index]) ?
				runningTotal[guess[index]] + 1 : 1);
		}
	}
	
	for (var index = 0; index < 5; ++index) {
		if (guess.length <= index) {
			break;
		}
		
		if (cells[index].className == "red") {
			continue;
		}
		
		runningTotal[guess[index]] = (runningTotal.hasOwnProperty(guess[index]) ?
			runningTotal[guess[index]] + 1 : 1);
			
		if (!wordTotals.hasOwnProperty(guess[index]) || runningTotal[guess[index]] > wordTotals[guess[index]]) {
			cells[index].className = "black";
		}
		else {
			cells[index].className = "blue";
		}
	}
}

function submitGuess(guess) {
	var table = document.getElementById("lingoTable"),
		guessBox = document.getElementById("roundGuess");
	guessBox.value = "";
	if (!table) {
		return;
	}
	
	if (table.timeout) {
		clearTimeout(table.timeout);
		table.timeout = null;
	}
	
	if (table.expireTimeout) {
		clearTimeout(table.expireTimeout);
		table.expireTimeout = null;
	}
	
	putGuessIntoRow(guess, ++table.guess);
	table.setAttribute("data-guess", table.guess);
	if (guess.toLowerCase() == table.word) {
		endRound(true);
		return;
	}
	
	if (table.guess >= 5) {
		endRound(false);
		return;
	}
	startGuess();
}

function endRound(isWon) {
	console.log("ROUND ENDED");
	console.log("VICTORY? " + isWon);
	
	document.getElementById("lingoTable").className = "lingoTable " + (isWon ? "won" : "lost");
	
	ajax("./ajax/recordRound.php?victory=" + isWon).success(function (data) {
		console.dir(data);
		document.getElementById("stats").innerHTML = data.nRoundsPlayed + " round" +
			(data.nRoundsPlayed != 1 ? "s" : "") + " played (" + data.nRoundsWon + " won)";
	}).issue();
	document.getElementById("gameplayPanel").className = "panel";
	document.getElementById("roundPanel").className = "panel hidden";
	document.getElementById("reveal").innerHTML = "<b>" + (isWon ? "CORRECT!" : "SORRY!") + "</b> The word was <word>" +
		document.getElementById("lingoTable").word + "</word>.<br />Press the 'New Round' button above to play a new round!";
}

function startNewRound(previousWord) {
	// Clean up the old round
	var oldTable = document.getElementById("lingoTable");
	if (oldTable) {
		oldTable.remove();
	}
	document.getElementById("reveal").innerHTML = "";
	
	// Create the DOM elements for the table
	var table = document.createElement("table");
	table.setAttribute("id", "lingoTable");
	table.className = "lingoTable";
	
	for (var row = 0; row < 6; ++row) {
		var tr = document.createElement("tr");
		tr.setAttribute("id", "lingoTable-tr" + row);
		
		for (var cell = 0; cell < 5; ++cell) {
			var td = document.createElement("td");
			tr.appendChild(td);
		}
		
		table.appendChild(tr);
	}
	

	// Get the word and set up
	getWord(function (word) {
		table.word = word;
		table.guess = 0;
		table.setAttribute("data-guess", 0);
		document.getElementById("mainBody").appendChild(table);
		document.getElementById("gameplayPanel").className = "panel hidden";
		document.getElementById("roundPanel").className = "panel";
		putGuessIntoRow(word[0], 0);
		startGuess();
	}, previousWord);
}

window.onload = function () {
	var submitButton = document.getElementById("submitGuess"),
		guessBox = document.getElementById("roundGuess");
	
	submitButton.onclick = function () {
		submitGuess(guessBox.value);
	};
	
	guessBox.onkeyup = function (e) {
		if ((e.which || e.keyCode) == 13) {
			submitGuess(this.value);
		}
	};
}