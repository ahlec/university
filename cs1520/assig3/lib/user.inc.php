<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

define ("ASSIG3_USER_SESSION", "jbd19Assig3UserSession");
class User
{
	private $_userId;
	private $_email;
	private $_name;
	private $_password;
	private $_nRoundsPlayed;
	private $_nRoundsWon;
	
	private function __construct($userInfo)
	{
		$this->_userId = $userInfo["userId"];
		$this->_email = $userInfo["emailAddress"];
		$this->_name = $userInfo["name"];
		$this->_password = $userInfo["password"];
		$this->_nRoundsPlayed = $userInfo["nRoundsPlayed"];
		$this->_nRoundsWon = $userInfo["nRoundsWon"];
	}
	
	public static function getCurrent()
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (!isset($_SESSION[ASSIG3_USER_SESSION]))
		{
			return null;
		}
		
		$userInfo = $database->query("SELECT userId, emailAddress, name, password, nRoundsPlayed, nRoundsWon FROM assig3_users " .
			"WHERE userId='" . $database->escape_string($_SESSION[ASSIG3_USER_SESSION]) . "' LIMIT 1");
		if ($userInfo === null || $userInfo->num_rows != 1)
		{
			return null;
		}
		
		return new User($userInfo->fetch_assoc());
	}
	
	public static function register($email, $name, $password, &$outMessage)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (mb_strpos($email, "%") !== false)
		{
			$outMessage = "Emails may not contain the '%' sign.";
			return false;
		}
		
		if (mb_strlen($password) < MIN_PASSWORD_LENGTH)
		{
			$outMessage = "A password must be at least " . MIN_PASSWORD_LENGTH . " characters in length.";
			return false;
		}
		
		$emailResults = $database->query("SELECT * FROM assig3_users WHERE emailAddress LIKE '" .
			$database->escape_string($email) . "' LIMIT 1");
		if ($emailResults !== null && $emailResults->num_rows > 0)
		{
			$outMessage = "This email has already been registered.";
			return false;
		}
		
		$creationResults = $database->query("INSERT INTO assig3_users(emailAddress, password, name) VALUES('" .
			$database->escape_string($email) . "','" . $database->escape_string(md5($password)) . "','" .
			$database->escape_string($name) . "')");
		if ($creationResults === false)
		{
			$outMessage = "There was an error with creating the user account.";
			return false;
		}
		
		$userInfo = $database->query("SELECT * FROM assig3_users WHERE emailAddress='" .
			$database->escape_string($email) . "' LIMIT 1");
		if ($userInfo === null || $userInfo->num_rows != 1)
		{
			$outMessage = "Could not retrieve the new user data.";
			return false;
		}
		
		$user = new User($userInfo->fetch_assoc());
		$_SESSION[ASSIG3_USER_SESSION] = $user->getId();
		
		return true;
	}
	
	public static function retrievePassword($email)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (mb_strpos($email, "%") !== false)
		{
			return false;
		}
		
		$userInfo = $database->query("SELECT * FROM assig3_users WHERE emailAddress LIKE '" .
			$database->escape_string($email) . "' LIMIT 1");
		if ($userInfo === null || $userInfo->num_rows != 1)
		{
			return false;
		}
		
		$user = new User($userInfo->fetch_assoc());
		$oldPassword = $user->_password;
		
		$newPassword = "";
		$passwordAlphabet = PASSWORD_ALPHABET;
		foreach (array_rand(str_split(PASSWORD_ALPHABET), MIN_PASSWORD_LENGTH) as $character)
		{
			$newPassword .= $passwordAlphabet[$character];
		}
		
		if (!$user->changePassword($newPassword))
		{
			return false;
		}
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: ' . $user->getName() . ' <' . $user->getEmail() . ">\r\n";
		if (!mail($user->getEmail(), "Your password has been reset.", "Hello, " . $user->getName() . "! Your password has been " .
			"reset! For the time being, your password is as follows:<br /><br /><strong><big>". $newPassword .
			"</big></strong><br /><br />Please log in by visiting <a href=\"" . URL_ROOT . "/\" target=\"_blank\">" .
			URL_ROOT . "</a>. Once you are logged in, you will be free to change your password to something more memorable!<br />" .
			"<br />Alec", $headers))
		{
			$user->changePassword($oldPassword);
			return false;
		}
		
		return true;
	}
	
	public static function login($email, $password)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (mb_strpos($email, "%") !== false)
		{
			return false;
		}
		
		if (isset($_SESSION[ASSIG3_USER_SESSION]))
		{
			unset($_SESSION[ASSIG3_USER_SESSION]);
		}
		
		$userInfo = $database->query("SELECT * FROM assig3_users WHERE emailAddress LIKE '" . $database->escape_string($email) .
			"' AND password='" . $database->escape_string(md5($password)) . "' LIMIT 1");
		if ($userInfo === null || $userInfo->num_rows != 1)
		{
			return null;
		}
		
		$user = new User($userInfo->fetch_assoc());
		$_SESSION[ASSIG3_USER_SESSION] = $user->getId();
		return $user;
	}
	
	/** ACCESSORS **/
	public function getId()
	{
		return $this->_userId;
	}
	public function getEmail()
	{
		return $this->_email;
	}
	public function getName()
	{
		return $this->_name;
	}
	public function getRoundsPlayed()
	{
		return $this->_nRoundsPlayed;
	}
	public function getRoundsWon()
	{
		return $this->_nRoundsWon;
	}
	
	/** METHODS **/
	public function isCurrentPassword($password)
	{
		return ($this->_password == md5($password));
	}
	public function logout()
	{
		if (isset($_SESSION[ASSIG3_USER_SESSION]))
		{
			unset($_SESSION[ASSIG3_USER_SESSION]);
		}
	}
	public function changePassword($newPassword)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (!$database->query("UPDATE assig3_users SET password='" . $database->escape_string(md5($newPassword)) .
			"' WHERE userId='" . $this->_userId . "'"))
		{
			return false;	
		}
		
		$this->_password = md5($newPassword);
		return true;
	}
	public function recordRound($isWon)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (!$database->query("UPDATE assig3_users SET nRoundsPlayed = nRoundsPlayed + 1" .
			($isWon ? ", nRoundsWon = nRoundsWon + 1" : "") . " WHERE userId='" . $this->_userId . "'"))
		{
			return false;
		}
		
		++$this->_nRoundsPlayed;
		if ($isWon)
		{
			++$this->_nRoundsWon;
		}
		return true;
	}
}

?>