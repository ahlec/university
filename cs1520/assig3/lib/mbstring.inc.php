<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

if (!function_exists("mb_strpos"))
{
  function mb_strpos($haystack, $needle, $offset = 0)
  {
    return strpos($haystack, $needle, $offset);
  }
}

if (!function_exists("mb_substr"))
{
  function mb_substr($str, $start, $length = 0)
  {
    if (func_num_args() <= 2)
    {
      return substr($str, $start);
    }
    else
    {
      return substr($str, $start, $length);
    }
  }
}

if (!function_exists("mb_strtolower"))
{
  function mb_strtolower($str)
  {
    return strtolower($str);
  }
}

if (!function_exists("mb_strlen"))
{
  function mb_strlen($str)
  {
    return strlen($str);
  }
}
?>