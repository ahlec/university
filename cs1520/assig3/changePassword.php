<?php
require_once ("config.inc.php");
$database = openDatabase();
$user = User::getCurrent();
if ($user == null)
{
	header ("Location: login.php");
	exit();
}

$errorMessage = null;
if (isset($_POST["currentPassword"]) && isset($_POST["newPassword"]) && isset($_POST["confirmPassword"]))
{
	$newPassword = trim($_POST["newPassword"]);
	$confirmPassword = $_POST["confirmPassword"];
	if ($newPassword == $confirmPassword)
	{
		if ($user->isCurrentPassword($_POST["currentPassword"]))
		{
			if (mb_strlen($newPassword) >= MIN_PASSWORD_LENGTH)
			{
				$errorMessage = ($user->changePassword($newPassword) ? true : "Your password could not be changed.");
			}
			else
			{
				$errorMessage = "Passwords must be at least " . MIN_PASSWORD_LENGTH . " characters in length.";
			}
		}
		else
		{
			$errorMessage = "The provided password does not match your current password.";
		}
	}
	else
	{
		$errorMessage = "Your new password does not match the confirmation of the new password.";
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<?php
			outputUserPanel($user);
			if ($errorMessage != null)
			{
				if ($errorMessage === true)
				{
					echo "<div class=\"success\">Your password has been changed!</div>\n";
				}
				else
				{
					echo "<div class=\"error\">", $errorMessage, "</div>\n";
				}
			}
		?>
		<div class="body" id="mainBody">
			<form method="POST" action="./changePassword.php">
				<label for="currentPassword">Current password:</label>
				<input type="password" id="currentPassword" name="currentPassword" /><br />
				<label for="newPassword">New password:</label>
				<input type="password" id="newPassword" name="newPassword" /><br />
				<label for="confirmPassword">Confirm new password:</label>
				<input type="password" id="confirmPassword" name="confirmPassword" /><br />
				<input type="submit" value="Change" />
			</form>
		</div>
		<div class="reveal" id="reveal"></div>
	</body>
</html>