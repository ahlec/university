<?php
require_once ("config.inc.php");
$database = openDatabase();
$user = User::getCurrent();
if ($user == null)
{
	header ("Location: login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<?php
			outputUserPanel($user);
		?>
		<div class="body" id="mainBody">
			<div class="panel" id="gameplayPanel">
				<input type="button" value="New Round" onclick="startNewRound();" />
			</div>
			<div class="panel hidden" id="roundPanel">
				<label for="roundGuess">Guess:</label>
				<input type="text" id="roundGuess" />
				<input type="button" id="submitGuess" value="Submit" />
				<label for="countdown">Time left:</label>
				<span id="countdown"></span>
			</div>
		</div>
		<div class="reveal" id="reveal"></div>
	</body>
</html>