<?php
require_once ("config.inc.php");
$database = openDatabase();
$user = User::getCurrent();
if ($user != null)
{
	header("Location: index.php");
	exit();
}

$registered = null;
if (isset($_POST["email"]) && isset($_POST["name"]) && isset($_POST["password"]))
{
	if (User::register($_POST["email"], $_POST["name"], $_POST["password"], $registered))
	{
		header("Location: index.php");
		exit();
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<?php
			if ($registered !== null)
			{
				echo "<h1>" . $registered . "</h1>\n";
			}
		?>
		<form method="POST" action="./newUser.php">
			<label for="email">Email:</label>
			<input type="email" id="email" name="email" />
			<label for="name">Name:</label>
			<input type="text" id="name" name="name" />
			<label for="password">Password:</label>
			<input type="password" id="password" name="password" />
			<input type="submit" value="Register" />
		</form>
	</body>
</html>