<?php
require_once ("config.inc.php");
$database = openDatabase();
$user = User::getCurrent();
if ($user != null)
{
	header("Location: index.php");
	exit();
}

$passwordRetrieved = null;
if (isset($_POST["email"]))
{
	if (User::retrievePassword($_POST["email"]))
	{
		header("Location: login.php?retrieved=true");
		exit();
	}
	$passwordRetrieved = false;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<?php
			if ($passwordRetrieved)
			{
				echo "<h1>TRY AGAIN.</h1>\n";
			}
		?>
		<form method="POST" action="./forgotPassword.php">
			<label for="email">Email:</label>
			<input type="email" id="email" name="email" />
			<input type="submit" value="Retrieve" />
		</form>
	</body>
</html>