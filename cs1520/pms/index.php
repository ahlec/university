<?php
function outputError($message)
{
	echo "<div class=\"error\">ERROR:: ", $message, "</div>";
}

if (isset($_POST["query"]))
{
	define ("MYSQL_HOST", "localhost");
	
	// Connect to MySQL
	$userid = rtrim(strip_tags($_POST["id"]));
	$userpass = rtrim(strip_tags($_POST["password"]));
	$db = @(new mysqli(MYSQL_HOST, "$userid", "$userpass", "$userid"));
	if ($db->connect_errno)
	{
		outputError("Could not connect to MySQL");
	}
	else
	{
		// Clean up the given query (delete leading and trailing whitespace)
		require_once("geshi.php");
		$query = stripslashes($_POST["query"]);
		trim($query);
		$geshiQuery = new GeSHi();
		$geshiQuery->set_language("MySQL");
		$geshiQuery->set_source($query);
		echo "<div><b>Query:</b> " , $geshiQuery->parse_code(), "</div>";

		// Execute the query
		$results = $db->query($query);
		if ($db->errno)
		{
			outputError($db->error);
		}
		else
		{
			// Handle boolean results
			if ($results === true || $results === false)
			{
				echo "<div>Database query executed and returned (", ($results ? "TRUE" : "FALSE"), ").</div>";
			}
			else
			{
				// Display the results in a table

				/*if ($results->num_rows == 0)
				{
					echo "No results were found.";
				}
				else*/
				{
					echo $results->num_rows, " result", ($results->num_rows == 1 ? "" : "s"), " were retrieved.<br />\n";
					echo "<table>";
					
					// Produce column labels
					echo "<tr class=\"header\">";
					$fields = $results->fetch_fields();
					foreach ($fields as $field)
					{
						echo "<th>", $field->name, "</th>";
					}
					echo "</tr>";

					// Output the values of the fields in the row
					while ($row = $results->fetch_array())
					{
						echo "<tr>";
						foreach ($fields as $field)
						{
							echo "<td>", $row[$field->name], "</td>";
						}
						echo "</tr>";
					}
					echo "</table>";
				}
			}
		}
	}
}
?>
<html>
	<head>
		<style>
			pre {
				display:inline;
			}
			a {
				text-decoration:none;
			}
			a:hover {
				text-decoration:underline;
			}
			textarea {
				resize:none;
			}
			table {
				border:1px solid black;
				width:100%;
				border-collapse:collapse;
				margin-top:30px;
				margin-bottom:30px;
			}
			table tr.header {
				border-bottom:1px solid black;
				background-color:#D3D3D3;
			}
			table th:not(:last-child), table td:not(:last-child) {
				border-right:1px solid black;
			}
		</style>
	</head>
	<body>
	<?php
		if (defined("MYSQL_HOST"))
		{
			echo "<hr />";
		}
		else
		{
			$query = "";
			$userid = "";
			$userpass = "";
		}
	?>
	Please enter query:
	<form action="index.php" method="post">
		<textarea rows="6" cols="120" name="query"><?php echo $query; ?></textarea>
		<br /><br />
		MySQL Id: <input type="text" name="id" value="<?php echo $userid; ?>" /><br />
		Password: <input type="password" name="password" value="<?php echo $userpass; ?>" /><br />
		<input type="submit" value="Query" />
	</form>
	</body>
</html>
