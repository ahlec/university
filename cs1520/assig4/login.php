<?php
require_once ("config.inc.php");
$database = openDatabase();
$player = Player::getCurrent();
if ($player != null)
{
	header("Location: index.php");
	exit();
}

$failedLogin = false;
if (isset($_POST["name"]) && isset($_POST["password"]))
{
	$player = Player::login($_POST["name"], $_POST["password"]);
	if ($player != null)
	{
		header("Location: index.php");
		exit();
	}
	$failedLogin = true;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<?php
			if ($failedLogin)
			{
				echo "<h1>TRY AGAIN.</h1>\n";
			}
		?>
		<div class="mainBody">
			<form method="POST" action="./login.php">
				<label for="name">Name:</label>
				<input type="text" id="name" name="name" />
				<label for="password">Password:</label>
				<input type="password" id="password" name="password" />
				<input type="submit" value="Login" />
			</form>
		</div>
	</body>
</html>