<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();
header("Content-type: text/xml; charset=utf-8");
echo "<?xml version=\"1.0\"?>\n";
echo "<PairSync>\n";

// Get player
$player = Player::getCurrent();
if ($player == null)
{
	echo "\t<Success>false</Success>\n";
	echo "\t<Message>There is no current player.</Message>\n";
}
else
{
	$pair = $player->getPair();
	if ($pair == null)
	{
		echo "\t<Success>true</Success>\n";
		echo "\t<HasPair>false</HasPair>\n";
		echo "\t<HasPartner>false</HasPartner>\n";
	}
	else
	{
		echo "\t<Success>true</Success>\n";
		echo "\t<HasPair>true</HasPair>\n";
		
		$partner = $player->getPartner();
		if ($partner == null)
		{
			echo "\t<HasPartner>false</HasPartner>\n";
		}
		else
		{
			echo "\t<HasPartner>true</HasPartner>\n";
			echo "\t<Partner>\n";
			echo "\t\t<Name>", $partner->getName(), "</Name>\n";
			echo "\t</Partner>\n";
		}
	}
}

// Wrap up
echo "</PairSync>\n";
?>