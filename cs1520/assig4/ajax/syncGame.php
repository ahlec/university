<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();
header("Content-type: text/xml; charset=utf-8");
echo "<?xml version=\"1.0\"?>\n";
echo "<GameSync>\n";

// Get player
$player = Player::getCurrent();
if ($player == null)
{
	echo "\t<Success>false</Success>\n";
	echo "\t<Message>There is no current player.</Message>\n";
}
else
{
	$pair = $player->getPair();
	if ($pair == null)
	{
		echo "\t<Success>false</Success>\n";
		echo "\t<Message>You do not currently have a partner!</Message>\n";
	}
	else
	{
		$game = $pair->getGame();
		echo "\t<Success>true</Success>\n";
		if ($game == null)
		{
			echo "\t<HasGame>false</HasGame>\n";
		}
		else
		{
			echo "\t<HasGame>true</HasGame>\n";
			
			echo "\t<Game>\n";
			if ($player->isMaker())
			{
				echo "\t\t<IsMaker />\n";
			}
			echo "\t\t<State>", $game->getState(), "</State>\n";
			if ($game->getWord() != null)
			{
				echo "\t\t<WordLength>", mb_strlen($game->getWord()), "</WordLength>\n";
				echo "\t\t<Letters>\n";
				foreach ($game->getWordBreakdown() as $index => $occupant)
				{
					echo "\t\t\t<Letter Index=\"", $index, "\">", $occupant, "</Letter>\n";
				}
				echo "\t\t</Letters>\n";
				if ($game->getState() != GAMESTATE_GUESSING) {
					echo "\t\t<Word>", $game->getWord(), "</Word>\n";
				}
			}
			echo "\t\t<IncorrectGuessesRemaining>", max(0,
				MAX_NUMBER_INCORRECT_GUESSES - $game->getNumberIncorrectGuesses()), "</IncorrectGuessesRemaining>\n";
			echo "\t\t<Guesses>\n";
			foreach ($game->getGuesses() as $guess)
			{
				if ($guess["isCorrect"])
				{
					echo "\t\t\t<Correct>", $guess["letter"], "</Correct>\n";
				}
				else
				{
					echo "\t\t\t<Incorrect>", $guess["letter"], "</Incorrect>\n";
				}
			}
			echo "\t\t</Guesses>\n";
			echo "\t</Game>\n";
		}
	}
}

// Wrap up
echo "</GameSync>\n";
?>