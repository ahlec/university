<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();
header("Content-type: text/xml; charset=utf-8");
echo "<?xml version=\"1.0\"?>\n";

function sendFlag_error($message)
{
	echo "<SendFlag>\n";
	echo "\t<Success>false</Success>\n";
	echo "\t<Message>", $message, "</Message>\n";
	exit ("</SendFlag>");
}

function sendFlag_success()
{
	echo "<SendFlag>\n";
	echo "\t<Success>true</Success>\n";
	exit ("</SendFlag>");
}

function addFlag($database, $gameId, $playerId, $flagType)
{
	$flagCount = $database->query("SELECT count(*) FROM assig4_gameFlags WHERE gameId='" .
		$database->escape_string($gameId) . "' AND playerId='" . $database->escape_string($playerId) .
		"' AND flagType='" . $database->escape_string($flagType) . "'");
	if ($flagCount === null || $flagCount->num_rows != 1)
	{
		sendFlag_error("DB ERROR (Code: 11).");
	}
	$count = $flagCount->fetch_array();
	if ($count[0] > 0)
	{
		return;
	}
	
	if (!$database->query("INSERT INTO assig4_gameFlags(gameId, playerId, flagType) VALUES('" .
		$database->escape_string($gameId) . "','" . $database->escape_string($playerId) . "','" .
		$database->escape_string($flagType) . "')"))
	{
		sendFlag_error("DB ERROR (Code: 12).");
	}
	
	return;
}

function getPairedFlags($database, $gameId, $flagType)
{
	$flagCount = $database->query("SELECT count(*) FROM assig4_gameFlags WHERE gameId='" .
		$database->escape_string($gameId) . "' AND flagType='" . $database->escape_string($flagType) .
		"'");
	if ($flagCount === null || $flagCount->num_rows != 1)
	{
		sendFlag_error("DB ERROR (Code: 13).");
	}
	$count = $flagCount->fetch_array();
	if ($count[0] < 2)
	{
		return false;
	}
	
	if (!$database->query("DELETE FROM assig4_gameFlags WHERE gameId='" .
		$database->escape_string($gameId) . "' AND flagType='" . $database->escape_string($flagType) .
		"'"))
	{
		sendFlag_error("DB ERROR (Code: 14).");
	}
	return true;
}

// Get player
$player = Player::getCurrent();
if ($player == null)
{
	sendFlag_error("There is no current player.");
}

// Get pair
$pair = $player->getPair();
if ($pair == null)
{
	sendFlag_error("You do not currently have a partner!");
}

// Get game
$game = $pair->getGame();
if ($game == null)
{
	sendFlag_error("You do not currently have a game!");
}

// Process the message type
if (!isset($_GET["flagType"]) || !ctype_digit($_GET["flagType"]))
{
	sendFlag_error("flagType must be provided!");
}
$flagType = $_GET["flagType"];

// Is it a startup ready flag?
if ($flagType == GAMEFLAG_STARTUP_READY)
{
	if ($game->getState() != GAMESTATE_STARTUP)
	{
		sendFlag_success();
	}
	
	addFlag($database, $game->getId(), $player->getId(), GAMEFLAG_STARTUP_READY);
	if (getPairedFlags($database, $game->getId(), GAMEFLAG_STARTUP_READY))
	{
		if (!$game->setState(GAMESTATE_WORDINPUT))
		{
			sendFlag_error("DB ERROR (Code: 15).");
		}
	}
	
	sendFlag_success();
}

// Is it a shutdown ready flag?
if ($flagType == GAMEFLAG_SHUTDOWN_READY)
{
	if ($game->getState() != GAMESTATE_VICTORY && $game->getState() != GAMESTATE_FAILURE)
	{
		sendFlag_success();
	}
	
	addFlag($database, $game->getId(), $player->getId(), GAMEFLAG_SHUTDOWN_READY);
	if (getPairedFlags($database, $game->getId(), GAMEFLAG_SHUTDOWN_READY))
	{
		if (!$game->shutdown())
		{
			sendFlag_error("DB ERROR (Code: 30).");
		}
	}
	
	sendFlag_success();
}

sendFlag_error("Unsupported flagType.");
?>