<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();
header("Content-type: text/xml; charset=utf-8");
echo "<?xml version=\"1.0\"?>\n";

function setWord_error($message)
{
	echo "<SetWord>\n";
	echo "\t<Success>false</Success>\n";
	echo "\t<Message>", $message, "</Message>\n";
	exit ("</SetWord>");
}

function setWord_success()
{
	echo "<SetWord>\n";
	echo "\t<Success>true</Success>\n";
	exit ("</SetWord>");
}

// Get player
$player = Player::getCurrent();
if ($player == null)
{
	setWord_error("There is no current player.");
}

// Get pair
$pair = $player->getPair();
if ($pair == null)
{
	setWord_error("You do not currently have a partner!");
}

// Get game
$game = $pair->getGame();
if ($game == null)
{
	setWord_error("You do not currently have a game!");
}
if ($game->getState() != GAMESTATE_WORDINPUT)
{
	setWord_error("The game is not currently in the appropriate state.");
}
if (!$player->isMaker())
{
	setWord_error("You are not the maker for this game!");
}

// Process the message type
if (!isset($_GET["word"]))
{
	setWord_error("word must be provided!");
}
$word = mb_strtoupper($_GET["word"]);
if (mb_strlen($word) < MIN_WORD_LENGTH || mb_strlen($word) > MAX_WORD_LENGTH)
{
	setWord_error("The provided word is either too short or too long.");
}
foreach (str_split($word) as $character)
{
	if (mb_strpos(WORD_ALPHABET, $character) === false)
	{
		setWord_error("One or more invalid characters was found within the word.");
	}
}

// Set the word
if (!$game->setWord($word))
{
	setWord_error("DB ERROR (Code: 16).");
}

setWord_success();
?>