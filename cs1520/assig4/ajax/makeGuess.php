<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();
header("Content-type: text/xml; charset=utf-8");
echo "<?xml version=\"1.0\"?>\n";

function makeGuess_error($message)
{
	echo "<MakeGuess>\n";
	echo "\t<Success>false</Success>\n";
	echo "\t<Message>", $message, "</Message>\n";
	exit ("</MakeGuess>");
}

function makeGuess_success()
{
	echo "<MakeGuess>\n";
	echo "\t<Success>true</Success>\n";
	exit ("</MakeGuess>");
}

// Get player
$player = Player::getCurrent();
if ($player == null)
{
	makeGuess_error("There is no current player.");
}

// Get pair
$pair = $player->getPair();
if ($pair == null)
{
	makeGuess_error("You do not currently have a partner!");
}

// Get game
$game = $pair->getGame();
if ($game == null)
{
	makeGuess_error("You do not currently have a game!");
}

// Process the letter from the input
if (!isset($_GET["letter"]))
{
	makeGuess_error("letter must be provided!");
}
$letter = mb_strtoupper($_GET["letter"]);
if (mb_strlen($letter) != 1)
{
	makeGuess_error("The provided letter is not one character in length.");
}
if (mb_strpos(WORD_ALPHABET, $letter) === false)
{
	makeGuess_error("The provided letter is not in the alphabet of valid characters!");
}

if ($game->hasGuessedLetter($letter))
{
	makeGuess_error("The provided letter has already been guessed!");
}

// Make the guess
if (!$game->makeGuess($letter))
{
	makeGuess_error("DB ERROR (Code: 20).");
}

makeGuess_success();
?>