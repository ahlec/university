<?php
require_once ("config.inc.php");
$database = openDatabase();
$player = Player::getCurrent();
if ($player == null)
{
	header ("Location: login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			outputHead();
		?>
	</head>
	<body>
		<div class="utilities">
			<div class="ui-state-highlight ui-corner-all"><strong>User:</strong> <?php
				echo $player->getName();
			?></div>
			<div class="ui-state-highlight ui-corner-all" id="partnerContainer"><strong>Partner:</strong> <span id="partner"></span></div>
			<div class="ui-state-highlight ui-corner-all"><a href="./logout.php">Logout</a></div>
		</div>
		<div class="mainBody" id="mainBody">
		</div>
	</body>
</html>