// Function definitions
Game = {};

// Implementation
(function () {
	var isFirstRun = true,
		gameState = null,
		wordLength = 0,
		lastGameWord = null, // the word is never stored until after a game is completed
		losesPartnerCallback = [],
		getsPartnerCallback = [];
		
	/*** BEGIN GAME STATE FUNCTION PROGRAMMING ***/
	
	// startup
	function onStartup() {
		modalScreen("Preparing a new game...", "HANGMAN");
		sendGameFlag(1);
		modalScreen("Waiting for communication from your partner.");
	}
	
	// wordInput
	function wordInput() {
		var frame = $(document.createElement("div")),
			label = $(document.createElement("div")),
			inputBox = $(document.createElement("input")),
			submitButton = $(document.createElement("input")),
			hasWordBeenSet = false;
		
		label.html("Enter a word, between <strong>" + Game.MIN_WORD_LENGTH + "</strong> and <strong>" +
			Game.MAX_WORD_LENGTH + "</strong> characters (inclusive) in length.").appendTo(frame);
		inputBox.attr("type", "text").attr("maxlength", Game.MAX_WORD_LENGTH).keydown(function (e) {
			if (e.which == 13) {
				submitButton.click();
			}
		}).appendTo(frame);
		submitButton.attr("type", "button").val("Submit").click(function () {
			var word = inputBox.val();
			if (word.length < Game.MIN_WORD_LENGTH) {
				alert("The provided word is too short!");
				return;
			}
			
			if (word.length > Game.MAX_WORD_LENGTH) {
				alert("The provided word is too long!");
				return;
			}
			
			$.ajax({
				url: "./ajax/setWord.php",
				data: {
					'word': word
				},
				async: false
			}).done(function (data) {
				if (!ajaxFailHandler(data)) {
					hasWordBeenSet = true;
					frame.dialog("close");
					modalScreen("Please wait...", "Hangman");
				}
			}).fail(function (data) {
				ajaxFailHandler(data);
			});
		}).appendTo(frame);
		frame.appendTo($("#bodyElem"));
		frame.dialog({
			title: "Enter a word",
			modal: true,
			resizable: false,
			dialogClass: "no-close",
			closeOnEscape: false
		});
	};
	
	function waitForWordInput() {
		modalScreen("Please wait for the other player to choose a word for this game!");
	};
	
	
	// guessing
	function makeGuess(letter) {
		var inputBox = $("#guess-inputBox"),
			inputSubmit = $("#guess-inputSubmit");
			
		if (inputBox.is(":disabled")) {
			return;
		}
		
		inputBox.prop("disabled", true).val("");
		inputSubmit.prop("disabled", true);
	
		$.ajax({
			url: "./ajax/makeGuess.php",
			data: {
				'letter': letter
			},
			async: false
		}).always(function (data) {
			ajaxFailHandler(data)
			inputBox.prop("disabled", false);
			inputSubmit.prop("disabled", false);
			Game.sync();
		});
	};
	
	function syncGuessingGameData(data) {
		// letter guess syncing
		var letters = $("Game > Letters > Letter", data);
		for (var index = 0; index < letters.length; ++index) {
			var cell = $("table.word tr.word td.letter:nth-child(" + (index + 1) + ")");
			
			cell.text($(letters[index]).text());
		}
		
		// guess count syncing
		$("#guessing-remainingGuesses").text($("Game > IncorrectGuessesRemaining", data).text());
		
		// update the guess history boxes
		var guessHistory = $("#guessHistory"),
			guesses = $("Game > Guesses > *", data);
		for (var index = 0; index < guesses.length; ++index) {
			var letter = $(guesses[index]).text();
			if (guessHistory.find("div[data-letter='" + letter + "']").length > 0) {
				continue;
			}
			
			var cell = $(document.createElement("div"));
			cell.attr("data-letter", letter).addClass($(guesses[index]).prop("tagName")).text(letter).appendTo(guessHistory);
		}
	}
	
	function wordGuessing() {
		wordGuessingObserve();
		
		var bodyElem = $("#mainBody"),
			inputBox = $(document.createElement("input")),
			inputSubmit = $(document.createElement("input"));
			
		inputBox.attr("type", "text").attr("id", "guess-inputBox").attr("maxlength", "1").keyup(function (e) {
			if (e.which == 13) {
				inputSubmit.click();
			}
		}).appendTo(bodyElem);
		inputSubmit.attr("type", "button").attr("id", "guess-inputSubmit").val("Guess!").click(function () {
			var letter = inputBox.val().toUpperCase();
			
			if ($("#guessHistory").find("div[data-letter='" + letter + "']").length > 0) {
				alert("The letter '" + letter + "' has already been guessed!");
				inputBox.val("");
				return;
			}
			
			makeGuess(letter);
		}).appendTo(bodyElem);
	};
	
	function wordGuessingObserve() {
		var bodyElem = $("#mainBody"),
			wordTable = $(document.createElement("table")),
			wordRow = $(document.createElement("tr")),
			remainingGuessLabel = $(document.createElement("label")),
			remainingGuessCount = $(document.createElement("span")),
			guessHistory = $(document.createElement("div")),
			clear = $(document.createElement("div"));
			
		guessHistory.addClass("guessHistory").attr("id", "guessHistory").appendTo(bodyElem);
			
		wordTable.addClass("word").appendTo(bodyElem);
		wordRow.addClass("word").appendTo(wordTable);
		
		for (var index = 0; index < wordLength; ++index) {
			var cell = $(document.createElement("td"));
			cell.addClass("letter").appendTo(wordRow);
		}
		
		remainingGuessLabel.text("Remaining incorrect guesses:").appendTo(bodyElem);
		remainingGuessCount.text("???").attr("id", "guessing-remainingGuesses").appendTo(bodyElem);
		
		clear.addClass("clear").appendTo(bodyElem);
	};
	
	// shutdown
	function displayVictory(isMaker) {
		var frame = $(document.createElement("div"));
		
		frame.addClass("victory").appendTo($("#mainBody"));
		if (isMaker) {
			frame.html("Your partner player was successful in guessing the word <strong>" +
				lastGameWord + "</strong>.");
		}
		else {
			frame.html("<strong>CONGRATS!</strong> You correctly guessed that the word was <strong>" +
				lastGameWord + "</strong>!");
		}
		frame.dialog({
			modal: true,
			resizable: false,
			draggable: false,
			buttons: {
				"Ok": function () {
					$(this).dialog("close");
				}
			},
			close: function () {
				sendGameFlag(2);
				modalScreen("Waiting for your partner to respond.");
			}
		});
	}
	
	function displayFailure(isMaker) {
		var scope = $(document.createElement("div")),
			frame = $(document.createElement("div"));
		
		scope.addClass("blitzer").appendTo($("#mainBody")).append(frame);
		if (isMaker) {
			frame.html("Your partner player was unsuccessful in guessing the word <strong>'" +
				lastGameWord + "'</strong>.");
		}
		else {
			frame.html("<strong>CRIPES!</strong> You were close, but the word was actually <strong>'" +
				lastGameWord + "'</strong>! Oh well!");
		}
		frame.dialog({
			modal: true,
			resizable: false,
			draggable: false,
			dialogClass: "blitzer",
			buttons: {
				"Ok": function () {
					$(this).dialog("close");
				}
			},
			title: "Ah, crud...",
			close: function () {
				scope.remove();
				sendGameFlag(2);
				modalScreen("Waiting for your partner to respond.");
			}
		});
	}
	
	/*** END GAME STATE FUNCTION PROGRAMMING ***/	
	
	function sendGameFlag(flagType) {
		var flagSuccess = false;
		
		$.ajax({
			url: "./ajax/sendFlag.php",
			data: {
				'flagType': flagType
			},
			async: false
		}).done(function (data) {
			flagSuccess = !ajaxFailHandler(data);
		}).fail(function (data) {
			ajaxFailHandler(data);
		})
		
		return flagSuccess;
	};
	
	function processGameState(syncState, isClientMaker) {
		if (syncState != gameState) {
			console.log("game state has changed from " + gameState + " to " + syncState);
			gameState = syncState;
			onGameStateChanged(syncState, isClientMaker);
		}
	};
	
	function onGameStateChanged(newState, isClientMaker) {
		if (newState != "victory" && newState != "failure") {
			$("#mainBody").empty();
			closeModalScreen();
		}
		
		if (newState == "startup") {
			onStartup();
		}
		else if (newState == "wordInput") {
			(isClientMaker ? wordInput : waitForWordInput)();
		}
		else if (newState == "guessing") {
			(isClientMaker ? wordGuessingObserve : wordGuessing)();
		}
		else if (newState == "victory") {
			displayVictory(isClientMaker);
		}
		else if (newState == "failure") {	
			displayFailure(isClientMaker);
		}
	};
	
	Game.sync = function () {
		$.ajax({
			url: "./ajax/syncGame.php",
			async: false
		}).done(function (data) {
			if (!ajaxFailHandler(data) && xml.bool(data, "HasGame")) {
				wordLength = xml.text(data, "Game > WordLength");
				
				var word = $("Game > Word", data);
				if (word.length > 0) {
					lastGameWord = word.text();
				}
				
				processGameState(xml.text(data, "Game > State"), $("Game > IsMaker", data).length > 0);
				
				if (gameState != "startup") {
					syncGuessingGameData(data);
				}
			}
			else {
				processGameState(null, false);
			}
		}).fail(function (data) {
			ajaxFailHandler(data);
			processGameState(null, false);
		}).always(function () {
			isFirstRun = false;
		});
	};
	
	Game.startSync = function () {
		Game.sync();
		setInterval(function () {
			Game.sync();
		}, 3000);
	};
	
	Game.getState = function () {
		return gameState;
	};
})();