function modalScreen(message, title) {
	closeModalScreen();
	
	var frame = $(document.createElement("div"));
	title = title || "Waiting for other player...";
	
	frame.html(message).appendTo($("#mainBody")).attr("id", "modalScreen").dialog({
		closeOnEscape: false,
		dialogClass: "no-close",
		title: title,
		modal: true,
		resizable: false,
		draggable: false
	});
}

function closeModalScreen() {
	var frame = $("#modalScreen");
	if (frame.length > 0) {
		frame.remove();
	}
}
	
// AJAX error handling
function ajaxFailHandler(data) {
	var message = null;
	
	// Test XML Success element
	if (typeof data == "object") {
		if (data.hasOwnProperty("documentURI") || data.contentType == "text/xml") {
			if (xml.bool(data, "Success")) {
				return false;
			}
			
			message = xml.text(data, "Message");
		}
		else if (data.hasOwnProperty("jqXHR")) {
			message = data.exception;
		}
	}
	
	// Accepted
	console.error(message);
	
	var scope = $(document.createElement("div")),
		error = $(document.createElement("div")),
		icon = $(document.createElement("span")),
		textElem = $(document.createElement("span")),
		timeout,
		closing = false;
	
	function closeErrorTooltip() {
		if (closing) {
			return;
		}
		closing = true;
		
		if (timeout) {
			clearTimeout(timeout);
		}
		
		error.fadeOut(1500, function () {
			scope.remove();
		});
	}
	
	icon.addClass("ui-icon").addClass("ui-icon-alert").css("float", "left").css("marginRight", "0.3em").appendTo(error);
	textElem.html("<strong>Error</strong> " + message).appendTo(error);
	error.addClass("ui-state-error").addClass("ui-corner-all").click(function () {
		closeErrorTooltip();
	}).appendTo(scope);
	scope.addClass("blitzer").addClass("errorTooltip").appendTo(document.body);
	
	timeout = setTimeout(function () {
		closeErrorTooltip();
	}, 4000);
	
	return true;
}

$(document).ajaxError(function(e, jqXHR, settings, exception ) {
	ajaxFailHandler({
		'jqXHR': jqXHR,
		'exception': exception
	});
});

// XML
xml = {
	bool: function (xmlDoc, nodePath) {
		var element = $(nodePath, xmlDoc);
		if (element.length == 1 && $(element[0]).text() == "true") {
			return true;
		}
		return false;
	},
	
	text: function (xmlDoc, nodePath) {
		var element = $(nodePath, xmlDoc);
		if (element.length == 1) {
			return $(element[0]).text();
		}
		return false;
	}
};