// Function definitions
hasPartner = null;
getPartner = null;
syncPair = null;

startPairSyncing = null;
losesPartner = null;
getsPartner = null;

// Implementation
(function () {
	var isFirstRun = true,
		partner = null,
		losesPartnerCallback = [],
		getsPartnerCallback = [];
	
	function onPartnerLost() {
		for (var index = 0; index < losesPartnerCallback.length; ++index) {
			losesPartnerCallback[index].call();
		}
	}
	
	syncPair = function () {
		$.ajax({
			url: "./ajax/syncPair.php",
			async: false
		}).done(function (data) {
			if (!ajaxFailHandler(data) && xml.bool(data, "HasPair") && xml.bool(data, "HasPartner")) {
				var partnerGained = (partner == null || isFirstRun);
				partner = {
					'name': xml.text(data, "Partner > Name")
				};
				if (partnerGained) {
					for (var index = 0; index < getsPartnerCallback.length; ++index) {
						getsPartnerCallback[index].call();
					}
				}
			}
			else {
				var partnerLost = (partner != null || isFirstRun);
				partner = null;
				if (partnerLost) {
					onPartnerLost();
				}
			}
		}).fail(function (data) {
			ajaxFailHandler(data);
			var partnerLost = (partner != null || isFirstRun);
			partner = null;
			if (partnerLost) {
				onPartnerLost();
			}
		}).always(function () {
			isFirstRun = false;
		});
	};
	
	hasPartner = function () {
		return (partner != null);
	};
	
	getPartner = function () {
		return partner;
	};
	
	losesPartner = function (callback) {
		if (typeof callback == "function") {
			losesPartnerCallback.push(callback);
		}
	}
	
	getsPartner = function (callback) {
		if (typeof callback == "function") {
			getsPartnerCallback.push(callback);
		}
	}
	
	startPairSyncing = function () {
		if (isFirstRun) {
			syncPair();
			setInterval(function () {
				syncPair();
			}, 3000);
		}
	};	
})();

// Setup all of the callback handling
losesPartner(function () {
	modalScreen("Please wait for a player to join as your partner!", "HANGMAN");
	$("#partnerContainer").hide();
});

getsPartner(function () {
	closeModalScreen();
	$("#partnerContainer").show();
	$("#partner").text(getPartner().name);
});