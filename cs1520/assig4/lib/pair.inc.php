<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

class Pair
{
	private $_pairId;
	private $_playerA;
	private $_playerB;
	private $_isOpen;
	private $_currentMaker;
	
	private function __construct($pairInfo)
	{
		$this->_pairId = $pairInfo["pairId"];
		$this->_playerA = Player::get($pairInfo["playerA"], false);
		$this->_playerB = ($pairInfo["playerB"] == null ? null : Player::get($pairInfo["playerB"], false));
		$this->_isOpen = ($pairInfo["isOpen"] == 1);
		$this->_currentMaker = $pairInfo["currentMaker"];
	}
	
	public static function getForPlayer($playerId)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		// See if the player is already part of an open pair; if so, return this pair
		$existingPairInfo = $database->query("SELECT * FROM assig4_pairs WHERE isOpen=b'1' AND (playerA='" .
			$database->escape_string($playerId) . "' OR playerB='" . $database->escape_string($playerId) . "') LIMIT 1");
		if ($existingPairInfo != null && $existingPairInfo->num_rows == 1)
		{
			return new Pair($existingPairInfo->fetch_assoc());
		}
		
		// The player is not part of any open pairs, so find the first pair that is open that is also missing a second player
		$openPairInfo = $database->query("SELECT pairId FROM assig4_pairs WHERE isOpen=b'1' AND playerB IS NULL LIMIT 1");
		while ($openPairInfo != null && $openPairInfo->num_rows == 1)
		{
			$openPair = $openPairInfo->fetch_assoc();
			// Attempt to register this new playerId as playerB; if unable to do so (someone else is already registered, or the
			// pair is now closed), find the next open pair
			if ($database->query("UPDATE assig4_pairs SET playerB='" . $database->escape_string($playerId) .
				"' WHERE pairId='" . $openPair["pairId"] . "' AND isOpen=b'1' AND playerB IS NULL"))
			{
				return new Pair($database->query("SELECT * FROM assig4_pairs WHERE pairId='" . $openPair["pairId"] .
					"' LIMIT 1")->fetch_assoc());
			}
			
			$openPairInfo = $database->query("SELECT pairId FROM assig4_pairs WHERE isOpen=b'1' AND playerB IS NULL LIMIT 1");
		}
		
		// No open and waiting pairs could be found, so we should create a new pair
		if ($database->query("INSERT INTO assig4_pairs(playerA) VALUES('" . $database->escape_string($playerId) . "')"))
		{
			return new Pair($database->query("SELECT * FROM assig4_pairs WHERE pairId='" . $database->insert_id . "' LIMIT 1")->fetch_assoc());
		}
		
		// Some error along the way, return null -- no pair could be made
		return null;
	}
		
	/** ACCESSORS **/
	public function getId()
	{
		return $this->_pairId;
	}
	
	/** METHODS **/
	public function getPartnerOf($playerId)
	{
		if ($playerId == $this->_playerA->getId())
		{
			return $this->_playerB;
		}
		else
		{
			return $this->_playerA;
		}
	}
	
	public function getGame()
	{
		if (!$this->_isOpen)
		{
			return null;
		}
		
		if ($this->_playerB == null)
		{
			return null;
		}
		
		return Game::getOrCreateForPair($this->_pairId, $this->_currentMaker);
	}
	
	public function isMakerForGame($playerId)
	{
		$game = $this->getGame();
		if ($game == null)
		{
			return false;
		}
		
		if ($playerId == $this->_playerA->getId())
		{
			return ($game->getMaker() == "a");
		}
		else
		{
			return ($game->getMaker() == "b");
		}
	}
	
	public function close()
	{
		if (!$this->_isOpen)
		{
			return true;
		}
		
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (!$database->query("UPDATE assig4_pairs SET isOpen=b'0' WHERE pairId='" . $database->escape_string($this->_pairId) . "'"))
		{
			return false;
		}
		
		$this->_isOpen = false;
		return true;
	}
}

?>
