<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

define ("GAMESTATE_STARTUP", "startup");
define ("GAMESTATE_WORDINPUT", "wordInput");
define ("GAMESTATE_GUESSING", "guessing");
define ("GAMESTATE_VICTORY", "victory");
define ("GAMESTATE_FAILURE", "failure");

class Game
{
	private $_gameId;
	private $_pairId;
	private $_state;
	private $_maker;
	private $_word;
	private $_nGuesses = 0;
	private $_nIncorrectGuesses = 0;
	private $_guesses = array();
	
	private function __construct($gameInfo, $guesses)
	{
		$this->_gameId = $gameInfo["gameId"];
		$this->_pairId = $gameInfo["pairId"];
		$this->_state = $gameInfo["state"];
		$this->_maker = $gameInfo["maker"];
		$this->_word = $gameInfo["word"];
		
		while ($guess = $guesses->fetch_assoc())
		{
			++$this->_nGuesses;
			if (!$guess["isCorrect"])
			{
				++$this->_nIncorrectGuesses;
			}
			$this->_guesses[] = array("letter" => $guess["guess"], "isCorrect" => !(!$guess["isCorrect"]));
		}
	}
	
	public static function getOrCreateForPair($pairId, $maker)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		$gameResult = $database->query("SELECT * FROM assig4_games WHERE pairId='" .
			$database->escape_string($pairId) . "' LIMIT 1");
		if ($gameResult == null || $gameResult->num_rows == 0)
		{
			if (!$database->query("INSERT INTO assig4_games(pairId, maker) VALUES('" .
				$database->escape_string($pairId) . "','" . $database->escape_string($maker) . "')"))
			{
				return null;
			}
			
			$gameResult = $database->query("SELECT * FROM assig4_games WHERE gameId='" .
				$database->insert_id . "' LIMIT 1");
		}
		
		$gameInfo = $gameResult->fetch_assoc();
		$guesses = $database->query("SELECT * FROM assig4_guesses WHERE gameId='" . $gameInfo["gameId"] .
			"'");
		return new Game($gameInfo, $guesses);
	}
		
	/** ACCESSORS **/
	public function getId()
	{
		return $this->_gameId;
	}
	public function getMaker()
	{
		return $this->_maker;
	}
	public function getState()
	{
		return $this->_state;
	}
	public function getWord()
	{
		return $this->_word;
	}
	public function getNumberGuesses()
	{
		return $this->_nGuesses;
	}
	public function getNumberIncorrectGuesses()
	{
		return $this->_nIncorrectGuesses;
	}
	public function getGuesses()
	{
		return $this->_guesses;
	}
	
	/** METHODS **/
	public function setState($state)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (!$database->query("UPDATE assig4_games SET state='" . $database->escape_string($state) .
			"' WHERE gameId='" . $database->escape_string($this->_gameId) . "'"))
		{
			return false;
		}
		
		$this->_state = $state;
		return true;
	}
	
	public function hasGuessedLetter($letter)
	{
		foreach ($this->_guesses as $guess)
		{
			if ($guess["letter"] == $letter)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public function getWordBreakdown()
	{
		$breakdown = array();
		
		foreach (str_split($this->_word) as $letter)
		{
			if ($this->hasGuessedLetter($letter))
			{
				$breakdown[] = $letter;
			}
			else
			{
				$breakdown[] = null;
			}
		}
		
		return $breakdown;
	}
	
	public function setWord($word)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (!$database->query("UPDATE assig4_games SET state='guessing', word='" .
			$database->escape_string($word) . "' WHERE gameId='" . $database->escape_string($this->_gameId) .
			"'"))
		{
			return false;
		}
		
		$this->_state = "guessing";
		$this->_word = $word;
		return true;
	}
	
	public function makeGuess($letter)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		$isLetterCorrect = (mb_strpos($this->_word, $letter) !== false);
		
		if (!$database->query("INSERT INTO assig4_guesses(gameId, guess, isCorrect) VALUES('" .
			$database->escape_string($this->_gameId) . "','" . $database->escape_string($letter) .
			"',b'" . ($isLetterCorrect ? "1" : "0") . "')"))
		{
			return false;
		}
		
		++$this->_nGuesses;
		if (!$isLetterCorrect)
		{
			++$this->_nIncorrectGuesses;
		}
		$this->_guesses[] = array("letter" => $letter, "isCorrect" => $isLetterCorrect);
		
		// check to see if the game is over!
		if ($this->_nIncorrectGuesses >= MAX_NUMBER_INCORRECT_GUESSES)
		{
			$this->setState(GAMESTATE_FAILURE);
		}
		else
		{
			$isWordComplete = true;
			foreach ($this->getWordBreakdown() as $letter)
			{
				if ($letter == null)
				{
					$isWordComplete = false;
					break;
				}
			}
			
			if ($isWordComplete)
			{
				$this->setState(GAMESTATE_VICTORY);
			}
		}
		
		// finish up
		return true;
	}
	
	public function shutdown()
	{		
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		$newMaker = ($this->_maker == "a" ? "b" : "a");
		if (!$database->query("UPDATE assig4_pairs SET currentMaker='" . $newMaker . "' WHERE pairId='" .
			$database->escape_string($this->_pairId) . "'"))
		{
			return false;
		}
		
		$database->query("DELETE FROM assig4_guesses WHERE gameId='" . $database->escape_string($this->_gameId) . "'");
		$database->query("DELETE FROM assig4_games WHERE gameId='" . $database->escape_string($this->_gameId) . "'");
		$this->_isOpen = false;
		return true;
	}
}

?>
