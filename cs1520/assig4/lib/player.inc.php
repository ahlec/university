<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

define ("ASSIG4_AUTH_SESSION", "jbd19Assig4AuthSession");
class Player
{
	private $_playerId;
	private $_name;
	private $_pair;
	
	private function __construct($playerInfo, $getPair = true)
	{
		$this->_playerId = $playerInfo["playerId"];
		$this->_name = $playerInfo["name"];
		
		if ($getPair)
		{
			$this->_pair = Pair::getForPlayer($playerInfo["playerId"]);
		}
	}
	
	public static function getCurrent()
	{
		if (!isset($_SESSION[ASSIG4_AUTH_SESSION]))
		{
			return null;
		}
		
		return self::get($_SESSION[ASSIG4_AUTH_SESSION]);
	}
	
	public static function get($playerId, $getPair = true)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		$playerInfo = $database->query("SELECT playerId, name FROM assig4_players WHERE playerId='" .
			$database->escape_string($playerId) . "' LIMIT 1");
		if ($playerInfo === null || $playerInfo->num_rows != 1)
		{
			return null;
		}
		
		return new Player($playerInfo->fetch_assoc(), $getPair);
	}
	
	public static function login($name, $password)
	{
		global $database;
		if (!isset($database) || $database == null)
		{
			$database = openDatabase();
		}
		
		if (mb_strpos($name, "%") !== false)
		{
			return false;
		}
		
		if (isset($_SESSION[ASSIG4_AUTH_SESSION]))
		{
			unset($_SESSION[ASSIG4_AUTH_SESSION]);
		}
		
		$playerInfo = $database->query("SELECT * FROM assig4_players WHERE name LIKE '" . $database->escape_string($name) .
			"' AND password='" . $database->escape_string(md5($password)) . "' LIMIT 1");
		if ($playerInfo === null || $playerInfo->num_rows != 1)
		{
			return null;
		}
		
		$player = new Player($playerInfo->fetch_assoc());
		$_SESSION[ASSIG4_AUTH_SESSION] = $player->getId();
		return $player;
	}
	
	/** ACCESSORS **/
	public function getId()
	{
		return $this->_playerId;
	}
	public function getName()
	{
		return $this->_name;
	}
	public function getPair()
	{
		return $this->_pair;
	}
	
	/** METHODS **/
	public function getPartner()
	{
		if ($this->_pair == null)
		{
			return null;
		}
		
		return $this->_pair->getPartnerOf($this->_playerId);
	}
	public function isMaker()
	{
		if ($this->_pair == null)
		{
			return false;
		}
		
		return $this->_pair->isMakerForGame($this->_playerId);
	}
	public function logout()
	{
		if (isset($_SESSION[ASSIG4_AUTH_SESSION]))
		{
			unset($_SESSION[ASSIG4_AUTH_SESSION]);
		}
		
		if ($this->_pair != null)
		{
			$this->_pair->close();
			$this->_pair = null;
		}
	}
}

?>
