<?php
require_once ("./config.inc.php");
$player = Player::getCurrent();
if ($player != null)
{
	$player->logout();
}
header ("Location: login.php");
?>