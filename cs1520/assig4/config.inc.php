<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

define ("MAX_NUMBER_INCORRECT_GUESSES", 7);
define ("MIN_WORD_LENGTH", 1);
define ("MAX_WORD_LENGTH", 10);
define ("WORD_ALPHABET", "ABCDEFGHIJKLMNOPQRSTUVWXYZ-");

define ("GAMEFLAG_STARTUP_READY", 1);
define ("GAMEFLAG_SHUTDOWN_READY", 2);
// -------------------------
//   DO NOT MODIFY BELOW THIS LINE
// -------------------------
define ("URL_ROOT", "http://cs1520.cs.pitt.edu/~jbd19/assig4");
define ("DOCUMENT_ROOT", __DIR__);
define ("DATE_FORMAT", "l<b\\r />j/m/y<b\\r />h:i a");
date_default_timezone_set("America/New_York");

// Sessions
if (!isset($_SESSION))
{
	@session_start();
}

// Database
function openDatabase()
{
	return new mysqli("localhost", "DeitloffJ", "dada.pray", "DeitloffJ");
}

// Library
require_once (DOCUMENT_ROOT . "/lib/mbstring.inc.php");
require_once (DOCUMENT_ROOT . "/lib/player.inc.php");
require_once (DOCUMENT_ROOT . "/lib/pair.inc.php");
require_once (DOCUMENT_ROOT . "/lib/game.inc.php");

// HTML
function outputHead()
{
	echo "\t\t\t<title>Hangman - Assignment 4 (jbd19)</title>\n";
	echo "\t\t\t<base href=\"", URL_ROOT, "/\" />\n";
	echo "\t\t\t<link href=\"http://fonts.googleapis.com/css?family=Overlock+SC\" rel=\"stylesheet\" type=\"text/css\" />\n";
	echo "\t\t\t<link href=\"http://fonts.googleapis.com/css?family=Della+Respira\" rel=\"stylesheet\" type=\"text/css\">\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/jquery.js\"></script>\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/jquery-ui.js\"></script>\n";
	echo "\t\t\t<link rel=\"stylesheet\" href=\"./style/jquery-ui.css\" />\n";
	echo "\t\t\t<link rel=\"stylesheet\" href=\"./style/blitzer/blitzer.css\" />\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/pair.js\"></script>\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/game.js\"></script>\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/config.js.php\"></script>\n";
	echo "\t\t\t<script type=\"text/javascript\" src=\"./script/script.js\"></script>\n";
	echo "\t\t\t<link rel=\"stylesheet\" href=\"./style/style.css\" />\n";
	
	$scriptName = basename($_SERVER["SCRIPT_NAME"], ".php");
	if (file_exists(DOCUMENT_ROOT . "/script/" . $scriptName . ".js"))
	{
		echo "\t\t\t<script type=\"text/javascript\" src=\"./script/" . $scriptName . ".js\"></script>\n";
	}
	if (file_exists(DOCUMENT_ROOT . "/style/" . $scriptName . ".css"))
	{
		echo "\t\t\t<link rel=\"stylesheet\" href=\"./style/" . $scriptName . ".css\" />\n";
	}
}
?>
