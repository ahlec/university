<?php
// Do not modify below this point!
require_once ("config.inc.php");
$database = openDatabase();

?>
<style>
html {
	font-family:"Courier New", Courier, monospace;
}
y {
	color:#488214;
	font-weight:bold;
}
n {
	color:#B22222;
	font-weight:bold;
}
error {
	display:block;
	margin-left:30px;
}
</style>
<?php

// assig4_gameFlags
echo "assig4_gameFlags... ";
$database->query("DROP TABLE IF EXISTS assig4_gameFlags");
if ($database->query("CREATE TABLE assig4_gameFlags(gameId INT NOT NULL, playerId INT NOT NULL, " .
		"flagType INT NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig4_games
echo "<br />assig4_games... ";
$database->query("DROP TABLE IF EXISTS assig4_games");
if ($database->query("CREATE TABLE assig4_games(gameId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"pairId INT NOT NULL, state ENUM('startup','wordInput','guessing','victory','failure') NOT NULL DEFAULT 'startup', " .
		"maker ENUM ('a','b') NOT NULL DEFAULT 'a', word VARCHAR(10) NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig4_guesses
echo "<br />assig4_guesses... ";
$database->query("DROP TABLE IF EXISTS assig4_guesses");
if ($database->query("CREATE TABLE assig4_guesses(gameId INT NOT NULL , guess VARCHAR(1) NOT NULL, " .
		"isCorrect BIT DEFAULT b'0')"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig4_pairs
echo "<br />assig4_pairs... ";
$database->query("DROP TABLE IF EXISTS assig4_pairs");
if ($database->query("CREATE TABLE assig4_pairs(pairId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"playerA INT NOT NULL, playerB INT, isOpen BIT DEFAULT b'1', " .
		"currentMaker ENUM('a','b') NOT NULL DEFAULT 'a')"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig4_players
echo "<br />assig4_players... ";
$database->query("DROP TABLE IF EXISTS assig4_players");
if ($database->query("CREATE TABLE assig4_players(playerId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"name TEXT, password VARCHAR(32))"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

echo "<br />";
// Create players
foreach (array("alec", "conni") as $name)
{
	echo "<br />Creating player '" . $name . "'... ";
	if ($database->query("INSERT INTO assig4_players(name, password) VALUES('" .
		$name . "','" . md5("default") . "')"))
	{
		echo "<y>CREATED.</y> (password: default)";
	}
	else
	{
		echo "<n>ERROR.</n>";
		echo "<error>" . $database->error . "</error>";
	}
}

// Done
echo "<br /><br />DONE.";
?>