<?php
require_once ("config.inc.php");
$database = openDatabase();

// Check to make sure the user is logged in
if (!Auth::isLoggedIn())
{
	pageError("In order to perform this action, you must be logged in.");
}

// Title
if (!isset($_POST["title"]))
{
	pageError("Attempted to circumvent the form. Please do not do this (Error Code: 31).", "createSchedule.php");
}
$title = trim($_POST["title"]);
if (mb_strlen($title) == 0 || mb_strpos($title, "\n") !== false || mb_strpos($title, "\r") !== false)
{
	pageError("The title provide is invalid, or is in an invalid format (Error Code: 30).", "createSchedule.php");
}
$title = $database->escape_string($title);

// Users
if (!isset($_POST["users"]) || !is_array($_POST["users"]) || count($_POST["users"]) == 0)
{
	pageError("Attempted to circumvent the form. Please do not do this (Error Code: 29).", "createSchedule.php");
}
$users = array();
foreach ($_POST["users"] as $user)
{
	if (!ctype_digit($user))
	{
		pageError("Attempted to circumvent the form. Please do not do this (Error Code: 28).", "createSchedule.php");
	}
	$users[] = intval($user);
}
if (!in_array(Auth::getCurrentAccountNo(), $users))
{
	$users[] = Auth::getCurrentAccountNo();
}

// Slots
if (!isset($_POST["slots"]) || !is_array($_POST["slots"]) || count($_POST["slots"]) == 0)
{
	pageError("Attempted to circumvent the form. Please do not do this (Error Code: 27).", "createSchedule.php");
}
$slots = array();
foreach ($_POST["slots"] as $slot)
{
	if (!ctype_digit($slot))
	{
		pageError("Attempted to circumvent the form. Please do not do this (Error Code: 25).", "createSchedule.php");
	}
	
	if (in_array(intval($slot), $slots))
	{
		pageError("Attempted to circumvent the form. Please do not do this (Error Code: 26).", "createSchedule.php");
	}
	
	$slots[] = intval($slot);
}

// Generate a new schedule hash
$try = 0;
do
{
	$try++;
	if ($try == 30)
	{
		exit("LOOPED");
	}
	$scheduleHash = md5(rand());
} while ($database->query("SELECT * FROM assig2_schedules WHERE scheduleHash='" .
										 $scheduleHash . "' LIMIT 1")->num_rows > 0);
										 
// Create the schedule
if (!$database->query("INSERT INTO assig2_schedules(scheduleHash, accountId, title) VALUES('" .
	$scheduleHash . "','" . Auth::getCurrentAccountNo() . "','" . $title . "')"))
{
	pageError("Unable to create the schedule due to a programmatic error (Error Code: 22).");
}
$scheduleId = $database->insert_id;

// Create the slots
foreach ($slots as $slot)
{
	if (!$database->query("INSERT INTO assig2_scheduleSlots(scheduleId, startTime) VALUES('" .
		$scheduleId . "','" . date("Y-m-d H:i:s", $slot) . "')"))
	{
		// Clean up the database so we don't have fragments
		$database->query("DELETE FROM assig2_scheduleSlots WHERE scheduleId='" . $scheduleId . "'");
		$database->query("DELETE FROM assig2_schedules WHERE scheduleId='" . $scheduleId . "'");
		pageError("Unable to create the schedule due to a programmatic error (Error Code 23).");
	}
}

// Add the users
foreach ($users as $user)
{
	if (!$database->query("INSERT INTO assig2_scheduleUsers(scheduleId, userId) VALUES('" .
		$scheduleId . "','" . $user . "')"))
	{
		// Clean up the database so we don't have fragments
		$database->query("DELETE FROM assig2_scheduleUsers WHERE scheduleId='" . $scheduleId . "'");
		$database->query("DELETE FROM assig2_scheduleSlots WHERE scheduleId='" . $scheduleId . "'");
		$database->query("DELETE FROM assig2_schedules WHERE scheduleId='" . $scheduleId . "'");
		pageError("Unable to create the schedule due to a programmatic error (Error Code 24).");
	}
}

// Send out the emails (now that we know all database actions were successful)
$userInfo = $database->query("SELECT userHash, firstName, lastName, email FROM assig2_users " .
			"WHERE userId IN (" . implode(",", $users) . ")");
$creatorInfo = Auth::getCurrentAcountInfo();
$emailFails = 0;
while ($user = $userInfo->fetch_array())
{
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'To: ' . $user["firstName"] . " " . $user["lastName"] . ' <' . $user["email"] . ">\r\n";
	if (!mail($user["email"], "New scheduling request from " . $creatorInfo["firstName"] . " " . $creatorInfo["lastName"],
			"Hello, " . $user["firstName"] . "! " . $creatorInfo["firstName"] . " " . $creatorInfo["lastName"] . " (<a href=\"mailto:" .
			$creatorInfo["email"] . ";\">" . $creatorInfo["email"] . "</a>) has created a new schedule, titled '<b>" .
			$title . "</b>'. " . $creatorInfo["firstName"] . " seeks your participation in determining the most oportune meeting time!<br /><br />" .
			"To participate, please navigate your web browser to <a href=\"http://cs1520.cs.pitt.edu/~jbd19/assig2/schedule.php?schedule=" .
			$scheduleHash . "&user=" . $user["userHash"] . "\" target=\"_blank\">http://cs1520.cs.pitt.edu/~jbd19/assig2/schedule.php?schedule=" .
			$scheduleHash . "&user=" . $user["userHash"] . "</a>. Clicking the 'Edit' button to the right of your name will allow you to select " .
			"which times you are available to participate during. Once a time has been selected, you will be contacted again with the date and time " .
			"chosen.<br /><br />On behalf of " . $creatorInfo["firstName"] . " and our team, thank you!", $headers))
	{
		$emailFails++;
	}
}

// Go to the new schedule
if ($emailFails == 0)
{
	pageSuccess("Your new schedule has been created.", "schedule.php?schedule=" . $scheduleHash . "&user=" . Auth::getCurrentUserHash());
}
else
{
	pageError("Your new schedule was created, but one or more emails failed to send.", "schedule.php?schedule=" . $scheduleHash . "&user=" . Auth::getCurrentUserHash());
}
?>