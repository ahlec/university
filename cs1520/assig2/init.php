<?php
define("IS_SCRIPT_ALLOWED_TO_RUN", true);
define("INIT_BASE_EMAIL", "jbd19@pitt.edu");
define("INIT_DEFAULT_PASSWORD", "!password1");

// Do not modify below this point!
if (!IS_SCRIPT_ALLOWED_TO_RUN)
{
	exit("For safety purposes, this script is disabled from executing. In order to allow this script to be run, please change " .
		 "the init.php file variable 'IS_SCRIPT_ALLOWED_TO_RUN' from false to true.");
}
require_once ("config.inc.php");
$database = openDatabase();
Auth::logout();

?>
<style>
html {
	font-family:"Courier New", Courier, monospace;
}
y {
	color:#488214;
	font-weight:bold;
}
n {
	color:#B22222;
	font-weight:bold;
}
error {
	display:block;
	margin-left:30px;
}
</style>
<?php

// assig2_accounts
echo "assig2_acounts... ";
$database->query("DROP TABLE IF EXISTS assig2_accounts");
if ($database->query("CREATE TABLE assig2_accounts(accountId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"firstName TEXT NOT NULL, lastName TEXT NOT NULL, email TEXT NOT NULL, password VARCHAR(32) NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig2_scheduleSlots
echo "<br />assig2_scheduleSlots... ";
$database->query("DROP TABLE IF EXISTS assig2_scheduleSlots");
if ($database->query("CREATE TABLE assig2_scheduleSlots(slotId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"scheduleId INT NOT NULL, startTime datetime NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig2_scheduleUsers
echo "<br />assig2_scheduleUsers... ";
$database->query("DROP TABLE IF EXISTS assig2_scheduleUsers");
if ($database->query("CREATE TABLE assig2_scheduleUsers(associationId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"scheduleId INT NOT NULL, userId INT NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig2_schedules
echo "<br />assig2_schedules... ";
$database->query("DROP TABLE IF EXISTS assig2_schedules");
if ($database->query("CREATE TABLE assig2_schedules(scheduleId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"scheduleHash VARCHAR(32) NOT NULL, accountId INT NOT NULL, isFinalized BIT DEFAULT 0, " .
		"finalizedSlotId INT, title TEXT NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig2_signups
echo "<br />assig2_signups... ";
$database->query("DROP TABLE IF EXISTS assig2_signups");
if ($database->query("CREATE TABLE assig2_signups(signupId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"userId INT NOT NULL, slotId INT NOT NULL, scheduleId INT NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

// assig2_users
echo "<br />assig2_users... ";
$database->query("DROP TABLE IF EXISTS assig2_users");
if ($database->query("CREATE TABLE assig2_users(userId INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
		"accountId INT, userHash VARCHAR(32) NOT NULL, firstName TEXT NOT NULL, " .
		"lastName TEXT NOT NULL, email TEXT NOT NULL)"))
{
	echo "<y>SUCCESS.</y>";
}
else
{
	echo "<n>ERROR.</n>";
	echo "<error>" . $database->error . "</error>";
}

echo "<br />";
// Create accounts
$emailPieces = explode("@", INIT_BASE_EMAIL);
foreach (array("one", "two", "three") as $name)
{
	echo "<br />Creating maker '" . $emailPieces[0] . "+" . $name . "@" . $emailPieces[1] . "... ";
	if ($database->query("INSERT INTO assig2_accounts(firstName, lastName, email, password) VALUES('" .
		"Maker','" . ucfirst($name) . "','" . $emailPieces[0] . "+" . $name . "@" . $emailPieces[1] .
		"','" . md5(INIT_DEFAULT_PASSWORD) . "')"))
	{
		
		$try = 0;
		do
		{
			$try++;
			if ($try == 30)
			{
				exit("LOOPED");
			}
			$userHash = md5(rand());
		} while ($database->query("SELECT * FROM assig2_users WHERE userHash='" .
												 $userHash . "' LIMIT 1")->num_rows > 0);
		
		$userId = $database->insert_id;
		if ($database->query("INSERT INTO assig2_users(accountId, userHash, firstName, " .
			"lastName, email) VALUES('" . $userId . "','" . $userHash . 
			"','Maker','" . $name . "','" . $emailPieces[0] . "+" . $name . "@" . $emailPieces[1] .
			"')"))
		{
			echo "<y>CREATED.</y> (password: " . INIT_DEFAULT_PASSWORD . ")";
		}
		else
		{
			$database->query("DELETE FROM assig2_accounts WHERE accountId='" . $userId . "'");
			echo "<n>ERROR.</n>";
			echo "<error>" . $database->error . "</error>";
		}
	}
	else
	{
		echo "<n>ERROR.</n>";
		echo "<error>" . $database->error . "</error>";
	}
}

// Done
echo "<br /><br />DONE.";
?>
