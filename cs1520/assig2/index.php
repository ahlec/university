<?php
// Setup
require_once ("config.inc.php");

// Ensure we have the ability to be here
if (!Auth::isLoggedIn())
{
	header("Location: login.php");
	exit();
}

$schedules = Schedule::getForUser(openDatabase(), 1);
$activeSchedules = array();
$finalizedSchedules = array();
foreach ($schedules as $schedule)
{
	if ($schedule->isFinalized())
	{
		$finalizedSchedules[] = $schedule;
	}
	else
	{
		$activeSchedules[] = $schedule;
	}
}
$userHash = Auth::getCurrentUserHash();

// Begin
require_once ("start.inc.php");

// Page content
echo "<h2>Active Schedules</h2>\n";
if (count($activeSchedules) > 0)
{
	foreach ($activeSchedules as $schedule)
	{
		echo "<a href=\"schedule.php?schedule=" . $schedule->getHash() . "&user=" . $userHash . "\">" .
			$schedule->getTitle() . "</a><br />\n";
	}
}
else
{
	echo "You currently have no active schedules open.";
}

echo "<h2>Finalized Schedules</h2>\n";
if (count($finalizedSchedules) > 0)
{
	foreach ($finalizedSchedules as $schedule)
	{
		echo "<a href=\"schedule.php?schedule=" . $schedule->getHash() . "&user=" . $userHash . "\">" .
			$schedule->getTitle() . "</a><br />\n";
	}
}
else
{
	echo "You currently have no finalized schedules.";
}

// End
require_once ("end.inc.php");
?>