<?php
class Schedule
{
	private $_scheduleId;
	private $_scheduleHash;
	private $_accountId;
	private $_creatorFirstName;
	private $_creatorLastName;
	private $_creatorEmail;
	private $_isFinalized;
	private $_finalizedSlotId;
	private $_title;
	private $_slots = array();
	private $_users = array();
	private $_canFinalize = false;
	
	private function __construct($infoRow, $database)
	{
		$this->_scheduleId = intval($infoRow["scheduleId"]);
		$this->_scheduleHash = $infoRow["scheduleHash"];
		$this->_accountId = intval($infoRow["accountId"]);
		$this->_creatorFirstName = $infoRow["firstName"];
		$this->_creatorLastName = $infoRow["lastName"];
		$this->_creatorEmail = $infoRow["email"];
		$this->_isFinalized = ($infoRow["isFinalized"] == 1);
		$this->_title = $infoRow["title"];

		$slotResults = $database->query("SELECT slotId, startTime FROM assig2_scheduleSlots WHERE scheduleId='" .
										$this->_scheduleId . "' ORDER BY startTime ASC");
		while ($slot = $slotResults->fetch_array())
		{
			$this->_slots[$slot["slotId"]] = array("slotId" => intval($slot["slotId"]),
												   "startTime" => strtotime($slot["startTime"]),
												   "total" => 0,
												   "isSelectedSlot" => false);
		}
		
		$userResults = $database->query("SELECT u.userId, u.accountId, u.userHash, u.firstName, u.lastName, u.email, " .
										"s.signupId, s.slotId FROM assig2_scheduleUsers su JOIN assig2_users u " .
										"ON u.userId = su.userId LEFT JOIN assig2_signups s ON s.scheduleId = su.scheduleId AND " .
										"s.userId = su.userId WHERE su.scheduleId='" . $this->_scheduleId . "'");
		while ($userResult = $userResults->fetch_array())
		{
			if (!isset($this->_users[$userResult["userId"]]))
			{
				$this->_users[$userResult["userId"]] = array("userId" => intval($userResult["userId"]),
															 "userHash" => $userResult["userHash"],
														     "accountId" => ($userResult["accountId"] != null ?
																			 intval($userResult["accountId"]) : null),
															 "firstName" => $userResult["firstName"],
															 "lastName" => $userResult["lastName"],
															 "email" => $userResult["email"],
															 "slots" => array());
			}
			
			if ($userResult["signupId"] != null)
			{
				$this->_slots[$userResult["slotId"]]["total"]++;
				$this->_users[$userResult["userId"]]["slots"][] = intval($userResult["slotId"]);
				$this->_canFinalize = true;
			}
		}
		
		if ($this->_isFinalized)
		{
			$this->_canFinalize = false;
			$this->_slots[$infoRow["finalizedSlotId"]]["isSelectedSlot"] = true;
		}
	}
	
	public static function get($database, $scheduleHash)
	{
		if (mb_strpos($scheduleHash, "%") !== false)
		{
			// Not allowed to have the '%' character in the hash
			return null;
		}
		
		$scheduleInfo = $database->query("SELECT scheduleId, scheduleHash, s.accountId, " .
						     "a.firstName, a.lastName, a.email, isFinalized, finalizedSlotId, title FROM assig2_schedules s " .
						     "JOIN assig2_accounts a ON s.accountId = a.accountId WHERE " .
						     "scheduleHash like '" . $database->escape_string($scheduleHash) .
						     "' LIMIT 1");
		if ($scheduleInfo == null || $scheduleInfo->num_rows == 0)
		{
			return null;
		}
		
		return new Schedule($scheduleInfo->fetch_assoc(), $database);
	}
	
	public static function getForUser($database, $userNo)
	{
		$schedules = array();
		$scheduleInfo = $database->query("SELECT s.scheduleId, scheduleHash, s.accountId, " .
							"a.firstName, a.lastName, a.email, isFinalized, finalizedSlotId, title FROM assig2_schedules s " .
							"JOIN assig2_accounts a ON s.accountId = a.accountId JOIN " .
							"assig2_scheduleUsers su ON su.scheduleId = s.scheduleId WHERE " .
							"su.userId='" . $database->escape_string($userNo) . "'");
		if ($scheduleInfo != null)
		{
			while ($scheduleRow = $scheduleInfo->fetch_assoc())
			{
				$schedules[] = new Schedule($scheduleRow, $database);
			}
		}
		
		return $schedules;
	}

	// ACCESSORS
	public function getId()
	{
		return $this->_scheduleId;
	}
	
	public function getHash()
	{
		return $this->_scheduleHash;
	}
	
	public function getCreatorAccountId()
	{
		return $this->_accountId;
	}
	
	public function getCreatorName()
	{
		return $this->_creatorFirstName . " " . $this->_creatorLastName;
	}
	
	public function getCreatorEmail()
	{
		return $this->_creatorEmail;
	}

	public function isFinalized()
	{
		return $this->_isFinalized;
	}

	public function getSlots()
	{
		return $this->_slots;
	}

	public function getTitle()
	{
		return $this->_title;
	}

	public function getUsers()
	{
		return $this->_users;
	}
	
	public function canFinalize()
	{
		return $this->_canFinalize;
	}
}
?>