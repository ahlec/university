<?php
require_once ("config.inc.php");

// Process login (if applicable)
if (isset($_POST["email"]) && isset($_POST["password"]))
{
  if (Auth::login($_POST["email"], $_POST["password"]))
  {
     pageSuccess("You are now logged in.");
  }
}

// Begin
require_once ("start.inc.php");

// Content
if (isset($_POST["email"]) || isset($_POST["password"]))
{
	echo "<div class=\"error\">The username and password combination provided is invalid.</div>\n";
}
?>
    <form method="POST" action="./login.php" class="login">
      <label for="email">Email</label>
	  <div class="container" id="emailContainer">
	    <input type="email" name="email" id="email" disabled="disabled" />
	  </div>
      <label for="password">Password</label>
	  <div class="container" id="passwordContainer">
	    <input type="password" name="password" id="password" disabled="disabled" />
      </div>
	  <input type="submit" id="login" value="Login" class="button" disabled="disabled" />
	  <span class="fauxLink reset" onclick="forgotPassword();">Forgot password</span>
    </form>
	<form id="forgotPasswordForm" style="display:none;">
		To retrieve your password, type your email address below:<br />
		<input type="email" id="forgotPasswordEmail" />
	</form>
<?php
// End
require_once ("end.inc.php");
?>