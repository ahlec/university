<?php
require_once ("config.inc.php");
$database = openDatabase();

// Check to make sure the user is logged in
if (!Auth::isLoggedIn())
{
	pageError("In order to perform this action, you must be logged in.");
}

// Check to make sure that we were provided with a schedule, and that it is one that the current
// user is in control of.
if (!isset($_GET["schedule"]))
{
	pageError("In order to perform this action, the schedule must be specified (Error Code: 32).");
}
$schedule = Schedule::get($database, $_GET["schedule"]);
if ($schedule == null || $schedule->getCreatorAccountId() != Auth::getCurrentAccountNo())
{
	pageError("The provided schedule does not exist, or you do not have permission to perform this action upon it.");
}

// Make sure that the schedule can be finalized
if (!$schedule->canFinalize())
{
	pageError("The specified schedule cannot be finalized at the moment (it is either finalized already, or has no respondents).",
			"schedule.php?schedule=" . $schedule->getHash() . "&user=" . Auth::getCurrentUserHash());
}

// Finalize the schedule
$finalizedSlot = null;
foreach ($schedule->getSlots() as $slot)
{
	if ($slot["total"] > 0 && ($finalizedSlot == null || $slot["total"] > $finalizedSlot["total"]))
	{
		$finalizedSlot = $slot;
	}
}
if ($finalizedSlot == null)
{
	pageError("The data for the schedule does not seem to be consistent. Please try again (Error Code: 33).",
			"schedule.php?schedule=" . $schedule->getHash() . "&user=" . Auth::getCurrentUserHash());
}

if (!$database->query("UPDATE assig2_schedules SET isFinalized=1, finalizedSlotId='" . $finalizedSlot["slotId"] .
	"' WHERE scheduleId='" . $schedule->getId() . "'"))
{
	pageError("The schedule could not be finalized due to a programming error at the moment (Error Code: 34).",
			"schedule.php?schedule=" . $schedule->getHash() . "&user=" . Auth::getCurrentUserHash());
}

// Send the emails to the participants
$creatorInfo = Auth::getCurrentAcountInfo();
$emailFails = 0;
foreach ($schedule->getUsers() as $user)
{
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'To: ' . $user["firstName"] . " " . $user["lastName"] . ' <' . $user["email"] . ">\r\n";
	if (!mail($user["email"], $creatorInfo["firstName"] . " finalized their schedule '" . $schedule->getTitle() . "'",
			"Hello again, " . $user["firstName"] . "! " . $creatorInfo["firstName"] . "'s schedule '<b>" . $schedule->getTitle() .
			"</b>' was finalized; a date and time have been chosen to best fit everybody's calendars. Based on the respondents, the best time was " .
			"chosen to be:<br /><br />" . date("l <b>j F</b> Y", $finalizedSlot["startTime"]) . " from " .
			date("<b>H:i</b> (g:i A)", $finalizedSlot["startTime"]) . " to " . date("<b>H:i</b> (g:i A)", $finalizedSlot["startTime"] + (60 * 60)) .
			"<br /><br />If this time is a problem for you, directly contact " . $creatorInfo["firstName"] . " at <a href=\"mailto:" .
			$creatorInfo["email"] . "\">" . $creatorInfo["email"] . "</a>. The schedule may no longer be changed, but you can continue to view it " .
			"from the same link as before: <a href=\"http://cs1520.cs.pitt.edu/~jbd19/assig2/schedule.php?schedule=" . $schedule->getHash() .
			"&user=" . $user["userHash"] . "\" target=\"_blank\">http://cs1520cs.pitt.edu/~jbd19/assig2/schedule.php?schedule=" .
			$schedule->getHash() . "&user=" . $user["userHash"] . "</a>.<br /><br />On behalf of " . $creatorInfo["firstName"] . " and our team, thank you!",
			$headers))
	{
		$emailFails++;
	}
}

// Go to the new schedule
if ($emailFails == 0)
{
	pageSuccess("The schedule was successfully finalized.", "schedule.php?schedule=" . $schedule->getHash() . "&user=" . Auth::getCurrentUserHash());
}
else
{
	pageError("The schedule has been finalized, but one or more emails failed to send.", "schedule.php?schedule=" . $schedule->getHash() .
			"&user=" . Auth::getCurrentUserHash());
}
?>