$(document).ready(function () {
	var email = $("input#email"),
		emailContainer = $("div#emailContainer"),
		password = $("input#password"),
		passwordContainer = $("div#passwordContainer"),
		loginButton = $("input#login");
	
	function validateEmail() {
		var currentVal = email.val();
		
		if (currentVal.length == 0) {
			emailContainer.addClass("invalid").removeClass("valid").attr("data-message", "Please provide an email address!");
			return false;
		}
		
		if (!isValidEmail(currentVal)) {
			emailContainer.addClass("invalid").removeClass("valid").attr("data-message", "The provided email is not in a form recognized as valid.");
			return false;
		}
		
		emailContainer.removeClass("invalid").addClass("valid");
		return true;
	}
	
	function validatePassword() {
		var currentVal = password.val();
		
		if (currentVal.length == 0) {
			passwordContainer.addClass("invalid").removeClass("valid").attr("data-message", "Please provide a password!");
			return false;
		}
		
		passwordContainer.removeClass("invalid").addClass("valid");
		return true;
	}
	
	function validateForm() {
		if (email.val().length == 0) {
			loginButton.prop("disabled", true);
			return;
		}
		
		if (password.val().length == 0) {
			loginButton.prop("disabled", true);
			return;
		}
		
		loginButton.prop("disabled", false);
	}
		
	email.keyup(function () {
		emailContainer.removeClass("invalid").removeClass("valid");
		validateForm();
	}).blur(validateEmail).prop("disabled", false);
	password.keyup(function () {
		passwordContainer.removeClass("invalid").removeClass("valid");
		validateForm();
	}).blur(validatePassword).prop("disabled", false);
	
	validateForm();
});

function forgotPassword() {
	$("input#forgotPasswordEmail").val($("input#email").val());
	
	$("form#forgotPasswordForm").dialog({
		modal:true,
		resizable:false,
		draggable:false,
		title: "Forgot password?",
		buttons: {
			"Retrieve": function () {
				var results = null;
	
				$.ajax({
					data: {
						email: $("input#forgotPasswordEmail").val()
					},
					url: "./ajax/forgotPassword.php",
					async: false,
					success: function (data) {
						data = $.parseJSON(data);
						if (data.success) {
							alertSuccess(data.message);
						}
						else {
							alertError(data.message);
						}
					}
				});
				
				$(this).dialog("close");
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});
}