$(document).ready(function () {
  function hasRowTick(row) {
    return (row.find("input[type=checkbox]:checked").length > 0);
  }

  function isRowVanilla(row) {
    // Returns true if all of the checkboxes currently mirror their original state, false otherwise.
    var isVanilla = true;
    row.find("input[type=checkbox]").each(function () {
      if ($(this).data("isdefaultchecked") != $(this).is(":checked")) {
        isVanilla = false;
        return false;
      }
    });
    return isVanilla;
  }

  function shouldRowCancelOnNoTicks(row) {
    // By explicitly comparing to true, we check for existence as well as equality to boolean true.
    return (row.data("cancelonnoticks") === true);
  }

  var SUBMIT_STARTED = false;
  function submitRow(row) {
    // Ensure seclusion
    if (SUBMIT_STARTED) {
      console.log("A submission has already begun. No more may be submitted. If an exception occurred, then this is the end of the road.");
      return;
    }
    SUBMIT_STARTED = true;

    // Begin the cloning process
    var form = $("#submitForm");
    form.empty();
	var scheduleHashElement = $(document.createElement('input'));
	scheduleHashElement.attr("type", "hidden").attr("name", "scheduleHash").val(form.data("schedulehash"));
	form.append(scheduleHashElement);
    row.find("input").each(function () {
      var me = $(this);
      if (me.is("input[type=checkbox]:not(:checked)")) {
        return;
      }
 
      var cloneElement = $(document.createElement('input'));
      if (me.hasClass("action")) {
        cloneElement.attr("type", "hidden").attr("name", "process").val(me.data("defaulttext"));
      }
      else {
        cloneElement.attr("type", me.attr("type")).attr("name", me.attr("name")).val(me.val()).attr("checked", true);
      }
      form.append(cloneElement);
    });

    // Submit it!
    form.submit();
  }

  $("input[type=checkbox]").click(function () {
    var me = $(this),
        parentRow = me.closest("tr"),
        actionButton = parentRow.find("input.action");

    if (!parentRow.hasClass("activated")) {
      return;
    }

    if (!isRowVanilla(parentRow)) {
      parentRow.removeClass("unselected");
      actionButton.val("Submit");
    }
    else {
      parentRow.addClass("unselected");
      actionButton.val("Cancel");
    }
  });

  function closeRow(row) {
    row.removeClass("activated").removeClass("unselected");
    actionButton = row.find("input.action");
    actionButton.val(actionButton.data("defaulttext"));
  }

  $("input.action").each(function () {
    $(this).val($(this).data("defaulttext")).prop("disabled", false).click(function () {
      var me = $(this),
          parentRow = me.closest("tr");
      
      // Initial display
      if (!parentRow.hasClass("activated")) {
        $("tr.activated").each(function () {
          closeRow($(this));
        });
        parentRow.find("input[type=checkbox]").each(function () {
          $(this).prop("checked", $(this).data("isdefaultchecked"));
        });
        parentRow.find("input[type=text]").each(function () {
          $(this).val("");
        });
        me.val("Cancel");
        parentRow.addClass("activated");
        return;
      }

      // Cancel
      if (isRowVanilla(parentRow)) {
        me.val(me.data("defaulttext"));
        parentRow.removeClass("activated").removeClass("unselected");
        return;
      }

      if (confirm("Are you sure you would like to save your work here?")) {
        submitRow(parentRow);
      }
    });
  });

  $("table input[type=checkbox], table input[type=text]").keyup(function (e) {
    if (e.which == 13) {
      // Enter
      var me = $(this),
          parentRow = me.closest("tr");

      if (parentRow.hasClass("activated")) {
        parentRow.find("input.action").click();
        e.preventDefault();
      }
    }
  });

  $(window).keydown(function (e) {
    if (e.which == 27) {
      // Escape
      $("tr.activated").each(function () {
        closeRow($(this));
      });
      e.preventDefault();
    }
  });
});

function finalizeSchedule(scheduleHash) {
	$("div#finalizeDialog").dialog({
		modal:true,
		resizable:false,
		draggable:false,
		title: "Finalize schedule?",
		buttons:{
			"Yes": function () {
				window.location = "./finalize.php?schedule=" + scheduleHash;
			},
			"No": function () {
				$(this).dialog("close");
			}
		}
	});
}