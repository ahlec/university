/** FORM **/
function validateCreateForm(form) {
	var title = $.trim($("input#title").val());
	
	if (title.length == 0 || title.indexOf("\n") >= 0 || title.indexOf("\r") >= 0) {
		alertError("All schedules must have a title provided for them that does not consist solely of whitespace, and which " +
				   "does not contain any newline characters.");
		$("input#title").focus();
		return false;
	}
	
	if ($("fieldset#usersFieldset").find("input[type=hidden]").length == 0) {
		alertError("All schedules must have at least one user defined.");
		return false;
	}
	
	if ($("fieldset#slotsFieldset").find("input[type=hidden]").length == 0) {
		alertError("All schedules must have at least one time slot.");
		return false;
	}
	
	return true;
}

/** USERS **/
function addUser() {
	$("input#addUser_email").val("");
	$("input#addUser_firstName").val("");
	$("input#addUser_lastName").val("");
	
	$("div#addUserForm").dialog({
		modal:true,
		width:300,
		title: "Add new user",
		draggable: false
	});
}

function addUser_submit() {
	var email = $("input#addUser_email").val(),
		firstName = $("input#addUser_firstName").val(),
		lastName = $("input#addUser_lastName").val(),
		resolvedResults = resolveUserId(firstName, lastName, email),
		usersFieldset = $("fieldset#usersFieldset");
		
	if (typeof resolvedResults != "number") {
		alertError(resolvedResults);
		return;
	}
	
	if ($("input#user_" + resolvedResults + "[type=hidden]").length != 0) {
		alertError("The specified user has already been added to this schedule.");
		return;
	}
	
	var newHidden = $(document.createElement("input")),
		newLabel = $(document.createElement("div")),
		removeButton = $(document.createElement("div"));
	
	newHidden.attr("type", "hidden").attr("id", "user_" + resolvedResults).attr("name", "users[]").val(resolvedResults).appendTo(usersFieldset);
	newLabel.text(firstName + " " + lastName + " (" + email + ")").appendTo(usersFieldset);
	removeButton.addClass("removeButton").addClass("user").click(function () {
		newHidden.remove();
		newLabel.remove();
	}).prependTo(newLabel);
	$("div#addUserForm").dialog("close");
}

function resolveUserId(firstName, lastName, email) {
	var results = null;
	
	$.ajax({
		data: {
			firstName: firstName,
			lastName: lastName,
			email: email
		},
		url: "./ajax/resolveUser.php",
		async: false,
		success: function (data) {
			data = $.parseJSON(data);
			if (data.success) {
				results = data.userId;
			}
			else {
				results = data.message;
			}
		}
	});
	return results;
}

/** SLOTS **/
function addSlot() {
	$("input#addSlot_date").val("");
	$("input#addSlot_time").val("");
	
	$("div#addSlotForm").dialog({
		modal:true,
		width:300,
		title: "Add new time slot",
		draggable: false
	});
}

function addSlot_submit() {
	var date = $("input#addSlot_date").val(),
		time = $("input#addSlot_time").val(),
		startTime = (new Date(date + " " + time).getTime()) / 1000,
		slotsFieldset = $("fieldset#slotsFieldset");
		
	if (startTime == NaN) {
		alertError("The specified date and time combination are invalid, or are not properly formatted.");
		return;
	}
	
	if ($("input#slot_" + startTime + "[type=hidden]").length != 0) {
		alertError("The specified time has already been added to this schedule.");
		return;
	}
	
	var newHidden = $(document.createElement("input")),
		newLabel = $(document.createElement("div")),
		removeButton = $(document.createElement("div"));
	
	newHidden.attr("type", "hidden").attr("id", "slot_" + startTime).attr("name", "slots[]").val(startTime).appendTo(slotsFieldset);
	newLabel.text((new Date(date + " " + time)).toLocaleString()).appendTo(slotsFieldset);
	removeButton.addClass("removeButton").addClass("slot").click(function () {
		newHidden.remove();
		newLabel.remove();
	}).prependTo(newLabel);
	$("div#addSlotForm").dialog("close");
}

$(document).ready(function () {
	$("input#addSlot_date").datepicker();
});