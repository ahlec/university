function alertError(message) {
	var alertErrorDialog = $(document.createElement("div")),
		alertErrorIcon = $(document.createElement("span"));
	
	$(document.createElement("span")).addClass("alertErrorIcon").appendTo(alertErrorDialog);
	$(document.createElement("span")).text(message).appendTo(alertErrorDialog);
	alertErrorDialog.appendTo(document.body);
	
	alertErrorDialog.dialog({
		modal: true,
		draggable: false,
		title: "Error",
		close: function () {
			alertErrorDialog.remove();
		}
	});
}

function alertSuccess(message) {
	var alertSuccessDialog = $(document.createElement("div")),
		alertSuccessIcon = $(document.createElement("span"));
	
	$(document.createElement("span")).addClass("alertSuccessIcon").appendTo(alertSuccessDialog);
	$(document.createElement("span")).text(message).appendTo(alertSuccessDialog);
	alertSuccessDialog.appendTo(document.body);
	
	alertSuccessDialog.dialog({
		modal: true,
		draggable: false,
		title: "Success",
		close: function () {
			alertSuccessDialog.remove();
		}
	});
}

function isValidEmail(email) {
	if (email.indexOf("%") >= 0) {
		return false;
	}
	
	return /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b/.test(email);
}