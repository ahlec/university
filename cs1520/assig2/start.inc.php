<!DOCTYPE html>
<html>
  <head>
    <title>Assignment 2 - Alec Jacob Deitloff</title>
    <link rel="stylesheet" href="./style/style.css" />
    <link rel="stylesheet" href="./style/jquery-ui.css" />
    <script type="text/javascript" src="./script/jquery.js"></script>
    <script type="text/javascript" src="./script/jquery-ui.js"></script>
    <script type="text/javascript" src="./script/global.js"></script>
<?php
		$pathinfoPhpSelf = pathinfo($_SERVER["PHP_SELF"]);
		if (isset($pathinfoPhpSelf["filename"]))
		{
			$baseRequestedFilename = $pathinfoPhpSelf["filename"];
		}
		else
		{
			$baseRequestedFilename = mb_substr($pathinfoPhpSelf["basename"], 0,
											   mb_strlen($pathinfoPhpSelf["basename"]) - 1 - mb_strlen($pathinfoPhpSelf["extension"]));
		}
		
		if (file_exists(__DIR__ . "/script/" . $baseRequestedFilename . ".js"))
		{
			echo "    <script type=\"text/javascript\" src=\"./script/" . $baseRequestedFilename . ".js\"></script>\n";
		}
?>
  </head>
  <body>
    <div class="navbar">
<?php
		if (class_exists("Auth") && Auth::isLoggedIn())
		{
			echo "      <a href=\"index.php\">Home</a>\n";
			echo "      <a href=\"createSchedule.php\">New schedule</a>\n";
			echo "      <a href=\"changePassword.php\">Change password</a>\n";
			echo "      <a href=\"logout.php\">Logout</a>\n";
		}
		else
		{
			echo "      <a href=\"login.php\">Login</a>\n";
		}
	?>    </div>
<?php
	$meta = getMeta();
	if ($meta != null)
	{
		echo "    <div class=\"" . $meta["type"] . "\">" . $meta["message"] . "</div>\n";
	}
?>
