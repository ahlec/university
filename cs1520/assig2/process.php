<?php
// Setup
require_once ("config.inc.php");
$database = openDatabase();

// Only allow if we are the result of a form submission
if (!isset($_POST["process"]) || mb_strtolower($_POST["process"]) != "edit")
{
  pageError("You may only access this page through the form.");
}

// Process scheduleHash
if (!isset($_POST["scheduleHash"]))
{
  pageError("You may only access this page through the form.");
}
$scheduleHash = $database->escape_string($_POST["scheduleHash"]);
$scheduleInfo = $database->query("SELECT scheduleId FROM assig2_schedules WHERE scheduleHash='" .
				$scheduleHash . "' AND isFinalized='0' LIMIT 1");
if ($scheduleInfo == null || $scheduleInfo->num_rows !== 1)
{
  pageError("The specified schedule does not appear to exist anymore, or has been finalized.");
}
$scheduleId = current($scheduleInfo->fetch_array());

// Process userHash
if (!isset($_POST["userHash"]))
{
  pageError("You may only access this page through the form.");
}
$userHash = $database->escape_string($_POST["userHash"]);
$userInfo = $database->query("SELECT u.userId FROM assig2_users u JOIN assig2_scheduleUsers su ON " .
							 "su.userId = u.userId WHERE su.scheduleId='" . $scheduleId .
							 "' AND u.userHash='" . $userHash . "' LIMIT 1");
if ($userInfo == null || $userInfo->num_rows !== 1)
{
  pageError("The specified user does not appear to exist, or is not involved within this schedule.");
}
$userId = current($userInfo->fetch_array());

// Process slotIds
$slotIds = array();
if (isset($_POST["slotIds"]) && is_array($_POST["slotIds"]))
{
	foreach ($_POST["slotIds"] as $slotId)
	{
		if (ctype_digit($slotId))
		{
		  $slotIds[] = $slotId;
		}
	}
}
if (count($slotIds) > 0)
{
	$scheduleVerificationResults = $database->query("SELECT slotId FROM assig2_scheduleSlots " .
													"WHERE scheduleId='" . $scheduleId . "' AND slotId IN (" .
													implode(",", $slotIds) . ")");
	if ($scheduleVerificationResults == null)
	{
	  pageError("An error was encountered while verifying data (Error Code: 18).");
	}
	if (count($slotIds) != $scheduleVerificationResults->num_rows)
	{
		$slotIds = array();
		while ($slotId = $slotIds->fetch_array())
		{
			$slotIds[] = $slotId[0];
		}
		print_r("TRICKY");
	}
}

// Remove extraneous signup rows
if (!$database->query("DELETE FROM assig2_signups WHERE userId='" . $userId . "' " . (count($slotIds) > 0 ?
					  "AND slotId NOT IN (" . implode(",", $slotIds) . ") " : "") .
					  "AND scheduleId='" . $scheduleId . "'"))
{
  pageError("An error was encountered while processing your changes (Error Code: 17).");
}

// Get entries that the user still has within the database
$remainingSlotResults = $database->query("SELECT slotId FROM assig2_signups WHERE userId='" .
										 $userId . "' AND scheduleId='" . $scheduleId . "'");
if ($remainingSlotResults == null)
{
  pageError("An error was encountered while processing your changes (Error Code: 19).");
}
$remainingSlots = array();
while ($slotId = $remainingSlotResults->fetch_array())
{
  $remainingSlots[] = $slotId[0];
}

// Add entries to the database that aren't already there.
foreach (array_diff($slotIds, $remainingSlots) as $slotId)
{
	if (!$database->query("INSERT INTO assig2_signups(userId, slotId, scheduleId) VALUES('" .
						  $userId . "','" . $slotId . "','" . $scheduleId . "')"))
	{
		pageError("An error was encountered while processing your changes (Error Code: 20).");
	}
}

pageSuccess("Your changes have been saved.", "schedule.php?schedule=" . $scheduleHash . "&user=" . $userHash);
?>
