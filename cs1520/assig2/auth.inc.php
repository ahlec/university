<?php
if (!defined("ASSIG2_JBD19_CONFIG_INC_PHP"))
{
	exit("Cannot load this file directly!");
}

// AUTH 
class Auth
{
	private static $sessionName = "jbd19assig2authSessName";

	public static function isLoggedIn()
	{
		if (!isset($_SESSION[self::$sessionName]))
		{
			return false;
		}
		return true;
	}
	
	public static function login($email, $password)
	{
		self::logout();
		
		if (mb_strpos($email, "%") !== false)
		{
			return false;
		}
		$hashedPassword = md5($password);
		$database = openDatabase();
	
		$results = $database->query("SELECT a.accountId, u.userHash FROM assig2_accounts a JOIN assig2_users u " .
									"ON a.accountId = u.accountId WHERE a.email like '" . $database->escape_string($email) .
									"' AND a.password='" . $hashedPassword . "' LIMIT 1");
		if ($results === false || $results->num_rows != 1)
		{
			return false;
		}
		
		$_SESSION[self::$sessionName] = $results->fetch_assoc();
		return true;
	}
	
	public static function logout()
	{
		if (self::isLoggedIn())
		{
			unset($_SESSION[self::$sessionName]);
		}
	}
	
	public static function getCurrentAccountNo()
	{
		if (isset($_SESSION[self::$sessionName]))
		{
			return $_SESSION[self::$sessionName]["accountId"];
		}
		
		return -1;
	}
	
	public static function getCurrentUserHash()
	{
		if (isset($_SESSION[self::$sessionName]))
		{
			return $_SESSION[self::$sessionName]["userHash"];
		}
		
		return null;
	}
	
	public static function getCurrentAcountInfo()
	{
		if (isset($_SESSION[self::$sessionName]))
		{
			return self::getAccountInfo($_SESSION[self::$sessionName]["accountId"]);
		}
		
		return null;
	}
	
	public static function getAccountInfo($accountId)
	{
		$database = openDatabase();
		$accountInfo = $database->query("SELECT accountId, firstName, lastName, email FROM assig2_accounts " .
										"WHERE accountId='" . $database->escape_string($accountId) . "' LIMIT 1");
		$database->close();
		
		if ($accountInfo == null || $accountInfo->num_rows != 1)
		{
			return null;
		}
		
		return $accountInfo->fetch_array();
	}
}
?>