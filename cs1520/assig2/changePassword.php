<?php
// Setup
require_once ("config.inc.php");

// Ensure we have the ability to be here
if (!Auth::isLoggedIn())
{
	header("Location: login.php");
	exit();
}

// Begin
require_once ("start.inc.php");
?>
    <form method="POST" action="./change.php">
		<label for="oldPassword">Old password:</label>
		<input type="password" name="oldPassword" id="oldPassword" /><br /><br />
		
		<label for="newPassword">New password:</label>
		<input type="password" name="newPassword" id="newPassword" /><br />
		<label for="confirmPassword">Confirm new password:</label>
		<input type="password" name="confirmPassword" id="confirmPassword" /><br /><br />
		
		<input type="submit" value="Change password" />
    </form>
<?php
//End
require_once ("end.inc.php");
?>