<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();

// Check to make sure we aren't logged in
if (Auth::isLoggedIn())
{
	exit(json_encode(array("success" => false, "message" => "A password can only be retrieved while you are not logged in.")));
}

// Check the parameters
if (!isset($_GET["email"]) || mb_strlen($_GET["email"]) == 0 || !filter_var($_GET["email"], FILTER_VALIDATE_EMAIL) ||
	mb_strpos($_GET["email"], "%") !== false)
{
	exit(json_encode(array("success" => false, "message" => "The email was either not provided, or was invalid.")));
}
$email = $database->escape_string($_GET["email"]);

// Get the database information for this email
$accountResults = $database->query("SELECT accountId, firstName, lastName, email, password FROM assig2_accounts WHERE email like '" .
									$email . "' LIMIT 1");
if ($accountResults == null || $accountResults->num_rows != 1)
{
	exit(json_encode(array("success" => false, "message" => "There is no account which makes use of the provided email.")));
}
$account = $accountResults->fetch_assoc();

// Create a new temporary password for the account
$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
$tempPassword = "";
foreach (array_rand(str_split($alphabet), 7) as $index)
{
	$tempPassword .= $alphabet[$index];
}

// Set the new temporary password in the database
if (!$database->query("UPDATE assig2_accounts SET password='" . md5($tempPassword) .
	"' WHERE accountId='" . $account["accountId"] . "'"))
{
	exit(json_encode(array("success" => false, "message" => "There was an error encountered with updating the password (Error Code: 40).")));
}

// Send the email to the account holder
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'To: ' . $account["firstName"] . " " . $account["lastName"] . ' <' . $account["email"] . ">\r\n";
if (!mail($account["email"], "Your password has been reset.",
	"Hello again, " . $account["firstName"] . ". Your password has been reset (hopefully by you). For the time being, your " .
	"login information is as follows:<br /><br /><strong>Email:</strong> " . $account["email"] . "<br /><strong>Password:</strong> " .
	$tempPassword . "<br /><br/>Please log in by visiting <a href=\"http://cs1520.cs.pitt.edu/~jbd19/assig2/login.php\" target=\"" .
	"_blank\">http://cs1520.cs.pitt.edu/~jbd19/assig2/login.php</a>. Once you are logged in, you will be free to change your " .
	"password to something more memorable!<br /><br />Happy scheduling!", $headers))
{
	$database->query("UPDATE assig2_accounts SET password='" . $account["password"] . "' WHERE accountId='" .
		$account["accountId"] . "'");
	exit(json_encode(array("success" => false, "message" => "There was an error encountered with updating the password (Error Code: 41).")));
}

exit(json_encode(array("success" => true, "message" => "The password for this email has been reset. Please check your inbox shortly.")));
?>