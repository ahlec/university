<?php
// Setup
require_once ("../config.inc.php");
$database = openDatabase();

// Check the parameters
if (isset($_GET["accountId"]))
{
	$accountInfo = Auth::getAccountInfo($_GET["accountId"]);
	if ($accountInfo == null)
	{
		exit(json_encode(array("success" => false, "message" => "AccountId provided does not represent a valid account.")));
	}
	
	$firstName = $accountInfo["firstName"];
	$lastName = $accountInfo["lastName"];
	$email = $accountInfo["email"];
	$accountId = $accountInfo["accountId"];
}
else
{
	if (!isset($_GET["firstName"]) || mb_strlen($_GET["firstName"]) == 0 || mb_strpos($_GET["firstName"], "%") !== false)
	{
		exit(json_encode(array("success" => false, "message" => "The first name was either not provided, or was invalid.")));
	}
	$firstName = $database->escape_string($_GET["firstName"]);
	
	if (!isset($_GET["lastName"]) || mb_strlen($_GET["lastName"]) == 0 || mb_strpos($_GET["lastName"], "%") !== false)
	{
		exit(json_encode(array("success" => false, "message" => "The last name was either not provided, or was invalid.")));
	}
	$lastName = $database->escape_string($_GET["lastName"]);
	
	if (!isset($_GET["email"]) || mb_strlen($_GET["email"]) == 0 || mb_strpos($_GET["email"], "%") !== false)// || !filter_var($_GET["email"], FILTER_VALIDATE_EMAIL))
	{
		exit(json_encode(array("success" => false, "message" => "The email was either not provided, or was invalid.")));
	}
	$email = $database->escape_string($_GET["email"]);
	
	$accountId = null;
}

// Check if the user already exists, and if so, then return their previous userId
$existsResults = $database->query("SELECT userId FROM assig2_users WHERE firstName like '" .
								  $database->escape_string($firstName) . "' AND lastName like '" .
								  $database->escape_string($lastName) . "' AND email like '" .
								  $database->escape_string($email) . "'");
if ($existsResults == null)
{
	exit(json_encode(array("success" => false, "message" => "There was a problem encountered when querying the database.")));
}
if ($existsResults->num_rows == 1)
{
	$existRow = $existsResults->fetch_array();
	exit(json_encode(array("success" => true, "userId" => intval($existRow["userId"]))));
}

// Find an unused hash for this user
$try = 0;
do
{
	$try++;
	if ($try == 30)
	{
		exit("LOOPED");
	}
	$userHash = md5(rand());
} while ($database->query("SELECT * FROM assig2_users WHERE userHash='" .
										 $userHash . "' LIMIT 1")->num_rows > 0);
	
// Create the user, since they don't exist.
if (!$database->query("INSERT INTO assig2_users(accountId, userHash, firstName, lastName, email) VALUES(" .
	($accountId == null ? "NULL" : "'" . $accountId . "'") . ",'" . $userHash . "','" .
	$database->escape_string($firstName) . "','" . $database->escape_string($lastName) . "','" .
	$database->escape_string($email) . "')"))
{
	exit(json_encode(array("success" => false, "message" => "Unable to create the new user within the database.")));
}

exit(json_encode(array("success" => true, "userId" => $database->insert_id)));
?>