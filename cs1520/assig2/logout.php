<?php
require_once ("config.inc.php");

if (!Auth::isLoggedIn())
{
	header("Location: index.php");
	exit();
}

Auth::logout();

if (Auth::isLoggedIn())
{
	pageError("You were unable to log out due to technical difficulties. Please try again.");
}

pageSuccess("You have successfully logged out. Thank you!");
?>