<?php
require_once ("config.inc.php");
$database = openDatabase();

// Check to make sure the user is logged in
if (!Auth::isLoggedIn())
{
	pageError("In order to perform this action, you must be logged in.");
}

// Check old password
if (!isset($_POST["oldPassword"]))
{
	pageError("Attempted to circumvent the form. Please do not do this (Error Code: 50).", "changePassword.php");
}
$accountResults = $database->query("SELECT accountId FROM assig2_accounts WHERE password='" . md5($_POST["oldPassword"]) .
	"' AND accountId='" . Auth::getCurrentAccountNo() . "'");
if ($accountResults == null || $accountResults->num_rows != 1)
{
	pageError("The current password provided does not match with what is stored as your current password.", "changePassword.php");
}
$account = $accountResults->fetch_assoc();

// New password
if (!isset($_POST["newPassword"]) || !isset($_POST["confirmPassword"]))
{
	pageError("Attempted to circumvent the form. Please do not do this (Error Code: 51).", "changePassword.php");
}
if ($_POST["newPassword"] != $_POST["confirmPassword"])
{
	pageError("The new password does not match your confirmation of the new password.", "changePassword.php");
}
if (mb_strlen($_POST["newPassword"]) < MIN_PASSWORD_LENGTH)
{
	pageError("The new password is not long enough (minimum length for password is " . MIN_PASSWORD_LENGTH . ").", "changePassword.php");
}

// Update the new password
if (!$database->query("UPDATE assig2_accounts SET password='" . md5($_POST["newPassword"]) .
	"' WHERE accountId='" . $account["accountId"] . "'"))
{
	pageError("An error was encountered when changing your password. Please try again (Error Code: 52).", "changePassword.php");
}

pageSuccess("Your password has been successfully changed.", "index.php");
?>