<?php
if (preg_match("/(.*)\.inc\.php$/i", $_SERVER["SCRIPT_FILENAME"]))
{
  exit ("This file may only be included within PHP; direct access is forbidden.");
}

// CONFIG PARAMETERS
define ("MIN_PASSWORD_LENGTH", 8);
define ("SERVICE_EMAIL_ACCOUNT", "jbd19@pitt.edu");

// -------------------------
//   DO NOT MODIFY BELOW THIS LINE
// -------------------------
define ("DATE_FORMAT", "l<b\\r />j/m/y<b\\r />h:i a");
define ("CHECK_SYMBOL", "&#x2713;");
define ("YEAR_IN_MILLISECONDS", 365 * 24 * 60 * 60);
date_default_timezone_set("America/New_York");

// Defining multibyte functions if they don't exist
if (!function_exists("mb_strpos"))
{
  function mb_strpos($haystack, $needle, $offset = 0)
  {
    return strpos($haystack, $needle, $offset);
  }
}

if (!function_exists("mb_substr"))
{
  function mb_substr($str, $start, $length = 0)
  {
    if (func_num_args() <= 2)
    {
      return substr($str, $start);
    }
    else
    {
      return substr($str, $start, $length);
    }
  }
}

if (!function_exists("mb_strtolower"))
{
  function mb_strtolower($str)
  {
    return strtolower($str);
  }
}

if (!function_exists("mb_strlen"))
{
  function mb_strlen($str)
  {
    return strlen($str);
  }
}

// Sessions
if (!isset($_SESSION))
{
	@session_start();
}

// Meta utility functions
define ("META_SESSION_NAME", "assig2_jbd19_metaPassing");
define ("META_DESTINATION_SESSION_NAME", "assig2_jbd19_metaDestination");
function pageError($message = "Unspecified error has occurred.", $destination = "index.php")
{
	$_SESSION[META_SESSION_NAME] = array("message" => $message, "type" => "error");
	header("Location: " . $destination);
	exit();
}

function pageSuccess($message = "Your operation has been successful.", $destination = "index.php")
{
	$_SESSION[META_SESSION_NAME] = array("message" => $message, "type" => "success");
	header("Location: " . $destination);
	exit();
}

function getMeta()
{
	$meta = null;
	if (isset($_SESSION[META_SESSION_NAME]))
	{
		$meta = $_SESSION[META_SESSION_NAME];
		unset($_SESSION[META_SESSION_NAME]);
	}
	return $meta;
}

// Include component libraries
if (!defined("ASSIG2_JBD19_CONFIG_INC_PHP"))
{
	define("ASSIG2_JBD19_CONFIG_INC_PHP", "Alec Jacob Deitloff");
}
require_once ("database.inc.php");
require_once ("auth.inc.php");
require_once ("schedule.inc.php");
?>
