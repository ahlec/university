<?php
// Setup
require_once ("config.inc.php");

// Ensure we have the ability to be here
if (!Auth::isLoggedIn())
{
	header("Location: login.php");
	exit();
}

// Begin
require_once ("start.inc.php");
?>
    <form method="POST" action="./create.php" id="scheduleForm" onsubmit="return validateCreateForm(this);">
		<label for="title">Title:</label> <input type="text" name="title" id="title" /><br />
		
		<fieldset id="usersFieldset">
			<legend>Users</legend>
			<input type="hidden" id="user_<?php echo Auth::getCurrentAccountNo(); ?>" name="users[]" value="<?php echo Auth::getCurrentAccountNo(); ?>" />
			<div><?php
				$userAccountInfo = Auth::getCurrentAcountInfo();
				echo $userAccountInfo["firstName"] . " " . $userAccountInfo["lastName"] . " (" .
					$userAccountInfo["email"] . ") <b>[YOU]</b>";
			?></div>
		</fieldset>
		<input type="button" onclick="addUser();" value="Add user" />
		
		<fieldset id="slotsFieldset">
			<legend>Time slots</legend>
		</fieldset>
		<input type="button" onclick="addSlot();" value ="Add time slot" />
		<input type="submit" id="create" name="create" value="CREATE" />
    </form>
	
    <div id="addUserForm" style="display:none;">
       <label for="addUser_email">Email:</label> <input type="email" id="addUser_email" /><br />
       <label for="addUser_firstName">First name:</label> <input type="text" id="addUser_firstName" /><br />
       <label for="addUser_lastName">Last name:</label> <input type="text" id="addUser_lastName" /><br />
       <input type="button" onclick="addUser_submit();" value="Add" />
    </div>
	
	<div id="addSlotForm" style="display:none;">
		<label for="addSlot_date">Date:</label> <input type="text" id="addSlot_date" /><br />
		<label for="addSlot_time">Time:</label> <input type="time" id="addSlot_time" /><br />
		<input type="button" onclick="addSlot_submit();" value="Add" />
	</div>
<?php
//End
require_once ("end.inc.php");
?>