<?php
// Setup
require_once ("config.inc.php");

// Ensure we have the ability to be here
if (!isset($_GET["schedule"]))
{
	pageError("No schedule was specified.");
}

$database = openDatabase();
$schedule = Schedule::get($database, $_GET["schedule"]);
if ($schedule == null)
{
	pageError("The specified schedule does not exist, or could not be loaded.");
}

// Prepare common data
$userHash = null;
$userFirstName = null;
if (isset($_GET["user"]))
{
	foreach ($schedule->getUsers() as $user)
	{
		if ($user["userHash"] == $_GET["user"])
		{
			$userHash = $user["userHash"];
			$userFirstName = $user["firstName"];
			break;
		}
	}
}
$slots = $schedule->getSlots();
$numSlots = count($slots);

// Begin
require_once ("start.inc.php");
?>
    <table class="schedule">
      <tr>
        <td class="scheduleHeader" colspan="<?php echo ($numSlots + 2); ?>"><?php
		echo $schedule->getTitle();
		echo "<span class=\"creator\">";
		echo $schedule->getCreatorName();
		echo "</span>\n";
		
		// If this is the current authenticated user's own schedule, provide them with management controls
		if (Auth::getCurrentAccountNo() == $schedule->getCreatorAccountId())
		{
			// Finalize button
			echo "    <div class=\"finalizeButton " . ($schedule->isFinalized() ? "finalized" : ($schedule->canFinalize() ?
					"active" : "disabled")) . "\"" . ($schedule->canFinalize() ? " onclick=\"finalizeSchedule('" . 
					$schedule->getHash() . "');\"" : "") . " title=\"" . ($schedule->isFinalized() ? "Finalized" : ($schedule->canFinalize() ?
					"Click here to finalize the schedule" : "A schedule cannot be finalized until at least one invitee has reported information.")) .
					"\"></div>\n";
					
			if ($schedule->canFinalize())
			{
				echo "    <div id=\"finalizeDialog\" style=\"display:none;\">Are you sure you wish to <b>finalize this schedule</b>? Doing so will " .
					 "prevent the schedule from being further modified, and will <b>email all participants</b> with the optimal date and time.</div>\n";
			}
		}
	?></td>
      </tr>
      <tr>
        <th>User</th>
        <th>Action</th>
        <?php
		  foreach ($slots as $slot)
		  {
		    echo "        <th" . ($slot["isSelectedSlot"] ? " class=\"selected\"" : "") . ">",
				date(DATE_FORMAT, $slot["startTime"]), "</th>\n";
          }
        ?>
      </tr>
      <?php
	foreach ($schedule->getUsers() as $user)
	{
          echo "      <tr class=\"user", ($user["accountId"] == $schedule->getCreatorAccountId() ? " creator" : ""), "\">\n";
          echo "        <td class=\"name\">", $user["firstName"], " ", $user["lastName"];
		  if ($user["userHash"] == $userHash && !$schedule->isFinalized())
		  {
			echo "<input type=\"hidden\" name=\"userHash\" value=\"",
               $user["userHash"], "\" />";
		  }
		  echo "</td>\n";
          echo "        <td class=\"action\">";
          if ($user["userHash"] == $userHash && !$schedule->isFinalized())
          {
            echo "<input type=\"button\" class=\"action\" disabled=\"disabled\" data-defaulttext=\"Edit\" value=\"Edit\" />";
          }
          echo "</td>\n";
	   foreach ($slots as $slot)
	   {
            echo "        <td" . ($slot["isSelectedSlot"] ? " class=\"selected\"" : "") . ">\n";
            if (in_array($slot["slotId"], $user["slots"]))
            {
              echo "          <span class=\"check\">", CHECK_SYMBOL, "</span>\n";
            }
            echo "          <input type=\"checkbox\" name=\"slotIds[]\" value=\"", $slot["slotId"], "\" data-isdefaultchecked=\"",
                 (in_array($slot["slotId"], $user["slots"]) ? "true\" checked=\"checked\"" : "false\"") . " />\n"; 
            echo "</td>\n";
          }
          echo "      </tr>\n";
        }
      ?>
      <tr class="totals">
        <td colspan="2">TOTAL:</td>
        <?php
          foreach ($slots as $slot)
          {
            echo "        <td" . ($slot["isSelectedSlot"] ? " class=\"selected\"" : "") . ">", $slot["total"], "</td>\n";
          }
        ?>
      </tr>
    </table>
    <form action="./process.php" id="submitForm" method="POST" data-schedulehash="<?php echo $schedule->getHash(); ?>">
    </form>
	<hr />
<?php
if ($userHash == null)
{
	echo "<h3>Greetings, stranger!</h3>\n";
	echo "You must be new around these parts. This here schedule was organized to help plan an upcoming event or meeting, " .
		"and make sure that everybody can attend!<br /><br />Participating is easy: <b>if you received an email</b>, the " .
		"link provided to you will allow you to select which times you're available &mdash; no nonsense with accounts or " .
		"passwords. If you didn't receive an email, that means that unfortunately, you won't be able to participate in this " .
		"schedule, though you're more than welcome to view it all you want.<br /><br />If you have used the web link that was " .
		"given to you from your email, but you are still seeing this message, then that means we have a problem! Try " . 
		"contacting <a href=\"mailto:" . SERVICE_EMAIL_ACCOUNT . "\">" . SERVICE_EMAIL_ACCOUNT . "</a> to clear up the problem!"; 
}
else
{
	echo "<h3>Howdy, " . $userFirstName . "!</h3>\n";
	echo "You've found the link from your email, so now it's time to get to work! And that's actually really easy to do! " .
		"As you've already seen, you didn't have to bother with usernames and passwords. That link is uniquely your own, " .
		"and it'll be right hard to guess, so there's no need to worry!<br /><br />";
		
	if ($schedule->isFinalized())
	{
		echo "However, you see that purple column up on that table? That means that this schedule has already been finalized! " .
			"The purple column is the date and time that was determined to be the best fit for the most participants. Because " .
			"the date and time was already chosen, we've disabled modifying the chart any further; if you have a problem and " .
			"can't make the meeting, you'll want to contact " . $schedule->getCreatorName() . " at <a href=\"mailto:" .
			$schedule->getCreatorEmail() . "\">" . $schedule->getCreatorEmail() . "</a>. Hopefully you two will be able to " .
			"sort that all out!";
	}
	else
	{
		echo "Do you see your name up on the chart above? You do? Good. See how there is a button next to it that says 'Edit'? " .
			"Click that. You'll notice that suddenly, the whole rest of the row is filled with checkboxes! All you have to do " .
			"now is just tick the checkboxes for each time slot that you're available and willing to meet! As you change your " .
			"checkboxes, the 'Edit' button magically turns into the 'Submit' button! Clicking that will save your information! " .
			"However, feel you've made a mistake? Press the 'ESC' button on your keyboard to cancel any unsaved changes you've " .
			"made!";
	}
}

// End
require_once ("end.inc.php");
?>