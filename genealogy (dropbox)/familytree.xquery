let $levelHigh := 2
let $levelLow := 2
let $boleslaw := doc('/db/genealogy/genealogy.xml')//person[@id eq 'boleslaw3']
let $familyTree := <familyTree><level0></level0>{for $descending in (0 to $levelLow) return <level-d-{$descending}></level-d-{$descending}>}</familyTree>
return $familyTree