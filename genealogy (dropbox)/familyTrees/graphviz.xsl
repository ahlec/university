<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="text" />
    <xsl:template match="/">
        <xsl:variable name="family" select="."/>
        digraph familyTree
        {
            center = true;
            <xsl:for-each-group group-by="@absoluteGeneration" select="//entry">
                <xsl:sort order="descending" select="current-grouping-key()" />
                subgraph Generation<xsl:value-of select="current-grouping-key()"/>
                {
                    rank = same;
                    <xsl:for-each select="current-group()">
                        <xsl:variable name="nodeColor" select="if(@gender = 'male') then('blue') else(if (@gender = 'female') then('pink') else('green'))"/>
                        <xsl:value-of select="translate(@handle, '-', '')"/> [shape = box, URL = "findPerson.php?person=<xsl:value-of select="@handle"/>", label = "<xsl:value-of select="@name"/>", color = <xsl:value-of select="$nodeColor"/>];
                    </xsl:for-each>
                
                <xsl:for-each select="current-group()[@gender = 'male' and count(relation[@type = 'spouse']) > 0]">
                        <xsl:variable name="male" select="@handle"/>
                        <xsl:variable name="numberMarriages" select="count(relation[@type = 'spouse'])"/>
                        <xsl:for-each select="current()//relation[@type = 'spouse']">
                            <xsl:variable name="female" select="@to"/>
                            <xsl:variable name="haveChildren" select="count($family//relation[@type = 'child' and @to = $male and @with = $female]) > 0"/>
                            <xsl:choose>
                                <xsl:when test="$haveChildren">
                                    <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/>AND<xsl:value-of select="translate(@to, '-', '')"/> [shape = point, width = 0, height = 0];
                                    <xsl:choose>
                                        <xsl:when test="not($numberMarriages = 1) and position() > $numberMarriages div 2">
                                            <xsl:value-of select="translate(@to, '-', '')"/> -> <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/>AND<xsl:value-of select="translate(@to, '-', '')"/> [dir = none, style=dashed];
                                            <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/>AND<xsl:value-of select="translate(@to, '-', '')"/> ->  <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/> [dir = none, style=dashed];
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/> -> <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/>AND<xsl:value-of select="translate(@to, '-', '')"/> [dir = none, style=dashed];
                                            <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/>AND<xsl:value-of select="translate(@to, '-', '')"/> -> <xsl:value-of select="translate(@to, '-', '')"/> [dir = none, style=dashed];
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="position() > $numberMarriages div 2">
                                            <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/> -> <xsl:value-of select="translate(@to, '-', '')"/> [dir = none, style=dashed];
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="translate(@to, '-', '')"/> -> <xsl:value-of select="translate(ancestor-or-self::entry/@handle, '-', '')"/> [dir = none, style=dashed];
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </xsl:for-each>
                }
                <xsl:for-each select="current-group()[count(relation[@type = 'child']) > 0]">
                    <xsl:variable name="marriageHandle" select="(relation[@type = 'child'])[1]/@marriageId"/>
                    <xsl:choose>
                        <xsl:when test="count($family//entry[relation[@type = 'child' and @marriageId = $marriageHandle]]) > 1">
                            <xsl:value-of select="translate(@handle, '-', '')"/>Son -> <xsl:value-of select="translate(@handle, '-', '')"/> [dir = none];
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="translate((relation[@type = 'child'])[1]/@to, '-', '')"/>AND<xsl:value-of select="translate((relation[@type = 'child'])[1]/@with, '-', '')"/> -> <xsl:value-of select="translate(@handle, '-', '')"/> [dir = none];
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:variable name="generationHandles" select="current-group()/@handle"/>
                <xsl:variable name="absoluteGeneration" select="current-grouping-key()"/>
                <xsl:variable name="currentGenMarriageHandlesForMarriagesWithChildren"
                    select="distinct-values($family//relation[@type = 'child' and (@to = $generationHandles or @width = $generationHandles)]/@marriageId)"/>
                <xsl:if test="count($family//relation[@type = 'child' and @to = $generationHandles]) > 0">
                    subgraph Generation<xsl:value-of select="$absoluteGeneration"/>Sons
                    {
                        rank = same;
                        <xsl:for-each select="$family//entry[count(relation[@type = 'child' and @to = $generationHandles]) > 0]">
                            <xsl:variable name="marriageHandle" select="(relation[@type = 'child'])[1]/@marriageId"/>
                            <xsl:if test="count($family//entry[relation[@type = 'child' and @marriageId = $marriageHandle]]) > 1">
                                <xsl:value-of select="translate(@handle, '-', '')"/>Son [shape = rect, width = 0, height = 0, fixedsize = true, label = ""];
                            </xsl:if>
                        </xsl:for-each>
                    
                        <xsl:for-each select="$currentGenMarriageHandlesForMarriagesWithChildren">
                            <xsl:variable name="marriageId" select="current()"/>
                            <xsl:variable name="marriageChildren"
                                select="$family//entry[count(relation[@type = 'child' and @marriageId = $marriageId]) > 0]"/>
                            <xsl:if test="count($marriageChildren) > 1">
                                <xsl:for-each select="$marriageChildren">
                                    <xsl:variable name="position" select="position()"/>
                                    <xsl:if test="count($marriageChildren) mod 2 = 0">
                                        <xsl:choose>
                                            <xsl:when test="count($marriageChildren) div 2 = $position">
                                                <xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@to, '-', '')"/>AND<xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@with, '-', '')"/>Son [shape = point, width = 0, height = 0];
                                                <xsl:value-of select="translate(@handle, '-', '')"/>Son -> <xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@to, '-', '')"/>AND<xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@with, '-', '')"/>Son [dir = none];
                                                <xsl:value-of select="translate($marriageChildren[$position + 1]/@handle, '-', '')"/>Son -> <xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@to, '-', '')"/>AND<xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@with, '-', '')"/>Son [dir = none];
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:if test="$position &lt; count($marriageChildren)">
                                                    <xsl:value-of select="translate(@handle, '-', '')"/>Son -> <xsl:value-of select="translate($marriageChildren[$position + 1]/@handle, '-', '')"/>Son [dir = none];
                                                </xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                    <xsl:if test="not(count($marriageChildren) mod 2 = 0)">
                                        <xsl:if test="not(position() = ceiling(count($marriageChildren) div 2))">
                                            <xsl:value-of select="translate(@handle, '-', '')"/>Son -> <xsl:value-of select="translate($marriageChildren[ceiling(count($marriageChildren) div 2)]/@handle, '-', '')"/>Son [dir = none];
                                        </xsl:if>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:if>
                        </xsl:for-each>
                    }
                    <xsl:for-each select="$currentGenMarriageHandlesForMarriagesWithChildren">
                        <xsl:variable name="marriageId" select="current()"/>
                        <xsl:variable name="marriageChildren"
                            select="$family//entry[count(relation[@type = 'child' and @marriageId = $marriageId]) > 0]"/>
                        <xsl:if test="count($marriageChildren) > 1">
                            <xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@to, '-', '')"/>AND<xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@with, '-', '')"/> ->
                            <xsl:choose>
                                <xsl:when test="count($marriageChildren) mod 2 = 0">
                                    <xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@to, '-', '')"/>AND<xsl:value-of select="translate(($marriageChildren//relation[@type = 'child' and @marriageId = $marriageId])[1]/@with, '-', '')"/>Son [dir = none];
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="translate($marriageChildren[ceiling(count($marriageChildren) div 2)]/@handle, '-', '')"/>Son [dir = none];
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each-group>
        }
    </xsl:template>
</xsl:stylesheet>