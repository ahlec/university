import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;
import javax.xml.xpath.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class treeSearch
{
  public static XPath xpath;
  public static int HI = 1;
  public static int LO = -1;
  public static class FamilyMember
  {
    String id, origin, firstName, epithet, patronymic, deathDate;
    int relativeGeneration;
    Node xmlNode;
    boolean beenTheCenter = false;
    ArrayList<Relation> relations;
    public FamilyMember(String id, String origin, String firstName, String epithet, String patronymic, String deathDate,
	int relativeGeneration, Node xmlNode)
    {
      this.id = id;
      this.origin = origin;
      this.firstName = firstName;
      this.epithet = (epithet == null ? "" : epithet);
      this.patronymic = (patronymic == null ? "" : patronymic);
      this.deathDate = deathDate;
      this.relativeGeneration = relativeGeneration;
      this.xmlNode = xmlNode;
      this.relations = new ArrayList<Relation>();
    }
    public void addRelation(Relation relation) { this.relations.add(relation); }
    public ArrayList<Relation> getRelations() { return relations; }
    public String getId() { return id; }
    public Node getXMLNode() { return xmlNode; }
    public boolean equals(Object otherObject)
    {
      if (!(otherObject instanceof FamilyMember))
        return false;
      return ((FamilyMember)otherObject).id.equals(id);
    }
    public boolean hasBeenCenter() { return beenTheCenter; }
    public void isCenter() { beenTheCenter = true; }
    public int getRelativeGeneration() { return relativeGeneration; }
    public String toString()
    {
       String stringValue = "[" + relativeGeneration + "] " + firstName + (epithet.length() > 0 ? " '" + epithet + "'" : "") +
	(patronymic.length() > 0 ? " " + patronymic : "") + (!origin.equals("Rus'") && !origin.equals("unknown") ?
	" of " + origin : "") + " (d. " + deathDate + ") {{" + id + "}}";
       return stringValue;
    }
  }
  public static class Relation
  {
    String position, toId;
    public Relation(String position, String toId)
    {
      this.position = position;
      this.toId = toId;
    }
    public String toString()
    {
      return position + " to {{" + toId + "}}";
    }
    public String getToId() { return toId; }
    public String getPosition() { return position; }
    public boolean equals(Object otherObject)
    {
      if (!(otherObject instanceof Relation))
        return false;
      return ((Relation)otherObject).position.equals(position) && ((Relation)otherObject).toId.equals(toId);
    }
  }
  public static class FamilyMemberComparator implements Comparator<FamilyMember>
  {
    @Override
    public int compare(FamilyMember member1, FamilyMember member2)
    {
      if (member1.getRelativeGeneration() == member2.getRelativeGeneration())
        return 0;
      return (member1.getRelativeGeneration() < member2.getRelativeGeneration() ? -1 : 1);
    }
  }

  public static void main(String[] args)
  {
    if (args.length < 1)
    {
      System.out.println(">> Requires the input of one parameter, the valid id of an individual within the " +
	"genealogy project.");
      return;
    }
    String startId = args[0];
    for (int argsIndex = 1; argsIndex < args.length; argsIndex++)
    {
      if (args[argsIndex].startsWith("-lo="))
      {
        LO = Integer.parseInt(args[argsIndex].substring(4));
        if (LO < 0)
        {
          System.err.println("-lo must be a number that is greater than or equal to zero.");
          return;
        }
        LO *= -1;
      } else if (args[argsIndex].startsWith("-hi="))
      {
        HI = Integer.parseInt(args[argsIndex].substring(4));
        if (HI < 0)
        {
          System.err.println("-hi must be a number that is greater than or equal to zero.");
          return;
        }
      }
    }

    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder;
    try { documentBuilder = dbFactory.newDocumentBuilder(); }
	catch (Exception e)
	{
	  System.out.println("1Exception caught");
	  return;
	}
    Document genealogyXML;
    try { genealogyXML = documentBuilder.parse("genealogy.xml"); }
	catch (Exception e)
	{
	  System.out.println("2Exception caught");
	  return;
	}

    XPathFactory xPathfactory = XPathFactory.newInstance();
    xpath = xPathfactory.newXPath();

    ArrayList<FamilyMember> familyTree = new ArrayList<FamilyMember>();
    FamilyMember resultingMember = parseMember(startId, genealogyXML, familyTree, 0);
    if (resultingMember == null)
    {
      System.out.println(">> The id that you input does not correspond to a member ofthe genealogy project.");
      return;
    }
    FamilyMember currentFocus = resultingMember;
    familyTree.add(currentFocus);
    System.out.println("Traversing the family tree");
    System.out.println("--------------------------");
    while (remainingTargetsExist(familyTree))
    {
      for (FamilyMember iterate : familyTree)
        if (!iterate.hasBeenCenter())
        {
          currentFocus = iterate;
          break;
        }
      familyTree = populateBranches(familyTree, currentFocus, genealogyXML);
      System.out.print("*");
    }
    System.out.println("");
    Collections.sort(familyTree, new FamilyMemberComparator());
    try
    {
      System.out.println("Writing family tree to .txt file");
      FileWriter fileStream = new FileWriter("familyTrees/familyTree-" + startId + ".txt");
      PrintWriter writingStream = new PrintWriter(fileStream);
      writingStream.println("Family Tree for: " + resultingMember);
      writingStream.println(HI + " generation" + (HI != 1 ? "s" : "") + " up, " + (LO * -1) + " generation" + (LO != -1 ? "s" : "") + " down");
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMMM yyyy 'at' HH:mm:ss z");
      writingStream.println("Generated: " + dateFormat.format(new Date()));
      writingStream.println("===============================================================");
      writingStream.println("");
      for(FamilyMember familyMember : familyTree)
      {
        writingStream.println ("  " + familyMember);
        for (Relation relation : familyMember.getRelations())
          writingStream.println("       - " + relation);
      }
      writingStream.close();
      System.out.println("Write successful.");
    } catch(Exception e)
    {
      System.err.println("Exception during writing");
    }
    try
    {
      System.out.println("Starting XML generation.");
      Document generatedXML = documentBuilder.newDocument();
      Element rootElement = generatedXML.createElement("familyTree");
      generatedXML.appendChild(rootElement);
      Element definitionElement = generatedXML.createElement("definition");
      definitionElement.appendChild(generatedXML.createTextNode(startId));
      Attr loAttribute = generatedXML.createAttribute("lo");
      loAttribute.setValue(Integer.toString(-1 * LO));
      definitionElement.setAttributeNode(loAttribute);
      Attr hiAttribute = generatedXML.createAttribute("hi");
      hiAttribute.setValue(Integer.toString(HI));
      definitionElement.setAttributeNode(hiAttribute);
      rootElement.appendChild(definitionElement);
      for (FamilyMember familyMember : familyTree)
      {
        Element memberElement = generatedXML.createElement(familyMember.getId().equals(startId) ? "center" : "node");
        Attr generationAttribute = generatedXML.createAttribute("relative-generation");
        generationAttribute.setValue(Integer.toString(familyMember.getRelativeGeneration()));
        memberElement.setAttributeNode(generationAttribute);
        Attr idAttribute = generatedXML.createAttribute("id");
        idAttribute.setValue(familyMember.getId());
        memberElement.setAttributeNode(idAttribute);
        for (Relation relation : familyMember.getRelations())
        {
          Element relationElement = generatedXML.createElement("relation");
          relationElement.appendChild(generatedXML.createTextNode(relation.getToId()));
          Attr positionAttribute = generatedXML.createAttribute("position");
          positionAttribute.setValue(relation.getPosition());
          relationElement.setAttributeNode(positionAttribute);
          memberElement.appendChild(relationElement);
        }
        rootElement.appendChild(memberElement);
      }
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      DOMSource domSource = new DOMSource(generatedXML);
      StreamResult result = new StreamResult(new File("familyTrees/" + startId + ".xml"));
      transformer.transform(domSource, result);
      System.out.println("XML generated successfully.");
    } catch (Exception e)
    {
      System.err.println("Exception during xml generation.");
    }
  }

  public static FamilyMember parseMember(String id, Document xmlDocument, ArrayList<FamilyMember> familyTree, int relativeGeneration)
  {
    for (int index = 0; index < familyTree.size(); index++)
      if (familyTree.get(index).getId().equals(id))
        return null;

    XPathExpression xpathExpression;
    try { xpathExpression = xpath.compile("//person[@id = '" + id + "']"); }
	catch (Exception e)
	{ System.out.println("3Exception caught.");
	  return null; }
    NodeList results;
    try { results = (NodeList)xpathExpression.evaluate(xmlDocument, XPathConstants.NODESET); }
	catch (Exception e)
	{ System.out.println("4Exception caught.");
	  return null; }
    Node resultNode = results.item(0);
    NodeList resultNodeChildren = resultNode.getChildNodes();
    Node nameNode = null;
    Node dateNode = null;
    for (int nodeIndex = 0; nodeIndex < resultNodeChildren.getLength(); nodeIndex++)
      if (resultNodeChildren.item(nodeIndex).getNodeName().equals("name"))
        nameNode = resultNodeChildren.item(nodeIndex);
      else if (resultNodeChildren.item(nodeIndex).getNodeName().equals("dates"))
        dateNode = resultNodeChildren.item(nodeIndex);
    String firstName = "";
    String epithet = "";
    String patronymic = "";
    NodeList nameElements = nameNode.getChildNodes();
    for (int nameIndex = 0; nameIndex < nameElements.getLength(); nameIndex++)
    {
      if (nameElements.item(nameIndex).getNodeName().equals("firstName"))
        firstName = nameElements.item(nameIndex).getChildNodes().item(0).getNodeValue();
      else if (nameElements.item(nameIndex).getNodeName().equals("epithet"))
        epithet = nameElements.item(nameIndex).getChildNodes().item(0).getNodeValue();
      else if (nameElements.item(nameIndex).getNodeName().equals("patronymic"))
        patronymic = nameElements.item(nameIndex).getChildNodes().item(0).getNodeValue();
    }
    NamedNodeMap resultNodeAttributes = resultNode.getAttributes();
    String origin = resultNodeAttributes.getNamedItem("origin").getNodeValue();
    String deathDate = "";
    NodeList dateElements = dateNode.getChildNodes();
    for (int dateIndex = 0; dateIndex < dateElements.getLength(); dateIndex++)
      if (dateElements.item(dateIndex).getNodeName().equals("death"))
      {
        Node deathNode = dateElements.item(dateIndex);
        NodeList deathElements = deathNode.getChildNodes();
        for (int deathIndex = 0; deathIndex < deathElements.getLength(); deathIndex++)
          if (deathElements.item(deathIndex).getNodeName().equals("deathDate"))
          {
            NodeList deathDateElements = deathElements.item(deathIndex).getChildNodes();
            for (int deathDateIndex = 0; deathDateIndex < deathDateElements.getLength(); deathDateIndex++)
              if (deathDateElements.item(deathDateIndex).getNodeName().equals("date"))
                deathDate = deathDateElements.item(deathDateIndex).getChildNodes().item(0).getNodeValue();
          }
      }
    return new FamilyMember(id, origin, firstName, epithet, patronymic, deathDate, relativeGeneration, resultNode);
  }
  public static Node getNode(String tagName, Node node)
  {
    NodeList children = node.getChildNodes();
    for (int index = 0; index < children.getLength(); index++)
      if (children.item(index).getNodeName().equals(tagName))
        return children.item(index);
    return null;
  }
  public static ArrayList<FamilyMember> populateBranches(ArrayList<FamilyMember> familyTree, FamilyMember startingPoint, Document xmlDocument)
  {
    for (int index = 0; index < familyTree.size(); index++)
      if (familyTree.get(index).equals(startingPoint))
      {
        FamilyMember temp = familyTree.get(index);
        temp.isCenter();
        familyTree.set(index, temp);
      }
    /* Get Parents */
    if (startingPoint.getRelativeGeneration() + 1 <= HI && getNode("parents", startingPoint.getXMLNode()) != null)
    {
      Node parentsNode = getNode("parents", startingPoint.getXMLNode());
      Node fatherNode = getNode("father", parentsNode);
      if (fatherNode != null && ((Element)fatherNode).hasAttribute("pointer"))
      {
        FamilyMember father = parseMember(((Element)fatherNode).getAttribute("pointer"), xmlDocument, familyTree,
          startingPoint.getRelativeGeneration() + 1);
        if (father != null)
        {
          father.addRelation(new Relation("father", startingPoint.getId()));
          familyTree.add(father);
        } else if (containsId(familyTree, ((Element)fatherNode).getAttribute("pointer")))
          familyTree = appendRelation(familyTree, ((Element)fatherNode).getAttribute("pointer"), "father", startingPoint.getId());
      }
      Node motherNode = getNode("mother", parentsNode);
      if (motherNode != null && ((Element)motherNode).hasAttribute("pointer"))
      {
        FamilyMember mother = parseMember(((Element)motherNode).getAttribute("pointer"), xmlDocument, familyTree,
          startingPoint.getRelativeGeneration() + 1);
        if (mother != null)
        {
          mother.addRelation(new Relation("mother", startingPoint.getId()));
          familyTree.add(mother);
        } else if (containsId(familyTree, ((Element)motherNode).getAttribute("pointer")))
          familyTree = appendRelation(familyTree, ((Element)motherNode).getAttribute("pointer"), "mother", startingPoint.getId());
      }
    }
    /* Get Spouses/Children */
    NodeList marriages = executeXPath("//marriage[@male='" + startingPoint.getId() + "' or @female='" + startingPoint.getId() + "']", xmlDocument);
    if (marriages.getLength() > 0)
    {
      for (int index = 0; index < marriages.getLength(); index++)
      {
        Node marriage = marriages.item(index);
        String maleId = ((Element)marriage).getAttribute("male");
        String femaleId = ((Element)marriage).getAttribute("female");
        if (maleId.equals(startingPoint.getId())) // we're about to add his wife
        {
          FamilyMember spouse = parseMember(femaleId, xmlDocument, familyTree, startingPoint.getRelativeGeneration());
          if (spouse != null)
          {
            spouse.addRelation(new Relation(((Element)marriage).getAttribute("status"), startingPoint.getId()));
            familyTree.add(spouse);
          } else if (containsId(familyTree, femaleId))
            familyTree = appendRelation(familyTree, femaleId, ((Element)marriage).getAttribute("status"), startingPoint.getId());
        } else // we're about to add her husband
        {
          FamilyMember spouse = parseMember(maleId, xmlDocument, familyTree, startingPoint.getRelativeGeneration());
          String relationValue = ((Element)marriage).getAttribute("status");
          String overridePreposition = null;
          if (relationValue.equals("wife"))
            relationValue = "husband";
          else if (relationValue.equals("unknown"))
            relationValue = "unknown relation";
          else if (relationValue.equals("concubine"))
            relationValue = "mate (other in the concubinage relationship)";
          if (spouse != null)
          {
            spouse.addRelation(new Relation(relationValue, startingPoint.getId()));
            familyTree.add(spouse);
          } else if (containsId(familyTree, maleId))
            familyTree = appendRelation(familyTree, maleId, relationValue, startingPoint.getId());
        }
        /* Get Children */
        if (startingPoint.getRelativeGeneration() - 1 >= LO)
        {
          Node childNode = getNode("children", marriage);
          if (childNode != null)
          {
            NodeList children = childNode.getChildNodes();
            for (int childIndex = 0; childIndex < children.getLength(); childIndex++)
            {
              if (children.item(childIndex).getNodeName().equals("child"))
              {
                String childId = ((Element)children.item(childIndex)).getAttribute("pointer");
                FamilyMember child = parseMember(childId, xmlDocument, familyTree, startingPoint.getRelativeGeneration() - 1);
                if (child != null)
                {
                  child.addRelation(new Relation("child", maleId));
                  child.addRelation(new Relation("child", femaleId));
                  familyTree.add(child);
                } else if (containsId(familyTree, childId))
                {
                  familyTree = appendRelation(familyTree, childId, "child", maleId);
                  familyTree = appendRelation(familyTree, childId, "child", femaleId);
                }
              }
            }
          }
        }
      }
    }
    return familyTree;
  }
  public static boolean remainingTargetsExist(ArrayList<FamilyMember> familyTree)
  {
    for (FamilyMember familyMemeber : familyTree)
      if (!familyMemeber.hasBeenCenter())
        return true;
    return false;
  }
  public static NodeList executeXPath(String xpathQuery, Document xmlDocument)
  {
    XPathExpression xpathExpression;
    try { xpathExpression = xpath.compile(xpathQuery); }
	catch (Exception e)
	{ System.out.println("5Exception caught.");
	  return null; }
    NodeList results;
    try { results = (NodeList)xpathExpression.evaluate(xmlDocument, XPathConstants.NODESET); }
	catch (Exception e)
	{ System.out.println("6Exception caught.");
	  return null; }
    return results;
  }
  public static boolean containsId(ArrayList<FamilyMember> familyTree, String id)
  {
    for (FamilyMember familyMember : familyTree)
      if (familyMember.getId().equals(id))
        return true;
    return false;
  }
  public static ArrayList<FamilyMember> appendRelation(ArrayList<FamilyMember> familyTree, String targetId, String relation, String toId)
  {
    Relation newRelation = new Relation(relation, toId);
    for (int index = 0; index < familyTree.size(); index++)
      if (familyTree.get(index).getId().equals(targetId))
      {
        FamilyMember temp = familyTree.get(index);
        for (Relation iterate : temp.getRelations())
          if (iterate.equals(newRelation))
            return familyTree;
        temp.addRelation(newRelation);
        familyTree.set(index, temp);
        return familyTree;
      }
    return familyTree;
  }
}