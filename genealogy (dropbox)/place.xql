declare namespace util="http://exist-db.org/xquery/util";
declare namespace request="http://exist-db.org/xquery/request";

let $place := request:get-parameter("place","Kiev")
let $regnal := doc("/db/genealogy/genealogy.xml")//ruled[.//place=$place]
return
for $i in $regnal 
order by number($i/startYear/date)
return
<p>
<a href="findPerson.php?person={$i/parent::person/@id}">{normalize-space(string-join((
$i/parent::person//firstName,
if ($i/parent::person//epithet) then string-join((" “",$i/parent::person//epithet,"”"),"") else "",
$i/parent::person//patronymic
),' '))}</a> ({$i/title/data(.)},
r. <span title="{normalize-space(string-join(($i/startYear/source),' '))}">{
    if ($i/startYear/date/@circa eq "circa") then "ca. " else "", 
    number($i/startYear/date)}</span>–<span title="{normalize-space(string-join(($i/endYear/source),' '))}">{
    if ($i/endYear/date/@circa eq "circa") then "ca. " else "",
    number($i/endYear/date)}</span>)
</p>