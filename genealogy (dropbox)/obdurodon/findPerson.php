<?php
header('Content-type: application/xhtml+xml; charset=UTF-8');
$person = $_REQUEST['person'];

$idQuery = <<< idQuery
    string-join(doc('/db/genealogy/genealogy.xml')//person/@id/data(.),'\n')
idQuery;

$idArray = array();
$contents = "http://localhost:8080/exist/rest/db/genealogy?_howmany=100&_wrap=no&_query=" . urlencode($idQuery);
$idValues = file_get_contents($contents);
foreach (explode("\n", $idValues) as $currentItem) {
    $idArray[] = $currentItem;
}
$pattern = '/^[-A-Za-z0-9]+$/D'; // id values are alphanumeric plus hyphens
if (strlen($person) > 40 || !preg_match($pattern, $person)) { // longest is current 38 chars
    return_error(1, $person);
} elseif (!in_array($person, $idArray)) { // is it in the corpus?
    return_error(2, $person, $idArray);
} else { // it's okay
    $contents = "http://localhost:8080/exist/rest/db/genealogy/xquery/person.xql?person=$person";
    $personReport = file_get_contents($contents);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
            <p class="boilerplate"><span><strong>Maintained by: </strong>
                    Christian Raffensperger (<a href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>)
                    and David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>)
                    <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" style="outline: none;">
                        <img src="http://obdurodon.org/images/cc/88x31.png"
                             alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                             title="Creative Commons BY-NC-SA 3.0 Unported License"
                             style="height: 1em; vertical-align: text-bottom;"/>
                    </a></span>
                <span><strong>Last modified:</strong>
                    <?php echo date(DATE_ISO8601, filemtime(basename($_SERVER['SCRIPT_NAME'])));
                    ?></span>
                <span><strong>Links:</strong> 
                    <a href="query.php">New query</a> | <a href="sources.php">Sources</a></span>
            </p>
        </div>
        <hr/>
        <?php
        echo "$personReport";
        ?>
    </body>
    <?php
    /*
     * error1 = malformed filename; should be a\d{3}\.xml; user error
     * error2 = well-formed filename but not in the corpus; user error
     * error3 = html doesn't exist but couldn't generate it; my fault
     * error4 = html exists but xml is newer, yet couldn't regenerate html; my fault
     */

    function return_error($errorno, $person, $idArray) {
        $msg = "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><title>Error $errorno</title>
    <link type=\"text/css\" href=\"http://obdurodon.org/css/style.css\" rel=\"stylesheet\"/>
</head>
<body>
    <h1>Error $errorno</h1>
        <p>$person</p>
    <p>No such person. If you believe that this message results from a bug
    (that is, that the file exists and the system failed to find it), please
    send the url (from the address line in your browsers) to 
    <a href=\"mailto:djbpitt@gmail.com\">djbpitt@gmail.com</a> with the date and
    time (including your time zone) and a note that you received <strong>Error $errorno</strong>.</p>
</body></html>
";
        die($msg);
    }
    ?>
</html> 