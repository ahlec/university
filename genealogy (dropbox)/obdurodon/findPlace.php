<?php
header('Content-type: application/xhtml+xml; charset=UTF-8');
$place = $_REQUEST['place'];

function return_error($errorno, $place) {
    $msg = "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><title>Error $errorno</title>
    <link type=\"text/css\" href=\"http://obdurodon.org/css/style.css\" rel=\"stylesheet\"/>
</head>
<body>
    <h1>Error $errorno</h1>
        <p>$place</p>
    <p>No such place. If you believe that this message results from a bug
    (that is, that the place exists and the system failed to find it), please
    send the url (from the address line in your browsers) to 
    <a href=\"mailto:djbpitt@gmail.com\">djbpitt@gmail.com</a> with the date and
    time (including your time zone) and a note that you received <strong>Error $errorno</strong>.</p>
</body></html>
";
    die($msg);
}

$placeQuery = <<< placeQuery
    string-join(distinct-values(doc('/db/genealogy/genealogy.xml')//place),'\n')
placeQuery;
$placeArray = array();
$contents = "http://localhost:8080/exist/rest/db/genealogy?_howmany=100&_wrap=no&_query=" . urlencode($placeQuery);
$placeValues = file_get_contents($contents);
foreach (explode("\n", $placeValues) as $currentItem) {
    $placeArray[] = $currentItem;
}
$pattern = '/^[-′ łA-Za-z0-9]+$/D'; // places are English alphanumeric, barred l, hyphen, prime, space
if (strlen($place) > 40 || !preg_match($pattern, $place)) { // longest is current 38 chars
    return_error(1, $place);
} elseif (!in_array($place, $placeArray)) { // is it in the corpus?
    return_error(2, $place);
} else { // it's okay
    $place = urlencode($place);
    $contents = "http://localhost:8080/exist/rest/db/genealogy/xquery/place.xql?place=$place";
    $result = file_get_contents($contents);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
            <p class="boilerplate"><span><strong>Maintained by: </strong>
                    Christian Raffensperger (<a href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>)
                    and David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>)
                    <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" style="outline: none;">
                        <img src="http://obdurodon.org/images/cc/88x31.png"
                             alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                             title="Creative Commons BY-NC-SA 3.0 Unported License"
                             style="height: 1em; vertical-align: text-bottom;"/>
                    </a></span>
                <span><strong>Last modified:</strong>
                    <?php echo date(DATE_ISO8601, filemtime(basename($_SERVER['SCRIPT_NAME'])));
                    ?></span>
                <span><strong>Links:</strong> 
                    <a href="query.php">New query</a> | <a href="sources.php">Sources</a></span>
            </p>
        </div> 
        <hr/>
        <?php 
        echo "<h2>Rulers of " . urldecode($place) . "</h2>";
        echo $result; 
        ?>
    </body>
</html>
