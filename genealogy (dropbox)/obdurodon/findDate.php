<?php
header('Content-type: application/xhtml+xml; charset=UTF-8');
$date = $_REQUEST['date'];

function return_error($errorno, $date) {
    $msg = "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><title>Error $errorno</title>
    <link type=\"text/css\" href=\"http://obdurodon.org/css/style.css\" rel=\"stylesheet\"/>
</head>
<body>
    <h1>Error $errorno</h1>
        <p>$date</p>
    <p>No such date. If you believe that this message results from a bug
    (that is, that the date exists and the system failed to find it), please
    send the url (from the address line in your browsers) to 
    <a href=\"mailto:djbpitt@gmail.com\">djbpitt@gmail.com</a> with the date and
    time (including your time zone) and a note that you received <strong>Error $errorno</strong>.</p>
</body></html>
";
    die($msg);
}

$pattern = '/^[0-9]+$/D'; // dates are 3-4-digit integers between 800 and 1500
if (strlen($date) > 4
        || strlen($date) < 3
        || $date > 1500
        || $date < 800
        || !preg_match($pattern, $date)) {
    return_error(1, $date);
} else { // it's okay
    $intvalue = intval($date);
    $contents = "http://localhost:8080/exist/rest/db/genealogy/xquery/date.xql?date=$intvalue";
    $result = file_get_contents($contents);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
            <p class="boilerplate"><span><strong>Maintained by: </strong>
                    Christian Raffensperger (<a href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>)
                    and David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>)
                    <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" style="outline: none;">
                        <img src="http://obdurodon.org/images/cc/88x31.png"
                             alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                             title="Creative Commons BY-NC-SA 3.0 Unported License"
                             style="height: 1em; vertical-align: text-bottom;"/>
                    </a></span>
                <span><strong>Last modified:</strong>
                    <?php echo date(DATE_ISO8601, filemtime(basename($_SERVER['SCRIPT_NAME'])));
                    ?></span>
                <span><strong>Links:</strong> 
                    <a href="query.php">New query</a> | <a href="sources.php">Sources</a></span>
            </p>
        </div> 
        <hr/>
        <?php
        echo "<h2>Rulers in $intvalue</h2>";
        if ($result) { echo $result;} else {echo "<p>No rulers in $intvalue in database</p>";}
        ?>
    </body>
</html>
