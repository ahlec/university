<?php
header('Content-type: application/xhtml+xml');

$getPersonNamesQuery = <<< EOQ1
<select name="person">{for \$i in doc('/db/genealogy/genealogy.xml')//person[not(.//firstName eq "unknown")]
    order by string-join(\$i/name/*," ")
    return
        <option value="{\$i/@id}">{normalize-space(concat(
        \$i/name/firstName,
        " ",
        if (\$i/name/epithet) then concat("“",\$i/name/epithet,"”") else "",
        " ",
        \$i/name/patronymic," ",
        if (not(\$i/@origin eq "Rus'" or \$i/@origin eq "unknown")) then concat("of ",\$i/@origin) else ""
        ))}</option>
    }</select>
EOQ1;
$contents = "http://localhost:8080/exist/rest/db/genealogy?_howmany=10000&_wrap=no&_query=" . urlencode($getPersonNamesQuery);
$personNames = file_get_contents($contents);

$getPlaceNamesQuery = <<< EOQ2
<select name="place">{for \$i in distinct-values(doc('/db/genealogy/genealogy.xml')//place)
    order by \$i
    return
        <option value="{\$i}">{\$i}</option>
    }</select>
EOQ2;
$contents = "http://localhost:8080/exist/rest/db/genealogy?_howmany=10000&_wrap=no&_query=" . urlencode($getPlaceNamesQuery);
$placeNames = file_get_contents($contents);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
            <p class="boilerplate"><span><strong>Maintained by: </strong>
                    Christian Raffensperger (<a href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>)
                    and David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>)
                    <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" style="outline: none;">
                        <img src="http://obdurodon.org/images/cc/88x31.png"
                             alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                             title="Creative Commons BY-NC-SA 3.0 Unported License"
                             style="height: 1em; vertical-align: text-bottom;"/>
                    </a></span>
                <span><strong>Last modified:</strong>
                    <?php echo date(DATE_ISO8601, filemtime(basename($_SERVER['SCRIPT_NAME']))); ?></span>
            </p>
        </div>
        <hr />
        <h2>Explore the site</h2>
        <form method="get" action="findPerson.php">
            <p>
                <label for="person">Search by person: </label>
                <?php
                echo "$personNames\n";
                ?>
                <input type="submit" value="Submit"/></p>
        </form>
        <form method="get" action="findPlace.php">
            <p>
                <label for="place">Search by place: </label>
                <?php
                echo "$placeNames\n";
                ?>
                <input type="submit" value="Submit"/></p>
        </form>
        <form method="get" action="./findDate.php">
            <p>
                <label for="date">Search by regnal year: </label>
                <input size="4" maxlength="4" name="date" id="date"/>&#xa0;
                <input type="submit" value="Submit"/>
            </p>
        </form>
    </body>
</html>
