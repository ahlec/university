<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
        <style type="text/css">
            img.vladimir {max-width: 250px; width: 40%; float: right; margin: 0 0 1em 1em; border: none;}</style>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<span class="logo">&lt;rg&gt;</span> Rusian genealogy</h1>
        <hr />
        <div>
            <a href="http://commons.wikimedia.org/wiki/File:Vladimir_I_of_Kiev_detail.jpg"><img class="vladimir" alt="[Image of Vladimir I]" 
                                                                                                src="http://genealogy.obdurodon.org/images/Vladimir_I_of_Kiev_detail.jpg"/></a>
            <p class="boilerplate"><span><strong>Maintained by: </strong>
                    Christian Raffensperger (<a href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>)
                    and David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>)
                    <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" style="outline: none;">
                        <img src="http://obdurodon.org/images/cc/88x31.png"
                             alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                             title="Creative Commons BY-NC-SA 3.0 Unported License"
                             style="height: 1em; vertical-align: text-bottom;"/>
                    </a></span>
                <span><strong>Last modified:</strong>
                    <?php echo date(DATE_ISO8601, filemtime(basename($_SERVER['SCRIPT_NAME']))); ?></span>
                <span><strong>About this site: </strong><a href="about.php">http://genealogy.obdurodon.org/about.php</a></span>
            </p>
        </div>
        <hr />
        <p><a href="query.php">Query</a> | <a href="sources.php">Sources</a></p>
        <hr/>
        <h2>Introduction</h2>
        <p>Watch this space ...</p>
    </body>
</html> 
