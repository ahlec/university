<?oxygen RNGSchema="notes.rnc" type="compact"?><?xml-model href="notes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<note>
    <title>Evfimija Volodimerovna</title>
    <p>The marriage of <link target="evfimiia">Evfimija Volodimerovna</link> to <link target="koloman">Koloman</link>, king of Hungary, has been a subject of intense scrutiny
        for hundreds of years because of its outcome. The marriage seems to have taken place in
        1112, when <link target="evfimiia">Evfimija</link>, who is identified by name in the
        Hypatian chronicle, is sent to Hungary to marry the king.<fn>
            <fn_p>
                <bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1112.</fn_p>
        </fn>Though the king is not identified by name, the king at the time was <link target="koloman">Koloman</link> and the marriage is recognized in numerous other sources.<fn>
            <fn_p>For a breakdown of those sources, as well as an interesting perspective on the
                marriage, see S. P. Rozanov, <articleTitle>"Evfimija Vladimirovna i Boris
                    Kolomanovič: Iz evropeiskoj politiki XII v.,"</articleTitle> Izvestija Akademij
                Nauk SSSR 8 (1930).</fn_p>
        </fn>The purpose of the marriage seems clear, with the death of <link target="koloman">Koloman</link>’s first wife earlier that year,<fn>
            <fn_p>Wertner, <bookTitle>Az Árpádok családi története</bookTitle>, chart.</fn_p>
        </fn>
        <link target="koloman">Koloman</link> was free to create a new alliance, and <link target="vladimir3">Volodimer Monomax</link> felt himself in need of a connection with
        Hungary. At this time <link target="sviatopolk2">Svjatopolk Izjaslavič</link> still ruled in
        Kyiv, and the Izjaslaviči held the upper hand in terms of foreign dynastic connections,
        especially with Poland and Hungary, two dangerous border areas that could shelter them and
        provide troops as necessary. To remedy that, and counter the marriage of <link target="sviatopolk2">Svjatopolk</link>’s daughter <link target="predslava2">Peredslava</link> with <link target="koloman">Koloman</link>’s brother <link target="almos">Almos</link>, <link target="vladimir3">Volodimer</link> sent his daughter
            <link target="evfimiia">Evfimija</link> to marry <link target="koloman">Koloman</link>.<fn>
            <fn_p>See above for the political context of Peredslava Svjatopolkovna’s
                marriage.</fn_p>
        </fn>As in all of these cases, there must be reciprocal advantage, and for <link target="koloman">Koloman</link> it may have been connected to the death of his eldest
        son Ladislaus that same year.<fn>
            <fn_p>Rozanov, <articleTitle>"Evfimija Vladimirovna i Boris Kolomanovič,"</articleTitle>
                591.</fn_p>
        </fn>Though he was already old and unwell at the time, perhaps he hoped to sire more sons to
        protect his lineage from his brother, with whom he often warred.</p>
    <p>The marriage lasted less than a year before <link target="koloman">Koloman</link> repudiated
            <link target="evfimiia">Evfimija</link> and sent her home to Kyiv.<fn>
            <fn_p>
                <articleTitle>"Boguphali II episcopi Posnaniensis Chronicon Poloniae, cum
                    continuatione Basconis custodis Posnaniensis,"</articleTitle> in
                    <bookTitle>Monumenta Poloniae Historica</bookTitle>, ed. August Bielowski
                (Warsaw: Mouton and Co., 1961), 508. </fn_p>
        </fn>At the time, <link target="evfimiia">Evfimija</link> was pregnant, and the assumption
        has been that she was pregnant with someone else’s child. However, multiple contemporary
        sources identify the son she bore in Kyiv, <link target="boris4">Boris</link>, as the son of
            <link target="koloman">Koloman</link>.<fn>
            <fn_p>
                <articleTitle>"Cosmae chronicon Boemorum cum continuatoribus,"</articleTitle>
                215–16; and <bookTitle>Otto, Bishop of Freising</bookTitle>, bk. 7, sec. 21.</fn_p>
        </fn>Interestingly, both Cosmas and Otto would have had better reason to disprove <link target="boris4">Boris</link>’s lineage, as they were each allied with <link target="boris4">Boris</link>’s foes in Hungary. Thus their testimony in favor of his
        legitimacy means a great deal. Unfortunately, we are then left with an unanswerable
        conundrum. <link target="evfimiia">Evfimija</link>, who went to Hungary to seal an alliance,
        and produce sons for the king, became pregnant with a son (seemingly by the king), but was
        repudiated and sent home. The cause of the repudiation is unknown. It is clear from future
        events that the marriage did not secure its purpose, as would seem obvious from its abrupt
        end. Indeed, after <link target="koloman">Koloman</link>’s death in 1114 his son Stephen II
        supported <link target="iaroslav4">Jaroslav Svjatopolčič</link> against <link target="vladimir3">Volodimer Monomax</link> in <link target="iaroslav4">Jaroslav</link>’s attempts to stay independent.<fn>
            <fn_p>There are multiple examples of Hungarian support for Jaroslav in the various
                chronicles. <bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1118, 1123, for two
                examples.</fn_p>
        </fn>
    </p>
    <p>The fate of <link target="boris4">Boris</link> was constant warfare to reclaim his
        birthright. He was raised in Rus′ and over the course of his life allied with <link target="boleslaw3">Bolesław III of Poland</link> and the Comneni emperors of Byzantium
        to attempt to take the throne of Hungary after the death of his half brother Stephen II.<fn>
            <fn_p>
                <bookTitle>Kinnamos</bookTitle>, 93; <bookTitle>Polish Great
                Chronicle</bookTitle>, ch. 29.</fn_p>
        </fn>As for <link target="evfimiia">Evfimija</link>, some maintain that she entered a
        monastery in Rus′.<fn>
            <fn_p>Tatiščev, for instance, records her original name as Sofiia and her monastic name
                as Evfimija. Tatiščev, <bookTitle>Istorija Rossiiskaja</bookTitle>, vol. 2, 128,
                149.</fn_p>
        </fn>′. This would certainly have been a common option for a princess in her position, but
        no reliable primary source records such an event as it was recorded for <link target="evpraksia">Evpraksia Vsevolodovna</link>.<fn>
            <fn_p>
                <bookTitle>PVL</bookTitle>, s.a. 1106.</fn_p>
        </fn>
        <link target="evfimiia">Her</link> death is recorded in 1138 and she was laid to rest
        in the Holy Savior’s Church.<fn>
            <fn_p>
                <bookTitle>Laurentian Chronicle</bookTitle>, s.a. 1138; <bookTitle>Hypatian
                    Chronicle</bookTitle>, s.a. 1139; <bookTitle>Nikon Chronicle</bookTitle>, s.a.
                1138.</fn_p>
        </fn>
        <link target="evfimiia">Her</link> burial in a high–status location indicates that the
        family felt no shame over her failed marriage, but the lack of a response to her curt
        dismissal from Hungary and the absence of sources on her activities in the intervening years
        leave a mystery surrounding the dissolution of her marriage.</p>
</note>