declare option exist:serialize "method=text media-type=text/plain omit-xml-declaration=yes";
let $tab := (:'&#x09;':) '[TAB]'
let $mss := collection('/db/qc')
let $watermarks := distinct-values($mss//watermark/normalize-space(@n))
let $articles := distinct-values($mss//msItemStruct/normalize-space(title))
let $header := string-join(('title','material','numLeaves','foliaHeight',
    'foliaWidth','plateHeight','plateWidth','writtenHeight','writtenWidth',
    'columnsHeight','columnsWidth','foliation','ruling','ink','columns',
    'ruledLines','writtenLines','illuminated','headpieces','tailpieces',
    'initials','titles',
    'marginalia','borders','tables','bindingHeight','bindingWidth','alphabet',
    'lettersHeight','lineHeight','scribeLang','scribeName','jer','jus',
    for $i in $watermarks return $i,
    for $i in $articles return $i),$tab)
(: Questions
ruling (Leroy?); sometimes <formula>, sometimes @n plus data content
gregoryRule
jus and jer are per scribe, as is scribeLang; how about alphabet?; letter height in 39625
may have multiple dimension values (30063)
ruled lines may be a range (32162)
letter height is sometimes bare number and sometimes includes mm (37233)
assuming all measurements are mm
scribe name, as long as it isn't anonymous
multiple layouts (41256)
md848 has dimensions @type="leaves"; is this the same as folia?
AM1161CM has <extent unit="folia">148</extent> vs <num type="leaves">
:)
return ($header,
(
for $i in $mss
let $title := ($i//titleStmt/title/text(), '[NONE]')[1] 
let $material := ($i//supportDesc/@material,' [NONE]')[1]
let $numLeaves := ($i//num[@type="leaves"], '[NONE]')[1]
let $foliaHeight := ($i//dimensions[@type="folia"]//height,'[NONE]')[1]
let $foliaWidth := ($i//dimensions[@type="folia"]//width,'[NONE]')[1]
let $plateHeight := ($i//dimensions[@type="plate"]//height,'[NONE]')[1]
let $plateWidth := ($i//dimensions[@type="plate"]//width,'[NONE]')[1]
let $writtenHeight := ($i//dimensions[@type="written"]//height,'[NONE]')[1]
let $writtenWidth := ($i//dimensions[@type="written"]//width,'[NONE]')[1]
let $columnsHeight := ($i//dimensions[@type="columns"]//height,'[NONE]')[1]
let $columnsWidth := ($i//dimensions[@type="columns"]//width,'[NONE]')[1]
let $foliation := ($i//foliation, '[NONE]')[1]
let $ruling := ($i//ruling/formula,'[NONE]')[1]
let $ink := ($i//ink/tokenize(normalize-space(lower-case(.)),'\W+')[1], '[NONE]')[1]
(: all in corpus are currently entirely 1 or 2 column, so no multiple values
    change to separate boolean matrix columns if ms can have multiple values :)
let $columns := ($i//layout/@columns,'[NONE]')[1]
let $ruledLines := ($i//layout/@ruledLines,'[NONE]')[1]
let $writtenLines := ($i//layout/@writtenLines,'[NONE]')[1]
let $illuminatedBoolean := if ($i//decoNote[@type="miniatures"]) then 1 else 0
let $headpiecesBoolean := if ($i//decoNote[@type="headpieces"]) then 1 else 0
let $tailpiecesBoolean := if ($i//decoNote[@type="tailpieces"]) then 1 else 0
let $initialsBoolean := if ($i//decoNote[@type="initials"]) then 1 else 0
let $titlesBoolean := if ($i//decoNote[@type="titles"]) then 1 else 0
let $marginaliaBoolean := if ($i//decoNote[@type=("marginalia","marg")]) then 1 else 0
let $bordersBoolean := if ($i//decoNote[@type=("borders")]) then 1 else 0
let $tablesBoolean := if ($i//decoNote[@type=("table","tables")]) then 1 else 0
let $bindingHeight := ($i//(binding|bindingDesc)//height, '[NONE]')[1]
let $bindingWidth := ($i//(binding|bindingDesc)//width,'[NONE]')[1]
let $alphabet := ($i//alphabet, '[NONE]')[1]
let $lettersHeight := ($i//dimensions[@type="letters"]/height, '[NONE]')[1]
let $lineHeight := ($i//dimensions[@type=("line", "scriptline")]/height, '[NONE]')[1]
let $scribeLang := ($i//scribeLang/p, '[NONE ] ')[1]
let $scribeName := ($i//name[@type = "scribe"], '[NONE]')[1]
let $jer := ($i//jer/@type, '[NONE]')[1]
let $jus := ($i//jus/@type, '[NONE]')[1]
return
    ('&#xa0;',string-join(
    ($title, $material, $numLeaves,
    $foliaHeight, $foliaWidth, $plateHeight, $plateWidth, $writtenHeight, $writtenWidth, $columnsHeight, $columnsWidth,
    $foliation, $ruling, $ink, $columns, $ruledLines, $writtenLines, 
    $illuminatedBoolean, $headpiecesBoolean, $tailpiecesBoolean, $initialsBoolean, $titlesBoolean, $marginaliaBoolean, $bordersBoolean, $tablesBoolean,
    $bindingHeight, $bindingWidth, 
    $alphabet, $lettersHeight, $lineHeight, $scribeLang, $scribeName, $jer, $jus,
    for $j in $watermarks return if ($i//watermark[@n eq $j]) then 1 else 0,
    for $j in $articles return if ($i//msItemStruct[title eq $j]) then 1 else 0),
    $tab
    ))
))