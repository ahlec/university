import module namespace kwic="http://exist-db.org/xquery/kwic";
declare option exist:serialize "method=xhtml enforce-xhtml=yes";
let $target := request:get-parameter('target','аввакум')
return
<html>
<head><title>Avvakum search for {$target}</title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="http://www.obdurodon.org/css/style.css"/>
<link rel="stylesheet" type="text/css" href="http://www.obdurodon.org/avvakum/avvakum.css"/>
</head>
<body>
<h1>Life of Avvakum</h1>
        <hr/>
        <p class="boilerplate">
            <span><strong>Maintained by:</strong> David J. Birnbaum (<a
                href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>) <a
                    href="http://creativecommons.org/licenses/by-nc-sa/3.0/"
                    style="outline: none;">
                    <img src="http://www.obdurodon.org/images/cc/88x31.png"
                        alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                        title="Creative Commons BY-NC-SA 3.0 Unported License"
                        style="height: 1em; vertical-align: text-bottom;"/>
                </a>
            </span>
            <span>
                <strong>Report generated: </strong>
                {current-dateTime()}
            </span>
        </p>
        <hr/>
<h2>Search for <em>{$target}</em></h2>
{for $hit in doc("/db/avvakum/avvakum.xml")//p[ft:query(., $target)]
let $expanded := kwic:expand($hit)
return
kwic:get-summary($expanded, ($expanded//exist:match)[1], <config width="40"/>)
}</body>
</html>