declare default element namespace "http://www.tei-c.org/ns/1.0";
let $cook := doc('/db/course/pacific/HawkesworthVolume2Chapter4-8Complete.xml')
let $pps := $cook//p
for $p at $pos in $pps
let $latCoords := $p/geo[@select="lat"]
let $lonCoords := $p/geo[@select="lon"]
let $ns := $latCoords[@n]
for $n in $ns
let $nval := $n/@n/data(.)
let $lats := data($latCoords[@n=$nval])
let $lons := data($lonCoords[@n=$nval])

for $lat in $lats
let $latsign := ends-with($lat,'N')
let $latdms1 := replace(normalize-space($lat),"' N|' S|′ N|′ S","")
let $latdms := replace(normalize-space($latdms1)," N| S"," 0")
let $latdeg := number(substring-before($latdms,"°"))
let $latmin := number(substring-after($latdms,"° "))
let $latdec := compare($latsign,"trud") * ($latdeg + ($latmin div 60)) (: trud > true > truf :)

for $lon in $lons
let $lonsign := ends-with($lon,'W')
let $londms1 := replace(normalize-space($lon),"' E|' W|′ E|′ W","")
let $londms := replace(normalize-space($londms1)," E| W"," 0")
let $lonmin := number(substring-after($londms,"° "))
let $londeg := number(substring-before($londms,"°"))
let $londec := compare($lonsign,"truf") * ($londeg + ($lonmin div 60)) (: trud > true > truf :)

return concat(string-join(($londec,$latdec),','),',0')

