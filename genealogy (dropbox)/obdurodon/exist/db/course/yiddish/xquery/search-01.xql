declare namespace request="http://exist-db.org/xquery/request";
declare namespace transform="http://exist-db.org/xquery/transform";
declare option exist:serialize "method=xhtml enforce-xhtml=yes";
let $papers := collection('/db/course/yiddish')
let $category := request:get-parameter('category','actor')
let $target := request:get-parameter('value','Bob Royce')
return
    <html>
        <head>
            <title>Results</title>
            <link rel="stylesheet" type="text/css" href="http://yiddish.obdurodon.org/websiteCSS.css"/>
        </head>
        <body>
            <h1>Results</h1>
            <ul>{
            for $i in $papers//show
            where $i[.//*[name() = $category][contains(.,$target)]]
            return <li>{transform:transform($i,'../xslt/xml-to-html.xsl',())}</li>
            }</ul>
        </body>
    </html>
