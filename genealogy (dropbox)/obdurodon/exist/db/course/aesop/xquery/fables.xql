xquery version "1.0";
<html>
<head>
    <title>Fables</title>
</head>
<body>
    <ol>
{
let $fables := doc('/db/course/aesop/data/FABLES_ALL.xml')//fable
for $i in $fables
where $i[.//@type eq 'fox' or .//@type eq 'lion' or .//@type eq 'man' or 
.//@type eq 'wolf' or .//@type eq 'ass' or .//@type eq 'dog' or .//@type 
eq 'eagle' or .//@type eq 'sheep' or .//@type eq 'shepard' or .//@type 
eq 'jupiter']
return <li>{$i}</li>
}
</ol>
</body>
</html>