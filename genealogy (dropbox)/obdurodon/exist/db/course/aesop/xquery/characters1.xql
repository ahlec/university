xquery version "1.0";
<html>
<head>
    <title>Aesop's Top 10</title>
</head>
<body>
    <ul>
    {
let $fables := doc('/db/course/aesop/data/FABLES_ALL.xml')/*
let $characters := distinct-values($fables//@type)
for $i in $characters
let $count := count($fables/fable[cast/char/@type eq $i])
where $count gt 10
return <li>{$i}</li>
}
</ul>
</body>
</html>