<listBibl><bibl type="philosophy"><author ana="m">Kames, Henry Home, Lord</author><title>Sketches of the history of man: In four volumes.</title><editor/><publisher>printed for James Williams</publisher><pubPlace>Dublin</pubPlace><date>1774-1775</date><note resp="#mmd79">In ECCO TCP. Volume I. Sketch 1 ("DIVERSITY of MEN and of LANGUAGES.")
            See pp. 20-21 and 24-25. Pp. 20-21: on <persName>Commodore Byron's</persName> and on
                <persName>Cook's</persName> voyages: Mentions of <persName>Banks</persName> and
                <persName>Solander</persName> leaving <placeName>Otaheite</placeName> to find
                <placeName>Society Islands</placeName>, <placeName>Islands of Oahena</placeName>.
            See p. 24: <placeName>Otaheite</placeName> mentioned in opposition to other more violent
            cultures, and compared with the ancient <orgName>Caledonians</orgName>: Intelligent
            resistance to <persName>Wallis</persName>, ease of making peace--more impressive than
            most, apparently.</note><note resp="#mmd79">In ECCO TCP. Volume I. Sketch 2 ("Progress Porgress of Men with respect
            to FOOD and POPULATION.)See p. 58: "The is∣land of <placeName>Otaheite</placeName> is
            healthy, the people tall and well made; and by temperance, <rs type="object">vegetables</rs> and <rs type="object">fish</rs> being their chief nourishment, they
            live to a good old age, with scarce any ailment. There is no such thing known among them
            as rotten teeth: the very smell of <rs type="object">wine</rs> or <rs type="object">spirits</rs> is disagreeable; and they never deal in <rs type="object">tobacco</rs>
            nor <rs type="object">spiceries</rs>."</note><note resp="#mmd79">In ECCO TCP. Volume I. Sketch 6 ("Progress of the FEMALE SEX.") See pp.
            194-196: on free love in <placeName>Otaheite</placeName>: They're good people, but "seem
            to have as little notion of <rs type="concept">modesty</rs> as of <rs type="concept">chastity</rs>. More on "<rs type="concept">polygamy</rs>" on p. 196.</note><note resp="#mmd79">In ECCO TCP. Volume II. Book I. Continued: Sketch 7 ("Progress of
            Manners"), pp. 6-7: <placeName>Otaheite</placeName> mentioned in a syncretic description
            of bodily cleaning "in several nations that have made little progress in the arts of
            life." <orgName>Otaheitian</orgName><rs type="concept">cleansliness</rs> in both sexes is mentioned after <orgName>Caribbean
                islanders</orgName> and before historic descriptions of the ancient
                <orgName>Gauls</orgName>.</note><note resp="3mmd79">In ECCO TCP. Volume II: Sketch 1 ("Progress of Men in Society"), p. 156:
            On the practices of <rs type="concept">stealing</rs> and <rs type="concept">piracy</rs>,
                <orgName>Otaheitians</orgName> mentioned after the <orgName>Scottish
                Highlanders</orgName> and the rebellion of 1745! Idea: that theft isn't committed
            among their own, but against outsiders. "<persName>Bougainville</persName> observes,
            that the <orgName>inhabitants of Otaheite</orgName> → , named by <orgName>the
                English</orgName>, <placeName/>, made no difficulty of stealing from his people; and
            yet never steal among themselves, having neither locks nor bars in their houses."</note><note resp="#mmd79">In ECCO TCP. Volume II: Sketch 1 ("Progress of Men in Society"), p. 167:
            Attempt to address a philosophical question about whether people are naturally sociable
            or antisocial and brutally violent. "In a nascent society, where men hunt and fish in
            common, where there is plenty of game, and where the sense of property is faint, mutual
            affec∣tion prevails, because there is no cause of discord; and dissocial passions find
            sufficient vent against neighbouring tribes. Such is the condition of the
                <orgName>North-American savages</orgName>, who continue hunters and fishers to this
            day; and such is the condition of all brute animals that live in society, as mentioned
            above. The island ← <placeName>Otaheite</placeName> is divided into many small cantons,
            having each a chief of its own. These cantons never make war on each other, tho' they
            are frequently at war with the inhabitants of neigh∣bouring islands. The
                <orgName>inhabitants of the new Philip∣pine islands</orgName>, if <persName>Father
                Gobien</persName> be credited, are bet∣ter fitted for society than any other known
            nation. Sweetness of temper, and love to do good, form their character. They never
            commit acts of violence: <rs type="concept">war</rs> they have no notion of; and it is a
            proverb among them, That a man never puts a man to death. <persName>Plato</persName>
            places the seat of <rs type="concept">justice</rs> and of <rs type="concept">happi∣ness</rs> among the first men; and among them existed the golden age, if it
            ever did exist. But when a nation, becoming populous, begins with rearing flocks and
            herds, proceeds to appropriate land, and is not satisfied without matters of <rs type="concept">luxury</rs> over and above; <rs type="concept">selfishness</rs> and
                <rs type="concept">pride</rs> gain ground, and be∣come ruling and unruly passions.
            Causes of discord multiply, vent is given to <rs type="concept">avarice</rs> and <rs type="concept">resentment</rs>; and among a people not yet perfectly submissive to
            government, dissocial passions rage, and threaten a total dissolution of society:
            nothing indeed suspends the impending blow,..." </note><note resp="#mmd79">In ECCO TCP. Volume IV, Part II: Sketch 3 ("Principles and Progress of
            THEOLOGY.") Chapter 2, p. 152: <rs type="concept">Syncretism</rs> in action here:
                <placeName>Otaheite's</placeName> spiritual beliefs are sketched here by way of
            comparison with several other island or "savage" cultures with (according to this
            author) tendencies toward <rs type="concept">monotheism</rs>. Passage: " The <orgName>North-Ame∣rican
                savages</orgName> have all of them a notion of a su∣preme Deity, creator and
            governor of the world, and of inferior deities, some good, some ill. These are supposed
            to have bodies, and to live much as men do, but without being subjected to any distress.
            The same creed prevails among the negroes of <placeName>Benin</placeName> and
                <placeName>Congo</placeName>, among the people of <placeName>New Zeland</placeName>,
            among the inhabitants of <placeName>Java</placeName>, of
                <placeName>Madagascar</placeName>, of the <placeName>Molucca islands</placeName>,
            and of the <placeName>Caribbee islands</placeName>. The <orgName>Chingulese</orgName>, a
            tribe in the island of <placeName>Ceylon</placeName>, acknowledge one <name type="myth">God</name> creator of the universe, with subordinate deities who act as his
            deputies: agriculture is the pecu∣liar province of one, navigation of another. The creed
            of the <orgName>Tonquinese</orgName> is nearly the same. The inhabitants of
                <placeName>Otaheite</placeName>, termed <placeName>King George's island</placeName>,
            believe in one supreme Deity; and in inferior deities without end, who preside over
            particular parts of the creation. They pay no adoration to the supreme Deity, thinking
            him too far elevated above his creatures to concern himself with what they do. They
            believe the stars to be children of the sun and moon, and an eclipse to be the time of
            copulation."</note></bibl><bibl type="essay"><author ana="m">Late Member of the Continental Congress</author><title>The true merits of a late treatise, printed in America, intitled, Common sense:
            clearly pointed out. Addressed to the inhabitants of America. By a late member of the
            Continental Congress, a native of a republican state.</title><editor/><publisher>printed for W. Nicoll</publisher><pubPlace>London</pubPlace><date>1776</date><note resp="#mmd79">In ECCO TCP. Essay attacking <persName>Thomas Paine's</persName> "Common
            Sense." See pp. 23-24: that <placeName>England</placeName> made it possible for
            Protestants to live in religious freedom in the New World, and if
                <persName>Paine</persName> (et. al.) don't like it, they can go to
                "<placeName>Otaheite</placeName>, and the <placeName>Islands in the North
                West</placeName>" as a "Sanctuary for him and his Friends."</note></bibl><bibl type="philosophy"><author ana="m">MacNally, Leonard</author><title>Sentimental excursions to Windsor: and other places, with notes critical,
            illustrative, and explanatory</title><editor/><publisher>printed for J. Walker</publisher><pubPlace>London</pubPlace><date>1781</date><note resp="#mmd79">In ECCO TCP. See "A Meditation," pp. 16-20: on the <rs type="concept" subtype="pac">"Timiradi dance"</rs> of
                <placeName>Otaheite</placeName>, saying this dance was apparently practiced by the
            virtuous yet not modest <orgName>Lacedaemonians</orgName>, with a footnote citing
                "<persName>Forster</persName>, who attended <persName>Captain Cooke</persName> on
            his second voyage." Reflection on whether or not <rs type="concept">chastity</rs> is
            necessary--"I concluded with a modern voyager (citing <persName>Forster</persName>)...that <rs type="concept">modesty</rs> and <rs type="concept">chastity</rs>, which have long been supposed to
            be inherent in the human mind, are local ideas, unknown in the state of nature, and
            modified according to the various degrees of <rs type="concept">civilization</rs>."</note></bibl><bibl type="poem"><author ana="m">Mason, William</author><title>The Dean and the 'Squire: A Political Eclogue. Humbly dedicated to <persName>Soame
                Jenyns, Esq.</persName></title><editor/><publisher>printed for J. Debrett, successor to Mr. Almon</publisher><pubPlace>London</pubPlace><date>1782</date><note resp="#mmd79">In ECCO TCP. See lines 210 - 230: References to
                "<placeName>Zealand-New</placeName>," "<placeName>Otaheite</placeName>," and
                "<persName>Queen Oberea</persName>" with the idea of criminals going into exile to
                <placeName>the Pacific</placeName>, and questioning the kinds of freedom they'd
            find, "taxed" by <persName>Queen Oberea's</persName> supposed sexual appetite. Continues
            by discussing such a life as comparable to Adam and Eve: "But a cool hundred Eves and
            Adams; / I think they would, or soon, or late, by <em>quasi compact</em> found a
            state.</note></bibl><bibl type="philosophy"><author ana="f">More, Hannah</author><title>Thoughts on the importance of the manners of the great to general society</title><editor/><publisher>printed for T. Cadell</publisher><pubPlace>London</pubPlace><date>1788</date><note resp="#mmd79">In ECCO TCP. See pp. 35-36: mention of hypocritical people willing to
            support missions to <placeName>Tahiti</placeName>, but sacrilegiously enjoying Sunday
            haircuts: "I am persuaded that there are multitudes of well-meaning people who would
            gladly contribute to a mission of <rs type="concept">Christianity</rs> to
                <placeName>Japan</placeName> or <placeName>Otaheite</placeName>, to whom it never
            occurred, that the hair-dresser, whom they are every Sunday detaining from church, has a
            soul to be saved; that the law of the land co-operates with the law of <name type="myth">God</name>, to forbid their employing him; and that they have no right, either
            legal or moral, to this portion of his time." </note></bibl><bibl type="poem"><author ana="m">Perry, James</author><title>Mimosa: or, The sensitive plant; A poem. Dedicated to Mr. Banks, and addressed to
            Kitt Frederick, Dutchess of Queensberry, Elect.</title><editor/><publisher/><pubPlace>London</pubPlace><date> 1779</date><note resp="#mmd79"> This publication is a poem about the <rs type="object">Mimosa
                plant</rs>, discovered on <placeName>Otaheite</placeName>. In the dedication to this
            poem, author <persName>James Perry</persName> speaks to <persName>Mr. Banks</persName>,
            imploring him to spread the word about this plant and its many significant properties.
            Passage: "The world will determine with what justice I dedicate the <rs type="object">SENSITIVE PLANT</rs>, to a <rs type="person" ana="Mr. Banks">Gentleman so deeply
                skilled in the science of <rs type="concept">Botany</rs></rs>; and whose desire of
            acquiring knowledge has led him to climates, most happily adapted to the nourishment and
            the cultivation of that wonderful <em>lusus naturæ</em>. The plains of
                <placeName>Otaeite</placeName>, (known as they are to us by the luxurious
            descriptions we have recieved from you, and your compatriots;) rear this plant to an
            amazing height... and <persName>Queen Oberea</persName>, as well as her enamoured
            subjects, feel the most sensible delight in handling, exercising, and proving its
            virtues." </note></bibl><bibl type="poem"><author ana="m">Preston, William</author><title>The poetical works of William Preston, Esq. in two volumes. ...</title><editor/><publisher/><pubPlace>Dublin</pubPlace><date>1793</date><vol>1</vol><note resp="#mmd79">Another poem, this time from "a <rs type="person">lady of quality in
                England"</rs> to <persName>Omiah</persName>. Passage:"If yet thy land preserves
                <persName>Opano's</persName> name, / And <persName>Oberea</persName> pines with
            am'rous flame; / If yet, untouch'd, the sacred <rs type="object">bread-tree</rs> grows,
            / Which saw their trasports, and retains their vows; / If joys remember'd rapture can
            impart, / And <placeName>London</placeName> lives within <persName>Omiah's</persName>
            heart; / Dear shall this greeting from they <placeName>Britain</placeName> prove, / And
            dear these wishes of eternal <rs type="concept">love</rs>."</note></bibl></listBibl>