declare option exist:serialize "method=xhtml media-type=text/html";
for $i in collection('/db/menology')/* 
let $filename := substring-after(base-uri($i),'/db/menology/')
let $code := $i//code/data(.)
order by normalize-space($i//manuscriptName)
return
<tr>
<td>{$i//manuscriptName/data(.)} ({$i//code/data(.)})</td>
<td><a href="readfile.php?filename={$filename}">Read {$code}</a></td>
<td><a href="rawfile.php?filename={$filename}">{$filename}</a></td>
</tr>
