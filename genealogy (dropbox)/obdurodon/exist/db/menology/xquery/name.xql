xquery version "1.0";
 
declare namespace util="http://exist-db.org/xquery/util";
declare namespace request="http://exist-db.org/xquery/request";

let $term := request:get-parameter("term", ())
let $temp := if ($term ne '*') then collection("/db/menology")//(saint | event)[ft:query(@name, $term)]
    else collection("/db/menology")//(saint | event)

return 
<html>
<head>
<title>Menology Report (name)</title>
        <link id="ms_style" rel="stylesheet" type="text/css" href="css/menology.css"/>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
</head>
<body>
<h1>Menology Report (name)</h1>
<p>List of all entries that contain the word: {$term}</p>
<ul>
{for $i in $temp
let $date := data($i/ancestor::datedEntry/data(@entryDate))
let $manuscript := root($i)//code
let $fqname := $i/data(@fqname)
let $text := $i
return
<li>{$date} ({$manuscript}): <span style="font-family: serif">{$fqname}</span>:<br/>{<span class="ocs">{$text}</span>}</li>
}
</ul>
<p><a href="./menology_queries.html">New search</a></p>
</body>
</html>
