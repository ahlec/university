declare namespace request="http://exist-db.org/xquery/request";
declare option exist:serialize "method=xhtml media-type=xhtml+xml charset=utf-8";
let $filename := request:get-parameter("filename",())
let $fullName := concat("/db/menology/",$filename)
let $xml := doc($fullName)
let $xsl := doc("/db/menology/xsl/readFile.xsl")
return transform:transform($xml,$xsl,())
