xquery version "1.0";
let $supr := collection('/db/suprasliensis')//span[@class="os"]
return
<root>{
for $i in $supr
return
    <line folio="{substring(base-uri($i),19,8)}">{$i}</line>
}</root>