<p:pipeline xmlns:c="http://www.w3.org/ns/xproc-step" xmlns:p="http://www.w3.org/ns/xproc" xmlns:xproc="http://xproc.net/xproc" name="pipeline"><p:xquery><p:input port="query"><p:inline><c:query xproc:escape="true">
               let $r := 'this pipeline successfully processed'
               return
                  <test>{$r}</test></c:query></p:inline></p:input></p:xquery></p:pipeline>