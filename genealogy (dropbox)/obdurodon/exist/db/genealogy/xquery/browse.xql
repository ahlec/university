let $names := doc('/db/genealogy/genealogy.xml')//person[not(.//firstName eq "unknown")]/string-join(name/(* except source), " ")
let $people := (for $i in doc('/db/genealogy/genealogy.xml')//person[not(.//firstName eq "unknown")]
    let $joinedName := string-join($i/name/(* except source), " ")
    order by $joinedName, $i/dates/death/deathDate
    return $i)
for $index in (1 to count($people))
    let $i := $people[$index]
    let $joinedName := string-join($i/name/(* except source), " ")
    let $previousName := concat(if ($index gt 1) then (substring(string-join($people[$index -
        1]/name/(* except source), " "), 1, 1)) else ("~"), substring($joinedName, 1, 1))
    return (if (not(substring($previousName, 1, 1) = substring($previousName, 2, 1))) then(<h2>{substring($previousName, 2, 1)}</h2>) else (""),
        <a href="findPerson.php?person={$i/@id}">{normalize-space(concat(
        $i/name/firstName,
        " ",
        if ($i/name/epithet) then concat("&#8220;",$i/name/epithet,"&#8221;") else "",
        " ",
        $i/name/patronymic," ",
        if (not($i/@origin eq "Rus'" or $i/@origin eq "unknown")) then concat("of ",$i/@origin) else "",
    if (count($names[. eq $joinedName]) gt 1) then concat(" (d. ", $i/dates/death/deathDate, ")") else ""
        ))}</a>, <br /> )
