declare namespace request="http://exist-db.org/xquery/request";
declare function local:getPersonName($person as node()) as xs:string {
    let $name := concat(
        $person//firstName,
        " ",
        if ($person//epithet) then concat("“", $person//epithet, "”") else "",
        " ",
        $person//patronymic,
        if ($person//christian) then concat(" ", $person//christian) else "",
        if (not($person/@origin eq "unknown" or $person/@origin eq "Rus'")) then 
            concat(" of ", $person/@origin) else ""
        )
    return $name
};
let $date := number(request:get-parameter("date","1000"))
for $ruling in doc('/db/genealogy/genealogy.xml')//ruled
    let $ruler := $ruling/ancestor-or-self::person
    where (if (data($ruling/startYear/date) = "unknown") then(number($ruling/endYear/date) - 100)
        else(number($ruling/startYear/date))) <= $date and (if (data($ruling/endYear/date) = "unknown")
        then(number($ruling/startYear/date) + 100) else(number($ruling/endYear/date))) >= $date
    return (<div><a href="findPerson.php?person={$ruler/@id}">{local:getPersonName($ruler)}</a>, {data($ruling/title)} of <a
        href="findPlace.php?place={data($ruling/place)}">{data($ruling/place)}</a> (r. {if($ruling/startYear/date/@circa eq "circa")
        then("ca. ") else("")}{if (data($ruling/startYear/date) = "unknown") then("unknown")
        else(number($ruling/startYear/date))} - {if($ruling/endYear/date/@circa eq "circa")
        then("ca. ") else("")}{if (data($ruling/endYear/date) = "unknown") then("unknown") else(number($ruling/endYear/date))})</div>, <br />)