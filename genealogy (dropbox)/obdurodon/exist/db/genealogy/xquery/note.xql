let $person := request:get-parameter("person","vladimir1")
let $personNode := doc('/db/genealogy/genealogy.xml')//person[@id eq $person]
return transform:transform(doc(concat('/db/genealogy/notes/', $personNode/note/@link))/note,
    "xmldb:exist:///db/genealogy/notes/transformation.xsl", ())