declare namespace request="http://exist-db.org/xquery/request";
declare namespace transform="http://exist-db.org/xquery/transform";
declare function local:getPersonName($pointer as xs:string, $document as node()*) as xs:string {
    let $target := $document//person[@id eq $pointer]
    let $name := string-join((
        $target//firstName,
        if ($target//epithet) then(" “", $target//epithet, "”") else "",
        if ($target//patronymic) then (" ", $target//patronymic) else "",
        if ($target//christian) then (" ", $target//christian) else "",
        if (not($target/@origin eq "unknown" or $target/@origin eq "Rus'")) then 
            (" of ", $target/@origin) else ""
        ), "")
    return $name
};
declare function local:getFamily ($marriage as node(), $document as node()*) as node()* {
    for $i in $marriage//child return
        <li><a href="findPerson.php?person={$i/@pointer}">{local:getPersonName($i/@pointer, $document)}</a></li>
};
declare function local:formatDate ($date as node()) as xs:string
{
    let $dateString := string-join((
        if ($date/@circa eq "circa") then "ca. " else "",
        if ($date/@circa eq "pre-") then "before " else "",
        if ($date/@circa eq "post-") then "after " else "",
        $date/data(.)), "")
    return $dateString
};

let $person := request:get-parameter("person","vladimir1")
let $doc := doc('/db/genealogy/genealogy.xml')
let $current := $doc//person[@id eq $person]
let $footnotes := ()
let $name := 
    (local:getPersonName($person, $doc), if (count($current/name/source) > 0) then(<a href="#footnote_{count($footnotes) + 1}"
        id="text_{count($footnotes) + 1}" class="intext_footnote"><sup>[{count($footnotes) + 1}]</sup></a>) else())
let $footnotes := if (count($current/name/source) > 0) then($footnotes, <fn><location>Name</location><fn_p>{string-join(data($current/name/source),
    "; ")}</fn_p></fn>) else($footnotes)
let $dates :=
    <div class="dates{if (not($current/note)) then (' noNote') else ('')}">b. <span>{local:formatDate($current//birthDate/date)}</span> &#8211;
    d. <span>{local:formatDate($current//deathDate/date)}</span>{if (count($current//birth/source) + count($current//death/source) > 0)
    then(<a href="#footnote_{count($footnotes) + 1}" id="text_{count($footnotes) + 1}"
    class="intext_footnote"><sup>[{count($footnotes) + 1}]</sup></a>) else()}</div>
let $footnotes := ($footnotes, <fn><location>Birth/Death</location><fn_p>{string-join(data($current//*[name() = ('birth', 'death')]/source), "; ")}</fn_p></fn>)
let $flags := 
    (
    if ($current//father/@pointer) 
        then (<h3 class="sectionHeader">Father</h3>,<a class="intraLink"
            href="findPerson.php?person={$current//father/@pointer}">{local:getPersonName($current//father/@pointer, $doc)}</a>,
            if (count($current//father/source) > 0) then(<a href="#footnote_{count($footnotes) + 1}"
                    id="text_{count($footnotes) + 1}" class="intext_footnote"><sup>[{count($footnotes) + 1}]</sup></a>) else()) 
        else "",
    if ($current//mother/@pointer)
        then (<h3 class="sectionHeader">Mother</h3>,<a class="intraLink"
            href="findPerson.php?person={$current//mother/@pointer}">{local:getPersonName($current//mother/@pointer, $doc)}</a>,
            if (count($current//mother/source) > 0) then(<a href="#footnote_{count($footnotes) + 1 + (if (count($current//father/source) > 0) then(1) else(0))}"
                    id="text_{count($footnotes) + 1 + (if (count($current//father/source) > 0) then(1) else(0))}"
                    class="intext_footnote"><sup>[{count($footnotes) + 1 + (if (count($current//father/source) > 0) then(1) else(0))}]</sup></a>) else())
        else ""
    )
let $footnotes := ($footnotes, <fn><location>Father</location><fn_p>{string-join(data($current//father/source), "; ")}</fn_p></fn>)
let $footnotes := ($footnotes, <fn><location>Mother</location><fn_p>{string-join(data($current//mother/source), "; ")}</fn_p></fn>)
let $ruled := 
    if ($current//ruled)
        then (<h3 class="sectionHeader">Titles</h3>,<ul class="rulings">{for $i in $current//ruled return 
            <li>{$i/title/data(.)} of <a href="findPlace.php?place={$i/place}">{$i/place/data(.)}</a>
            (r. {local:formatDate($i/startYear/date)} &#8211; {local:formatDate($i/endYear/date)}){
                    if (count($i//source) > 0)
                    then(<a href="#footnote_{count($footnotes) + 1 + count($i/preceding::ruled[ancestor-or-self::person[@id = $person] and count(.//source) > 0])}"
                    id="text_{count($footnotes) + 1 + count($i/preceding::ruled[ancestor-or-self::person[@id = $person] and count(.//source) > 0])}"
                    class="intext_footnote"><sup>[{count($footnotes) + 1 +
                        count($i/preceding::ruled[ancestor-or-self::person[@id = $person] and count(.//source) > 0])}]</sup></a>) else()}</li>}</ul>)
        else ("")
let $footnotes := ($footnotes,
    (for $ruling in $current//ruled
        return <fn><location>{$ruling/title/data(.)} of {$ruling/place/data(.)}</location><fn_p>{string-join(data($ruling//source), "; ")}</fn_p></fn>))
let $marriages :=
    if (count(doc('/db/genealogy/genealogy.xml')//marriage[@female = $person or @male = $person]) gt 0)
        then (<h3 class="sectionHeader">Marriages</h3>,<ul class="marriages">{(
    for $i in doc('/db/genealogy/genealogy.xml')//marriage[@female = $person]
        let $spouse := $i/@male
        return (
            <li><a href="findPerson.php?person={$spouse}">{local:getPersonName($spouse, $doc)}</a>
                (m. {local:formatDate($i//date)}) {
                    if (count($i//source) > 0)
                    then(<a href="#footnote_{count($footnotes) + 1 + count($i/preceding::marriage[@female = $person and count(.//source) > 0])}"
                    id="text_{count($footnotes) + 1 + count($i/preceding::marriage[@female = $person and count(.//source) > 0])}"
                    class="intext_footnote"><sup>[{count($footnotes) + 1 +
                        count($i/preceding::marriage[@female = $person and count(.//source) > 0])}]</sup></a>) else()}
           {if ($i//child)
                then (<ul>{local:getFamily($i, $doc)}</ul>)
                else ("")}</li>),
    for $i in doc('/db/genealogy/genealogy.xml')//marriage[@male = $person]
        let $spouse := $i/@female
        return (
            <li><a href="findPerson.php?person={$spouse}">{local:getPersonName($spouse, $doc)}</a>
                (m. {local:formatDate($i//date)}) {
                    if (count($i//source) > 0)
                    then(<a href="#footnote_{count($footnotes) + 1 + count($i/preceding::marriage[@male = $person and count(.//source) > 0])}"
                    id="text_{count($footnotes) + 1 + count($i/preceding::marriage[@male = $person and count(.//source) > 0])}"
                    class="intext_footnote"><sup>[{count($footnotes) + 1 +
                        count($i/preceding::marriage[@male = $person and count(.//source) > 0])}]</sup></a>) else()}
            {if ($i//child) then <ul>{local:getFamily($i, $doc)}</ul> else ("")}</li>)
        )}</ul>)
        else ("")
let $footnotes := ($footnotes, for $marriage in doc('/db/genealogy/genealogy.xml')//marriage[@female = $person and count(.//source) > 0]
    return <fn><location>Marriage to {local:getPersonName(data($marriage/@male), $doc)}</location><fn_p>{string-join(data($marriage//source), "; ")}</fn_p></fn>)
let $footnotes := ($footnotes, for $marriage in doc('/db/genealogy/genealogy.xml')//marriage[@male = $person and count(.//source) > 0]
    return <fn><location>Marriage to {local:getPersonName(data($marriage/@female), $doc)}</location><fn_p>{string-join(data($marriage//source), "; ")}</fn_p></fn>)
let $noteDocument := doc(if ($current/note) then (concat('/db/genealogy/notes/', $current/note/@link))
    else('/db/genealogy/notes/empty.xml'))/note
let $noteOutput := transform:transform($noteDocument, "xmldb:exist:///db/genealogy/notes/transformation.xsl",
    <parameters><param name="footnoteOffset" value="{count($footnotes)}"/></parameters>)
let $footnotes := <footnotes>{($footnotes, $noteDocument//fn)}</footnotes>
let $footnoteOutput := transform:transform($footnotes,
    "xmldb:exist:///db/genealogy/notes/footnotes_transformation.xsl", ())
return (
<h2 class="name{if (not($current/note)) then(' noNote') else('')}">{$name}</h2>, $dates,
<div class="info{if ($current/note) then(' hasNote') else('')}">{$flags, $ruled, $marriages}(:<a
    href="familyTree.php?person={$person}" class="familyTree"><img src="images/family_tree.png" border="0"/></a>:)</div>,
    $noteOutput, $footnoteOutput
)