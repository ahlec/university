<?oxygen RNGSchema="notes.rnc" type="compact"?><?xml-model href="notes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><note><title>Evfrosinija M′stislavna</title><p><link target="evfrosiniia">Evfrosinija M′stislavna</link> was the youngest of the daughters
        of <link target="mstislav5">M′stislav Volodimerič</link> of Kyiv, born near the end of his
        reign to <link target="daughter-dmitrii-zavidich">his second wife</link>, the daughter of
            <link target="dmitrii">Dmitrij Zavidič of Novgorod</link>.<fn><fn_p>See above for M′stislav Volodimerič’s marriages.</fn_p></fn><link target="evfrosiniia">Evfrosinija</link>’s marriage is not explicitly recorded in
        any sources that I have been able to find, but there is plentiful ancillary evidence to
        identify <link target="evfrosiniia">her</link>, <link target="geza2">her husband</link>, and
        even her name. The best source on the marriage itself is Abbot Wilhelm’s genealogy of the
        Danish kings, which mentions that <link target="ingeborg">Ingeborg M′stislavna</link>’s
            <link target="evfrosiniia">sister</link> was married to <link target="geza2">a Hungarian
            king</link> and was the mother of Béla, the then current Hungarian king.<fn><fn_p>Wilhelm, <articleTitle>"Wilhelmi abbatis genealogia regum Danorum,"</articleTitle>
                182.</fn_p></fn>From the date of composition of the genealogy, it is easy to see that the current
        Hungarian king was Béla III, whose father was <link target="geza2">Géza II</link>, and thus
            <link target="ingeborg">Ingeborg</link>’s <link target="evfrosiniia">sister</link> was
        married to <link target="geza2">Géza II</link>. The name of <link target="ingeborg">Ingeborg</link>’s <link target="evfrosiniia">sister</link> was not recorded by Abbot
        Wilhelm or by any Rusian chronicle, but another child of hers did record her <link target="evfrosiniia">mother</link>’s name. Wertner cites a Latin document of Elisabeth,
        daughter of <link target="geza2">Géza II</link>, in which she refers to her mother as
        “Eurosine,” from whence we get the Rusian Evfrosinija.<fn><fn_p>Wertner, <bookTitle>Az Árpádok családi története</bookTitle>, 314.</fn_p></fn>This type of a name also confirms the Slavic ancestry of Elisabeth’s <link target="evfrosiniia">mother</link> and <link target="geza2">Géza II</link>’s wife,
        adding to the identification of Abbot Wilhelm. Though the Rusian sources do not mention
            <link target="evfrosiniia">Evfrosinija</link> explicitly there are multiple mentions of
            <link target="geza2">her husband</link>. <link target="iziaslav3">Izjaslav
            M′stislavič</link>, <link target="evfrosiniia">Evfrosinija</link>’s brother, had become
        ruler of Kyiv in 1146,<fn><fn_p><bookTitle>NPL</bookTitle>, s.a. 1146; <bookTitle>Hypatian Chronicle</bookTitle>,
                s.a. 1146; <bookTitle>Laurentian Chronicle</bookTitle>, s.a. 1146;
                    <bookTitle>NČL</bookTitle>, s.a. 1146; <bookTitle>Voskresenskij
                    Chronicle</bookTitle>, s.a. 1146; <bookTitle>Rogozhskij Chronicle</bookTitle>,
                s.a. 1146; <bookTitle>Tver Chronicle</bookTitle>, s.a. 1146; <bookTitle>Nikon
                    Chronicle</bookTitle>, s.a. 1146.</fn_p></fn>and in at least one of <link target="iziaslav3">his</link> many battles to retain <link target="iziaslav3">his</link> power he called on his zjat′ the king of Hungary.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1149.</fn_p></fn>Furthermore, there is a fascinating note in the Hypatian chronicle under the year 1155,
        when <link target="vladimir7">Volodimer M′stislavič</link>, the youngest of the
        M′stislaviči, sent his mother to Hungary to his zjat′ the king, essentially because she is
        the Hungarian king’s mother–in–law (teščia).<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1155.</fn_p></fn>These small pieces of evidence built around Abbot Wilhelm’s identification of the
        marriage show a complete picture in which <link target="evfrosiniia">Evfrosinija
            M′stislavna</link> married <link target="geza2">Géza II of Hungary</link>.</p><p>The purpose of the marriage also becomes clear from the evidence that has been presented
        here. <link target="iziaslav3">Izjaslav M′stislavič</link> needed to maintain <link target="iziaslav3">his</link> throne against a variety of claimants and <link target="iziaslav3">he</link> needed allies to do that. In 1149 <link target="iziaslav3">he</link> not only called on <link target="iziaslav3">his</link> zjat′ from Hungary,
        but various svat′(s) from Poland, for military assistance.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1149.</fn_p></fn><link target="geza2">Géza II</link> intervened multiple times to help his <link target="iziaslav3">brother–in–law</link> remain in Kyiv and thus the marriage was well
        arranged by <link target="iziaslav3">Izjaslav</link> to advance his and the M′stislaviči’s
        position in Rus′. This dynastic marriage also had an interesting additional angle, part of
        which was the role of <link target="boris4">Boris</link>, son of <link target="koloman">Koloman of Hungary</link> and grandson of <link target="vladimir3">Volodimer
            Monomax</link>. <link target="boris4">Boris</link>, as mentioned above under his
        mother’s, <link target="evfimiia">Evfimija</link>’s, entry, fought for most of his life to
        attempt to regain what he considered his birthright, the Hungarian throne. In those battles,
        he allied with the Byzantine emperors, the Comneni, and fought against <link target="geza2">Géza II</link>.<fn><fn_p>Kinnamos, 93.</fn_p></fn>For <link target="iziaslav3">Izjaslav</link>, this was probably not even part of his
        consideration, as he was also fighting off Monomaxoviči uncles, such as Jurij of Suzdal′, to
        maintain his throne.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1146, for example. Franklin and
                Shepard discuss the situation well. Shepard and Franklin, <bookTitle>The Emergence
                    of Rus 750–1200</bookTitle>, 330.</fn_p></fn>The marriage of <link target="evfrosiniia">Evfrosinija</link> and <link target="geza2">Géza II</link> was part of a much larger struggle for power in which Jurij of Suzdal′,
        Volodimerko of Halyč, and the Comneni emperors were allied against the M′stislaviči, <link target="geza2">Géza II</link> of Hungary, and the Boleslaviči of Poland. This enormous
        struggle was played out for control over the kingdoms of Hungary, Rus′, and Poland, with
        only the Byzantine Empire safe. Understanding the place of one small dynastic marriage in
        such a morass of politics, infighting, and war is essential to understanding the place of
        Rus′ in Europe as a whole.</p></note>