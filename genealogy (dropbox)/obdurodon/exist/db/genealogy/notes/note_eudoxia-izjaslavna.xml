<?oxygen RNGSchema="notes.rnc" type="compact"?><?xml-model href="notes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><note><title>Eudoxia Izjaslavna</title><p>The name of the <link target="evdoksia">daughter</link> of <link target="iziaslav2">Izjaslav
            Jaroslavič</link> is not properly known, but she is called <link target="evdoksia">Eudoxia</link> because of a brief reference to that name in a version of Długosz’s
        chronicle, one of the only sources for <link target="evdoksia">her</link> marriage.<fn><fn_p>Długosz, <bookTitle>Annales</bookTitle>, 161, see especially note 5.</fn_p></fn>As with many other Rusian royal daughters, <link target="evdoksia">she</link> is not
        mentioned at all in the PVL. However, due to <link target="evdoksia">her</link> marriage to
            <link target="mieszko3">a Polish prince</link>, <link target="evdoksia">her</link>
        existence is known and the knowledge of <link target="evdoksia">her</link> dynastic marriage
        has survived.</p><p>In 1088 both <link target="wladyslaw">Władysław Herman, duke of Poland</link>, and his nephew
            <link target="mieszko3">Mieszko</link>, son of <link target="boleslaw2">Bolesław
            II</link> of Poland, took brides. This was the third marriage for <link target="wladyslaw">Władysław</link>, but only the first for <link target="mieszko3">Mieszko</link>, who was nineteen and had been called back from Hungary expressly for
        the purpose. <link target="mieszko3">He</link> had been living in Hungary and being raised
        by <link target="ladislaus">King Ladislaus</link> since <link target="mieszko3">his</link>
        father <link target="boleslaw2">Bolesław II</link>’s death, who died in exile there between
        1079 and 1081.<fn><fn_p><bookTitle>Gesta principum Polonorum</bookTitle>, 101.</fn_p></fn><link target="wladyslaw">Władysław</link> called his <link target="mieszko3">nephew</link> back from Hungary because the Rusians wanted to make a dynastic marriage
        with the Poles,<fn><fn_p>Długosz, <bookTitle>Annales</bookTitle>, 161.</fn_p></fn>and he was already set to marry <link target="judith">Judith</link>, the widow of <link target="salomon">Salomon of Hungary</link> and the daughter of <link target="henry3">Emperor Henry III</link>. Długosz, in his chronicle, expressly refers to the fact that
            <link target="sviatopolk2">Svjatopolk Izjaslavič</link> had long held an interest in
        dynastic marriage, one of the few such overt references on the subject.<fn><fn_p>Ibid. “Quatenos Russie regiones a patre suo subacte hac affinitate magis sinceres
                et devote in fide persisterent,” emphasis added.</fn_p></fn>While her name is not known, Gallus Anonymous refers to <link target="mieszko3">Mieszko</link>’s bride as “a girl from Ruthenia,”<fn><fn_p><bookTitle>Gesta principum Polonorum</bookTitle>, 101.</fn_p></fn>while Długosz is more explicit, “the <link target="evdoksia">sister</link> of <link target="sviatopolk2">Duke Svjatopolk of Kyiv</link>.”<fn><fn_p>Długosz, <bookTitle>Annales</bookTitle>, 161.</fn_p></fn>Unfortunately, nothing more is known of <link target="evdoksia">this Izjaslavna</link>,
        as <link target="mieszko3">Mieszko</link> died (possibly poisoned) the next year in 1089,<fn><fn_p><bookTitle>Gesta principum Polonorum</bookTitle>, 101–3.</fn_p></fn>and his line passed out of the Polish chronicles. It is possible to assume that <link target="evdoksia">Eudoxia</link> returned home to Kyiv to live or potentially joined a nunnery,<fn><fn_p>Długosz records a belief that Mieszko and Evdoksia joined a monastery together in
                Poland. Długosz, <bookTitle>Annales</bookTitle>, 214.</fn_p></fn>but the Rusian chronicles maintained their silence in regard to women and her fate is
        unknown.</p><p>The most notable feature of this marriage is the fact that one chronicler records explicitly
        the purpose of the wedding as the creation of ties with the neighboring kingdom of Rus′.
        However, the question remains, who arranged the marriage and why? In 1088 <link target="vsevolod2">Vsevolod Jaroslavič</link> was ruling in Kyiv, not <link target="sviatopolk2">Svjatopolk</link>, and only a few years before, <link target="sviatopolk2">Svjatopolk</link>’s brother <link target="iaropolk2">Jaropolk</link> had fled to Poland after rising up against <link target="vsevolod2">Vsevolod</link>. He may have even attempted to press <link target="wladyslaw">Władysław
            Herman</link> for assistance in claiming the throne of Kyiv, and only returned to make
        peace when that failed. <link target="vsevolod2">Vsevolod</link> then may have wanted to
        make an agreement with <link target="wladyslaw">Władysław</link> to prevent such things from
        happening in the future, but if so, an Izjaslaviči would not have been the instrument he
        would have chosen. Which leads us to the other occurrence of 1088: following his brother’s
        death in 1087, <link target="sviatopolk2">Svjatopolk</link> moved from Novgorod, a premier
        spot in the realm, to Turov, perhaps the family seat but still less prestigious than
        Novgorod though much closer to Kyiv.<fn><fn_p><bookTitle>PVL</bookTitle>, s.a. 1088.</fn_p></fn>The speculation of historians on this move has generally led to the consensus that he
        wanted to solidify his familial power base at Turov and be near to Kyiv should old age
        finally claim <link target="vsevolod2">Vsevolod</link>. A marriage tie with <link target="wladyslaw">Władysław Herman</link> would enhance <link target="sviatopolk2">Svjatopolk</link>’s position by reinforcing his Izjaslaviči relations with their most
        powerful ally, as well as perhaps keeping his back safe should he attempt to move on Kyiv.
        This reasoning is admittedly speculative, but the marriage of <link target="evdoksia">a
            Izjaslavna</link> and <link target="mieszko3">a Polish prince</link> at this time fits
        the political situation within Rus′ in such a manner that it seems to be a logical
        conclusion.</p></note>