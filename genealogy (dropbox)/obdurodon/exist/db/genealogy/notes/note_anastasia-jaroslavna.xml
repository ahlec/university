<?oxygen RNGSchema="notes.rnc" type="compact"?><?xml-model href="notes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><note><title>Anastasia Jaroslavna</title><p>In the 1030s Rus′ was home to another group of exiled royalty, the Hungarian princes <link target="andrew">Andrew</link> and Levente.<fn><fn_p><bookTitle>Chronica de gestis Hungarorum</bookTitle>, 111.</fn_p></fn>They had fled Hungary after King Stephen told them that their lives were in danger upon
        his death.<fn><fn_p>Ibid., 107. </fn_p></fn>Their other brother, <link target="bela1">Béla</link>, had originally fled with them,
        but stayed at their first stop in Poland where he had married the daughter of <link target="mieszko2">Mieszko II</link>, the ruler of Poland.<fn><fn_p>Ibid., 110–11.</fn_p></fn><link target="iaroslav1">Jaroslav</link> and his court took in these exiles, as they
        had others, with their own interests in mind. Interestingly, Dimitri Obolensky states that
            <link target="iaroslav1">Jaroslav</link> even had <link target="andrew">Andrew</link>
        baptized while he was in Kyiv,<fn><fn_p>Dimitri Obolensky, <bookTitle>The Byzantine Commonwealth: Eastern Europe
                    500–1453</bookTitle> (London: Weidenfeld and Nicolson, 1971), 159. </fn_p></fn>though there is no primary source attesting to it. Levente is known in the Hungarian
        chronicles as lapsing back into paganism and leading a pagan revolt in Hungary.<fn><fn_p><bookTitle>Chronica de gestis Hungarorum,</bookTitle> 113.</fn_p></fn>However, it is difficult to imagine that their parents would not have raised them as
        Christian, as their mother was a Rusian princess, as noted above.<fn><fn_p>Though there was a pagan resurgence in Hungary around the time of King Stephen’s
                death and this may have affected the brothers’ religious beliefs. Ibid.</fn_p></fn>Regardless of whether or not <link target="iaroslav1">Jaroslav</link> had <link target="andrew">Andrew</link> baptized, <link target="iaroslav1">Jaroslav</link> did
        marry one of his daughters to <link target="andrew">Andrew</link>.<fn><fn_p>Ibid. Adam of Bremen explicitly mentions the marriage as well. Adam of Bremen,
                xiii.12; schol. 62 (63).</fn_p></fn>As with many of the dynastic marriages <link target="iaroslav1">Jaroslav</link>
        arranged in this period, the marriage of <link target="andrew">Andrew</link> and <link target="anastasia">Anastasia</link> was a gamble. It was, at the time, unsure whether or
        not <link target="andrew">Andrew</link> would return to rule his country. There were
        multiple foreign policy implications at stake. The first and most basic was that it would
        benefit Rus′ to have a friendly ruler in a neighboring country, especially in a country that
        neighbored the steppe as well. Perhaps just as important was that <link target="andrew">Andrew's</link> next youngest brother was married to the daughter of the ruler of the
        Poles, and should he succeed to the throne the Poles would be in a stronger position than
        the Rusians vis–à–vis Hungary. Despite increasing ties with Poland, this was probably an
        unacceptable outcome for <link target="iaroslav1">Jaroslav</link>. Because of these factors
        it is possible to imagine that Rusian forces might have been used to assist <link target="andrew">Andrew</link> in claiming the Hungarian throne for himself, though this
        is not recorded in the primary sources.<fn><fn_p>As some historians have done before, including Z. J. Kosztolynik,
                    <bookTitle>Hungary under the Early Árpáds, 890s to 1063</bookTitle> (Boulder,
                Co.: East European Monographs, 2002), 398.</fn_p></fn></p><p><link target="andrew">Andrew</link> returned to Hungary in 1046 with his wife <link target="anastasia">Anastasia</link>. Not much is recorded of their rule there, though it
        is known that <link target="anastasia">Anastasia</link> had an influential role in the
        kingdom. They had three children, <link target="salomon">Salomon</link>, <link target="david3">David</link>, and <link target="adelheid">Adelheid</link>, and as is
        discussed elsewhere,<fn><fn_p>Raffensperger. <articleTitle>"Rusian Influence on European Onomastic
                    Traditions."</articleTitle></fn_p></fn><link target="anastasia">Anastasia</link> was instrumental in naming these children.
        János Bak, in his work on Hungarian queens, has stated that foreign queens were vital for
        the cultural life of medieval Hungary,<fn><fn_p>János M. Bak, <articleTitle>"Roles and Functions of Queens in Árpádian and Angevin
                    Hungary (1000–1386 A.D.),"</articleTitle> in <bookTitle>Medieval
                    Queenship</bookTitle>, ed. John Carmi Parsons (Gloucestershire: Sutton
                Publishing, 1994), 14.</fn_p></fn>and though <link target="anastasia">her</link> effect on Hungarian cultural life cannot
        be proven at this point, there are indeed multiple examples of the influence <link target="anastasia">she</link> had on her <link target="salomon">eldest son</link>’s
        career as king and also as exile.</p><p><link target="salomon">Salomon</link> was betrothed to <link target="judith">Judith</link>,
        the daughter of the late German Emperor <link target="henry">Henry III</link> and sister to
        Emperor <link target="henry4">Henry IV</link>, in 1058.<fn><fn_p><bookTitle>Chronica de gestis Hungarorum,</bookTitle> 115.</fn_p></fn>Like many dynastic marriages, it sealed a peace between the two countries after a
        period of war. Later that same year, <link target="andrew">Andrew</link> took ill and had
        his five–year–old son, <link target="salomon">Salomon</link>, crowned king alongside him.<fn><fn_p>Ibid.</fn_p></fn>This was in contravention of existing Hungarian practice, which, like Rus′, practiced
        collateral succession. The combination of <link target="salomon">Salomon's</link> connection
        with the hated Germans and the crowning of a child when <link target="andrew">Andrew</link>
        had a perfectly healthy brother alienated the Hungarians, and after <link target="andrew">Andrew's</link> death in 1060, <link target="salomon">Salomon</link> and <link target="anastasia">Anastasia</link> were forced to flee to the court of German Emperor
            <link target="henry4">Henry IV</link>.<fn><fn_p>Kosztolynik, <bookTitle>Hungary under the Early Árpáds, 890s to 1063,</bookTitle>
                376–77.</fn_p></fn>As <link target="henry4">Henry IV</link> was still a minor, his regency was managed by
        his and <link target="judith">Judith's</link> mother Agnes, who assisted <link target="anastasia">Anastasia</link> and her son–in–law <link target="salomon">Salomon</link>. Agnes gave <link target="anastasia">Anastasia</link> one of her estates
        in Austria for herself and kept <link target="salomon">Salomon</link> and <link target="judith">Judith</link> with her at the imperial court.<fn><fn_p>Ibid., 376.</fn_p></fn>This did not stop <link target="anastasia">Anastasia</link> from maneuvering on behalf
        of her son. Lambert of Hersfeld records that in 1063 <link target="anastasia">Anastasia</link> gave a Hungarian royal relic, the Sword of Attila, to Otto of
        Nordheim, duke of Bavaria, with the express intention of convincing him to aid her son in
        the reconquest of the kingdom which she viewed as rightfully his.<fn><fn_p>Lambert of Hersefeld, s.a. 1071.</fn_p></fn>Otto was at that time a member of <link target="henry4">Henry IV</link>’s regency and
        thus had some power over imperial policy. Later that same year imperial troops accompanied
        by <link target="salomon">Salomon</link> and some of his loyal Hungarian troops who had come
        with him and his mother to the German Empire returned to Hungary and took over.<fn><fn_p><bookTitle>Chronica de gestis Hungarorum</bookTitle>, 117, where his arrival is
                recorded after the death of Béla in 1063. Note 331 makes it clear that the arrival
                of the army happened before Béla’s death, though an easier victory may have come
                because of it.</fn_p></fn><link target="anastasia">Anastasia</link> is irregularly mentioned as advising her son
        during his tumultuous reign in (and out of) Hungary, in which he battled with his cousin
        Géza. <link target="anastasia">She</link> usually counseled peace between the cousins and
        urged <link target="salomon">Salomon</link> to be content with his role as king and accept
        his cousin as a duke.<fn><fn_p>Ibid., 125.</fn_p></fn>This advice was disregarded, and <link target="salomon">Salomon</link> preferred to
        attempt to drive his cousin out of the kingdom. It is recorded that <link target="anastasia">she</link>, as well as his wife <link target="judith">Judith</link>,<fn><fn_p>After Salomon’s death, Judith was married to Władysław Herman of Poland, securing
                an alliance between the Poles and the German Empire. Great Chronicle, ch. 15,
                    <bookTitle>Gesta Principum Polonorum,</bookTitle> Bk. 2, ch. 1.</fn_p></fn>traveled with <link target="salomon">Salomon</link> through Hungary,<fn><fn_p><articleTitle>Chronica de gestis Hungarorum,</articleTitle> 125.</fn_p></fn>presumably with the intention of advising him.</p><p>A short aside on the strength of the Rusian–Hungarian ties ensured by <link target="anastasia">Anastasia</link>’s marriage is the story of Ladislaus, brother of
        Géza and cousin to <link target="salomon">Salomon</link>, who came to Rus′ for aid. In the
        early 1070s Géza needed assistance in his war against <link target="salomon">Salomon</link>,
        and to that end he sent his brother Ladislaus to Rus′ to petition for aid.<fn><fn_p>Ibid., 121. Also <articleTitle>"Chronici Hungarici,"</articleTitle> 377.</fn_p></fn>Géza and Ladislaus were the grandchildren of <link target="premislava">N. N.
            Volodimerovna</link> and the first <link target="ladislaus">Ladislaus</link>, but that
        could not compete with the ties <link target="salomon">Salomon</link> had through both the
        Volodimerovna and his mother <link target="anastasia">Anastasia Jaroslavna</link>, so
        Ladislaus was turned away.<fn><fn_p>Chronica de gestis Hungarorum, 122.</fn_p></fn>As in many cases, we do not have the details from either the Hungarian or Rusian
        chronicles, but the evident marital ties make backing anyone against <link target="salomon">Salomon</link>, nephew of the Rusian ruler, unlikely.<fn><fn_p>Whoever was prince of Kyiv at the time of Ladislaus’s arrival in the early 1070s,
                all the Jaroslaviči were uncles of Salomon.</fn_p></fn><link target="anastasia">Anastasia</link> died in 1074, the same year that her son
            <link target="salomon">Salomon</link> was expelled for the last time from Hungary.<fn><fn_p>Wertner, <bookTitle>Az Árpádok családi története,</bookTitle> chart.</fn_p></fn><link target="anastasia">She</link> was laid to rest at Admont Abbey in Austria where
        she had stayed at times while <link target="salomon">Salomon</link> was warring.<fn><fn_p><bookTitle>Chronica de gestis Hungarorum</bookTitle>, 129; and
                    <articleTitle>"Chronici Hungarici,"</articleTitle> 411.</fn_p></fn>Her life was, unfortunately, not very well documented by contemporary historians, but
        her actions still come through nevertheless. She was able to influence the selection of
        names for her children, something rare in the Middle Ages, and she was a counselor to her
            <link target="andrew">husband</link> and to her <link target="salomon">son</link>,
        helping the latter to regain and hold Hungary. This was quite a life for a woman whose name
        or very existence is not recorded by the chroniclers in the land of her birth. But the
        dynastic marriage did seem to ally Hungary with Rus′. <link target="salomon">Salomon</link>
        was able to count on Rusian aid, or the lack thereof for a rival, at least twice in his
        lifetime that we know of from Hungarian accounts. This shows that the Rusians must also have
        been receiving something in return for them to continue to support the embattled Hungarian
        ruler, even if he was a first cousin to the Jaroslaviči who ruled Kyiv contemporaneously.
    </p></note>