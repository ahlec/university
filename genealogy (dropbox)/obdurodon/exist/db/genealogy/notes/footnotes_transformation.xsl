<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xh="http://www.w3.org/1999/xhtml" version="1.0"><xsl:template match="/"><h2 class="notes">Footnotes</h2><ol class="notes"><xsl:apply-templates select="//fn" mode="fn"/></ol>
        <!--<p>
            <a href="query.php">Return to
            search page</a> | <a href="sources.php">Sources</a> | <a href="about.php">About this
            site</a>
            </p>--></xsl:template><xsl:template match="bookTitle"><cite><xsl:apply-templates/></cite></xsl:template><xsl:template match="articleTitle"> “<xsl:apply-templates/>” </xsl:template><xsl:template mode="fn" match="fn">
        <!--<xsl:param name="fn_number">
            <xsl:number level="any"/>
            </xsl:param>--><li id="footnote_{position()}"><xsl:apply-templates mode="fn"><xsl:with-param name="fn_number" select="position()"/></xsl:apply-templates><a href="#text_{position()}" class="return" onclick="returnToText();">[↑]</a></li></xsl:template><xsl:template match="fn/location" mode="fn"><b><xsl:apply-templates/></b>: 
    </xsl:template><xsl:template match="fn/fn_p" mode="fn"><xsl:param name="fn_number"/><xsl:apply-templates><xsl:with-param name="fn_number" select="$fn_number"/></xsl:apply-templates></xsl:template><xsl:template match="link"><a href="findPerson.php?person={@target}"><xsl:apply-templates/></a></xsl:template></xsl:stylesheet>