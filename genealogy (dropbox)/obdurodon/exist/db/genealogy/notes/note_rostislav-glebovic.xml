<?oxygen RNGSchema="notes.rnc" type="compact"?><?xml-model href="notes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><note><title>Rostislav Glěbovič</title><p>Five of the thirteen internal Rusian dynastic marriages involve rulers from the Polack
        family, originating from <link target="iziaslav1">Izjaslav Volodimerič</link>. In large part
        this is because Polack went its own separate way much of this time, however the simple fact
        that the vast majority of Rjurikids in Rus′ were descended from <link target="iaroslav1">Jaroslav</link>, created a separation that allowed for acceptable (ie.
        non–consanguineous) marriages sooner than with other branches of the larger Rjurikid family.
        The marriage of <link target="rostislav8">Rostislav Glěbovič</link> and <link target="sofia">Sofia Jaroslavna</link>, daughter of <link target="iaroslav4">Jaroslav
            Svjatopolčič</link>, was one of those marriages between Polack and the more populous
            <link target="iaroslav1">Jaroslav Volodimerič</link> line of Rjurikids, but by strict
        standards it was, in fact, consanguineous. The couple shared a great–grandfather, <link target="iziaslav2">Izjaslav Jaroslavič</link>, and were thus related in six degrees, one
        shy of the minimum requirement for marriage. This is a rare thing for Rusian internal
        marriages and bears mentioning because of that fact. When Rusians marry outside of Rus′ one
        common explanation has been that they are serving to broaden the pool of consanguineous
        marriage partners.<fn><fn_p>Bouchard. <articleTitle>“Consanguinity and Noble Marriages in the Tenth and
                    Eleventh Centuries.”</articleTitle></fn_p></fn>This has not always been the case, as there are multiple consanguineous marriages
        between Rusian and Polish couples, as well as Rusian and Hungarian couples. Whether the
        Rusians cared about either the Latin or Constantinopolitan rules about consanguinity is
        unclear, and whether they cared or not may have been trumped by politics, as was so often
        the case in the rest of Europe, but the observance or lack thereof of religious rules and
        norms presents an interesting case study, and one must ask why the rule or norm was
        broken.</p><p>This marriage seems to be part of a larger web of alliances that is being created as Polack
        is drawn back into larger Rusian politics. The marriages of <link target="gleb4">Glěb
            Vseslavič</link> and <link target="briacheslav3">Brjačeslav Davydič</link> which
        preceded this,<fn><fn_p>Both marriages are discussed above.</fn_p></fn>set the tone of Polack being drawn either into the Izjaslaviči, children of <link target="iziaslav2">Izjaslav Jaroslavič</link>, or M′stislaviči, children of <link target="mstislav5">M′stislav Volodimerič</link>, camps (broadly construed). <link target="gleb4">Glěb</link>’s son <link target="rostislav8">Rostislav</link>, continues
        his own alliance with the Izjaslaviči, marrying a daughter of <link target="iaroslav4">Jaroslav Svjatopolčič</link> who had such trouble with the M′stislaviči, including his
        own failed marriage to a M′stislavna. The date of the marriage is difficult, if not
        impossible, to determine based on existing evidence. <link target="iaroslav4">Jaroslav
            Svjatopolčič</link> died in 1123,<fn><fn_p><bookTitle>Laurentian Chronicle</bookTitle>, s.a. 1123; <bookTitle>Hypatian
                    Chronicle</bookTitle>, s.a. 1123; <bookTitle>Voskresenskij
                Chronicle</bookTitle>, s.a. 1123; <bookTitle>Nikon Chronicle</bookTitle>, s.a.
                1123.</fn_p></fn>and there is little information on <link target="rostislav8">Rostislav Glěbovič</link>
        before his assumption of rule in 1151.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1151.</fn_p></fn>But an earlier date of marriage, perhaps in the 1120s is probably correct. It is likely
        that <link target="rostislav8">Rostislav</link>’s <link target="unknown-daughter-iaropolk2">mother</link> arranged the marriage, or at least participated heavily, after <link target="gleb4">his father</link>’s death in 1119.<fn><fn_p><bookTitle>Laurentian Chronicle</bookTitle>, s.a. 1119; <bookTitle>Hypatian
                    Chronicle</bookTitle>, s.a. 1119; <bookTitle>Voskresenskij
                Chronicle</bookTitle>, s.a. 1119; <bookTitle>Nikon Chronicle</bookTitle>, s.a.
                1119.</fn_p></fn><link target="unknown-daughter-iaropolk2">She</link> was the ruler of Minsk following
            <link target="gleb4">Glěb</link>’s death, and was a member of the Izjaslaviči, thus
        putting her in a perfect position to arrange such a marriage to bolster her natal and
        marital families. There is, unfortunately, a potential problem with this theory of alliances
        that needs to be enumerated. In the entry for 1158, <link target="iziaslav3">Izjaslav
            M′stislavič</link> makes war on <link target="iurii2">Jurij Jaroslavič</link> in Turov.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1158. I think it is coincidence,
                but should note that this is also the year that Sofia Jaroslavna, Rostislav’s wife,
                dies. </fn_p></fn>This is, in a larger context, a continuation of the M′stislaviči v. Izjaslaviči battles
        of the early century. To do so, he gathers a broad coalition including the people of Polack,
        a city which was ruled by <link target="iurii2">Jurij</link>’s brother–in–law <link target="rostislav8">Rostislav Glěbovič</link>. However, the chronicle entry further
        states that the Polack people support the war because of proximity and no ruler is ever
        mentioned for Polack, unlike all of the other forces assembled, and when defeated <link target="iurii2">Jurij</link> flees to Izjaslavl′ which was ruled by his other
        brother–in–law, <link target="vsevolod7">Vsevolod Glěbovič</link>.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1158. For Vsevolod’s rule in
                Izjaslavl′ see Hypatian Chronicle, s.a. 1159.</fn_p></fn>Thus, the single entry presents a conundrum. Why would the people of Polack support a
        war against their ruler’s brother, and could they even do so without his favor? Further, why
        would <link target="iurii2">Jurij</link> flee to his <link target="vsevolod7">brother–in–law</link> after forces from another <link target="rostislav8">brother–in–law</link> had just attacked him? Perhaps the entry for 1159 in which the
        people of Polack throw <link target="rostislav8">Rostislav</link> out, in favor of <link target="rogvolod3">Rogvolod Borisič</link> (perhaps not coincidentally married to <link target="daughter-iziaslav3">a daughter</link> of <link target="iziaslav3">Izjaslav
            M′stislavič</link>) helps explain the situation.<fn><fn_p><bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1159.</fn_p></fn>The boyars of Polack may have acted semi–independently in making war on <link target="iurii2">Jurij Jaroslavič</link> in 1158 and then worked to expel <link target="rostislav8">Rostislav Glěbovič</link> in 1159 in favor of a ruler who was better
        connected in the current political climate. Such would be a move worthy of the people of
        Novgorod who replaced their rulers with great frequency based on the strength of the ruler
        in Kyiv, or lack thereof. Though this does not provide us with concrete answers, it does
        begin to weave Polack back into the larger fabric of Rus′ through dynastic marriage.</p></note>