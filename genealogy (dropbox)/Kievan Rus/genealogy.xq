xquery version "1.0";

declare namespace util="http://exist-db.org/xquery/util";
declare namespace request="http://exist-db.org/xquery/request";

let $target := request:request-parameter("target","vladimir1")

let $document := doc("/db/genealogy/genealogy.xml")
let $current := doc("/db/genealogy/genealogy.xml")//person[@id eq $target]
let $marriages := doc("/db/genealogy/genealogy.xml")//marriage[@male eq $target or @female eq $target]

return
<html>
<head>
<title>Genealogy: Person report</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link href="stylesheets/ocs.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<h1>
{data($current//firstName)} 
{if ($current//epithet) then <span> “{data($current//epithet)}” </span> else " "}
{data($current//patronymic)}
{if ($current//christian) then <span> ({data($current//christian)}) </span> else " "}
(b. <span title="{data($current//birth/source)}">{data($current//birthDate/date)}</span>–<span title="{data($current//death/source)}">d. {data($current//deathDate/date)}</span>)
{if ($current//note) then <span> (<a href="{$current//note/data(@link)}" target="_blank">note</a>) </span> else ""}
</h1>

{if ($current//ruled) then <h2>{data($current//title)} of {data($current//place)} 
(r. <span title="{data($current//startYear/source)}">{data($current//startYear/date)}</span>–<span title="{data($current//endYear/source)}">{data($current//endYear/date)}</span>)</h2> else ""}

{
for $i in $document//person[not(@id eq $target)]
where $i/@id = $marriages/@female or $i/@id = $marriages/@male
return 
<div>
<h3>
<a href="genealogy.xq?target={$i/@id}">{data($i//firstName)}</a>
(m. <span title="{$marriages[(@male eq $target and @female eq $i/@id) or (@female eq $target and @male = $i/@id)]//source}">
{$marriages[(@male eq $target and @female eq $i/@id) or (@female eq $target and @male = $i/@id)]//date}</span>)
</h3>
<ul>{for $j in $marriages
where (@male eq $target and @female eq $i/@id) or (@female eq $target and @male eq $i/@id) 
return <span>{for $k in $document//person
where $k/@id = $j//child/data(@pointer)
return <li><a href="genealogy.xq?target={$k/@id}">{$k//firstName}</a></li>}</span>}</ul>
</div>
}

</body>
</html>
