<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="notes.rnc" type="compact"?>
<?xml-model href="notes.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<note>
    <title>M′stislav “Harald” Volodimerič</title>
    <p><link target="mstislav5">M′stislav</link> was the son of <link target="vladimir3">Volodimer
            Monomax</link>, and over the course of his life occupied the two premier spots in the
        Rusian hierarchy as ruler of Novgorod and of Kyiv. <link target="mstislav5">M′stislav</link>
        also married twice, once a dynastic marriage with <link target="kristin">a foreign
            princess</link>, and a second time with <link target="daughter-dmitrii-zavidich">a
            Rusian woman</link>.</p>
    <p>The date for <link target="mstislav5">M′stislav</link>’s first marriage is not known
        precisely, though Baumgarten following Tatiščev chooses to date it to the year 1095,<fn>
            <fn_p>Baumgarten, <articleTitle>"Généalogies,"</articleTitle> 22–23, table V.</fn_p>
        </fn>the year of <link target="mstislav5">M′stislav</link>’s inception as ruler of Novgorod.<fn>
            <fn_p><bookTitle>PVL</bookTitle>, s.a. 1095.</fn_p>
        </fn>At approximately this time <link target="mstislav5">M′stislav</link> married <link
            target="kristin">Kristín</link>, the daughter of King Inge Steinkelsson of Sweden.<fn>
            <fn_p>Wilhelm, <articleTitle>"Wilhelmi abbatis genealogia regum Danorum,"</articleTitle>
                182.</fn_p>
        </fn>Typically, the Rusian chronicles record none of these details, though the NPL does
        record <link target="kristin">her</link> name, “Xristina.”<fn>
            <fn_p><bookTitle>NPL</bookTitle>, s.a. 1122.</fn_p>
        </fn>Even the Scandinavian sources that record the marriage do not record the context.
        Sturluson simply mentions it as part of the genealogy of the marriage of <link
            target="malfrid">Malfrid</link> and <link target="sigurd">King Sigurd of Norway</link>,<fn>
            <fn_p>Sturluson, <bookTitle>Heimskringla</bookTitle>, 702.</fn_p>
        </fn>and Abbot Wilhelm uses it as background for the marriage of <link target="malfrid"
            >Malfrid</link>’s sister <link target="ingeborg">Ingeborg</link> to <link target="knud"
            >Knud Lavard</link>.<fn>
            <fn_p>Wilhelm, <articleTitle>"Wilhelmi abbatis genealogia regum Danorum,"</articleTitle>
                182.</fn_p>
        </fn>To search for a reason, we must stretch the sources a bit farther.</p>
    <p>Inge of Sweden was the son of King Steinkel. Steinkel died around the year 1066, and was
        followed by a series of other rulers, including two Erics and possibly Inge’s brother,
        Alstan, before Haakon took the throne.<fn>
            <fn_p><bookTitle>Adam of Bremen</bookTitle>, liii.52, schol. 84 (85, 86). See also
                Sturluson. <bookTitle>Heimskringla</bookTitle>, 678.</fn_p>
        </fn>During that time, Inge was forced to flee, most likely in the years 1075–80,<fn>
            <fn_p><bookTitle>Adam of Bremen</bookTitle>, note to schol. 84 (85, 86).</fn_p>
        </fn>because one of the current rulers was attempting to eliminate all possible rivals. Inge
        found refuge in that paradise for Scandinavian refugees, Rus′, where he probably lived in
        Kyiv, or possibly Novgorod, though the latter may have been too close to home for safety.
        During that time in Rus′, it is quite possible that he became friendly with the Rjurikids
        and was grateful for their assistance. When the Swedes were again in need of a king, they
        called him home from Rus′ to rule.<fn>
            <fn_p>Ibid.</fn_p>
        </fn>The possible reasons for a marriage between Inge and the then ruling branch of the
        Rjurikids, the Vsevolodoviči, are numerous, but there are two probable ones. The first is
        that it was an alliance made because of the friendship created during Inge’s exile. An
        outgrowth of this idea is that the marriage was pledged during Inge’s time in Rus′, and only
        occurred when both <link target="mstislav5">M′stislav</link> and Inge’s daughter <link
            target="kristin">Kristín</link> were old enough. Prearranged marriages of this sort were
        relatively common; see the marriage of <link target="sbyslava">Sbyslava
            Svjatopolkovna</link> and <link target="boleslaw3">Bolesław III of Poland</link> as just
        one example. The other potential reason for the alliance was that it was part of Inge’s
        attempt to forge peace with his neighbors. In the 1090s, he was warring with King Magnus
        Barefoot of Norway over disputed lands,<fn>
            <fn_p>Sturluson, <bookTitle>Heimskringla</bookTitle>, 678–80.</fn_p>
        </fn>and it would have been prudent for him to ensure that he was at peace with his other
        neighbors. Indeed that conflict was finally resolved with a marriage between Magnus and
        Inge’s other daughter, Margaret, who was called the “Frithkolla [Peace Woman].”<fn>
            <fn_p>Ibid., 680.</fn_p>
        </fn>Unfortunately there is not material to say definitely the reason for the marriage, but
        it would seem logical for a marriage agreement to have been concluded while Inge was in
        Rus′, despite the fact that <link target="mstislav5">M′stislav</link> was then only a boy.
        This would fit with the Rjurikid policy of marrying exiles in the hopes that they would
        return to power.</p>
    <p><link target="kristin">Kristín</link>’s death is recorded in various Rusian chronicles in 1122<fn>
            <fn_p><bookTitle>Laurentian Chronicle</bookTitle>, s.a. 1122;
                <bookTitle>NPL</bookTitle>, s.a. 1122.</fn_p>
        </fn>and there is an interesting question of property that becomes important at <link
            target="kristin">her</link> death. In Scandinavian tradition a daughter inherits part of
        her father’s patrimony, though not the core lands.<fn>
            <fn_p><bookTitle>Saxo Grammaticus</bookTitle>, bk. XIII, p. 110, n8. There is a similar,
                though not identical, tradition of female inheritance in Rus, which is described in
                the Russkaia Pravda. <bookTitle>Medieval Russian Laws</bookTitle>, trans. George
                Vernadsky (New York: Oxford University Press, 1947), 51–53.</fn_p>
        </fn>In Denmark in the early twelfth century, we see <link target="kristin">Kristín</link>’s
        sister, Margaret, pass on her patrimony to various people, including her niece <link
            target="ingeborg">Ingeborg</link>.<fn>
            <fn_p><bookTitle>Saxo Grammaticus</bookTitle>, bk. XIII, p. 110.</fn_p>
        </fn><link target="kristin">Kristín</link> would have had the same potential to pass on land
        she had inherited from her father King Inge, and the question is, where did that land go?
        Because of the dearth of Rusian sources, and their profound ignorance of the activities of
        women, we have no idea, but the fact that <link target="kristin">she</link> had such land
        and something must have happened to it is important to remember in attempting to understand
        this period of Rusian history. This land would have created or added to <link
            target="kristin">Kristín</link>’s power and influence in Rus′, and it could have been a
        gift <link target="kristin">she</link> passed on to <link target="kristin">her</link>
        children to increase their power and influence.</p>
    <p>The 1122 chronicle entry in the NPL that announces the death of <link target="kristin"
            >Kristín</link> also announces the second marriage of <link target="mstislav5"
            >M′stislav</link>, this time to <link target="daughter-dmitrii-zavidich">a
            daughter</link> of <link target="dmitrii">Posadnik Dmitri Zavidič of Novgorod</link>.<fn>
            <fn_p><bookTitle>NPL</bookTitle>, s.a. 1122. This is replicated in several other
                chronicles as well. <bookTitle>Hypatian Chronicle</bookTitle>, s.a. 1122;
                    <bookTitle>NChL</bookTitle>, s.a. 1122; <bookTitle>Nikon Chronicle</bookTitle>,
                s.a. 1122.</fn_p>
        </fn>This was also a politically advantageous marriage, but an internal one, attempting to
        strengthen the ties between Kyiv and Novgorod by having a Rjurikid marry one of the
        daughters of the Novgorodian nobility. Though there are certainly caveats, such as the
        simple lack of information on non–Rjurikid Novgorod politics at that time (despite the rich
        Novgorod chronicle tradition) this is an acceptable conclusion when put in such broad terms.
        A familial relationship between <link target="mstislav5">M′stislav</link> and <link
            target="dmitrii">Dmitrii Zavidič</link> is unlikely, as <link target="dmitrii"
            >Dmitrii</link> died in 1118,<fn>
            <fn_p><bookTitle>NChL</bookTitle>, s.a. 1118.</fn_p>
        </fn>and his son (a potential arranger of the marriage) did not hold the position of
        posadnik until 1128.<fn>
            <fn_p><bookTitle>NPL</bookTitle>, s.a. 1128; <bookTitle>NChL</bookTitle>, s.a. 1128.
                Also the year of Zavid Dmitreevič’s death after being posadnik for only a few
                months.</fn_p>
        </fn>However, the idea of a sense of corporate loyalty is possible. <link target="mstislav5"
            >M′stislav</link> was creating a familial relationship with the Novgorod nobility, as an
        entity rather than one single family. This would be especially important, as <link
            target="mstislav5">M′stislav</link> had left Novgorod in 1117 to assist <link
            target="vladimir3">his father</link> in Kyiv.<fn>
            <fn_p><bookTitle>NPL</bookTitle>, s.a. 1117.</fn_p>
        </fn>The marriage can then be seen to serve the purpose of uniting the two poles of Rus′,
        Kyiv and Novgorod, and ideally bringing the Novgorodian nobility onto the side of <link
            target="mstislav5">M′stislav</link> in an assumed succession battle after <link
            target="vladimir3">his ailing father</link>’s death. This conclusion, while speculative,
        offers a reasonable suggestion for a marriage between a Rjurikid and non–Rjurikid Rusian,
        the first of its kind. It also seemed to correctly anticipate the succession struggles that
        followed and perhaps helped to cement <link target="mstislav5">M′stislav</link>’s position
        on the throne of Kyiv.</p>
</note>
