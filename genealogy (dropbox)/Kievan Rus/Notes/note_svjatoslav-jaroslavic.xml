<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="notes.rnc" type="compact"?>
<note>
    <title>Svjatoslav Jaroslavič</title>
    <p>There is, perhaps, more mystery and controversy about the marriages of <link
            target="sviatoslav3">Svjatoslav Jaroslavič</link> than any other figure in Rusian
        medieval history. Even the preceding statement would engender controversy because of the use
        of the word “marriages,” as some believe there was only one marriage.<fn>
            <fn_p> S. M. Kaštanov might be such a one. S. M. Kaštanov, <articleTitle>"Byla li Oda
                    Štadenskaja ženoi velikogo knjazja Svjatoslava Jaroslaviča?"</articleTitle> in
                    <bookTitle>Vostočnaja Evropa v drevnosti i srednevekov′e</bookTitle>, ed. V. T.
                Pašuto (Moscow: RAN, 1994), 18; as would Baumgarten whose involved theory of the
                marriage of Svjatoslav will be discussed briefly later. Baumgarten,
                    <articleTitle>"Généalogies,"</articleTitle> 7, table I.</fn_p>
        </fn>This is not the place to get into the specifics of any of these arguments, though
        reference to them will be made in the text with further information or citations in the
        footnotes. Instead, this section will merely focus on a presentation of the available
        data.</p>
    <p>There are two women who are connected to <link target="sviatoslav3">Svjatoslav,</link> or at
        the very least two different names, the first of which is <link target="kilikia"
            >Kilikia</link>. The name <link target="kilikia">Kilikia</link> for the wife of <link
            target="sviatoslav3">Svjatoslav</link> is recorded in the Ljubeč Sinodik, a primary
        source for information on the Svjatoslaviči.<fn>
            <fn_p>Zotov, <bookTitle>O Chernigovskix knjaz′jakh</bookTitle>, 24, 33.</fn_p>
        </fn>Martin Dimnik, whose work has concentrated on the restoration of the Svjatoslaviči to
        the pages of Rusian history, has theorized that the marriage to <link target="kilikia"
            >Kilikia</link> took place between the marriages of his older and younger brothers,
            <link target="iziaslav2">Izjaslav</link> and <link target="vsevolod2">Vsevolod,</link>
        which he dates to 1043 and 1046 respectively.<fn>
            <fn_p>Dimnik, <bookTitle>The Dynasty of Chernigov, 1054–1146,</bookTitle> 37.</fn_p>
        </fn>This is merely a supposition, however, as no actual information exists about the
        marriage or about <link target="kilikia">Kilikia</link> herself, but for the sake of
        argument the logical supposition that the brothers would marry in order of seniority has
        been accepted as a starting point for discussion. As mentioned, there is no record of <link
            target="kilikia">Kilikia's</link> identity. Dimnik, among others, has advanced the idea
        that the name indicates that she is Greek, from the province of Cilicia, which is rendered
        in Greek and Slavonic as “Kekiliia.”<fn>
            <fn_p>Ibid.; Nazarenko, <articleTitle>"Kievskaja knjagina – vnuka papy L′va IX
                    (1049–1054),"</articleTitle> 28. </fn_p>
        </fn>This seems likely simply based on the onomastic evidence of the name, but in the window
        created by Dimnik, there does not seem to be a reason for a marriage between Rus′ and
        Byzantium. In 1043 Rus′ attacked Constantinople, and in 1046 there was the marriage of a
        Monomaxina to <link target="vsevolod2">Vsevolod Jaroslavič.</link><fn>
            <fn_p>More on which can be found in the marriage of Vsevolod Jaroslavich.</fn_p>
        </fn>A marriage with a Byzantine woman not from the royal house (which at the time was that
        of Constantine IX Monomachus), would seem an odd choice to make. The only Greek explanation
        would be if perhaps <link target="sviatoslav3">Svjatoslav</link> was allying himself or
        being allied with a rival faction, but one from Cilicia in this time period is not known,
        and no such marriage is recorded in contemporary Greek sources. Along with the overwhelming
        lack of evidence, it is impossible to confirm that this was a Greek marriage. Alternately,
        V. M. Mošin has suggested that the woman may have been of Norman origin, though his proof
        for this is not conclusive.<fn>
            <fn_p>Mošin, <articleTitle>"Russkie na Afone i russko–vizantiiskie otnoshenija v XI–XII
                    vv.,"</articleTitle> 773.</fn_p>
        </fn>The identity of <link target="kilikia">Kilikia</link> will have to wait for the
        discovery of more evidence.</p>
    <p>Following the theory of multiple marriages, <link target="kilikia">Kilikia</link> was the
        mother of all of <link target="sviatoslav3">Svjatoslav’s</link> children with the exception
        only of his youngest son, <link target="iaroslav2">Jaroslav</link>. The mother of <link
            target="iaroslav2">Jaroslav Svjatoslavič</link> was a German woman by the name of <link
            target="oda">Oda of Stade.</link>
        <link target="oda">Oda of Stade</link> was the daughter of Ida of Elsdorf and Lippold, count
        of Ditmarschen. Ida was herself the niece of <link target="henry">Emperor Henry III</link>
        and Pope Leo IX, and thus a woman of some importance in the German Empire.<fn>
            <fn_p>There are multiple sources for the genealogy of Ida. The best is Albert of Stade;
                Nazarenko provides a Russian translation as well. Nazarenko, <bookTitle>Drevnjaja
                    Rus′ na meždunarodnyx putjax</bookTitle>, 506–10.</fn_p>
        </fn>This marriage can be dated to the early 1070s. A continuer of Herman of Reichenau
        recorded under the year 1072 that the <link target="sviatoslav3">“king of Rus′”</link>
        married <link target="oda">the daughter of “Count Lippold and Ita of Oterisburg”</link><fn>
            <fn_p>Oterisburg is very near to Elsdorf, and she may have held both properties.</fn_p>
        </fn>with the mediation of <link target="henry4">Henry IV</link>.<fn>
            <fn_p>I was unable to find the primary source which Nazarenko cites, and have as a
                result simply cited Nazarenko himself. Nazarenko, <bookTitle>Drevnjaja Rus′ na
                    meždunarodnyx putjax,</bookTitle> 515.</fn_p>
        </fn>Potentially corroborating this account is a story from Bruno’s Saxon war that <link
            target="henry4">Henry IV</link> sent an embassy to Rus′, though the story is undated and
        serves only to advance the idea of <link target="henry4">Henry IV</link> as a crafty ruler.<fn>
            <fn_p><bookTitle>Bruno de Bello Saxonico</bookTitle>, 333, cap. 13.</fn_p>
        </fn>Nazarenko suggests that the embassy was sent in 1069/70 to discuss the marriage between
            <link target="sviatoslav3">Svjatoslav</link> and <link target="henry4">Henry IV’s</link>
        <link target="oda">cousin</link>.<fn>
            <fn_p>Nazarenko, <bookTitle>Drevnjaja Rus′ na meždunarodnyx putjax,</bookTitle>
                519–20.</fn_p>
        </fn>Though this is not concrete corroboration, it is acceptable as a basis to build the
        theory that <link target="henry4">Henry IV</link> was involved in <link target="oda">Oda of
            Stade</link>’s marriage. <link target="henry4">He</link> was Ida’s cousin as well as the
        ruler of the German Empire and thus would have had the ability to marry off his cousin’s
            <link target="oda">daughter</link>, should he so choose. Nazarenko has suggested that
        the reason behind such a marriage would be an anti–Polish alliance between the Svjatoslaviči
        and the Salian dynasty.<fn>
            <fn_p>Ibid., 520–21.</fn_p>
        </fn>If the marriage negotiations began in 1069/70, it was after the Poles had restored
            <link target="iziaslav2">Izjaslav</link> to the throne, and <link target="boleslaw2"
            >Bolesław II</link> was beginning to war against the Bohemians (who had attacked Poland
        while <link target="boleslaw2">Bolesław</link> was in Rus′), staunch allies of the German
        Empire. This reasoning makes sense on the surface, as the Svjatoslaviči and Izjaslaviči are
        often contrasted in their Polish–anti–Polish stances, but it is difficult to reconcile with
        the earlier marriage of <link target="vysheslava">Vyšeslava Svjatoslavna</link> to <link
            target="boleslaw2">Bolesław II</link> of Poland.<fn>
            <fn_p>Unfortunately due to the structure of this section, the marriages of Svjatoslav
                are dealt with before the marriage of his daughter Vyšeslava, which occurred after
                his marriage to Kilikia, but before his marriage to Oda of Stade. See below for the
                marriage of Vyšeslava Svjatoslavna.</fn_p>
        </fn>The marriage may have just been another form of insurance against further Polish help
        to <link target="iziaslav2">Izjaslav</link>, should he need it, or perhaps it was part of
        the future planning of <link target="sviatoslav3">Svjatoslav</link>, who was already
        preparing a campaign against <link target="iziaslav2">Izjaslav</link> and wanted to take
        extra measures to be sure that the Poles were both unwilling (because of <link
            target="boleslaw2">Bolesław’s</link> marriage to <link target="sviatoslav3"
            >Svjatoslav’s</link>
        <link target="vysheslava">daughter</link>) and unable (because of the threat of attack) to
        aid <link target="iziaslav2">Izjaslav’s</link> return to Kyiv. </p>
    <p><link target="sviatoslav3">Svjatoslav</link> and <link target="oda">Oda</link> only had one
        known child. The Izbornik Svjatoslava of 1073 carries a picture of <link
            target="sviatoslav3">Svjatoslav</link> and his family in which his children are grown
        except for a baby held by his wife. This baby was most likely <link target="iaroslav2"
            >Jaroslav</link>, and the wife, <link target="oda">Oda of Stade</link>.<fn>
            <fn_p>T. S. Morozov, ed., <bookTitle>Izbornik velikago knjazja Svjatoslava Jaroslaviča
                    1073 goda</bookTitle>, Monumenta Linguae Slavicae Dialecti Veteris Tom 3
                (Wiesbaden: Otto Harrassowitz, 1965), 1.</fn_p>
        </fn>After <link target="sviatoslav3">Svjatoslav's</link> death, <link target="oda"
            >Oda</link> returned to the German Empire with <link target="iaroslav2">Jaroslav</link>,
        and he was not seen again in Rus′ until 1096,<fn>
            <fn_p><bookTitle>PVL</bookTitle>, s.a. 1096.</fn_p>
        </fn>after which he began to rule various Svjatoslaviči towns.<fn>
            <fn_p>See Dimnik for more details of his life and rule. <bookTitle>The Dynasty of
                    Chernigov</bookTitle>, 1054–1146.</fn_p>
        </fn></p>
    <p><link target="sviatoslav3">Svjatoslav’s</link> marriage to <link target="oda">Oda</link> is
        best known for the assistance it gave him when <link target="henry4">Henry IV</link> sent an
        embassy to Rus′ in 1075 ostensibly on behalf of <link target="iziaslav2">Izjaslav</link>.
        The head of <link target="henry4">Henry's</link> delegation was Burchard, the dean of Trier,
        specifically chosen because he was the brother of <link target="oda">Oda of Stade</link>,
        and thus the brother–in–law of <link target="sviatoslav3">Svjatoslav</link>.<fn>
            <fn_p>Lambert of Hersefeld, s.a. 1075.</fn_p>
        </fn>Burchard was sent to Rus′ to negotiate a peaceful settlement to the struggles between
            <link target="iziaslav2">Izjaslav</link> and <link target="sviatoslav3"
            >Svjatopolk</link>. Burchard returned from his <link target="sviatoslav3"
            >brother–in–law’s</link> court with riches for both himself and <link target="henry4"
            >Henry IV,</link> most likely as further inducement not to intervene in the struggle.<fn>
            <fn_p>Ibid.. Also recorded obliquely in the PVL. <bookTitle>PVL</bookTitle>, s.a.
                1075.</fn_p>
        </fn>The marriage, then, had served its purpose of allowing <link target="sviatoslav3"
            >Svjatoslav</link> an entrée to <link target="henry4">Henry IV</link> that allowed him
        to maintain his seat in Kyiv and frustrate his brother’s chances to return to Rus′.</p>
    <p>This marriage, for all of the convoluted struggles in the historiography, was a very
        important one for Rus′. It fulfilled its goal of an alliance with <link target="henry4"
            >Henry IV</link>, as well as preventing Polish aid to <link target="iziaslav2"
            >Izjaslav</link> while <link target="sviatoslav3">Svjatoslav</link> ruled in Kyiv. But
        perhaps more importantly, it brought imperial Salian blood into the Rjurikid line (in the
        person of <link target="iaroslav2">Jaroslav Svjatoslavič</link>), an important symbol that
        the Rusians were eligible partners throughout Europe and potentially leading the way for
            <link target="henry4">Henry IV</link> himself to marry <link target="evpraksia">a Rusian
            princess.</link></p>
</note>
