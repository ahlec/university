import sys

def countLines(filename):
    try:
        file = open(filename, "r")
        count = 0
        for line in file:
            count += 1
        file.close()
        return count
    except IOError:
        print("The file '%s' does not exist." % filename, file = sys.stderr)
        return -1

numberLines = countLines("recitation.py")
print(numberLines)
secondNumber = countLines("nofile.txt")
print(secondNumber)

def intInput(prompt):
    usersInput = input(prompt)
    try:
        usersNumber = int(usersInput)
        return usersNumber
    except ValueError:
        return None

def getAge(minAge, maxAge):
    while True:
        age = intInput("Please enter an age: ")
        if age != None:
            if age >= minAge and age <= maxAge:
                return age
    
    
number = intInput("Type a number: ")
if number == None:
    print("You did not type a number.")
else:
    print("You entered %d." % number)
secondNumber = int(input("Type a new number: "))

    

def doesFileExist(filename):
    try:
        file = open(filename, "r")
        file.close()
        return True
    except IOError:
        return False

