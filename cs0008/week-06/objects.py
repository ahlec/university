# Below is the full code for the objects lesson. Detailed explanations can be found
# as part of the lesson. This is the full code as of the end of the lesson -- intermediate
# stages are not present.

itemsNeeded = []
numberNeeded = []

def addItem(itemName, quantity):
    global itemsNeeded
    global numberNeeded
    for index in range(len(itemsNeeded)):
        item = itemsNeeded[index]
        if item == itemName:
            # There is an item already in the grocery list that matches itemName
            numberNeeded[index] = numberNeeded[index] + quantity
            return
    # If there was any match found, we would have hit the return keyword and left the function.
    # Since we are still in the function, we can assume that there were no matches found,
    # correct?
    itemsNeeded = itemsNeeded + [itemName]
    numberNeeded = numberNeeded + [quantity]

def purchaseQuantity(itemName, quantity):
    global itemsNeeded
    global numberNeeded
    for index in range(len(itemsNeeded)):
        item = itemsNeeded[index]
        if item == itemName:
            numberNeeded[index] = numberNeeded[index] - quantity
            if numberNeeded[index] <= 0:
                itemsNeeded[index:index + 1] = [] # select the subset list that includes only the one element
                numberNeeded[index:index + 1] = []
            return

def printGroceryList():
    print("Grocery List:")
    print("-------------------")
    for index in range(len(itemsNeeded)):
        itemName = itemsNeeded[index]
        itemQuantity = numberNeeded[index]
        print("[" + str(itemQuantity) + "] " + itemName)

addItem("Bread", 2)
addItem("Soda", 12)
addItem("Apple", 4)
addItem("Soda", 2)
printGroceryList()
print("")
purchaseQuantity("Bread", 1)
purchaseQuantity("Soda", 20)
purchaseQuantity("Cheese", 3)
printGroceryList()