So now that we have a basic structure for our data (the lists) and a method for displaying the data (<python>printGroceryList()</python>), let's move onto the higher-level functionality.

Let's create a function <python>addItem()</python>. It will take two arguments &mdash; <python>itemName</python> and <python>quantity</python>. This function will simply add this information to the end of the grocery list.

<code>def addItem(itemName, quantity):
    itemsNeeded = itemsNeeded + [itemName]
    numberNeeded = numberNeeded + [quantity]</code>
If this looks confusing to you, make sure to review the lists lesson, specifically the page on adding elements to the end of a list.

Let's add a couple of test items to our grocery list and then run it:
<code>addItem("Bread", 2)
addItem("Soda", 12)
addItem("Apple", 4)
printGroceryList()

<error>>> Traceback (most recent call last):
>>   File "C:/Users/Jacob/Desktop/shoppingList.py", line 17, in <module>
>>     addItem("Bread", 12)
>>   File "C:/Users/Jacob/Desktop/shoppingList.py", line 5, in addItem
>>     itemsNeeded = itemsNeeded + [itemName]
>> UnboundLocalError: local variable 'itemsNeeded' referenced before assignment</error></code>

Well, that most certainly did not do what we expected it to. So, why? The answer here is simply because Python doesn't know what you mean when you try to modify a variable it doesn't know about. When we're inside of <python>addItem()</python>, Python knows about the variables <python>itemName</python> and <python>quantity</python>, and it has an inkling that some variables <python>itemsNeeded</python> and <python>numberNeeded</python> <emphasis>exist</emphasis> outside of it, but when we try to tell it to modify these latter variables, Python doesn't know what you mean &mdash; they don't actually <emphasis>exist</emphasis> within the function, and so Python doesn't feel comfortable modifying them. Which means it's up to us as programmers to tell Python that when we say <python>itemsNeeded</python>, we're talking about the variable that we created <emphasis>with that name</emphasis> outside of the function.

<code>def addItem(itemName, quantity):
    global itemsNeeded
    global numberNeeded
    itemsNeeded = itemsNeeded + [itemName]
    numberNeeded = numberNeeded + [quantity]</code>
    
By using <python>global</python>, we are signalling that, inside this function, when we use <python>itemsNeeded</python> or <python>numberNeeded</python>, we're talking about the variables we created that are <term>global</term> &mdash; they exist throughout the entire program.

After making these changes, if we try to run our code again, we'll get what we want now:
<code>addItem("Bread", 2)
addItem("Soda", 12)
addItem("Apple", 4)
printGroceryList()

>> Grocery List:
>> -------------------
>> [2] Bread
>> [12] Soda
>> [4] Apple</code>