# Below is the full code for the lists lesson. Detailed explanations can be found
# as part of the lesson. This is the full code as of the end of the lesson -- intermediate
# stages are not present.

schedule = ["Introduction to programming with Python",
            "Introduction to painting",
            "Logic and Intelligence",
            "Chemistry 2",
            "Calculus 1"]
grades = [95.7,
          88.2,
          93,
          83.1,
          79.8]

print(len(schedule))
print(len(grades))

for index in range(len(schedule)):
    course = schedule[index]
    grade = grades[index]
    print(course + ": " + str(grade))

schedule[1] = "Introduction to Painting"
grades[4] = 80.1

from week6 import *
printSchedule(schedule, grades)

schedule = schedule + ["Science Fiction in literature"]
grades = grades + [91]
printSchedule(schedule, grades)

schedule.pop()
grades.pop()
printSchedule(schedule, grades)

schedule[1:2] = []
grades[1:2] = []
printSchedule(schedule, grades)

schedule[2:2] = ["Computational Methods in the Humanities"]
grades[2:2] = [99.6]
printSchedule(schedule, grades) 