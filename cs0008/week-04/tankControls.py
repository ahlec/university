# CS0008 Recitation, Week 04: Additional Problem
#
# PROBLEM:
#   Using the code that I provided in keyboardDrawing.py, modify the program
#   from "snake controls" to "tank controls" -- that is, pressing Up
#   will move the turtle forward in whatever direction it is facing, pressing
#   Down will move the turtle backwards in whatever direction it is facing,
#   and pressing Left or Right will rotate the turtle by some constant
#   amount relative to whichever direction the turtle was facing.
# RULES:
#   *   The turtle must be able to be controlled using *both* the arrow keys
#       AND the WASD keys.
# SOME HINTS:
#   *   Make sure to read the comments that I provided in keyboardDrawing.py.
#       They should explain what happens along every step of the way, so that
#       it becomes easier to figure out what to edit and what to leave alone.
#   *   It is possible to have multiple "screen.onkey" statements use the same
#       function (eg, "moveUp") for different keys. For instance, if we had a
#       function "quitTurtle", we could have both the Q key and the E key use
#       the same function by writing:
#
#           screen.onkey(quitTurtle, "Q")
#           screen.onkey(quitTurtle, "E")