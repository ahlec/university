# CS0008 Recitation, Week 02: Additional Problem
#
# PROBLEM:
#   Ask the user a series of questions about how far they've driven already,
#   and respond according to whether they've made a wrong turn, or if they've
#   arrived already.
#
# QUESTIONS:
#   1. Did you Go down Melon Lane?
#   2. Did you Take a right on Apple Street?
#   3. Did you Make a U-turn onto Watermelon Drive?
#   4. Did you Stop for gas?
#   5. Did you Turn onto Orange Avenue?
#   6. Did you Look for the house on the right?
# RULES:
#   *   You must write your own function for asking the questionto the user!
#       I called my function "checkup" and I would call checkup with the
#       question I was asking; checkup would then return to me "Y" or "N"
#       depending on what the user input.
#   *   Input from the user only needs to be in the format of "Y" (capital Y)
#       or "N" (capital N); you don't need to worry about anything else for
#       this.
#   *   If the user responds negatively ("N"), you must print out
#       "Well, you need to!" and then ask no further questions.
#   *   If the user responds affirmatively ("Y") to all questions, after the
#       last question, print out "Then let's get out of the car already!"
#   *   Your function must return some indication of whether the user answered
#       affirmatively or negatively; I chose to return Y or N, but there are
#       many ways you could do this.
# SOME HINTS:
#   *   Remember that to create your own function, you use the def keyword
#       (p. 84 of your textbook covers this if you've forgotten).
#   *   You can pass a variable to a function you wrote; this is called a
#       parameter (p. 97-98 of your textbook). You can then use this parameter
#       as you would any other variable when you are inside of your function.
#
#       For example:
#
#           def mister(surname):
#               print("Mr.", surname)
#
#           mister("Deitloff")
#
#       Will print out "Mr. Deitloff"
#   *   You can return a value from a function you wrote using the return
#       keyword (p. 214 of your textbook). This is actually easier than
#       it might sound at first, if you haven't done it.
#
#       For example:
#
#           def askSex():
#               sex = input("Are you a boy (b) or girl (g)? ")
#               if sex == "b":
#                   return "male"
#               else:
#                   return "female"
#
#           if askSex() == "male":
#               print("You are male")
#           else:
#               print("You are female")
#
#       This example will call the function askSex(), which will prompt the
#       user, and if the user replies boy, askSex() will return the text
#       'male'; otherwise, it will return 'female'. You can use this just as
#       you would a variable. In fact, consider how you have used input()
#       in your code before -- input() *return*s what the user typed into the
#       keyboard.
#   *   You can put if statements inside of another if statement (hint hint).

def checkup(question):
    response = input("Did you " + question + "? ")
    if response == "N":
        print("Well, you need to!")
    return response

if checkup("Go down Melon Lane") == "Y":
    if checkup("Take a right on Apple Street") == "Y":
        if checkup("Make a U-turn onto Watermelon Drive") == "Y":
            if checkup("Stop for gas") == "Y":
                if checkup("Turn onto Orange Avenue") == "Y":
                    if checkup("Look for the house on the right") == "Y":
                        print("Then let's get out of the car already!")
