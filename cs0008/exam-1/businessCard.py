# Write a function named "businessCard" that takes one argument -- the field of
# information to return:
#
#   "firstName" = your first name
#   "lastName" = your last name
#   "email" = your email
#   "major" = your major(s)
#
# if anything else is given, return back "I don't know what you're asking for."

# put your code here!

# You cannot modify below this line!
print("First name: %s" % businessCard("firstName"))
print("Last name: %s" % businessCard("lastName"))
print("Email: %s" % businessCard("email"))
print("Major: %s" % businessCard("major"))
print("Phone number: %s" % businessCard("phoneNumber"))
