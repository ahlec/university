<code><kw>def</kw> <blue>moveForward</blue>():
    <kw>global</kw> position
    
    <kw>if</kw> isBlocked():
        changeDirection()
    
    <kw>if</kw> isRobotFacingRight():
        position = position + 1     <comment># increase his position by 1</comment>
    <kw>else</kw>:
        position = position - 1     <comment># decrease his position by 1</comment></code>
        
But there are a lot of other things that fit into an <kw>if</kw> statement as well &mdash; namely, the <term>boolean operators</term> (<kw>and</kw>, <kw>or</kw>, and <kw>not</kw>). I think you all have a pretty good handle on the first two of the three. Both <kw>and</kw> and <kw>or</kw> take two (or more) unique booleans or <term>boolean equations</term> (a piece of code that evaluates to a boolean, like <python>myName == "Jacob"</python>) and "creates" a boolean. If I belabour the point, it'll confuse everyone.

Let's write another function that wasn't part of the assignment. This new function will be <python>isRobotHome()</python>, which will return a <kw>True</kw> if the robot is in the first position on the track; if the robot is anywhere else, then it will return <kw>False</kw>. Try to write this function, and then check the next page for my solution. I will make use of the function <python>getRobotPosition()</python> instead of working with the global variables.