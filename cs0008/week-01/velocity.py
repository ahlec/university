# CS0008 Recitation, Week 01: Additional Problem
#
# PROBLEM:
#   A ball falls (freefall) from rest for a certain distance. Ask the user
#   for the distance the ball should fall, and then tells the user what the
#   ball's velocity is.
# FORMULA:
#   Vf^2 = Vi^2 + 2ad
#       Vf  = Velocity (final). That is, the velocity after traveling
#             the distance provided.
#       Vi  = Velocity (initial). Starting at rest, so consider this 0.
#       a   = Acceleration. The force of gravity is constant, so 9.8 is usable.
#       d   = Distance (what we're taking as input)
# RULES:
#   *   print() the final output in the following format:
#       "After falling DISTANCE meter(s), the ball is traveling VELOCITY m/s."
# SOME HINTS:
#   *   You will need to import the math library.
#   *   Don't be overwhelmed by the function above. See if you can remove
#       a part of it.
#   *   If a line is too long to fit on one screen, it is easier to finish it up
#       on the following line. To do this, we need to indicate at the end of the
#       first line that we will be continuing on the second line. This is done by
#       putting a \ at the end of the first line, and continuing on the second.
#       Example:
#           print("My name is Jacob Deitloff, and I am taking CS0008 during", \
#               "the spring 2013 semester.")
#       This is known as the "line continuation character." A detailed explanation
#       can be found in the textbook on pages 64-65.
#
#       Note: Indenting the second line is not mandatory, but it aids considerably
#             in keeping your code readable.
#
#           COMPARE:
#               print("My name is Jacob Deitloff, and I am taking CS0008 during", \
#                   "the spring 2013 semester.")
#               print("My name is Jacob Deitloff, and I am taking CS0008 during", \
#               "the spring 2013 semester.")