letter = ""
count = 0

def setLetter(newLetter):
    global letter
    letter = newLetter

def getLetter():
    global letter
    return letter

def getCount():
    global count
    return count

def countLine(thisLine):
    thisLetter = getLetter()
    thisCount = 0
    for character in thisLine:
        if character == thisLetter:
            thisCount += 1
    return thisCount

def countFile():
    global count
    file = open("words.txt", "r")
    count = 0
    for line in file:
        count = count + countLine(line)
    file.close()


setLetter(input("What letter to look for? "))
countFile()
print("'%s' was found %d times." % (getLetter(), getCount()))





