# first name
# height
# age

firstName = ""
height = 0.0
age = 0

def setFirstName(newFirstName):
    global firstName
    firstName = newFirstName
def getFirstName():
    global firstName
    return firstName
def setAge(newAge):
    global age
    age = newAge
def setHeight(feet, inches):
    global height
    height = feet * 12 + inches
def getHeightFeet():
    global height
    return height // 12
def getHeightInches():
    global height
    return height % 12
def getHeightCentimeters():
    global height
    return height * 2.54

setFirstName("Carl")
setAge(25)
setHeight(6, 1)
print("%s is %d cm tall." % (getFirstName(), getHeightCentimeters()))
