class BoundedNumber:
    def __init__(self, value, minValue, maxValue):
        self.value = value
        self.minValue = minValue
        self.maxValue = maxValue

    def getValue(self):
        return self.value

    def setValue(self, newValue):
        # if newValue is greater than maxValue
        # then set value equal to maxValue
        # if newValue is less than minValue
        # then set value equal to minValue
        # if newValue is between these two numbers
        # then set value equal to newValue
        if newValue > self.maxValue:
            self.value = self.maxValue
        if newValue < self.minValue:
            self.value = self.minValue
        if newValue >= self.minValue and \
           newValue <= self.maxValue:
            self.value = newValue

number = BoundedNumber(50, 1, 100)
print(number.getValue())
number.setValue(300)
print(number.getValue())
number.setValue(-1)
print(number.getValue())
