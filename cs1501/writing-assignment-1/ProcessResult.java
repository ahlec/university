public class ProcessResult
{
  public ProcessResult(int price, int patternA, int patternB, int patternC, int patternD)
  {
    Price = price;
    PatternACount = patternA;
    PatternBCount = patternB;
    PatternCCount = patternC;
    PatternDCount = patternD;
  }
  public final int Price;
  public final int PatternACount;
  public final int PatternBCount;
  public final int PatternCCount;
  public final int PatternDCount;
}