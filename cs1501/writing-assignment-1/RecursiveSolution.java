public class RecursiveSolution
{
  private static Pattern[] _patterns;
  private static int NUMBER_CALLS = 0;
  
  public static void main(String[] args)
  {
	_patterns = new Pattern[4];
	_patterns[0] = new Pattern("a", 2, 2, 1);
	_patterns[1] = new Pattern("b", 2, 6, 4);
	_patterns[2] = new Pattern("c", 4, 2, 3);
	_patterns[3] = new Pattern("d", 5, 3, 5);
	
	ProcessResult result = Process(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
	System.out.println("Cloth Size: " + args[0] + " x " + args[1]);
	System.out.println("");
	System.out.println("Total Price: " + result.Price);
	System.out.println("Pattern A: " + result.PatternACount);
	System.out.println("Pattern B: " + result.PatternBCount);
	System.out.println("Pattern C: " + result.PatternCCount);
	System.out.println("Pattern D: " + result.PatternDCount);
	System.out.println("");
	System.out.println("Number calls: " + NUMBER_CALLS);
  }
  
  public static ProcessResult Process(int clothWidth, int clothHeight)
  {
	NUMBER_CALLS++;
    int bestPrice = 0;
	int patternACount = 0;
	int patternBCount = 0;
	int patternCCount = 0;
	int patternDCount = 0;
	
    for (Pattern p : _patterns)
    {
		if (p.Width <= clothWidth && p.Height <= clothHeight && p.SellPrice > bestPrice)
		{
			bestPrice = p.SellPrice;
			patternACount = 0;
			patternBCount = 0;
			patternCCount = 0;
			patternDCount = 0;
			if (p.Name.equals("a"))
			{
				patternACount = 1;
			}
			else if (p.Name.equals("b"))
			{
				patternBCount = 1;
			}
			else if (p.Name.equals("c"))
			{
				patternCCount = 1;
			}
			else
			{
				patternDCount = 1;
			}
		}
    }
    
	if (bestPrice > 0)
	{
		for (int x = 1; x <= Math.ceil(clothWidth / 2); x++)
		{
			ProcessResult left = Process(x, clothHeight);
			ProcessResult right = Process(clothWidth - x, clothHeight);
			if (left.Price + right.Price > bestPrice)
			{
				bestPrice = left.Price + right.Price;
				patternACount = left.PatternACount + right.PatternACount;
				patternBCount = left.PatternBCount + right.PatternBCount;
				patternCCount = left.PatternCCount + right.PatternCCount;
				patternDCount = left.PatternDCount + right.PatternDCount;
			}
		}
		
		for (int y = 1; y <= Math.ceil(clothHeight / 2); y++)
		{
			ProcessResult top = Process(clothWidth, y);
			ProcessResult bottom = Process(clothWidth, clothHeight - y);
			if (top.Price + bottom.Price > bestPrice)
			{
				bestPrice = top.Price + bottom.Price;
				patternACount = top.PatternACount + bottom.PatternACount;
				patternBCount = top.PatternBCount + bottom.PatternBCount;
				patternCCount = top.PatternCCount + bottom.PatternCCount;
				patternDCount = top.PatternDCount + bottom.PatternDCount;			
			}
		}
	}
	
    return new ProcessResult(bestPrice, patternACount, patternBCount, patternCCount, patternDCount);
  }
}