public class Pattern
{
  public Pattern(String name, int width, int height, int sellPrice)
  {
    Name = name;
    Width = width;
    Height = height;
    SellPrice = sellPrice;
  }
  public final int Width;
  public final int Height;
  public final String Name;
  public final int SellPrice;
}