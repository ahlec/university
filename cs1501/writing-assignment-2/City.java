public class City
{
	public City(int x, int y, int index)
	{
		X = x;
		Y = y;
		Index = index;
	}
	public final int X;
	public final int Y;
	public final int Index;
}
