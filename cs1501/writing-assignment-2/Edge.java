public class Edge
{
	public Edge(City cityA, City cityB, double weight)
	{
		CityA = cityA;
		CityB = cityB;
		Weight = weight;
	}
	
	public final City CityA;
	public final City CityB;
	public final double Weight;
}