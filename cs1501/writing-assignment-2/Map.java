import java.util.Random;
import java.util.ArrayList;

public class Map
{
	private boolean _hasRunPrims = false;
	private double _primsWeight = 0;
	private boolean _hasRunTreewalk = false;
	private double _tourLength = 0;
	
	private CityBuilder[] _builders;
	private Edge[] _mst;
	private int[] _treewalkOrder;
	private int _currentTreewalkIndex = 0;
	private Edge[] _treewalk;
	
	public Map(int count, int boundX, int boundY)
	{
		Count = count;
		BoundX = boundX;
		BoundY = boundY;
		_builders = new CityBuilder[count];
		
		Random randomization = new Random();
		int x;
		int y;
		for (int index = 0; index < count; index++)
		{
			do
			{
				x = randomization.nextInt(BoundX + 1);
				y = randomization.nextInt(BoundY + 1);
			} while (HasCityBuilderAt(x, y));
			
			_builders[index] = new CityBuilder(x, y, index);
		}
	}
	
	private boolean HasCityBuilderAt(int x, int y)
	{
		for (CityBuilder city : _builders)
		{
			if (city == null)
			{
				break;
			}
			
			if (city.X == x && city.Y == y)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public final int Count;
	public final int BoundX;
	public final int BoundY;
	public City Get(int index)
	{
		return _builders[index].City;
	}
	
	public double GetPrimsWeight()
	{
		if (_hasRunPrims)
		{
			return _primsWeight;
		}
		
		PrimsAlgorithm();
		
		return _primsWeight;
	}
	public Edge[] GetMinimumSpanningTree()
	{
		if (_hasRunPrims)
		{
			return _mst;
		}
		
		PrimsAlgorithm();
		
		return _mst;
	}
	private void PrimsAlgorithm()
	{		
		ArrayList<Integer> queue = new ArrayList<Integer>();
		ArrayList<Edge> mst = new ArrayList<Edge>();
		for (int index = 0; index < Count; index++)
		{
			queue.add(index);
		}
		
		int location = queue.remove(0);
		for (Integer index : queue)
		{
			_builders[index.intValue()].MarkCityPrims(_builders[location]);
		}
		
		Integer current = -1;
		while (queue.size() > 0)
		{
			for (Integer queued : queue)
			{
				if (current == -1 || _builders[queued.intValue()].CurrentPrimsMinEdgeWeight <
					_builders[current.intValue()].CurrentPrimsMinEdgeWeight)
				{
					current = queued;
				}
			}
			
			Edge edge = new Edge(_builders[current.intValue()].City,
				_builders[_builders[current.intValue()].CurrentPrimsMinEdgeDestination].City,
				_builders[current.intValue()].CurrentPrimsMinEdgeWeight);
			mst.add(edge);
			_builders[current.intValue()].Edges.add(edge);
			_builders[_builders[current.intValue()].CurrentPrimsMinEdgeDestination].Edges.add(edge);
			
			queue.remove(current);
			
			for (Integer index : queue)
			{
				_builders[index].MarkCityPrims(_builders[current.intValue()]);
			}
			
			current = -1;
		}
		
		for (Edge edge : mst)
		{
			_primsWeight += edge.Weight;
		}
		
		_mst = new Edge[mst.size()];
		mst.toArray(_mst);
		
		_hasRunPrims = true;
	}
	public double GetTreewalkLength()
	{
		if (_hasRunTreewalk)
		{
			return _tourLength;
		}
		
		Treewalk();
		return _tourLength;
	}
	public Edge[] GetTreewalkTour()
	{
		if (_hasRunTreewalk)
		{
			return _treewalk;
		}
		
		Treewalk();
		return _treewalk;
	}
	private void Treewalk()
	{
		_treewalkOrder = new int[Count];
		_treewalk = new Edge[Count];
		Preorder(0);
		
		double weight;
		for (int index = 0; index < Count - 1; index++)
		{
			weight = _builders[_treewalkOrder[index]].GetDistanceTo(_builders[_treewalkOrder[index + 1]]);
			_treewalk[index] = new Edge(_builders[_treewalkOrder[index]].City, _builders[_treewalkOrder[index + 1]].City, weight);
			_tourLength += weight;
		}
		
		weight = _builders[_treewalkOrder[0]].GetDistanceTo(_builders[_treewalkOrder[_treewalkOrder.length - 1]]);
		_treewalk[_treewalk.length - 1] = new Edge(_builders[_treewalkOrder[_treewalkOrder.length - 1]].City,
			_builders[_treewalkOrder[0]].City, weight);
		_tourLength += weight;
		
		_hasRunTreewalk = true;
	}
	private void Preorder(int builderIndex)
	{
		_treewalkOrder[_currentTreewalkIndex] = builderIndex;
		_currentTreewalkIndex++;
		_builders[builderIndex].HitByTreewalk = true;
		
		int otherIndex;
		for (Edge edge : _builders[builderIndex].Edges)
		{
			otherIndex = (edge.CityA.Index == builderIndex ? edge.CityB.Index : edge.CityA.Index);
			if (!_builders[otherIndex].HitByTreewalk)
			{
				Preorder(otherIndex);
			}
			
		}
	}
}