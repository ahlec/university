import java.util.ArrayList;

public class CityBuilder
{
	public CityBuilder(int x, int y, int index)
	{
		X = x;
		Y = y;
		City = new City(x, y, index);
	}
	public final int X;
	public final int Y;
	public final City City;
	
	public double CurrentPrimsMinEdgeWeight = 0;
	public int CurrentPrimsMinEdgeDestination = -1;
	public boolean HitByTreewalk = false;
	public ArrayList<Edge> Edges = new ArrayList<Edge>();
	
	public void MarkCityPrims(CityBuilder marked)
	{
		double distanceTo = GetDistanceTo(marked);
		if (CurrentPrimsMinEdgeDestination == -1 || distanceTo <= CurrentPrimsMinEdgeWeight)
		{
			CurrentPrimsMinEdgeDestination = marked.City.Index;
			CurrentPrimsMinEdgeWeight = distanceTo;
		}
	}
	
	public double GetDistanceTo(CityBuilder other)
	{
		return Math.sqrt(Math.pow(other.X - X, 2) + Math.pow(other.Y - Y, 2));
	}
}