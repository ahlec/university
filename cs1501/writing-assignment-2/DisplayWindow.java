import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class DisplayWindow extends JFrame
{
  private static final int FULL_WIDTH = 500;
  private static final int FULL_HEIGHT = 500;
  private class MapDisplay extends JPanel
  {
	private Map _map;
	private Edge[] _mst;
	private Edge[] _treewalk;
	private double X_SCALE = 0.25;
	private double Y_SCALE = 0.25;
	
    public MapDisplay(Map map)
	{
		setPreferredSize(new Dimension(FULL_WIDTH + 10, FULL_HEIGHT + 10));
		_map = map;
		_mst = map.GetMinimumSpanningTree();
		_treewalk = map.GetTreewalkTour();
		X_SCALE = (double)FULL_WIDTH / map.BoundX;
		Y_SCALE = (double)FULL_HEIGHT / map.BoundY;
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(0,0, FULL_WIDTH + 10, FULL_HEIGHT + 10);

		int cityX;
		int cityY;
		int city2X;
		int city2Y;
		if (ShowMST)
		{
			g.setColor(Color.BLACK);
			for (Edge edge : _mst)
			{
				cityX = (int)Math.round(edge.CityA.X * X_SCALE);
				cityY = (int)Math.round(edge.CityA.Y * Y_SCALE);
				city2X = (int)Math.round(edge.CityB.X * X_SCALE);
				city2Y = (int)Math.round(edge.CityB.Y * Y_SCALE);
				g.drawLine(cityX + 5, cityY + 5, city2X + 5, city2Y + 5);
			}
		}
		
		if (ShowTreewalk)
		{
			g.setColor(Color.RED);
			for (Edge edge : _treewalk)
			{
				cityX = (int)Math.round(edge.CityA.X * X_SCALE);
				cityY = (int)Math.round(edge.CityA.Y * Y_SCALE);
				city2X = (int)Math.round(edge.CityB.X * X_SCALE);
				city2Y = (int)Math.round(edge.CityB.Y * Y_SCALE);
				g.drawLine(cityX + 5, cityY + 5, city2X + 5, city2Y + 5);
			}
		}
		
		if (!ShowLabels)
		{
			g.setColor(Color.BLUE);
		}
		for (int index = 0; index < _map.Count; index++)
		{
			City city = _map.Get(index);
			if (ShowLabels)
			{
				g.setColor(Color.BLUE);
			}
			cityX = (int)Math.round(city.X * X_SCALE);
			cityY = (int)Math.round(city.Y * Y_SCALE);
			g.fillOval(cityX - 2 + 5, cityY - 2 + 5, 4, 4);
			if (ShowLabels)
			{
				g.setColor(Color.GREEN);
				g.drawString(Integer.toString(city.Index), cityX, cityY);
			}
		}
		
		if (ShowBorder)
		{
			g.setColor(Color.BLACK);
			g.drawRect(0, 0, FULL_WIDTH + 9, FULL_HEIGHT + 9);
		}
	}
	
	public boolean ShowTreewalk = true;
	public boolean ShowMST = true;
	public boolean ShowBorder = true;
	public boolean ShowLabels = true;
  }

  private Map _map;
  private MapDisplay _display;
  private JCheckBox _mstCheckbox;
  private JCheckBox _treewalkCheckbox;
  private JCheckBox _borderCheckbox;
  private JCheckBox _labelsCheckbox;
  public DisplayWindow(Map map)
  {
    super("Project 3 - Euclidean Traveling Salesman (Jacob Deitloff)");
    setLayout(null);//new FlowLayout(FlowLayout.CENTER, 0, 0));
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	_display = new MapDisplay(map);
	_mstCheckbox = new JCheckBox("Show Prim's results", true);
	_treewalkCheckbox = new JCheckBox("Show treewalk tour", true);
	_borderCheckbox = new JCheckBox("Show border", true);
	_labelsCheckbox = new JCheckBox("Show node labels", true);
	getContentPane().add(_display);
	_display.setBounds(10, 10, _display.getPreferredSize().width, _display.getPreferredSize().height);
	getContentPane().add(_mstCheckbox);
	_mstCheckbox.setBounds(_display.getPreferredSize().width + 20, 5, _mstCheckbox.getPreferredSize().width,
		_mstCheckbox.getPreferredSize().height);
	getContentPane().add(_treewalkCheckbox);
	_treewalkCheckbox.setBounds(_display.getPreferredSize().width + 20, 5 + _mstCheckbox.getPreferredSize().height,
		_treewalkCheckbox.getPreferredSize().width, _treewalkCheckbox.getPreferredSize().height);
	getContentPane().add(_borderCheckbox);
	_borderCheckbox.setBounds(_display.getPreferredSize().width + 20,
		5 + _mstCheckbox.getPreferredSize().height + _treewalkCheckbox.getPreferredSize().height,
		_borderCheckbox.getPreferredSize().width, _borderCheckbox.getPreferredSize().height);
	getContentPane().add(_labelsCheckbox);
	_labelsCheckbox.setBounds(_display.getPreferredSize().width + 20,
		5 + _mstCheckbox.getPreferredSize().height + _treewalkCheckbox.getPreferredSize().height + _treewalkCheckbox.getPreferredSize().height,
		_labelsCheckbox.getPreferredSize().width, _labelsCheckbox.getPreferredSize().height);
	int bottomLabelsY = 25 + FULL_HEIGHT;
	JLabel primsWeightLabel = new JLabel("Weight (of minimum spanning tree): " +
		Double.toString(map.GetPrimsWeight()));
	getContentPane().add(primsWeightLabel);
	primsWeightLabel.setBounds(20, bottomLabelsY, primsWeightLabel.getPreferredSize().width,
		primsWeightLabel.getPreferredSize().height);
	JLabel treewalkTourLabel = new JLabel("Tour length: " +
		Double.toString(map.GetTreewalkLength()));
	getContentPane().add(treewalkTourLabel);
	treewalkTourLabel.setBounds(50 + primsWeightLabel.getPreferredSize().width, bottomLabelsY,
		treewalkTourLabel.getPreferredSize().width, treewalkTourLabel.getPreferredSize().height);
	getContentPane().setPreferredSize(new Dimension(_treewalkCheckbox.getBounds().x +
		_treewalkCheckbox.getBounds().width + 20, bottomLabelsY + 10 +
		primsWeightLabel.getPreferredSize().height));
	pack();
	_mstCheckbox.addItemListener(new ItemListener()
	{
		public void itemStateChanged(ItemEvent e)
		{
			if (e.getStateChange() == ItemEvent.SELECTED)
			{
				_display.ShowMST = true;
				_display.repaint();
			}
			else
			{
				_display.ShowMST = false;
				_display.repaint();
			}
		}
	});
	_treewalkCheckbox.addItemListener(new ItemListener()
	{
		public void itemStateChanged(ItemEvent e)
		{
			if (e.getStateChange() == ItemEvent.SELECTED)
			{
				_display.ShowTreewalk = true;
				_display.repaint();
			}
			else
			{
				_display.ShowTreewalk = false;
				_display.repaint();
			}
		}
	});
	_borderCheckbox.addItemListener(new ItemListener()
	{
		public void itemStateChanged(ItemEvent e)
		{
			if (e.getStateChange() == ItemEvent.SELECTED)
			{
				_display.ShowBorder = true;
				_display.repaint();
			}
			else
			{
				_display.ShowBorder = false;
				_display.repaint();
			}
		}
	});
	_labelsCheckbox.addItemListener(new ItemListener()
	{
		public void itemStateChanged(ItemEvent e)
		{
			if (e.getStateChange() == ItemEvent.SELECTED)
			{
				_display.ShowLabels = true;
				_display.repaint();
			}
			else
			{
				_display.ShowLabels = false;
				_display.repaint();
			}
		}
	});
	setResizable(false);
  }
}