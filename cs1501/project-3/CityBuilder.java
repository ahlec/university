public class CityBuilder
{
	public CityBuilder(int x, int y)
	{
		X = x;
		Y = y;
	}
	public final int X;
	public final int Y;
	
	public double CurrentPrimsMinEdgeWeight = 0;
	public CityBuilder CurrentPrimsMinEdgeDestination = null;
	public boolean HitByPrims = false;
	public void MarkCityPrims(CityBuilder marked)
	{
		if (HitByPrims)
		{
			return;
		}
		
		double distanceTo = GetDistanceTo(marked);
		if (CurrentPrimsMinEdgeDestination == null || distanceTo <= CurrentPrimsMinEdgeWeight)
		{
			CurrentPrimsMinEdgeDestination = marked;
			CurrentPrimsMinEdgeWeight = distanceTo;
		}
	}
	
	public double GetDistanceTo(CityBuilder other)
	{
		return Math.sqrt(Math.pow(other.X - X, 2) + Math.pow(other.Y - Y, 2));
	}
	
	public City Build()
	{
		return new City(X, Y);
	}
}