import java.util.Random;
import java.util.ArrayList;

public class Map
{
	private boolean _hasRunPrims = false;
	private double _primsWeight = 0;
	private boolean _hasRunTreewalk = false;
	private double _tourLength = 0;
	
	private City[] _cities;
	private CityBuilder[] _builders;
	private Edge[] _mst;
	private Edge[] _treewalk;
	public Map(int count, int boundX, int boundY)
	{
		Count = count;
		BoundX = boundX;
		BoundY = boundY;
		_builders = new CityBuilder[count];
		
		Random randomization = new Random();
		int x;
		int y;
		for (int index = 0; index < count; index++)
		{
			do
			{
				x = randomization.nextInt(BoundX + 1);
				y = randomization.nextInt(BoundY + 1);
			} while (HasCityBuilderAt(x, y));
			
			_builders[index] = new CityBuilder(x, y);
		}
	}
	private boolean HasCityBuilderAt(int x, int y)
	{
		for (CityBuilder city : _builders)
		{
			if (city == null)
			{
				break;
			}
			
			if (city.X == x && city.Y == y)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public final int Count;
	public final int BoundX;
	public final int BoundY;
	public City Get(int index)
	{
		if (!_hasRunTreewalk)
		{
			return _builders[index].Build();
		}
		
		return _cities[index];
	}
	
	public double GetPrimsWeight()
	{
		if (_hasRunPrims)
		{
			return _primsWeight;
		}
		
		PrimsAlgorithm();
		
		return _primsWeight;
	}
	public Edge[] GetMinimumSpanningTree()
	{
		if (_hasRunPrims)
		{
			return _mst;
		}
		
		PrimsAlgorithm();
		
		return _mst;
	}
	private void PrimsAlgorithm()
	{		
		ArrayList<CityBuilder> queue = new ArrayList<CityBuilder>();
		ArrayList<Edge> mst = new ArrayList<Edge>();
		for (int index = 0; index < Count; index++)
		{
			queue.add(_builders[index]);
		}
		
		CityBuilder location = queue.get(0);
		location.HitByPrims = true;
		for (int index = 1; index < Count; index++)
		{
			queue.get(index).MarkCityPrims(location);
		}
		
		CityBuilder current = null;
		while (true)
		{
			for (CityBuilder queued : queue)
			{
				if (!queued.HitByPrims && (current == null || queued.CurrentPrimsMinEdgeWeight < current.CurrentPrimsMinEdgeWeight))
				{
					current = queued;
				}
			}
			
			if (current == null)
			{
				break;
			}
			
			mst.add(new Edge(current.Build(), current.CurrentPrimsMinEdgeDestination.Build(), current.CurrentPrimsMinEdgeWeight));
			current.HitByPrims = true;
			
			for (int index = 0; index < Count; index++)
			{
				queue.get(index).MarkCityPrims(current);
			}
			
			current = null;
		}
		
		for (Edge edge : mst)
		{
			_primsWeight += edge.Weight;
		}
		
		_mst = new Edge[mst.size()];
		mst.toArray(_mst);
		
		_hasRunPrims = true;
	}
	public double GetTreewalkLength()
	{
		if (_hasRunTreewalk)
		{
			return _tourLength;
		}
		
		TreewalkTour();
		return _tourLength;
	}
	public Edge[] GetTreewalkTour()
	{
		if (_hasRunTreewalk)
		{
			return _treewalk;
		}
		
		TreewalkTour();
		return _treewalk;
	}
	private void TreewalkTour()
	{		
		ArrayList<CityBuilder> queue = new ArrayList<CityBuilder>();
		ArrayList<Edge> tour = new ArrayList<Edge>();
		for (int index = 0; index < Count; index++)
		{
			queue.add(_builders[index]);
		}
		
		CityBuilder current = queue.get(0);
		queue.remove(0);
		CityBuilder nearest = null;
		double tourLength = 0;
		double nearestDistance = 0;
		int nearestIndex = -1;
		while (true)
		{
			for (int index = 0; index < queue.size(); index++)
			{
				if (nearest == null || current.GetDistanceTo(queue.get(index)) < nearestDistance)
				{
					nearest = queue.get(index);
					nearestDistance = current.GetDistanceTo(queue.get(index));
					nearestIndex = index;
				}
			}
			
			if (nearest == null)
			{
				break;
			}
			
			queue.remove(nearestIndex);
			tour.add(new Edge(current.Build(), nearest.Build(), nearestDistance));
			tourLength += nearestDistance;
			current = nearest;
			nearest = null;
			nearestDistance = 0;
			nearestIndex = -1;
		}
		
		Edge finalEdge = new Edge(current.Build(), _builders[0].Build(), current.GetDistanceTo(_builders[0]));
		tour.add(finalEdge);
		tourLength += finalEdge.Weight;
		
		_treewalk = new Edge[tour.size()];
		tour.toArray(_treewalk);
		_tourLength = tourLength;
		
		_cities = new City[_builders.length];
		for (int index = 0; index < _builders.length; index++)
		{
			_cities[index] = _builders[index].Build();
		}
		
		_hasRunTreewalk = true;
	}
}