import javax.swing.UIManager;

public class EuclideanTravelingSalesman
{
	public static void main(String[] args)
	{
		try
		{
		  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e)
		{
		  System.out.println("Error setting native LAF: " + e);
		}
		
		int boundX = 2000;
		int boundY = 2000;
		
		// Begin command line arguments reading
		if (args.length < 1)
		{
			System.err.println("Calls to this program must provide one parameter, that parameter being the " +
				"number of cities to generate for testing.");
			return;
		}
		int numberCities = 0;
		try
		{
			numberCities = Integer.parseInt(args[0]);
		}
		catch (NumberFormatException e)
		{
			System.err.println("Calls to this program must provide one parameter, that parameter being the positive, nonzero integer " +
				"number of cities to generate for testing.");
			return;
		}
		if (numberCities <= 0)
		{
			System.err.println("Calls to this program must provide one parameter, that parameter being the positive, nonzero integer " +
				"number of cities to generate for testing.");
			return;
		}
		for (int index = 1; index < args.length; index += 2)
		{
			if (args[index].equals("--boundX") || args[index].equals("-bX"))
			{
				if (args.length > index + 1)
				{
					try
					{
						boundX = Integer.parseInt(args[index + 1]);
					}
					catch (NumberFormatException e)
					{
						System.err.println("Providing the command line argument for boundX requires a positive, nonzero integer value.");
						return;
					}
				}
				else
				{
					System.err.println("Provided the --boundX command line argument, without providing a value for the new bound.");
					return;
				}
			}
			else if (args[index].equals("--boundY") || args[index].equals("-bY"))
			{
				if (args.length > index + 1)
				{
					try
					{
						boundY = Integer.parseInt(args[index + 1]);
					}
					catch (NumberFormatException e)
					{
						System.err.println("Providing the command line argument for boundY requires a positive, nonzero integer value.");
						return;
					}
				}
				else
				{
					System.err.println("Provided the --boundY command line argument, without providing a value for the new bound.");
					return;
				}
			}
		}
		if (boundX < 500)
		{
			System.err.println("Providing the --boundX command line argument, the value must be a positive, nonzero integer " +
				"greater than or equal to 500.");
			return;
		}
		if (boundY < 500)
		{
			System.err.println("Providing the --boundY command line argument, the value must be a positive, nonzero integer " +
				"greater than or equal to 500.");
			return;
		}
		// End command line arguments reading
		
		Map map = new Map(numberCities, boundX, boundY);
		System.out.println("Euclidean Traveling Salesman");
		System.out.println("-------------------------------------------------");
		System.out.println("Number of cities: " + Integer.toString(map.Count));
		System.out.println("City position range:");
		System.out.println("\tX: 0–" + Integer.toString(map.BoundX));
		System.out.println("\tY: 0–" + Integer.toString(map.BoundY));
		System.out.println("Weight (of minimum spanning tree): " + Double.toString(map.GetPrimsWeight()));
		System.out.println("Treewalk tour: " + Double.toString(map.GetTreewalkLength()));
		DisplayWindow displayWindow = new DisplayWindow(map);
		displayWindow.setLocationRelativeTo(null);
		displayWindow.setVisible(true);
	}
}