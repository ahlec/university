public class RSA
{
	private static boolean[] ONE = { true };

	public static String crypto(String message, String exponent, String modulus)
	{
		boolean[] messageBools = TrimLeadingZeroes(Decode(message));
		boolean[] exponentBools = TrimLeadingZeroes(Decode(exponent));
		boolean[] modulusBools = TrimLeadingZeroes(Decode(modulus));
		return Encode(ModularExponentiation(messageBools, exponentBools, modulusBools));
	}
	
	public static boolean[] Decode(String str)
	{
		int firstOne = str.indexOf('1');
		if (firstOne < 0)
		{
			return new boolean[1];
		}
		
		boolean[] decoded = new boolean[Math.max(1, str.length() - firstOne)];
		for (int index = firstOne; index < str.length(); index++)
		{
			decoded[index - firstOne] = (str.charAt(index) == '1');
		}
		
		return decoded;
	}
	public static String Encode(boolean[] boolArray)
	{
		String encoded = "";
		for (boolean bit : boolArray)
		{
			encoded += (bit ? "1" : "0");
		}
		
		int firstOne = encoded.indexOf('1');
		if (firstOne < 0)
		{
			return "0";
		}
		
		return encoded.substring(firstOne);
	}
	public static boolean[] TrimLeadingZeroes(boolean[] boolArray)
	{
		boolean[] trimmed = null;
		int boolArrayFirstOne = -1;
		for (int index = 0; index < boolArray.length; index++)
		{
			if (boolArray[index])
			{
				if (trimmed == null)
				{
					trimmed = new boolean[boolArray.length - index];
					boolArrayFirstOne = index;
				}
				trimmed[index - boolArrayFirstOne] = true;
			}
			else if (trimmed != null)
			{
				trimmed[index - boolArrayFirstOne] = false;
			}
		}
		
		if (trimmed == null)
		{
			trimmed = new boolean[1];
		}
		
		return trimmed;
	}
	public static boolean IsAGreaterOrEqualB(boolean[] a, boolean[] b)
	{
		int maxLength = Math.max(a.length, b.length);
		boolean aBit = false;
		boolean bBit = false;
		for (int index = 0; index < maxLength; index++)
		{
			if (index >= maxLength - a.length)
			{
				aBit = a[index - maxLength + a.length];
			}
			if (index >= maxLength - b.length)
			{
				bBit = b[index - maxLength + b.length];
			}
			if (aBit != bBit)
			{
				return aBit; // aBit != bBit, so if aBit is true, aBit = 1 and bBit = 0, otherwise opposite
			}
		}
		
		return true; // equal
	}
	public static boolean[] ShiftLeft(boolean[] boolArray, int bits)
	{
		boolean[] shifted = new boolean[boolArray.length + bits];
		for (int index = 0; index < boolArray.length; index++)
		{
			shifted[index] = boolArray[index];
		}
		
		return shifted;
	}
	public static boolean[] ShiftRight(boolean[] boolArray, int bits)
	{
		if (bits >= boolArray.length)
		{
			return new boolean[1];
		}
		
		boolean[] shifted = new boolean[boolArray.length - bits];
		for (int index = 0; index < shifted.length; index++)
		{
			shifted[index] = boolArray[index];
		}
		
		return shifted;
	}	
	public static boolean[] Add(boolean[] a, boolean[] b)
	{
		int maxLength = Math.max(a.length, b.length);
		boolean[] sum = new boolean[maxLength + 1];
		boolean carry = false;
		boolean aBit = false;
		boolean bBit = false;
		int digitSum = 0;
		for (int index = 0; index < maxLength; index++)
		{
			if (index < a.length)
			{
				aBit = a[a.length - index - 1];
			}
			else
			{
				aBit = false;
			}
			if (index < b.length)
			{
				bBit = b[b.length - index - 1];
			}
			else
			{
				bBit = false;
			}
			digitSum = ((carry ? 1 : 0) + (aBit ? 1 : 0) + (bBit ? 1 : 0));
			carry = (digitSum >= 2);
			sum[sum.length - index - 1] = (digitSum == 1 || digitSum == 3);
		}
		
		sum[0] = carry;
		
		return TrimLeadingZeroes(sum);
	}
	public static boolean[] Subtract(boolean[] minuend, boolean[] subtrahend)
	{
		int minLength = Math.min(minuend.length, subtrahend.length);
		int maxLength = Math.max(minuend.length, subtrahend.length);
		boolean[] difference = new boolean[maxLength];
		int borrowedStacks = 0;
		boolean aBit;
		boolean bBit;
		boolean localResult;
		for (int index = 0; index < minLength; index++)
		{
			aBit = minuend[minuend.length - index - 1];
			bBit = subtrahend[subtrahend.length - index - 1];
			
			if (bBit)
			{
				if (aBit)
				{
					localResult = (borrowedStacks > 0);
				}
				else
				{
					if (borrowedStacks == 0)
					{
						localResult = true;
						borrowedStacks++;
					}
					else
					{
						localResult = false;
					}
				}
			}
			else
			{
				if (aBit)
				{
					if (borrowedStacks == 0)
					{
						localResult = true;
					}
					else
					{
						localResult = false;
						borrowedStacks--;
					}
				}
				else
				{
					localResult = (borrowedStacks > 0);
				}
			}
			
			difference[maxLength - index - 1] = localResult;
		}
		
		for (int index = Math.min(minuend.length + 1, minLength + 1); index <= minuend.length; index++)
		{
			localResult = false;
			if (minuend[minuend.length - index])
			{
				if (borrowedStacks > 0)
				{
					borrowedStacks--;
				}
				else
				{
					localResult = true;
				}
			}
			
			difference[maxLength - index] = localResult;
		}
		
		return TrimLeadingZeroes(difference);
	}
	public static boolean[] Multiply(boolean[] multiplicand, boolean[] multiplier)
	{
		if (multiplier.length == 1 && !multiplier[0])
		{
			return new boolean[1];
		}
		
		boolean[] tempResult = ShiftLeft(Multiply(multiplicand, ShiftRight(multiplier, 1)), 1);
		if (multiplier[multiplier.length - 1])
		{
			return Add(multiplicand, tempResult);
		}
		else
		{
			return tempResult;
		}
	}
	public static boolean[] Modulo(boolean[] number, boolean[] modulo)
	{
		if (number.length == 1 && !number[0])
		{
			return new boolean[1];
		}
		
		boolean[] tempResult = ShiftLeft(Modulo(ShiftRight(number, 1), modulo), 1);
		if (number[number.length - 1])
		{
			tempResult = Add(tempResult, ONE);
		}
		
		if (IsAGreaterOrEqualB(tempResult, modulo))
		{
			tempResult = Subtract(tempResult, modulo);
		}
		
		return TrimLeadingZeroes(tempResult);
	}
	public static boolean[] ModularExponentiation(boolean[] baseNumber, boolean[] exponent, boolean[] modulus)
	{
		if (exponent.length == 1 && !exponent[0])
		{
			boolean[] one = new boolean[1];
			one[0] = true;
			return one;
		}
		
		boolean[] localResult = ModularExponentiation(baseNumber, ShiftRight(exponent, 1), modulus);
		localResult = Multiply(localResult, localResult);
		
		if (exponent[exponent.length - 1])
		{
			localResult = Multiply(localResult, baseNumber);
		}
		
		localResult = Modulo(localResult, modulus);
		return localResult;
	}
}