public enum OptimalType
{
	None,
	PatternPlacement,
	HorizontalCut,
	VerticalCut
}