public class Pattern
{
	public Pattern(char handle, int width, int height, int sellPrice)
	{
		Handle = handle;
		Width = width;
		Height = height;
		SellPrice = sellPrice;
	}
	public final int Width;
	public final int Height;
	public final int SellPrice;
	public final char Handle;
}