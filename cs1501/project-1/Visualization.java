import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.border.EmptyBorder;

public class Visualization extends JFrame
{
  private static final int PIXELS_PER_CLOTH_UNIT = 30;
  private class CutterDrawing extends JPanel
  {
	private Optimal _optimal;
	private int _cutsToDraw = 1;
	private int _cutsDrawnSoFar = 0;
	private int _patternsToDraw = 0;
	private int _patternsDrawnSoFar = 0;
	private boolean _doneTickingCuts = false;
    public CutterDrawing(Optimal optimal)
	{
		setPreferredSize(new Dimension(optimal.Width * PIXELS_PER_CLOTH_UNIT,
			optimal.Height * PIXELS_PER_CLOTH_UNIT));
		_optimal = optimal;
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(0,0, _optimal.Width * PIXELS_PER_CLOTH_UNIT, _optimal.Height * PIXELS_PER_CLOTH_UNIT);
		g.setColor(Color.BLACK);
		
		_cutsDrawnSoFar = 0;
		_patternsDrawnSoFar = 0;
		DrawPattern(g, _optimal, 0, 0);
		DrawCut(g, _optimal, 0, 0);
		
		if (_cutsToDraw > _cutsDrawnSoFar)
		{
			_doneTickingCuts = true;
		}
		else if (_patternsToDraw > _patternsDrawnSoFar)
		{
			Visualization.this._isFinishedDynamicallyDrawing = true;
		}
	}
	
	public void Tick()
	{
		if (!_doneTickingCuts)
		{
			_cutsToDraw++;
		}
		else
		{
			_patternsToDraw++;
		}
	}
	
	private void DrawPattern(Graphics g, Optimal optimal, int offsetX, int offsetY)
	{
		if (optimal == null || optimal.Type == OptimalType.None || _patternsDrawnSoFar >= _patternsToDraw)
		{
			return;
		}
		
		if (optimal.Type == OptimalType.PatternPlacement)
		{
			Pattern pattern = (Pattern)optimal.TypeData;
			g.setColor(Color.YELLOW);
			g.fillRect(offsetX * PIXELS_PER_CLOTH_UNIT, offsetY * PIXELS_PER_CLOTH_UNIT, pattern.Width * PIXELS_PER_CLOTH_UNIT,
				pattern.Height * PIXELS_PER_CLOTH_UNIT);
			g.setColor(Color.BLACK);
			g.drawString(Character.toString(pattern.Handle), offsetX * PIXELS_PER_CLOTH_UNIT + 12,
				offsetY * PIXELS_PER_CLOTH_UNIT + 12);
			_patternsDrawnSoFar++;
		}
		
		int shardBOffset_x = 0;
		int shardBOffset_y = 0;
		
		if (optimal.Type == OptimalType.HorizontalCut)
		{
			shardBOffset_y = ((Integer)optimal.TypeData).intValue();
		}
		else if (optimal.Type == OptimalType.VerticalCut)
		{
			shardBOffset_x = ((Integer)optimal.TypeData).intValue();
		}
		
		DrawPattern(g, optimal.ShardA, offsetX, offsetY);
		DrawPattern(g, optimal.ShardB, offsetX + shardBOffset_x, offsetY + shardBOffset_y);
	}
	private void DrawCut(Graphics g, Optimal optimal, int offsetX, int offsetY)
	{
		if (optimal == null || optimal.Type == OptimalType.None || optimal.Type == OptimalType.PatternPlacement ||
			_cutsDrawnSoFar >= _cutsToDraw)
		{
			return;
		}
		
		int pointA_x;
		int pointA_y;
		int pointB_x;
		int pointB_y;
		int shardBOffset_x = 0;
		int shardBOffset_y = 0;
		
		if (optimal.Type == OptimalType.HorizontalCut)
		{
			pointA_x = offsetX * PIXELS_PER_CLOTH_UNIT;
			pointA_y = (offsetY + ((Integer)optimal.TypeData).intValue()) * PIXELS_PER_CLOTH_UNIT;
			pointB_x = (offsetX + optimal.Width) * PIXELS_PER_CLOTH_UNIT;
			pointB_y = pointA_y;
			shardBOffset_y = ((Integer)optimal.TypeData).intValue();
		}
		else
		{
			pointA_x = (offsetX + ((Integer)optimal.TypeData).intValue()) * PIXELS_PER_CLOTH_UNIT;
			pointA_y = offsetY * PIXELS_PER_CLOTH_UNIT;
			pointB_x = pointA_x;
			pointB_y = (offsetY + optimal.Height) * PIXELS_PER_CLOTH_UNIT;
			shardBOffset_x = ((Integer)optimal.TypeData).intValue();
		}
		
		g.drawLine(pointA_x, pointA_y, pointB_x, pointB_y);
		
		_cutsDrawnSoFar++;
		
		DrawCut(g, optimal.ShardA, offsetX, offsetY);
		DrawCut(g, optimal.ShardB, offsetX + shardBOffset_x, offsetY + shardBOffset_y);
	}	
  }

  private boolean _isFinishedDynamicallyDrawing = false;
  private CutterDrawing _drawing;
  public Visualization(Optimal optimal)
  {
    super("Project 1 - Cloth Cutter (Jacob Deitloff)");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	_drawing = new CutterDrawing(optimal);
	getContentPane().add(_drawing);
	setSize(getWidth() + 30, getHeight() + 30);
	pack();
  }
  
  public void Tick()
  {
	_drawing.Tick();
	repaint();
  }
  
  public boolean GetIsFinishedDrawing()
  {
	return _isFinishedDynamicallyDrawing;
  }
}