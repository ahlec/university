public class Optimal
{
	public Optimal(int width, int height, int cost, OptimalType type, Object typeData,
		Optimal shardA, Optimal shardB)
	{
		Width = width;
		Height = height;
		Cost = cost;
		Type = type;
		TypeData = typeData;
		ShardA = shardA;
		ShardB = shardB;
	}
	
	public final int Width;
	public final int Height;
	public final int Cost;
	public final OptimalType Type;
	public final Object TypeData;
	public final Optimal ShardA;
	public final Optimal ShardB;
}