import java.util.ArrayList;
import java.util.Date;

public class ClothCutter
{
  private Pattern[] _patterns;
  private Optimal[] _memo;
  
  public final int ClothWidth;
  public final int ClothHeight;
  
  public ClothCutter(int width, int height, ArrayList<Pattern> patterns)
  {
	ClothWidth = width;
	ClothHeight = height;
	_patterns = new Pattern[patterns.size()];
	_patterns = patterns.toArray(_patterns);
	_memo = new Optimal[width * height];
  }

  public static void main(String[] arguments)
  {
    ArrayList<Pattern> patterns = new ArrayList<Pattern>();
	patterns.add(new Pattern('a', 2, 2, 1));
	patterns.add(new Pattern('b', 2, 6, 4));
	patterns.add(new Pattern('c', 4, 2, 3));
	patterns.add(new Pattern('d', 5, 3, 5));
	
    int width = 0;
    int height = 0;

    if (arguments.length < 2)
    {
      System.err.println("You must provide at least two parameters when calling this program: \"> java ClothCutter WIDTH HEIGHT\"");
      return;
    }
	
	try
	{
		width = Integer.parseInt(arguments[0]);
		height = Integer.parseInt(arguments[1]);
	}
	catch (NumberFormatException e)
	{
		System.err.println("Dimensions of the cloth must be integers, both.");
		return;
	}
	
	if (width <= 0 || height <= 0)
	{
		System.err.println("Dimensions of the cloth must be positive, nonzero numbers, both.");
		return;
	}
	
	ClothCutter cutter = new ClothCutter(width, height, patterns);
	
	try
	{
		System.out.println("Optimizing...");
		Optimal optimal = cutter.Optimize();
		System.out.println("");
		System.out.println("Total sale price: $" + optimal.Cost);
		System.out.println("");
		System.out.println("Pattern locations:");
		PrintPatterns(optimal, 0, 0);
		
		Visualization visualization = new Visualization(optimal);
		visualization.setVisible(true);
		
		while (!visualization.GetIsFinishedDrawing())
		{
			visualization.Tick();
			sleep(75);
		}
	}
	catch (Exception e)
	{
		System.err.println("ENCOUNTERED AN ERROR WHILE OPTIMIZING:");
		System.err.println(e.toString());
		return;
	}
  }
  
  public static void sleep(long milliseconds)
  {
    Date date = new Date();
    long start = date.getTime();
	long now;
	do
	{
		date = new Date();
		now = date.getTime();
	} while ((now - start) < milliseconds);
  }
  
  public static void PrintPatterns(Optimal optimal, int offsetX, int offsetY)
  {
	if (optimal == null || optimal.Type == OptimalType.None)
	{
		return;
	}
	
	if (optimal.Type == OptimalType.PatternPlacement)
	{
		System.out.println("[" + ((Pattern)optimal.TypeData).Handle + ": " + offsetX + ", " +
			offsetY + "]");
	}
	else if (optimal.Type == OptimalType.HorizontalCut)
	{
		PrintPatterns(optimal.ShardA, offsetX, offsetY);
		PrintPatterns(optimal.ShardB, offsetX, offsetY + ((Integer)optimal.TypeData).intValue());
	}
	else if (optimal.Type == OptimalType.VerticalCut)
	{
		PrintPatterns(optimal.ShardA, offsetX, offsetY);
		PrintPatterns(optimal.ShardB, offsetX + ((Integer)optimal.TypeData).intValue(), offsetY);
	}
  }
  
  public Optimal Optimize() throws Exception
  {
	return GetOptimalCut(ClothWidth, ClothHeight);
  }
  
  private Optimal GetOptimalCut(int width, int height) throws Exception
  {
	int memoIndex = (width - 1) * ClothHeight + height - 1;
	if (_memo[memoIndex] != null)
	{
		return _memo[memoIndex];
	}
	
	ArrayList<Optimal> options = new ArrayList<Optimal>();
	for (Pattern pattern : _patterns)
	{
		if (pattern.Width > width || pattern.Height > height)
		{
			continue;
		}
		
		options.add(new Optimal(width, height, pattern.SellPrice, OptimalType.PatternPlacement,
			pattern, null, null));
	}
	
	if (options.size() > 0) // If no patterns alone fit, no combination of two or more will fit
	{
		for (int cutX = 1; cutX <= Math.ceil(width / 2); cutX++)
		{
			Optimal left = GetOptimalCut(cutX, height);
			Optimal right = GetOptimalCut(width - cutX, height);
			int totalPrice = 0;
			if (left != null)
			{
				totalPrice += left.Cost;
			}
			if (right != null)
			{
				totalPrice += right.Cost;
			}
			
			options.add(new Optimal(width, height, totalPrice, OptimalType.VerticalCut, cutX, 
				left, right));
		}
		
		for (int cutY = 1; cutY <= Math.ceil(height / 2); cutY++)
		{
			Optimal top = GetOptimalCut(width, cutY);
			Optimal bottom = GetOptimalCut(width, height - cutY);
			int totalPrice = 0;
			if (top != null)
			{
				totalPrice += top.Cost;
			}
			if (bottom != null)
			{
				totalPrice += bottom.Cost;
			}
			
			options.add(new Optimal(width, height, totalPrice, OptimalType.HorizontalCut, cutY,
				top, bottom));
		}
		
		Optimal optimalCut = null;
		for (Optimal option : options)
		{
			if (optimalCut == null || option.Cost > optimalCut.Cost)
			{
				optimalCut = option;
			}
		}
		
		if (optimalCut == null)
		{
			throw new Exception("Somehow found no optimal cuts despite having options for optimals?");
		}
		
		_memo[memoIndex] = optimalCut;
	}
	else
	{
		_memo[memoIndex] = new Optimal(width, height, 0, OptimalType.None, null, null, null);
	}
	
	return _memo[memoIndex];
  }
}
