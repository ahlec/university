public class StringTable
{
	private class ChainNode
	{
		public String Value;
		public ChainNode Next;
	}
	public StringTable(int capacity)
	{
		CAPACITY = capacity;
		_nodes = new ChainNode[capacity];
	}
	
	private final int CAPACITY;
	private ChainNode[] _nodes;
	
	public void insert(String str)
	{
		int hashKey = Hash(str);
		if (_nodes[hashKey] == null)
		{
			_nodes[hashKey] = new ChainNode();
			_nodes[hashKey].Value = str;
		}
		else
		{
			ChainNode current = _nodes[hashKey];
			while (current.Next != null)
			{
				current = current.Next;
			}
			current.Next = new ChainNode();
			current.Next.Value = str;
		}
	}
	public boolean contains(String str)
	{
		int hashKey = Hash(str);
		ChainNode current = _nodes[hashKey];
		while (current != null)
		{
			if (current.Value.equals(str))
			{
				return true;
			}
			current = current.Next;
		}
		return false;
	}	

	private int Hash(String str)
	{
		int hashCode = 0;
		int stringLength = str.length();
		for (int index = 0; index < stringLength; index++)
		{
		  hashCode += Math.pow(26, stringLength - index - 1) * (str.charAt(index) - 96);
		}
		hashCode -= 1; // set start of hash function at index = 0;
		return (hashCode % CAPACITY);
	}
}