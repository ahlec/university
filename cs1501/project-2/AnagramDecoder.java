import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class AnagramDecoder
{
	private class CharacterCount
	{
		public CharacterCount(char character)
		{
			Character = character;
			Count = 1;
		}
		public final char Character;
		public int Count;
		
		public boolean equals(Object obj)
		{
			if (!(obj instanceof CharacterCount))
			{
				return false;
			}
			return (this.Character == ((CharacterCount)obj).Character);
		}
	}
	
	ArrayList<String> _decoded;
	StringTable _dictionary;
	StringTable _prefixes;
	String _anagram;
	
	public AnagramDecoder(StringTable dictionary, StringTable prefixes, String anagram)
	{
		_decoded = new ArrayList<String>();
		_dictionary = dictionary;
		_prefixes = prefixes;
		_anagram = anagram;
	}
	
	public void PrintAnagrams()
	{
		ArrayList<CharacterCount> characters = new ArrayList<CharacterCount>();
		for (char character : _anagram.toCharArray())
		{
			CharacterCount newChar = new CharacterCount(character);
			if (characters.contains(newChar))
			{
				characters.get(characters.indexOf(newChar)).Count++;
			}
			else
			{
				characters.add(newChar);
			}
		}
		
		Find("", characters, _anagram.length(), _anagram.length());
	}
	
	public static void main(String[] args)
	{
		if (args.length != 2)
		{
			System.err.println("Requires two arguments, the first being the dictionary file, the second being the anagram to decode.");
			return;
		}
		
		System.out.print("Loading dictionary...");
		StringTable dictionary = new StringTable(977);
		StringTable prefixes = new StringTable(977);
		try
		{
			BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
			String readLine = fileReader.readLine();
			while (readLine != null)
			{
				dictionary.insert(readLine);
				for (int index = 1; index < readLine.length() - 1; index++)
				{
					if (!prefixes.contains(readLine.substring(0, index)))
					{
						prefixes.insert(readLine.substring(0, index));
					}
				}
				readLine = fileReader.readLine();
			}
			fileReader.close();
		} catch (IOException e)
		{
			System.err.println("\nError while loading the dictionary into the program. Aborting.");
			return;
		}
		
		System.out.println("\tDONE.");
		System.out.println("Anagrams:");
		
		AnagramDecoder decoder = new AnagramDecoder(dictionary, prefixes, args[1]);
		decoder.PrintAnagrams();
	}
	
	private String[] Find(String currentWord, ArrayList<CharacterCount> characters, int numberCharactersLeft, int totalCharacters)
	{
		ArrayList<String> unscrambles = new ArrayList<String>();
		for (CharacterCount character : characters)
		{
			if (character.Count == 0)
			{
				continue;
			}
			
			character.Count--;
			
			if (_dictionary.contains(currentWord + character.Character))
			{
				//System.out.println("!!" + Integer.toString(numberCharactersLeft) + "!!" + currentWord + character.Character);
				if (numberCharactersLeft <= 1)// == 0)
				{
					if (numberCharactersLeft == totalCharacters)
					{
						System.out.println("\t" + currentWord + character.Character);
					}
					else
					{
						unscrambles.add(currentWord + character.Character);
					}
				}
				else
				{
					for (String part : Find("", characters, numberCharactersLeft - 1, totalCharacters))
					{
						if (numberCharactersLeft == totalCharacters)
						{
							System.out.println("\t" + currentWord + character.Character + " " + part);
						}
						else
						{
							unscrambles.add(currentWord + character.Character + " " + part);
						}
					}
				}
			}
			
			if (numberCharactersLeft > 1 && _prefixes.contains(currentWord + character.Character))
			{
				String[] further = Find(currentWord + character.Character, characters, numberCharactersLeft - 1, totalCharacters);
				if (numberCharactersLeft == totalCharacters)
				{
					for (String result : further)
					{
						System.out.println("\t" + result);
					}
				}
				else
				{
					unscrambles.addAll(Arrays.asList(further));
				}
				//unscrambles.addAll(Arrays.asList(Find(currentWord + character.Character, characters, numberCharactersLeft - 1)));
			}
			
			character.Count++;
		}
		
		String[] returnArray = new String[unscrambles.size()];
		unscrambles.toArray(returnArray);
		
		return returnArray;
	}
}