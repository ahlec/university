<?php
require_once ("config.php");
header('Content-type: application/xhtml+xml; charset=UTF-8');
$place = $_REQUEST['place'];

function return_error($errorno, $place) {
    $msg = "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><title>Error $errorno</title>
    <link type=\"text/css\" href=\"http://obdurodon.org/css/style.css\" rel=\"stylesheet\"/>
</head>
<body>
    <h1>Error $errorno</h1>
        <p>$place</p>
    <p>No such place. If you believe that this message results from a bug
    (that is, that the place exists and the system failed to find it), please
    send the url (from the address line in your browsers) to 
    <a href=\"mailto:djbpitt@gmail.com\">djbpitt@gmail.com</a> with the date and
    time (including your time zone) and a note that you received <strong>Error $errorno</strong>.</p>
</body></html>
";
    die($msg);
}

$placeQuery = <<< placeQuery
    string-join(distinct-values(doc('/db/genealogy/genealogy.xml')//place),'\n')
placeQuery;
$placeArray = array();
$contents = REST_PATH . "/db/genealogy?_howmany=100&_wrap=no&_query=" . urlencode($placeQuery);
$placeValues = file_get_contents($contents);
foreach (explode("\n", $placeValues) as $currentItem) {
    $placeArray[] = $currentItem;
}
$pattern = '/^[-′ łA-Za-z0-9]+$/D'; // places are English alphanumeric, barred l, hyphen, prime, space
if (strlen($place) > 40 || !preg_match($pattern, $place)) { // longest is current 38 chars
    return_error(1, $place);
} elseif (!in_array($place, $placeArray)) { // is it in the corpus?
    return_error(2, $place);
} else { // it's okay
    $place = urlencode($place);
    $contents = REST_PATH . "/db/genealogy/xquery/place.xql?place=$place";
    $result = file_get_contents($contents);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
        <?php require_once ("boilerplate.php"); ?>
        </div> 
        <?php
        require_once ("navigation.php");
        echo "<h2>Rulers of " . urldecode($place) . "</h2>";
        echo $result; 
        ?>
    </body>
</html>
