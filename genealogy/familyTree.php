<?php
require_once ("config.php");
//header('Content-type: application/xhtml+xml; charset=UTF-8');
$person = $_REQUEST['person'];
$hi = $_REQUEST['hi'];
$lo = $_REQUEST['lo'];
if (!is_numeric($hi) || !is_numeric($lo) || $hi < 0 || $lo < 0)
{
  $hi = 2;
  $lo = 2;
}

$idQuery = <<< idQuery
    string-join(doc('/db/genealogy/genealogy.xml')//person/@id/data(.),'\n')
idQuery;

$idArray = array();
$contents = REST_PATH . "/db/genealogy?_howmany=100&_wrap=no&_query=" . urlencode($idQuery);
$idValues = file_get_contents($contents);
foreach (explode("\n", $idValues) as $currentItem) {
    $idArray[] = $currentItem;
}
$pattern = '/^[-A-Za-z0-9]+$/D'; // id values are alphanumeric plus hyphens
if (strlen($person) > 40 || !preg_match($pattern, $person)) { // longest is current 38 chars
    return_error(1, $person);
} elseif (!in_array($person, $idArray)) { // is it in the corpus?
    return_error(2, $person, $idArray);
} else { // it's okay
    $personName = $person;
    if (file_exists("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".svg"))
    {
      /*$databaseLastModified = filemtime(REST_PATH . "/db/genealogy/genealogy.xml");
      $xsltLastModified = filemtime(REST_PATH . "/db/genealogy/xquery/familyTreeGraphviz.xsl");
      $xqueryLastModified = filemtime(REST_PATH . "/db/genealogy/xquery/familyTree.xql");
      $svgLastModified = filemtime("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".svg");
      if ($svgLastModified < $databaseLastModified || $svgLastModified < $xsltLastModified || $svgLastModified < $xqueryLastModified)*/
        unlink("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".svg");
    }
    if (!file_exists("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".svg"))
    {
      $contents = REST_PATH . "/db/genealogy/xquery/familyTree.xql?person=$person&hi=$hi&lo=$lo";
      $personReport = htmlspecialchars_decode(file_get_contents($contents));
      if (false === file_put_contents("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".gv", $personReport))
        exit ("writing error");
      $familyTreeXML = file_get_contents($contents . "&doTransform=no");
      file_put_contents("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".xml", $familyTreeXML);
      shell_exec("dot -Tsvg familyTrees/" . $person . "_" . $hi . "_" . $lo . ".gv > familyTrees/" . $person .
	"_" . $hi . "_" . $lo . ".svg");
    }
    $familyTreeXML = simplexml_load_file("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".xml");
    $generationsUp = $familyTreeXML->generations[0]->attributes()->up;
    $generationsDown = $familyTreeXML->generations[0]->attributes()->down;
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
        <link type="text/css" href="http://genealogy.obdurodon.org/genealogy.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
        <?php require_once ("boilerplate.php"); ?>
        </div>
        <?php
        require_once ("navigation.php");
	echo "<a href=\"" . URL_PATH . "/findPerson.php?person=" . $person . "\" class=\"profileReturn\">&lt; Return to profile</a>\n";
	echo "<h2 class=\"name\">" . $personName . "</h2>\n";
	echo "<h4 class=\"subtitle\">Family tree, " . $generationsUp . " generation" . ($generationsUp == 1 ? "" : "s") . " up, " . $generationsDown .
		" generation" . ($generationsDown == 1 ? "" : "s") . " down</h4>\n";
	echo "<div id=\"familyTreeContainer\" class=\"familyTree\">\n";
	echo "<input type=\"hidden\" id=\"familyTreeXmlURL\" value=\"" . URL_PATH . "/familyTrees/" . $person . "_".  $hi . "_" .
		$lo . ".xml\" />\n";
	echo file_get_contents("familyTrees/" . $person . "_" . $hi . "_" . $lo . ".svg");
	echo "</div>\n";
        //echo "<embed class=\"familyTree\" src=\"familyTrees/" . $person . "_" . $hi . "_" . $lo . ".svg\" type=\"image/svg+xml\" />";
        ?>
    </body>
    <?php
    /*
     * error1 = malformed filename; should be a\d{3}\.xml; user error
     * error2 = well-formed filename but not in the corpus; user error
     * error3 = html doesn't exist but couldn't generate it; my fault
     * error4 = html exists but xml is newer, yet couldn't regenerate html; my fault
     */

    function return_error($errorno, $person, $idArray) {
        $msg = "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><title>Error $errorno</title>
    <link type=\"text/css\" href=\"http://obdurodon.org/css/style.css\" rel=\"stylesheet\"/>
</head>
<body>
    <h1>Error $errorno</h1>
        <p>$person</p>
    <p>No such person. If you believe that this message results from a bug
    (that is, that the file exists and the system failed to find it), please
    send the url (from the address line in your browsers) to 
    <a href=\"mailto:djbpitt@gmail.com\">djbpitt@gmail.com</a> with the date and
    time (including your time zone) and a note that you received <strong>Error $errorno</strong>.</p>
</body></html>
";
        die($msg);
    }
    ?>
</html> 
