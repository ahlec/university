function ft_StartRemovePerson()
{
  alert("Click on an individual on the family tree to remove them and their descendents.");
  var svgElement = document.getElementById("familyTreeContainer").getElementsByTagName("svg")[0];
  var gElements = svgElement.getElementsByTagName("a");
  var onclickDelegate;
  onclickDelegate = function(e)
  {
	e = e || window.event;
	var personHandle = this.parentNode.getElementsByTagName("title")[0].textContent ||
		this.parentNode.getElementsByTagName("title")[0].innerText;
	
	ft_Remove(personHandle);

	if (e.stopPropagation)
	  e.stopPropagation();
	if (e.cancelBubble)
	  e.cancelBubble = true;
        for (var index = 0; index < gElements.length; index++)
          gElements[index].onclick = null;
	return false;
  };
  for (var index = 0; index < gElements.length; index++)
    gElements[index].onclick = onclickDelegate;
}

function loadXML(filename)
{
  var frameXMLHTTP;
  if (window.XMLHttpRequest)
    frameXMLHTTP = new XMLHttpRequest();
  else
    frameXMLHTTP = new ActiveXObject("Microsoft.XMLHTTP");
  frameXMLHTTP.open("GET", filename, false);
  frameXMLHTTP.send("");
  return frameXMLHTTP.responseXML;
}

function ft_Remove(personHandle)
{
  var xml = loadXML(document.getElementById("familyTreeXmlURL").value);
  var relations = xml.getElementsByTagName("relation");
  var spousialRelations = new Array();
  var childRelations = new Array();
  for (var index = 0; index < relations.length; index++)
    if (relations[index].getAttribute("type") == "child")
      childRelations.push(relations[index]);
    else
      spousialRelations.push(relations[index]);
  ft_Remove_Recursion(personHandle, spousialRelations, childRelations);
}

function ft_Remove_Recursion(targetPerson, spousialRelations, childRelations)
{
  ft_RemoveNode(targetPerson);
  /*var spouses = new Array();
  for (var spouse = 0; spouse < spousialRelations.length; spouse++)
  {
    if (spouses.indexOf(spousialRelations[spouse].parentNode.getAttribute("handle")) >= 0 ||
	!ft_NodeExists(spousialRelations[spouse].parentNode.getAttribute("handle")))
      continue;
    var spouseShouldExist = false;
    var relations = spousialRelations[spouse].parentNode.getElementsByTagName("relation");
    for (var relation = 0; relation < relations.length; relation++)
      if (relations[relation].getAttribute("type") == "spouse" && ft_NodeExists(relations[relation].getAttribute("to")) &&
		relations[relation].getAttribute("to") != targetPerson)
      {
        spouseShouldExist = true;
        break;
      }
    if (!spouseShouldExist)
    {
      ft_Remove_Recursion(spousialRelations[spouse].parentNode.getAttribute("handle"), spousialRelations, childRelations);
      spouses.push(spousialRelations[spouse].parentNode.getAttribute("handle"));
    }
  }*/
  for (var child = 0; child < childRelations.length; child++)
  {
    if (!ft_NodeExists(childRelations[child].parentNode.getAttribute("handle")))
      continue;
    var noFather = !ft_NodeExists(childRelations[child].getAttribute("to")) || childRelations[child].getAttribute("to") == targetPerson;
    var noMother = !ft_NodeExists(childRelations[child].getAttribute("with")) || childRelations[child].getAttribute("with") == targetPerson;
    if (noFather && noMother)
      ft_Remove_Recursion(childRelations[child].parentNode.getAttribute("handle"), spousialRelations, childRelations);
  }
}

function ft_NodeExists(personHandle)
{
  var svgElement = document.getElementById("familyTreeContainer").getElementsByTagName("svg")[0];
  var nodeTitles = svgElement.getElementsByTagName("title");
  for (var index = 0; index < nodeTitles.length; index++)
    if ((nodeTitles[index].textContent || nodeTitles[index].innerText) == personHandle)
      return true;
  return false;
}

function ft_RemoveNode(personHandle)
{
  var svgElement = document.getElementById("familyTreeContainer").getElementsByTagName("svg")[0];
  var nodeTitles = svgElement.getElementsByTagName("title");
  for (var index = 0; index < nodeTitles.length; index++)
  {
    var title = nodeTitles[index].textContent || nodeTitles[index].innerText;
    if (title == personHandle)
    {
      nodeTitles[index].parentNode.parentNode.removeChild(nodeTitles[index].parentNode);
    }
  }
}
