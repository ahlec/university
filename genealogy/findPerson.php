<?php
require_once ("config.php");
header('Content-type: application/xhtml+xml; charset=UTF-8');
$person = $_REQUEST['person'];

$idQuery = <<< idQuery
    string-join(doc('/db/genealogy/genealogy.xml')//person/@id/data(.),'\n')
idQuery;

$idArray = array();
$contents = REST_PATH . "/db/genealogy?_howmany=100&_wrap=no&_query=" . urlencode($idQuery);
$idValues = file_get_contents($contents);
foreach (explode("\n", $idValues) as $currentItem) {
    $idArray[] = $currentItem;
}
$pattern = '/^[-A-Za-z0-9]+$/D'; // id values are alphanumeric plus hyphens
if (strlen($person) > 40 || !preg_match($pattern, $person)) { // longest is current 38 chars
    return_error(1, $person);
} elseif (!in_array($person, $idArray)) { // is it in the corpus?
    return_error(2, $person, $idArray);
} else { // it's okay
    $contents = REST_PATH . "/db/genealogy/xquery/person2.xql?person=$person";
    $personReport = file_get_contents($contents);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
        <link type="text/css" href="http://genealogy.obdurodon.org/genealogy.css" rel="stylesheet"/>
        <script>
          var initialScrollOffset = null;
          function goingToFootnote()
          {
            if (document.body.scrollTop)
              initialScrollOffset = document.body.scrollTop;
            else if (window.pageYOffset)
              initialScrollOffset = window.pageYOffset;
            else
              initialScrollOffset = (document.body.parentElement ? document.body.parentElement.scrollTop : 0);
          }
          function returnToText(e)
          {
            if (!e) var e = window.event;
            if (initialScrollOffset == null || !window.scroll)
              return;
            window.scroll(0, initialScrollOffset);
            initialScrollOffset = null;
            if (e.preventDefault)
              e.preventDefault();
            if (e.returnValue)
              e.returnValue = false;
          }
        </script>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
        <?php require_once ("boilerplate.php"); ?>
        </div>
        <?php
        require_once ("navigation.php");
        echo "$personReport";
        ?>
    </body>
    <?php
    /*
     * error1 = malformed filename; should be a\d{3}\.xml; user error
     * error2 = well-formed filename but not in the corpus; user error
     * error3 = html doesn't exist but couldn't generate it; my fault
     * error4 = html exists but xml is newer, yet couldn't regenerate html; my fault
     */

    function return_error($errorno, $person, $idArray) {
        $msg = "
<html xmlns='http://www.w3.org/1999/xhtml'>
<head><title>Error $errorno</title>
    <link type=\"text/css\" href=\"http://obdurodon.org/css/style.css\" rel=\"stylesheet\"/>
</head>
<body>
    <h1>Error $errorno</h1>
        <p>$person</p>
    <p>No such person. If you believe that this message results from a bug
    (that is, that the file exists and the system failed to find it), please
    send the url (from the address line in your browsers) to 
    <a href=\"mailto:djbpitt@gmail.com\">djbpitt@gmail.com</a> with the date and
    time (including your time zone) and a note that you received <strong>Error $errorno</strong>.</p>
</body></html>
";
        die($msg);
    }
    ?>
</html> 
