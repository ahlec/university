<p class="boilerplate"><span><strong>Maintained by: </strong>
                    Christian Raffensperger (<a href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>)
                    and David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>)
                    <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" style="outline: none;">
                        <img src="http://obdurodon.org/images/cc/88x31.png"
                             alt="[Creative Commons BY-NC-SA 3.0 Unported License]"
                             title="Creative Commons BY-NC-SA 3.0 Unported License"
                             style="height: 1em; vertical-align: text-bottom;"/>
                    </a></span>
                <span><strong>Last modified:</strong>
                    <?php echo date(DATE_ISO8601, filemtime(basename($_SERVER['SCRIPT_NAME']))); ?></span>
                <span><strong>About this site: </strong> <a href="about.php">http://genealogy.obdurodon.org/about.php</a></span>

            </p>

