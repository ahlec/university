<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
        <style type="text/css">
            img.vladimir {max-width: 250px; width: 40%; float: right; margin: 0 0 1em 1em; border: none;}</style>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
        <?php require_once("boilerplate.php"); ?>
        </div>
        <?php require_once("navigation.php"); ?>
        <h2>Family tree generation</h2>
        <p>The next enhancement planned for this project is a facility to generate on demand a graphical family tree for any
		individual within the genealogical database. Given certain user-specified parameters (the individual at the
		center of the tree and the number of generations up and down), a custom-designed algorithm is able to
		draw the appropriate family tree and present it to the user. This family tree allows for visual
		display of the data, while simultaneously enabling navigation throughout the project as well;
		clicking on the name of an individual on the family tree takes the user to the information
		profile of that person.</p>
		<p>This system is still in development as of May 2012, though the program has produced highly
		promising results already. The code, programmed primarily by Jacob Deitloff, was presented
		as a final presentation for the University of Pittsburgh First Experiences in Research spring 2012 
		symposium. However, development of this tool is ongoing.</p>
		<p>Below are some example tree generations as produced by the program:</p>
		<ol>
			<li><a href="treeDemo/sviatoslav1.svg" target="_blank">Sviatoslav Igorich</a>. An example
			of a fairly large family tree graph, displaying the scalability of the program thus far. <strong>(1
			generation up, 2 generations down)</strong></li>
			<li><a href="treeDemo/mieszko3.svg" target="_blank">Mieszko of Poland</a>. A demonstration of
			a family tree containing a number of generations but that manages to stack properly. <strong>(2
			generations up, 2 generations down)</strong></li>
			<li><a href="treeDemo/sofia.svg" target="_blank">Sofia Iaroslavna</a>. A larger family tree
			displaying the problem with overly-horizontal family trees, which the program will need to
			address by staggering the members of the generation. This family tree has a female focal point,
			displaying the program’s ability to function properly given a male or female focus point. <strong>(2
			generations up, 2 generations down)</strong></li>
		</ol>
		<p>It is our hope to continue to develop this program and remove a number of rendering bugs, as
		well as further the code and the capabilities of the program. The most immediate goals include:</p>
		<ul>
			<li>Staggering long generations into multiple sub-rows, alleviating the need for scrolling
			horizontally to view a full generation.</li>
			<li>Eliminating the curved lines that arc across the graph in cases involving certain
			intra-familial marriages.</li>
			<li>Designing a universal input format to allow data in formats other than the XML schema
			of this site, which would allow for the program to be used easily in other projects.</li>
			<li>Producing results that show selected pieces of information that the user requests
			(such as birth and death dates, regnal dates and places, birth place, etc.).</li>
		</ul>
    </body>
</html> 
