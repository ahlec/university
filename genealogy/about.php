<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
        <?php require_once ("boilerplate.php"); ?>   
        </div>
        <?php require_once ("navigation.php"); ?>
        <h2>About this site</h2>
        <h3>Introduction</h3>
        <p>The <cite>Rusian Genealogical Database</cite> is both a new genealogy of the Rusian royal
            family, the Riurikids, and their connections with other royal families throughout Europe
            and a new way of organizing and utilizing that information. </p>
        <p>The project began with an attempt to fully revise and update the genealogy of the
            Riurikids. The main reference for the last century for Rusian genealogy has been the
            work of N. de Baumgarten. Baumgarten’s work was excellent in its time, but it is
            currently outdated, and although almost all scholars working on Rus′ continue to use it,
            they acknowledge its limitations, errors, and problems. This modern experience confirmed
            a need for a new Rusian genealogy. </p>
        <p>The methodology behind this project was to return to the primary sources that Baumgarten
            and others have used to attempt to identify birth, death, marriage, and regnal
            information for the Riurikids and their spouses from Vladimir Sviatoslavich through his
            twelfth-century descendants. That information was used and interpreted in the light of
            current historical scholarship, such as Martin Dimnik’s challenges to the traditional
            interpretation of Kievan history and Alexander Kazhdan’s critique of the Byzantine
            inflation of the Rusian sources. Furthermore, the pool of genealogical descendants has
            been broadened to include women and their children, who in the past were often omitted
            from genealogies. Their inclusion presents a broader picture of Rus′ and Rusians across
            Europe, which more accurately reflects the place of Rus′ in medieval history. </p>
        <p>The end result is a compilation of the relevant genealogical information for the
            Riurikids of the eleventh and twelfth century. For most members of the family basic
            information, such as birth, death, marriage, and regnal dates, are available and
            sourced. Where sources are available for marriages made prior to 1146, there is a note
            that explains the sources and contextualizes the marriage in Rusian—and often broader
            European—history. This allows for much more than a static genealogical chart. The user
            has the ability to move seamlessly across generations, tracking rulers by city, by date,
            and by family, and reading deeply about the circumstances behind relationships. The goal
            is a better understanding of the Riurikids and Rus′ and their place in the wider
            European world.</p>
        <!--
        <h3>How to use the site</h3>
        <p><strong>[ADD CONTENT: instructions, screen caps, examples]</strong></p>
        <p>[Instructions should include both <q>how to interact with the interface</q> and <q>what
                sorts of queries might a historian want to conduct,</q> that is, <q>what sort of
                primary historical research can one conduct on this site that would be difficult or
                impossible without this tool?</q>]</p>
                -->
        <h3>Developers and contributors</h3>
        <p>The research, writing, and methodology behind the genealogical information on this site
            is the work of Christian Raffensperger (<a
                href="mailto:craffensperger@wittenberg.edu">craffensperger@wittenberg.edu</a>), and a hard copy version of this information is
            undergoing review for publiction. Chris has been assisted in a variety of ways, specifically data entry and
            data collection, by two faculty aides (paid for through the faculty aide program at
            Wittenberg University), Adam Matthews and Kelsey Mazur, whose help has been invaluable.
            The translation of this work to an interactive Internet-based <q>workstation</q>
            environment was initially designed, developed, and implemented by David J. Birnbaum (<a href="mailto:djbpitt@gmail.com">djbpitt@gmail.com</a>, <a
                href="http://www.obdurodon.org">http://www.obdurodon.org</a>), and subsequently
            improved and expanded in collaboration with Jacob Deitloff, who also designed and implemented the family-tree visualization system. A more substantial
            discussion of the methodology and technology behind this site has been published as
            Christian Raffensperger and David J. Birnbaum, <q>Mapping History: Using Technology to
                Showcase Medieval Familial Interconnectivity.</q> Festschrift in Honor of Orysia
            Karapinka in <cite>Russian History/Histoire Russe</cite> 37:4 (2010), 305–21.</p>
    </body>
</html>
