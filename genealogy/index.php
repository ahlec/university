<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
        <style type="text/css">
            img.vladimir {max-width: 250px; width: 40%; float: right; margin: 0 0 1em 1em; border: none;}</style>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<span class="logo">&lt;rg&gt;</span> Rusian genealogy</h1>
        <hr />
        <div>
            <a href="http://commons.wikimedia.org/wiki/File:Vladimir_I_of_Kiev_detail.jpg"><img class="vladimir" alt="[Image of Vladimir I]" 
                                                                                                src="http://genealogy.obdurodon.org/images/Vladimir_I_of_Kiev_detail.jpg"/></a>
        <?php require_once("boilerplate.php"); ?>
        </div>
        <?php require_once("navigation.php"); ?>
        <h2>Introduction</h2>
        <p>Genealogies have been constructed and used for hundreds of years to help families understand 
        their ancestry and more recently to help scholars understand the relationships between medieval 
        people and families. The Rusian genealogical database offers an update on this traditional discipline. 
        The research underlying this database is new and is built on the primary sources in Old East Slavic, 
        Latin, Greek, and Old Norse, as well as a thorough reading and understanding of the modern secondary 
        literature. That information is then accessed through an XML database that allows the user to search 
        through the variety of information presented here, including parentage, regnal dates, place of rule, 
        and other data points. The end result is the most accurate genealogy of Rus′ yet developed, presented 
        in an accessible and intuitive way for use by scholars, students, and others.</p>
    </body>
</html> 
