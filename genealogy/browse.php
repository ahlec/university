<?php
require_once ("config.php");
header('Content-type: application/xhtml+xml');

$contents = REST_PATH . "/db/genealogy/xquery/browse.xql";
$personNames = file_get_contents($contents);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Rusian genealogy</title>
        <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
        <meta http-equiv="Content-Script-Type" content="text/javascript"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <link type="text/css" href="http://obdurodon.org/css/style.css" rel="stylesheet"/>
    </head>
    <body>
        <h1><a class="logo" href="http://www.obdurodon.org">&lt;oo&gt;</a>&#x2192;<a class="logo" href="http://genealogy.obdurodon.org">&lt;rg&gt;</a> Rusian genealogy</h1>
        <hr />
        <div>
        <?php require_once ("boilerplate.php"); ?>
        </div>
        <?php require_once ("navigation.php"); ?>
        <h2>Browse</h2>
        <?php echo $personNames; ?>
    </body>
</html>
