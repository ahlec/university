from api.Board import Board

def main():
    raw_board = [[0,0,0,0,0,0,0],
                 [0,2,1,2,1,0,0],
                 [0,2,2,1,2,0,0],
                 [0,2,1,1,2,0,0],
                 [1,1,2,2,1,0,0],
                 [2,1,1,1,2,1,0]]
    b = Board(rawData = raw_board)
    b.printModel()
    print 'Block moves: %s' % b.toFeatureList()[126:-7]
    print 'Winning moves: %s' % b.toFeatureList()[133:]


if __name__ == "__main__":
    main()