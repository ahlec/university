from api.DataSet import DataSet
from api.Board import Board
from api.NeuralNetwork import NeuralNetwork
import random
import time

TRAINING_PERCENTAGE = 0.60

def main():
	data = DataSet("data/connect-4.data")
	print len(data)

	num_training_samples = int(TRAINING_PERCENTAGE*len(data))

	'''I ran this first which trains a network and then saves the network object into
	thee pickle file "networks/<network_name>.p"'''
	# print
	# print("Training on %.2f%% of the dataset (%d examples)..."
	# 			%(TRAINING_PERCENTAGE*100, num_training_samples))
	
	# start_time = time.time()
	
	# training_data = [d for d in data[:num_training_samples]]
	# random.shuffle(training_data)

	# network = NeuralNetwork(training_data)
	# network.Train()
	# network.SaveToFile("networks/network_60percent_3layer_fullconnect.p")

	# end_time = time.time()
	# delta_time = end_time - start_time
	# print "Training Finished"
	# print "Total Training time (s): %d"%delta_time


	'''Then I ran this and loaded the network object in from the file'''
	network = NeuralNetwork.LoadFromFile("networks/network_60percent_3layer_fullconnect.p")
	board = data.getRandom().toBoard()
	board.printModel()
	print "Winner:", board.winner()
	print network.MakePrediction(board)


if __name__ == "__main__":
	main()