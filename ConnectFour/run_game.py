from api.Game import Game
from api.Board import Board
from api.AI import Ai
from api.NeuralNetwork import NeuralNetwork
from api.DataSet import DataSet

import sys

RAW_BOARD = [[0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0]]

def main():
    # Can pass this to the Game constructor to run with a starting point
    start_board = Board(rawData = RAW_BOARD);

    # Get network name for cmd line
    if len(sys.argv) > 2:
        network_name = sys.argv[1]
    else:
        raise 'Must specify network.'

    # Get the number of games to be played from cmd line
    iterations = 100
    if len(sys.argv) > 2:
        iterations = int(sys.argv[2])

    # Hack in a way to specify random from the cmd line
    random_opp = False
    if len(sys.argv) > 3:
        if sys.argv[3] == '-rand':
            random_opp = True

    k = 10
    for w in range(k):
        results = [0,0,0]
        for i in range(iterations):
            # Possible game options:
            #     Game(start_board=start_board, print_boards=True, human_opp=True, random_opp=True, pause=0.00)
            game = Game(network=network_name, random_opp=random_opp)

            round_winner = game.play()

            if len(round_winner) == 0:
                # Tie
                results[0] = results[0] + 1
            elif round_winner[0] == 1:
                # Player 1 won
                results[1] = results[1] + 1
            elif round_winner[0] == 2:
                # Player 2 won
                results[2] = results[2] + 1
        

        total = iterations
        ties = results[0]
        wins = results[1]
        losses = results[2]
        win_rate = float(wins)/float(total)
        loss_rate = float(losses + ties)/float(total)

        print '****Trial %d/%d*****' % (w+1, k)
        print 'Total games: %d' % total
        print 'Ties: %d' % ties
        print 'Wins: %d' % wins
        print 'Losses: %d' % losses
        print 'Win rate: %f' % win_rate
        print 'Loss rate (ties included in losses): %f' % loss_rate
        print

if __name__ == "__main__":
    main()