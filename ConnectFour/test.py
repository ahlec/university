from api.DataSet import DataSet
from api.DataExample import DataExample

data = DataSet("data/connect-4.data")
print len(data)
data.kFold(10)

# vertical_block_board =  '_,_,_,_,_,_' +\
#                        ',x,o,o,o,_,_' +\
#                        ',x,x,o,o,o,_' +\
#                        ',o,o,o,_,_,_' +\
#                        ',_,_,_,_,_,_' +\
#                        ',_,_,_,_,_,_' +\
#                        ',_,_,_,_,_,_,loss'

# horizontal_block_board =  '_,_,_,_,_,_' +\
#                          ',o,_,_,_,_,_' +\
#                          ',x,o,_,_,_,_' +\
#                          ',o,o,o,_,_,_' +\
#                          ',o,o,x,_,_,_' +\
#                          ',o,_,_,_,_,_' +\
#                          ',_,_,_,_,_,_,loss'

# diagonal1_block_board =  '_,_,_,_,_,_' +\
#                         ',x,_,_,_,_,_' +\
#                         ',x,o,_,_,_,_' +\
#                         ',x,x,o,_,_,_' +\
#                         ',o,o,x,o,_,_' +\
#                         ',o,x,x,o,o,_' +\
#                         ',x,x,x,o,o,_,loss'

# diagonal2_block_board =  '_,_,_,_,_,_' +\
#                         ',o,x,x,x,_,_' +\
#                         ',x,o,x,o,_,_' +\
#                         ',o,x,o,_,_,_' +\
#                         ',o,o,_,_,_,_' +\
#                         ',x,_,_,_,_,_' +\
#                         ',_,_,_,_,_,_,loss'


# example = DataExample(1, vertical_block_board)
# example = DataExample(1, horizontal_block_board)
# example = DataExample(1, diagonal1_block_board)
# example = DataExample(1, diagonal2_block_board)

example = data.getRandom()
example.printModel()
print 'Block moves: %s' % example.toFeatureList()[126:-10]
print 'Winning moves: %s' % example.toFeatureList()[133:-3]

# board = example.toBoard()
# board.printModel()
# board.move(2, 3)
# board.printModel()
# print board.toFeatureList()