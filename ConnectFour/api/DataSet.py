from DataExample import DataExample
import random
import os

class DataSet:
	'''A manager for a full data file in the format specified by http://archive.ics.uci.edu/ml/datasets/Connect-4'''
	
	def __init__(self, filename):
		self.__filename = filename
		self.__data = []
		self.__rawLines = [] # [ALEC, 05 April] We store the raw lines as well, so we can use them easily for kFold
		
		# READ INTO MEMORY
		# [ALEC, 02 April] Originally I was going to have it not read it all into memory, because that wasn't
		# optimal for such a large data set. But right now, we just need it to work, not to be the most optimal
		# solution out there, and this isn't even the large part of the project, so I figured, "what the hell,
		# let's do it the simple way if that means we can move on to the bigger fish here."
		# [ALEC, 02 April] Normally, it isn't the best idea to put a lot of code in the constructor, but for the
		# same reason as above, I'm doing it here because we just need this to work consistently, not to be the
		# ideal perfection of code.
		dataFile = open(filename, "rb")
		exampleNo = 0
		for line in dataFile:
			self.__data.append(DataExample(exampleNo, line.rstrip())) # We strip the right side to get rid of the newline character
			self.__rawLines.append([exampleNo, line.rstrip()])
			exampleNo += 1
		dataFile.close()
		
	def getRandom(self):
		'''Returns a random DataExample from this DataSet.'''
		return random.choice(self.__data)
		
	def kFold(self, k, directoryPrefix = None):
		'''Splits this DataSet into the proper k-fold. Directories are created in the'''
		'''directory of the specified DataSet filename, which would allow for nested'''
		'''k-fold creation (ie, MAIN --> kfold0,kfold1...kfoldn; kfold1 --> ksubfold0...ksubfoldm.'''
		'''directoryPrefix is an optional override for the names of the folders that will be created'''
		'''for each of thek tests (format is {directoryPrefix}_{0...k}). If directoryPrefix is None'''
		'''or omitted, then it will default to the name of the data file used to create this DataSet.'''
		
		# Ensure that we have enough
		if len(self.__data) < k:
			raise Error("There are not enough data entries to perform a %d-fold split." % k)
		
		# Prepare for creating the different files
		blockSize = round(len(self.__data) / k)
		filenameNoExtension = os.path.splitext(self.__filename)[0]
		if directoryPrefix != None:
			prefix = os.path.join(os.path.dirname(self.__filename), directoryPrefix)
		else:
			prefix = filenameNoExtension
		random.shuffle(self.__rawLines)
		
		# Perform the creations
		for testNo in range(k):
			if not os.path.exists("%s_%d" % (prefix, testNo)):
				os.makedirs("%s_%d" % (prefix, testNo))
			trainingFile = open("%s_%d/train.data" % (prefix, testNo), "w")
			testFile = open("%s_%d/test.data" % (prefix, testNo), "w")
			for index in range(len(self.__data)):
				if index >= testNo * blockSize and index < (testNo + 1) * blockSize:
					testFile.write("%s%s" % (self.__rawLines[index][1], os.linesep))
				else:
					trainingFile.write("%s%s" % (self.__rawLines[index][1], os.linesep))

			trainingFile.close()
			testFile.close()
	
	def __len__(self):
		'''The len() operator overload.'''
		# Having it like this allows us to use the code as such:
		# 	dataSet = DataSet("training.data")
		#	nTotalExamples = len(dataSet)
		return len(self.__data)
		
	def __getitem__(self, key):
		'''Allows us to retrieve a specific example based on the index number of that example from the'''
		'''original file.'''
		return self.__data[key]