import random
import time
import os

from Board import Board
from AI import Ai
from NeuralNetwork import NeuralNetwork

AI = 1
OPPONENT = 2
MOVES = {'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6}

class Game:
    def __init__(self, network, start_board = Board(), human_opp = False, print_boards = False, random_opp = False, pause = 0.00):
        self.network = NeuralNetwork.LoadFromFile("networks/" + network)
        self.ai = Ai(1, self.network, maxDepth = 1)
        
        # Options
        self.board = start_board
        self.human_opp = human_opp
        self.print_boards = print_boards
        self.random_opp = random_opp
        self.pause = pause

    def play(self):
        turn = 1
        while len(self.board.winner()) == 0 and len(self.board.availableMoves()) > 0:
            # Flip between opponent and AI move
            if (turn%2) == 0:
                self.make_opponent_move()
            else:
                self.make_ai_move()            
            turn += 1
                
            if self.print_boards:
                self.board.printModel(displayHeader = False)
                print os.linesep

            time.sleep(self.pause)
        
        return self.board.winner()

    def make_opponent_move(self):
        if not self.human_opp:
            # Pick random value from the available moves
            available_moves = self.board.availableMoves()
            rand_move = available_moves[random.randrange(0, len(available_moves))]
            self.board = self.board.move(OPPONENT, rand_move)
        else:
            # Ask for letter of move on command line
            self.board.printModel(displayHeader = False)
            print os.linesep
            human_move = raw_input('Type your move (a,b,c,d,e,f,g): ')
            if human_move in MOVES:
                self.board = self.board.move(OPPONENT, MOVES[human_move])
                print os.linesep
            else:
                # The easiest thing to do if human enters invalid move is just to move on
                print "Invalid move, you lose your turn" + os.linesep                

    def make_ai_move(self):
        if self.random_opp:
            # Pick random value from the available moves
            available_moves = self.board.availableMoves()
            rand_move = available_moves[random.randrange(0, len(available_moves))]
            self.board = self.board.move(AI, rand_move)
        else:
            column = self.ai.getNextMove(self.board)
            self.board = self.board.move(AI, column)
