import Board
import copy

MAX_DEPTH = 6

def getMove(board, playerId, columnIndex):
	if board.getValue(0, columnIndex) == 0:
		return board.move(playerId, columnIndex), [playerId, columnIndex]
	return None, None

class Node:
	def __init__(self, board, maxDepth, depth = 0, move = None):
		self.__board = board
		self.__value = None
		self.__depth = depth
		self.__move = move
		if depth >= maxDepth:
			self.__subnodes = None
		else:
			workingMove = move
			if workingMove == None:
				workingMove = []
			self.__subnodes = []
			for colIndex in range(7):
				subBoard, thisMove = getMove(board, depth % 2 + 1, colIndex)
				subboardMove = copy.deepcopy(workingMove)
				subboardMove.append(thisMove)
				
				if subBoard != None:
					self.__subnodes.append(Node(subBoard, maxDepth, depth + 1, move = subboardMove))
				else:
					self.__subnodes.append(None)

	def getBoard(self):
		return self.__board

	def getValue(self):
		return self.__value

	def setValue(self, value):
		self.__value = value

	def getMove(self):
		return self.__move

	def hasSubnodes(self):
		return (self.__subnodes != None)

	def getSubnode(self, colIndex):
		if not self.hasSubnodes():
			raise Error("This Node doesn't have any subnodes.")
		if colIndex < 0 or colIndex > 6:
			raise Error("The requested index is out of bounds.")

		return self.__subnodes[colIndex]

	def printR(self):
		indentation = '\t' * (self.__depth - 1)
		if self.__depth > 1:
			print indentation + "ROOT"
			for move in self.__move:
				print indentation + "Player " + str(move[0]) + " moves in column " + str(move[1] + 1)
		print indentation + "VALUE: " + str(self.__value)
		self.__board.printModel(prefix = indentation)

		if self.hasSubnodes():
			for subnode in self.__subnodes:
				if subnode != None:
					print
					subnode.printR()
				else:
					print
					print "THIS WAS AN ENDGAME BOARD."
