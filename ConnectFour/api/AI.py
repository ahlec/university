import random
from sys import maxint
from Node import Node
from Board import Board
from NeuralNetwork import NeuralNetwork

class Ai:
    # Take the player number, we could make two Ai's one for player 1 and one
    # for player 2.
    def __init__(self,player,network, maxDepth=1):
        self.player = player
        self.network = network
        self.maxDepth = maxDepth

    def evaluateBoards(self, node):
        moves = []
        for i in range(0,7):
            subnode = node.getSubnode(i)
            if subnode != None:
                temp_board = subnode.getBoard()
                if len(temp_board.winner())== 0:
                    board_pred = self.network.MakePrediction(subnode.getBoard())
                    if subnode.hasSubnodes():
                        subnode.value = (self.evaluateBoards(subnode)[1] + board_pred)/2
                    else:
                        subnode.value = board_pred
                    moves.append((i,subnode.value))
                else:
                    moves.append((i,1))
                    #moves.append((i,self.network.MakePrediction(subnode.getBoard())))
                
        #print moves
        return max(moves,key=lambda x:x[1])
    
    def getNextMove(self, board):
        node = Node(board,self.maxDepth)
        temp_ret = self.evaluateBoards(node)
        #print temp_ret
        return temp_ret[0]

    def helper(self, node, currentDepth):
        chosen_move = None
        max_scores = []

        for i in range(0, 7):
            subnode = node.getSubnode(i)
            if (subnode == None):
                if (currentDepth%2 == 0):
                    max_scores.append(0)
                else:
                    max_scores.append(1)

                continue
            temp_board = subnode.getBoard()
            if temp_board.winner() == 1:
                if (currentDepth%2 == 0):
                    max_scores.append(1)
                else:
                    max_scores.append(0)
            max_scores.append(self.network.MakePrediction(subnode.getBoard()))

        if (currentDepth%2 == 0):
            chosen_move = max_scores.index(max(max_scores))
            max_score = max(max_scores)
        else:
            chosen_move = max_scores.index(min(max_scores))
            max_score = 1-min(max_scores)        

        # print(chosen_move, max_score)
        if (currentDepth == self.maxDepth-1):
            return (chosen_move, max_score)
        

        chosen_move = None
        max_scores = []

        for i in range(0, 7):
            subnode = node.getSubnode(i)
            if (subnode == None):
                if (currentDepth%2 == 0):
                    max_scores.append(-self.maxDepth)
                else:
                    max_scores.append(self.maxDepth)
                
                continue

            subnode.setValue(\
                self.network.MakePrediction(subnode.getBoard()) +\
                    self.helper(subnode, currentDepth+1)[1])
            
            max_scores.append(subnode.getValue())

        if (currentDepth%2 == 0):
            chosen_move = max_scores.index(max(max_scores))
            max_score = max(max_scores)
        else:
            chosen_move = max_scores.index(min(max_scores))
            max_score = 1-min(max_scores)

        return (chosen_move, max_score)



