from api.Board import Board, BOARD_HEIGHT, BOARD_WIDTH

class DataExample:
	'''A single instance of data from a DataSet. This contains information on the'''
	'''actual information contained within the data. Should NOT be constructed by'''
	'''anything other than DataSet; DataSet is responsible for exposing this to the client.'''
	# [ALEC,05 April] Data structure for this class matches that of the file format specification
	# (see diagrams on http://archive.ics.uci.edu/ml/datasets/Connect-4).
	
	# Constants
	BLANK			= 0x0
	PLAYER_1		= 0x1
	PLAYER_2		= 0x2
	NO_PLAYER		= 0x3

	# Member functions
	def __init__(self, exampleNo, rawDataLine):
		self.__exampleNo = exampleNo
		self.__cells = []
		
		# Split the values on commas. If we don't have exactly 43 entries after this,
		# then we are not dealing with a properly formatted row of data.
		splitValues = rawDataLine.split(',')
		if len(splitValues) != 43:
			raise Error("Invalid format for DataExample.")
		
		# Store the first 42 attributes as cells
		for index in range(42):
			if splitValues[index] == "x":
				self.__cells.append(DataExample.PLAYER_1)
			elif splitValues[index] == "o":
				self.__cells.append(DataExample.PLAYER_2)
			else:
				self.__cells.append(DataExample.BLANK)
				
		# Store the winner (43rd attribute)
		if splitValues[42] == "draw":
			self.__winner = DataExample.NO_PLAYER
		elif splitValues[42] == "win":
			self.__winner = DataExample.PLAYER_1
		else:
			self.__winner = DataExample.PLAYER_2
			
	def getExampleNo(self):
		return self.__exampleNo
		
	def getCell(self, rowIndex, colIndex):
		'''Returns one of the three constants (BLANK, PLAYER_1, or PLAYER_2) depending on the'''
		'''occupation of the specified cell. Note that the range of rowIndex is [0, 5] and the'''
		'''range of colIndex is [0,6].'''
		if rowIndex < 0 or rowIndex > 5:
			raise IndexError("rowIndex must be within the range of [0, 5]")
		if colIndex < 0 or colIndex > 6:
			raise IndexError("colIndex must be within the range of [0, 6]")
			
		return self.__cells[colIndex * 6 + rowIndex]

	def hasWinner(self):
		return (self.__winner != DataExample.NO_PLAYER)
		
	def getWinner(self):
		return self.__winner
		
	def getLoser(self):
		if self.__winner == DataExample.PLAYER_1:
			return DataExample.PLAYER_2
		elif self.__winner == DataExample.PLAYER_2:
			return DataExample.PLAYER_1
		return DataExample.NO_PLAYER
		
	def toBoard(self):
		'''Returns an instance of the Board class with the current setup of this example.'''
		return Board([[self.__cells[row * 6 + (5 - column)] for row in range(BOARD_WIDTH)] for column in range(BOARD_HEIGHT)])
		
	def toFeatureList(self):
		'''Converts this board to a list of 129 binary features. The first 126 are broken up'''
		'''into three per each cell of the board (going horizontally across a row, then going'''
		'''to the next row). The first is `is blank`, the second `is player 1`, and the third'''
		'''`is player 2`. The last three attributes are, in order, `is player 1 win`, `is player'''
		'''1 loss`, and `is draw`.'''
		attributeList = []

		# Add each board position
		for row in range(BOARD_WIDTH):
			for column in range(BOARD_HEIGHT):
				cell = self.__cells[row * 6 + (5 - column)]
				attributeList.append(cell == DataExample.BLANK)
				attributeList.append(cell == DataExample.PLAYER_1)
				attributeList.append(cell == DataExample.PLAYER_2)

		blocking_moves = self.__colsThatCompleteRunsForPlayer(DataExample.PLAYER_2)
		winning_moves = self.__colsThatCompleteRunsForPlayer(DataExample.PLAYER_1)
		attributeList.extend(blocking_moves)
		attributeList.extend(winning_moves)

		# Add winner
		attributeList.append(self.__winner == DataExample.NO_PLAYER)
		attributeList.append(self.__winner == DataExample.PLAYER_1)
		attributeList.append(self.__winner == DataExample.PLAYER_2)
		return attributeList

	def __colsThatCompleteRunsForPlayer(self, player_value):
		# Go through each cell and check if the column that cell is in could be a finishing move
		# If that column could be a finishing (complete run of four) move, add it to finishing_moves
		finishing_moves = [False for i in range(BOARD_WIDTH)]
		for row in range(BOARD_WIDTH):
			# For each column find the highest cell and check for finishing a run at that cell
			for column in range(BOARD_HEIGHT):

				# Find out if we're at the column before the the top of the row
				at_next_move_cell = False
				if (column + 1) >= BOARD_HEIGHT:
					# Bottom of column
					at_next_move_cell = True
				elif self.__cells[row * 6 + (5 - (column + 1))] != DataExample.BLANK:
					# Top of stack
					at_next_move_cell = True

				if at_next_move_cell:
					# Check for block move at this column
					finishing_move = False
					
					# Horizontal
					for i in range(4):
						# Need to iterate over four possible horizontal sets of four surrounding this cell
						start_row = row - i
						if start_row >= 0 and start_row + 3 < BOARD_WIDTH:
							four_cell = [self.__cells[start_row * 6 + (5 - column)],
										 self.__cells[(start_row + 1) * 6 + (5 - column)],
										 self.__cells[(start_row + 2) * 6 + (5 - column)],
									 	 self.__cells[(start_row + 3) * 6 + (5 - column)]]
							
							horizontal_finish = True
							for j, cur in enumerate(four_cell):
								# Skip the cell we're testing for (blank)
								if j != i and cur != player_value:
										horizontal_finish = False

							if horizontal_finish == True:
								finishing_move = horizontal_finish
								# Don't look at any further horizontal possibilities
								break

					# Vertical
					if not finishing_move:
						start_col = column + 3
						if start_col < BOARD_HEIGHT:
							four_cell = [self.__cells[row * 6 + (5 - (start_col - 3))],
										 self.__cells[row * 6 + (5 - (start_col - 2))],
										 self.__cells[row * 6 + (5 - (start_col - 1))],
									 	 self.__cells[row * 6 + (5 - start_col)]]

							# If all but the last cell are player 2 then we have a finishing move
							vertical_finish = True
							for cur in four_cell[1:]:
								if cur != player_value:
									vertical_finish = False
							finishing_move = vertical_finish

					# Check diagonal (bottom-left to top-right)
					if not finishing_move:
						for i in range(3):
							start_row = (row - 3) + i
							start_col = (column + 3) - i
							if start_col < BOARD_HEIGHT and start_row >= 0 and start_row + 3 < BOARD_WIDTH and start_col - 3 >= 0:
								four_cell = [self.__cells[start_row * 6 + (5 - start_col)],
											 self.__cells[(start_row + 1) * 6 + (5 - (start_col - 1))],
											 self.__cells[(start_row + 2) * 6 + (5 - (start_col - 2))],
										 	 self.__cells[(start_row + 3) * 6 + (5 - (start_col - 3))]]

								diagonal_finish = True
								for j, cur in enumerate(four_cell):
									# Skip the cell we're testing for (blank)
									if j != (3 - i) and cur != player_value:
										diagonal_finish = False

								if diagonal_finish == True:
									finishing_move = diagonal_finish
									# Don't look at any further diagonal (bl to tr) possibilities
									break

					# Check diagonal (bottom right to top-left)
					if not finishing_move:
						for i in range(3):
							start_row = (row + 3) - i
							start_col = (column + 3) - i
							if start_row < BOARD_WIDTH and start_col < BOARD_HEIGHT and start_row - 3 >= 0 and start_col - 3 >= 0:
								four_cell = [self.__cells[start_row * 6 + (5 - start_col)],
											 self.__cells[(start_row - 1) * 6 + (5 - (start_col - 1))],
											 self.__cells[(start_row - 2) * 6 + (5 - (start_col - 2))],
										 	 self.__cells[(start_row - 3) * 6 + (5 - (start_col - 3))]]

								diagonal_finish = True
								for j, cur in enumerate(four_cell):
									# Skip the cell we're testing for (blank)
									if j != (3 - i) and cur != player_value:
										diagonal_finish = False

								if diagonal_finish == True:
									finishing_move = diagonal_finish
									# Don't look at any further diagonal (br to tl) possibilities
									break

					finishing_moves[row] = finishing_move
					# We don't want to keep going down the column so break here
					break

		return finishing_moves

	def printModel(self, displayHeader = True):
		'''Prints out a visual model of the game board to the console. Optional parameter determines'''
		'''whether to include metadata, or just the board itself.'''
		if displayHeader:
			print "DataExample #%d" % self.__exampleNo
			if self.hasWinner():
				print "Result: PLAYER %d WIN" % self.getWinner()
			else:
				print "Result: DRAW"
			print "(PLAYER 1: x | PLAYER 2: o)"
		
		for row in range(5, -1, -1):
			print "%d %c %c %c %c %c %c %c" % ((row + 1), self.__getModelChar(row, 0), \
				self.__getModelChar(row, 1), self.__getModelChar(row, 2), self.__getModelChar(row, 3), \
				self.__getModelChar(row, 4), self.__getModelChar(row, 5), self.__getModelChar(row, 6))
		print "  a b c d e f g"
		
	def __getModelChar(self, rowIndex, colIndex):
		'''Returns an 'x' (player 1), 'o' (player 2), or '.' depending on the occupation of that cell'''
		'''in the data example game board. Meant only to be used by printModel().'''
		cell = self.getCell(rowIndex, colIndex)
		if cell == DataExample.PLAYER_1:
			return "x"
		elif cell == DataExample.PLAYER_2:
			return "o"
		else:
			return "."