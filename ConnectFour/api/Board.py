import os

BOARD_WIDTH = 7
BOARD_HEIGHT = 6
WIN_LENGTH = 4
BLANK = 0
PLAYER_1 = 1
PLAYER_2 = 2

class Board:
    # [ALEC,05 April] Data structure for Board is [row, col] with the origin [0,0] in the
    # top-left corner.

    def __init__(self, rawData = None):
        '''Always instatiate this with no parameters (rawData should always be None)'''
        if rawData == None:
            # Create board of all 0's
            self.board = [[0 for i in range(BOARD_WIDTH)] for j in range(BOARD_HEIGHT)]
        else:
            # [ALEC,05 April] For the sake of simplicity at this point, we're going to just
            # assume that we're smart enough and the project stays small enough that the only
            # place to use the overloaded constructor is DataExample. It saves us from having
            # to do a lot of what should hopefully (if we adhere to this) be needless checks.
            # Yeah, it's not ideal, but we don't need this to be flawless code, and it's easier
            # if we keep this all self-contained.
            self.board = rawData

    def move(self, player, col):
        ''' Makes a move on the board by a player (1 or 2) and a column '''

        # Argument checks
        if player < 1 or player > 2:
            raise ValueError('Player must be either 1 or 2')

        if col < 0 or col > (BOARD_WIDTH - 1):
            raise ValueError('Column must be between 0 and %d' % (BOARD_WIDTH - 1))

        # Add move to board matrix starting at the bottom row
	import copy
	copiedData = copy.deepcopy(self.board)
        for row in reversed(copiedData):
            if row[col] == 0:
                row[col] = player
                break

	# Return the new board
	return Board(copiedData)

    def getValue(self, row, col):
        ''' Returns value of a given location on the baord '''

        # Argument checks
        if row < 0 or row > (BOARD_HEIGHT - 1):
            raise ValueError('Row must be between 0 and %d' % (BOARD_HEIGHT - 1))

        if col < 0 or col > (BOARD_WIDTH - 1):
            raise ValueError('Column must be between 0 and %d' % (BOARD_WIDTH - 1))

        return self.board[row][col]

    def availableMoves(self):
        ''' Returns a list of column numbers that are available for moves '''
        moves = []
        for i,cell in enumerate(self.board[0]):
            if cell == 0:
                moves.append(i)
        return moves

    def winner(self):
        '''Tests board for a winner. Return a list of winners'''
        winners = []

        # Check for horizontal winners
        for row in range(BOARD_HEIGHT):
            for start_col in range(BOARD_WIDTH - (WIN_LENGTH - 1)):
                if self.board[row][start_col] != 0 and\
                    self.board[row][start_col + 1] == self.board[row][start_col] and\
                    self.board[row][start_col + 2] == self.board[row][start_col] and\
                    self.board[row][start_col + 3] == self.board[row][start_col]:
                    winners.append(self.board[row][start_col])

        # Check for vertical winners
        for col in range(BOARD_WIDTH):
            for start_row in range(BOARD_HEIGHT - (WIN_LENGTH - 1)):
                if self.board[start_row][col] != 0 and\
                    self.board[start_row + 1][col] == self.board[start_row][col] and\
                    self.board[start_row + 2][col] == self.board[start_row][col] and\
                    self.board[start_row + 3][col] == self.board[start_row][col]:
                    winners.append(self.board[start_row][col])

        # Check for diagonal winners (top-left to bottom-right)
        for start_row in range(BOARD_HEIGHT - (WIN_LENGTH - 1)):
            for start_col in range(BOARD_WIDTH - (WIN_LENGTH - 1)):
                if self.board[start_row][start_col] != 0 and\
                    self.board[start_row + 1][start_col + 1] == self.board[start_row][start_col] and\
                    self.board[start_row + 2][start_col + 2] == self.board[start_row][start_col] and\
                    self.board[start_row + 3][start_col + 3] == self.board[start_row][start_col]:
                    winners.append(self.board[start_row][start_col])

        # Check for diagonal winners (bottom-left to top-right)
        for start_row in range(BOARD_HEIGHT - 1, BOARD_HEIGHT - WIN_LENGTH, -1):
            for start_col in range(BOARD_WIDTH - (WIN_LENGTH - 1)):
                if self.board[start_row][start_col] != 0 and\
                    self.board[start_row - 1][start_col + 1] == self.board[start_row][start_col] and\
                    self.board[start_row - 2][start_col + 2] == self.board[start_row][start_col] and\
                    self.board[start_row - 3][start_col + 3] == self.board[start_row][start_col]:
                    winners.append(self.board[start_row][start_col])

        return winners
        
    def printModel(self, displayHeader = True, prefix = ""):
        '''Prints out a visual model of the game board to the console.'''
        if displayHeader:
            print prefix + "(PLAYER 1: x | PLAYER 2: o)"
        
        for row in range(0, 6):
            print prefix + "%d %c %c %c %c %c %c %c" % ((6 - row), self.__getModelChar(row, 0), \
                self.__getModelChar(row, 1), self.__getModelChar(row, 2), self.__getModelChar(row, 3), \
                self.__getModelChar(row, 4), self.__getModelChar(row, 5), self.__getModelChar(row, 6))
        print prefix + "  a b c d e f g"
        
    def toFeatureList(self):
        '''Converts this board to a list of 126 binary features. These are broken up'''
        '''into three per each cell of the board (going horizontally across a row, then going'''
        '''to the next row). The first is `is blank`, the second `is player 1`, and the third'''
        '''`is player 2`.'''
        attributeList = []
        for row in self.board:
            for cell in row:
                attributeList.append(cell == 0)
                attributeList.append(cell == 1)
                attributeList.append(cell == 2)

        blocking_moves = self.__colsThatCompleteRunsForPlayer(PLAYER_2)
        winning_moves = self.__colsThatCompleteRunsForPlayer(PLAYER_1)
        attributeList.extend(blocking_moves)
        attributeList.extend(winning_moves)

        return attributeList
    
    def __colsThatCompleteRunsForPlayer(self, player_value):
        # Go through each cell and check if the column that cell is in could be a finishing move
        # If that column could be a finishing (complete run of four) move, add it to finishing_moves
        finishing_moves = [False for i in range(BOARD_WIDTH)]
        for column in range(BOARD_WIDTH):
            # For each column find the highest cell and check for finishing a run at that cell
            for row in range(BOARD_HEIGHT):

                # Find out if we're at the cell before the the top of the column
                at_next_move_cell = False
                if (row + 1) == BOARD_HEIGHT:
                    # Bottom of column
                    at_next_move_cell = True
                elif self.getValue(row + 1, column) != BLANK:
                    # Top of stack
                    at_next_move_cell = True

                if at_next_move_cell:
                    # Check for finishing move at this cell
                    finishing_move = False
                    
                    # Horizontal
                    for i in range(4):
                        # Need to iterate over four possible horizontal sets of four surrounding this cell
                        start_col = column - i
                        if start_col >= 0 and start_col + 3 < BOARD_WIDTH:
                            four_cell = [self.getValue(row, start_col),
                                         self.getValue(row, start_col + 1),
                                         self.getValue(row, start_col + 2),
                                         self.getValue(row, start_col + 3)]
                            
                            horizontal_finish = True
                            for j, cur in enumerate(four_cell):
                                # Skip the cell we're testing for (blank)
                                if j != i and cur != player_value:  
                                    horizontal_finish = False

                            if horizontal_finish == True:
                                finishing_move = horizontal_finish
                                # Don't look at any further horizontal possibilities
                                break

                    # Vertical
                    if not finishing_move:
                        start_row = row + 3
                        if start_row < BOARD_HEIGHT:
                            four_cell = [self.getValue(start_row - 3, column),
                                         self.getValue(start_row - 2, column),
                                         self.getValue(start_row - 1, column),
                                         self.getValue(start_row, column)]

                            # If all but the last cell are player 2 then we have a finishing move
                            vertical_finish = True
                            for cur in four_cell[1:]:
                                if cur != player_value:
                                    vertical_finish = False
                            finishing_move = vertical_finish

                    # Check diagonal (bottom-left to top-right)
                    if not finishing_move:
                        for i in range(3):
                            start_row = (row + 3) - i
                            start_col = (column - 3) + i
                            if start_col >= 0 and start_row < BOARD_HEIGHT and start_col + 3 < BOARD_WIDTH and start_row - 3 >= 0:
                                four_cell = [self.getValue(start_row, start_col),
                                             self.getValue(start_row - 1, start_col + 1),
                                             self.getValue(start_row - 2, start_col + 2),
                                             self.getValue(start_row - 3, start_col + 3)]

                                diagonal_finish = True
                                for j, cur in enumerate(four_cell):
                                    # Skip the cell we're testing for (blank)
                                    if j != (3 - i) and cur != player_value:
                                        diagonal_finish = False

                                if diagonal_finish == True:
                                    finishing_move = diagonal_finish
                                    # Don't look at any further diagonal (bl to tr) possibilities
                                    break

                    # Check diagonal (bottom right to top-left)
                    if not finishing_move:
                        for i in range(3):
                            start_row = (row + 3) - i
                            start_col = (column + 3) - i
                            if start_row < BOARD_HEIGHT and start_col < BOARD_WIDTH and start_row - 3 >= 0 and start_col - 3 >= 0:
                                four_cell = [self.getValue(start_row, start_col),
                                             self.getValue(start_row - 1, start_col - 1),
                                             self.getValue(start_row - 2, start_col - 2),
                                             self.getValue(start_row - 3, start_col - 3)]

                                diagonal_finish = True
                                for j, cur in enumerate(four_cell):
                                    # Skip the cell we're testing for (blank)
                                    if j != (3 - i) and cur != player_value:
                                        diagonal_finish = False

                                if diagonal_finish == True:
                                    finishing_move = diagonal_finish
                                    # Don't look at any further diagonal (br to tl) possibilities
                                    break

                    finishing_moves[column] = finishing_move
                    # We don't want to keep going down the column so break here
                    break

        return finishing_moves


    def __getModelChar(self, rowIndex, colIndex):
        '''Returns an 'x' (player 1), 'o' (player 2), or '.' depending on the occupation of that cell'''
        '''in the game board. Meant only to be used by printModel().'''
        if self.board[rowIndex][colIndex] == 1:
            return "x"
        elif self.board[rowIndex][colIndex] == 2:
            return "o"
        else:
            return "."
        
    def __str__(self):
        # Print each row of the matrix with a line sep in between
        return reduce(lambda x, y: str(x) + os.linesep + str(y), self.board)
