from pybrain.structure import FeedForwardNetwork
from pybrain.structure import LinearLayer, SigmoidLayer, SoftmaxLayer
from pybrain.structure import FullConnection
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
import cPickle as pickle

NUM_FEATURES = 140

class NeuralNetwork():
	# Data Structure which initializes a network given a list of boards to train with
	def __init__(self, dataset):
		self.dataset = dataset
		self.__generateDataset()
		# self.__createNetwork()
		# self.__createWinLossNetwork()
		# self.__create3LayerNetwork()
		self.__create3LayerFullNetwork()

	@staticmethod
	def LoadFromFile(pickle_filename):
		with open(pickle_filename, "rb") as f:
			n = pickle.load(f)

		assert isinstance(n, NeuralNetwork)
		return n


	def __generateDataset(self):
		ds = SupervisedDataSet(NUM_FEATURES, 1)

		for i in range(len(self.dataset)):
			data_example = self.dataset[i]
			result = data_example.getWinner()
			
			# player1 win
			if (result == 1):
				result = 1.00

			# draw
			elif (result == 3):
				result = 0.50
			
			# player2 win
			else:
				result = 0.00
			

			features = data_example.toFeatureList()
			if (len(features) > NUM_FEATURES):
				features = features[:NUM_FEATURES]

			assert len(features) == NUM_FEATURES

			ds.appendLinked(features, (result))
		

		# might need this if we do a classification data set
		# ds._convertToOneOfMany()

		self.network_dataset = ds



	def __createNetwork(self):
		# David: setting up a single hidden layer network with 20 inner sigmoids
		# this went into making network_60percent_20hidden.p
		# and network_80percent_20hidden.p

		n = FeedForwardNetwork()

		# initial network with one hidden layer = 20 sigmoid nodes
		inLayer = LinearLayer(self.network_dataset.indim)
		hiddenLayer = SigmoidLayer(20)
		outLayer = SigmoidLayer(self.network_dataset.outdim)

		n.addInputModule(inLayer)
		n.addModule(hiddenLayer)
		n.addOutputModule(outLayer)
		
		n.addConnection(FullConnection(inLayer, hiddenLayer))
		n.addConnection(FullConnection(hiddenLayer, outLayer))

		n.sortModules()
		self.network = n


	def __createWinLossNetwork(self):
		# Now that we have win/block features, I'm adding those to their
		# own sections of the network.  still one hidden layer here

		#This was used for making network network_60percent_20hidden_winblock.p


		n = FeedForwardNetwork()

		inLayer1 = LinearLayer(self.network_dataset.indim - 14)
		inLayer2 = LinearLayer(7)
		inLayer3 = LinearLayer(7)
		
		# 126 features of board go to this
		hiddenLayer1 = SigmoidLayer(20)
		
		# 7 features for winning move go to this
		hiddenLayer2 = SigmoidLayer(1)
		
		# 7 features for block go to this
		hiddenLayer3 = SigmoidLayer(1)

		outLayer = SigmoidLayer(self.network_dataset.outdim)

		n.addInputModule(inLayer1)
		n.addInputModule(inLayer2)
		n.addInputModule(inLayer3)
		n.addModule(hiddenLayer1)
		n.addModule(hiddenLayer2)
		n.addModule(hiddenLayer3)
		n.addOutputModule(outLayer)
		
		n.addConnection(FullConnection(inLayer1, hiddenLayer1))
		n.addConnection(FullConnection(inLayer2, hiddenLayer2))
		n.addConnection(FullConnection(inLayer3, hiddenLayer3))
		n.addConnection(FullConnection(hiddenLayer1, outLayer))
		n.addConnection(FullConnection(hiddenLayer2, outLayer))
		n.addConnection(FullConnection(hiddenLayer3, outLayer))

		n.sortModules()
		self.network = n


	def __create3LayerNetwork(self):
		# this setup uses win/block in separate sections and also
		# uses 2 hidden layers for the board inputs

		#this was used for network_60percent_3layer_winblock.p

		n = FeedForwardNetwork()

		inLayer1 = LinearLayer(self.network_dataset.indim - 14)
		inLayer2 = LinearLayer(7)
		inLayer3 = LinearLayer(7)
		
		# 126 features of board go to this
		hiddenLayer1a = SigmoidLayer(20)
		hiddenLayer1b = SigmoidLayer(7)
		
		# 7 features for winning move go to this
		hiddenLayer2 = SigmoidLayer(1)
		
		# 7 features for block go to this
		hiddenLayer3 = SigmoidLayer(1)

		outLayer = SigmoidLayer(self.network_dataset.outdim)

		n.addInputModule(inLayer1)
		n.addInputModule(inLayer2)
		n.addInputModule(inLayer3)
		n.addModule(hiddenLayer1a)
		n.addModule(hiddenLayer2)
		n.addModule(hiddenLayer3)
		n.addModule(hiddenLayer1b)
		n.addOutputModule(outLayer)
		
		n.addConnection(FullConnection(inLayer1, hiddenLayer1a))
		n.addConnection(FullConnection(inLayer2, hiddenLayer2))
		n.addConnection(FullConnection(inLayer3, hiddenLayer3))
		n.addConnection(FullConnection(hiddenLayer1a, hiddenLayer1b))
		n.addConnection(FullConnection(hiddenLayer1b, outLayer))
		n.addConnection(FullConnection(hiddenLayer2, outLayer))
		n.addConnection(FullConnection(hiddenLayer3, outLayer))

		n.sortModules()
		self.network = n

	def __create3LayerFullNetwork(self):
		# this setup uses the separate win block sections, but these, along with
		# the board input features all feed to the second hidden layer

		#this was used for network_60percent_3layer_fullconnect.p

		n = FeedForwardNetwork()

		inLayer1 = LinearLayer(self.network_dataset.indim - 14)
		inLayer2 = LinearLayer(7)
		inLayer3 = LinearLayer(7)
		
		# 126 features of board go to this
		hiddenLayer1 = SigmoidLayer(20)

		#this is the second hidden layer
		hiddenLayer4 = SigmoidLayer(7)
		
		# 7 features for winning move go to this
		hiddenLayer2 = SigmoidLayer(1)
		
		# 7 features for block go to this
		hiddenLayer3 = SigmoidLayer(1)

		outLayer = SigmoidLayer(self.network_dataset.outdim)

		n.addInputModule(inLayer1)
		n.addInputModule(inLayer2)
		n.addInputModule(inLayer3)
		n.addModule(hiddenLayer1)
		n.addModule(hiddenLayer2)
		n.addModule(hiddenLayer3)
		n.addModule(hiddenLayer4)
		n.addOutputModule(outLayer)
		
		n.addConnection(FullConnection(inLayer1, hiddenLayer1))
		n.addConnection(FullConnection(inLayer2, hiddenLayer2))
		n.addConnection(FullConnection(inLayer3, hiddenLayer3))
		n.addConnection(FullConnection(hiddenLayer1, hiddenLayer4))
		n.addConnection(FullConnection(hiddenLayer2, hiddenLayer4))
		n.addConnection(FullConnection(hiddenLayer3, hiddenLayer4))
		n.addConnection(FullConnection(hiddenLayer4, outLayer))

		n.sortModules()
		self.network = n
	
	

	def Train(self, maxEpochs = 100):
		trainer = BackpropTrainer(self.network, self.network_dataset)
		trainer.trainUntilConvergence(maxEpochs = maxEpochs)

	

	def MakePrediction(self, board):
		# David:  Not really sure the best way to go about making a prediction since it requires 
		# creating a dataset that can be used with the network prior to actually
		# making the prediction.

		ds = SupervisedDataSet(NUM_FEATURES, 1)
		ds.appendLinked(board.toFeatureList(), (0))
		
		# might need this if we do a classification data set
		# ds._convertToOneOfMany()

		return self.network.activate(ds['input'][0])


	def SaveToFile(self, filename):
		with open(filename, "wb") as f:
			pickle.dump(self, f)



