from api.Board import Board
from api.AI import Ai
from api.Game import Game
from api.NeuralNetwork import NeuralNetwork
from api.DataSet import DataSet

import random

def main():
    # simple testing just to make sure things run
    # will need to play around and test the network more vigorously
    
    data = DataSet("data/connect-4.data")
    network = NeuralNetwork.LoadFromFile("networks/network_60percent_3layer_winblock.p")
    ai = Ai(1,network,maxDepth = 1)
    print 'win\tloss\ttie'
    for count in range(20):
        win = 0
        loss = 0
        tie = 0
    #example = data.getRandom()
    #board = example.toBoard()
        for x in range(100):
            board = Board()
        #board.printModel()
            while len(board.winner()) == 0 and len(board.availableMoves()) > 0:
            
                board = board.move(1,ai.getNextMove(board))
                #board.printModel()
                if len(board.winner()) != 0 or len(board.availableMoves()) == 0:
                    break
            
                moves = board.availableMoves()
                board = board.move(2,moves[random.randrange(0, len(moves))])
            #board.printModel()
            if len(board.winner()) == 0:
                tie = tie + 1
            #print board.winner()
            elif board.winner()[0] == 1:
                win = win + 1
            elif board.winner()[0] == 2:
                loss = loss + 1

        print "{}\t{}\t{}".format(win,loss,tie)
       # print "Losses: {}".format(loss)
    #for x in range(10):
    #    move,value = ai.getNextMove(board)
    #    board.printModel()
    #    board = board.move(1,move)
    #    board.printModel()
    #    moves = board.availableMoves()
    #    board = board.move(2,moves[random.randrange(0, len(moves))])
    #    if len(board.winner()) > 0:
    #        break
    #print board.winner()

        
if __name__ == "__main__":
	main()
