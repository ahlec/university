#!/bin/sh

ITERATIONS=100

# Run random
echo "Running $ITERATIONS iterations: random vs. random"
# NOTE: '-rand' has to go after the number of iterations because
#        of the way it's hacked in. Could be better but works.
python run_game.py network_60percent_3layer_fullconnect.p $ITERATIONS -rand

echo

# # Run network_60percent_20hidden
# echo "Running $ITERATIONS iterations: random vs. network_60percent_20hidden"
# python run_game.py network_60percent_20hidden.p $ITERATIONS

# echo

# # Run network_80percent_20hidden
# echo "Running $ITERATIONS iterations: random vs. network_80percent_20hidden"
# python run_game.py network_80percent_20hidden.p $ITERATIONS

# echo

# Run network_60percent_3layer_fullconnect
echo "Running $ITERATIONS iterations: random vs. network_60percent_3layer_fullconnect"
python run_game.py network_60percent_3layer_fullconnect.p $ITERATIONS

echo

# Run network_60percent_3layer_winblock
echo "Running $ITERATIONS iterations: random vs. network_60percent_3layer_winblock"
python run_game.py network_60percent_3layer_winblock.p $ITERATIONS

echo

# Run network_60percent_20hidden_winblock
echo "Running $ITERATIONS iterations: random vs. network_60percent_20hidden_winblock"
python run_game.py network_60percent_20hidden_winblock.p $ITERATIONS

echo