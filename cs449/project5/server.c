#include "server.h"

int main()
{
	int socket_file_descriptor;
	int client_file_descriptor;
	pthread_t client_thread;
	struct sockaddr_in bind_socket_address;

	printf("SERVER INITIALIZING.\n");
	
	socket_file_descriptor = socket(PF_INET, SOCK_STREAM, 0);
	if (socket_file_descriptor < 0)
	{
		printf("\tError attempting to create the socket file descriptor.\n");
		exit(1);
	}
	
	printf("\tSERVER SOCKET CREATED.\n");

	memset(&bind_socket_address, 0, sizeof(bind_socket_address));
	bind_socket_address.sin_family = AF_INET;
	bind_socket_address.sin_port = htons(50017);
	bind_socket_address.sin_addr.s_addr = inet_addr("127.0.0.1");
	
	if (bind(socket_file_descriptor, (struct sockaddr *)&bind_socket_address, sizeof(bind_socket_address)) < 0)
	{
		printf("\tError attempting to bind the socket file descriptor to the assigned port.\n");
		exit(1);
	}
	
	printf("\tSERVER SOCKET BOUND.\n");

	if (listen(socket_file_descriptor, 10) < 0)
	{
		printf("\tError attempting to mark the socket file descriptor as a passive listener.\n");
		exit(1);
	}
	
	printf("\tSERVER SOCKET MARKED AS PASSIVE.\n");
	
	if (open_server_stats()) // open up stats.txt in the server_stats portion of the program, or die trying
	{
		printf("Error attempting to open stats.txt.\n");
		exit(1);
	}
	printf("\tstats.txt OPENED.\n");
	
	printf("SERVER STARTED.\nHello! I hope you're having a good day!\n\n");

	printf("AWAITING CONNECTIONS.\n");
	while (1) // always and forever
	{
		client_file_descriptor = accept(socket_file_descriptor, NULL, NULL);
		if (client_file_descriptor < 0)
		{
			printf("Error attempting to accept a client to the socket.\n");
			exit(1);
		}
		if (pthread_create(&client_thread, NULL, process_client, (void *)client_file_descriptor))
		{
			printf("Error attempting to spawn a client process.\n");
			exit(1);
		}
	}
}

void *process_client(void *client)
{
	char *client_request_buffer;
	char *client_temp_buffer = NULL;
	char *client_request_buffer_start;
	char *requested_filename;
	int requested_filename_length;
	int client_request_buffer_size = 0;
	int recv_return_value;
	int client_file_descriptor = (int)client; // let's cast our client file descriptor from the void
		// pointer it was passed under
	printf("Client accepted! %d\n", client_file_descriptor);
	
	// Begin receiving the client request
	client_request_buffer = (char *)malloc(CLIENT_REQUEST_INCREMENT); // start off with our first buffer size
	client_request_buffer_start = client_request_buffer;
	client_request_buffer_size = CLIENT_REQUEST_INCREMENT;
	memset(client_request_buffer_start, 0, client_request_buffer_size); // flush it with NULL
	while (1)
	{
		recv_return_value = recv(client_file_descriptor, client_request_buffer, CLIENT_REQUEST_INCREMENT, 0);
		if (recv_return_value < 0) // encountered an error while receiving the client request
		{
			printf("\tError attempting to recv client request.\n");
			close(client_file_descriptor);
			free(client_request_buffer);
			exit(1);
		}
		if (recv_return_value < CLIENT_REQUEST_INCREMENT ||
			!socket_is_not_empty(client_file_descriptor)) // we have no more to read. break the cycle
		{
			break;
		}
		
		// we have more of the request to read than we can currently hold. let's fix this
		client_temp_buffer = (char *)malloc(client_request_buffer_size); // allocate a temp buffer to
			// hold what we've already read
		memcpy(client_temp_buffer, client_request_buffer_start, client_request_buffer_size); // copy what
			// we've read into the temp buffer
		client_request_buffer_size += CLIENT_REQUEST_INCREMENT; // increment our buffer size once more
		free(client_request_buffer_start); // free our old buffer, so we can allocate it larger
		client_request_buffer = (char *)malloc(client_request_buffer_size); // allocate it larger, then
		client_request_buffer_start = client_request_buffer; // put the start pointer to the start of this
			// new region
		memset(client_request_buffer_start, 0, client_request_buffer_size); // flush the new region with
			// NULL (just to be safe)
		memcpy(client_request_buffer, client_temp_buffer,
			client_request_buffer_size - CLIENT_REQUEST_INCREMENT); // copy what we read into the new
			// buffer
		client_request_buffer += client_request_buffer_size - CLIENT_REQUEST_INCREMENT; // increment
			// where we're currently reading into, so recv can just tack on the request
		free(client_temp_buffer); // free our temp buffer; we're done here
	}
	// Finish receiving the client request

	printf("\tClient request:\n");
	print_request(client_request_buffer_start); // print out an indented version of the client's request
	
	// Begin logging client request to stats file
	if (log_client_request(client_request_buffer_start)) // log the client request
	{
		close(client_file_descriptor); // error encountered during logging. exit server gracefully
		free(client_request_buffer_start);
		exit(1);
	}
	// Finish logging client request to stats file
	
	// Begin testing if the request is a "GET" request
	if (strncmp(client_request_buffer_start, "GET ", 4)) // does the request start with "GET "?
	{
		free(client_request_buffer_start); // it doesn't. send a 400 Bad Request header and leave
		if (send_error_400(client_file_descriptor))
		{
			close(client_file_descriptor);
			exit(1);
		}
		else
		{
			close(client_file_descriptor);
			return;
		}
	}
	// Finish testing if the request is a "GET" request
	
	// Retrieve the requested filename
	client_request_buffer = client_request_buffer_start + 4; // start just immediately past "GET "
	while (1)
	{
		client_temp_buffer = strpbrk(client_request_buffer, "H\r\n");
		if (client_temp_buffer == NULL) // we don't have any of the characters we're looking for! Bad Request
		{
			free(client_request_buffer_start); // free our memory region; we aren't coming back
			if (send_error_400(client_file_descriptor)) // send the header and close the client
			{
				close(client_file_descriptor);
				exit(1);
			}
			else
			{
				close(client_file_descriptor);
				return;
			}
		}
		
		if (strncmp(client_temp_buffer, "\r", 1) == 0 || strncmp(client_temp_buffer, "\n", 1) == 0)
			// we reached a newline character before we reached "HTTP/1." Invalid syntax, deserves a 400 header
		{
			free(client_request_buffer_start); // free our memory region; we aren't coming back
			if (send_error_400(client_file_descriptor)) // send the header and close the client
			{
				close(client_file_descriptor);
				exit(1);
			}
			else
			{
				close(client_file_descriptor);
				return;
			}
		}
		
		if (!strncmp(client_temp_buffer, "HTTP/1.", 7)) // does this match the end of the GET request?
			// note, because lynx and wget both use HTTP/1.0, but HTTP/1.1 is also a standard, we can't
			// assume minor version here
		{
			requested_filename_length = (client_temp_buffer - client_request_buffer_start) - 5;
			requested_filename = (char *)malloc(requested_filename_length + 1);
			memset(requested_filename, 0, requested_filename_length + 1); // fill buffer with zeroes
			client_request_buffer = client_request_buffer_start + 4; // move past "GET "
			strncpy(requested_filename, client_request_buffer, requested_filename_length); // copy from
				// immediately after "GET " until we've reached the position identified by "HTTP/1."
			requested_filename[requested_filename_length] = 0; // set last character to null terminator
			requested_filename_length++;
			
			if (requested_filename_length > 0 && (requested_filename[0] == '\\' ||
				requested_filename[0] == '/')) // if the filename isn't empty, but begins with a slash,
					// we need to modify it so that it doesn't assume the root directory later
			{
				client_temp_buffer = (char *)malloc(requested_filename_length + 1); // allocate temp
					// space for the filename and a prepended "."
				memset(client_temp_buffer, 0, requested_filename_length + 1); // fill with NULL
				client_temp_buffer[0] = '.'; // set initial "." for the filepath
				client_temp_buffer++; // advance pointer past "." so that we place everything after it
				strncpy(client_temp_buffer, requested_filename, requested_filename_length); // copy
				client_temp_buffer--; // return buffer pointer back to the start, so we now have the
					// full buffer
				free(requested_filename); // free the requested_filename space so we can reallocate
				requested_filename_length++; // the filename is now one character longer
				requested_filename = (char *)malloc(requested_filename_length); // realloce the space
				memset(requested_filename, 0, requested_filename_length); // flush it with NULL, just to
					// be safe
				memcpy(requested_filename, client_temp_buffer, requested_filename_length); // copy
				free(client_temp_buffer); // free the temporary buffer
			}
			
			break; // we have the filename. we can break the loop now
		}
		
		client_request_buffer = client_temp_buffer + 1; // false match. Move past this instance of it
	}
	// Finish retrieving the requested filename
	
	printf("\tRequested file: '%s'.\n", requested_filename);
	
	// Free regions which are no longer being used
	free(client_request_buffer_start);
	client_request_buffer_start = NULL;
	client_request_buffer = NULL;
	client_temp_buffer = NULL;
	// Finish freeing regions which are no longer being used
	
	if (send_file(client_file_descriptor, requested_filename, requested_filename_length))
	{
		close(client_file_descriptor); // close client connection. We had an error
		exit(1); // abort the program
	}
	
	printf("\tTransaction complete. Goodbye, client!.\n");
	close(client_file_descriptor); // close the client connection
}

void print_request(char *request) // print the client request onto multiple lines, with indents! (pretty)
{
	char *request_pointer;
	char *output_buffer;
	int line_length;
	int first_output = 1;
	request_pointer = request;
	
	while (1)
	{
		line_length = strcspn(request_pointer, "\r\n"); // how many characters until the next newline?
		
		if (line_length == strlen(request_pointer) && line_length <= 1)
		{
			break; // our input was just empty or a single newline character.
		}
		
		if (line_length <= 1) // 0-1 characters, skip it and move to the next position
		{
			request_pointer++;
			continue;
		}
		
		
		output_buffer = (char *)malloc(line_length + 1); // allocate buffer space for text + NULL
		memset(output_buffer, 0, line_length + 1); // fill the buffer with NULL characters
			// here, the NULL terminator is set as well, because we're copying in one extra character
			// that won't be replaced later
		strncpy(output_buffer, request_pointer, line_length); // copy over the text from current position
		printf("\t\t%s\n", output_buffer); // print it out
		free(output_buffer); // free the buffer
		request_pointer += line_length + 1; // advance by the number of characters read, plus 1 (to move
			// past the newline character we just read
		
		if (line_length == strlen(request_pointer)) // if we read to the NULL terminator, break the loop
		{
			break;
		}
	}
}

int send_file(int client_file_descriptor, char *filename, int filename_length)
{
	FILE *file;
	int file_size;
	char file_buffer[100];
	int read_return_value;
	int send_return_value;
	
	file = fopen(filename, "r");
	if (file == NULL) // if fopen returns NULL, the file couldn't be opened. We assume here
		// that's because it doesn't exist
	{
		return send_error_404(client_file_descriptor);
	}
	
	printf("\tOpened requested file.\n");
	
	// Begin getting file length
	fseek(file, 0, SEEK_END); // Seek to the end of the file (last available position)
	file_size = ftell(file); // Position where we are currently = length of file
	fseek(file, 0, SEEK_SET); // Return to the beginning of the file
	// Finish getting file length
	
	if (send_header_200(client_file_descriptor, file_size)) // Send the HTTP/1.1 200 OK header, using
		// the information we've already gathered.
	{
		fclose(file); // We encountered an error, so close the file
		return 1; // return gracefully
	}
	
	// Begin sending the file
	printf("\tBeginning to send file data.\n");
	while (!feof(file))
	{
		read_return_value = fread(file_buffer, sizeof(char), 100, file);
		if (!read_return_value)
		{
			continue;
		}
		
		send_return_value = send(client_file_descriptor, file_buffer, read_return_value, 0);
		if (send_return_value < 0)
		{
			printf("\tError attempting to send the requested file to the client.\n");
			return 1;
		}
	}
	// Finish sending the file
	
	fclose(file); // close the file now that we're finished with it.
	printf("\tFinished sending file data to the client.\n");
	return 0; // return success, or rather, "no errors"
}

int socket_is_not_empty(int client_file_descriptor) // this was a bitch to write
	// but it was necessary in the situations where the request was a multiple of the buffer
	// size that we were reading into. In that event, it would read the full request but wouldn't
	// abort the reading loop, so it would hang at recv(). This tests if we *have* more to read
{
	static fd_set read_file_descriptor_set;
	static struct timeval timeout;
	int select_return_value;
	timeout.tv_sec = 0; // we don't want to waste *any* time on this function call. Return immediately
	timeout.tv_usec = 0; // no time wasted
	FD_ZERO(&read_file_descriptor_set); // flush the descriptor set with zeroes
	FD_SET(client_file_descriptor, &read_file_descriptor_set); // add our client_file_descriptor to
		// the file descriptor set that we're looking to see if reading is possible with
	
	select_return_value = select(client_file_descriptor + 1, &read_file_descriptor_set, NULL, NULL,
		&timeout); // test to see if reading calls would be possible on the elements in our file descriptor
		// set with client_file_descriptor values up to (and including) our client_file_descriptor value
		// itself
	if (select_return_value < 0) // oops, an error
	{
		printf("\t\tError attempting to determine if the socket is empty.\n");
		exit(1);
	}
	
	return FD_ISSET(client_file_descriptor, &read_file_descriptor_set); // if client_file_descriptor is
		// set in our read_file_descriptor_set, we can read (which means we *have* more to read). if it
		// isn't, we have no more to read, and we're done here.
}
