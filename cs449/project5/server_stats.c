#include "server_stats.h"

static char stats_open = 0;
static FILE *stats_file;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int open_server_stats()
{
	if (stats_open)
	{
		printf("Attempted to open stats.txt when the file is already open.\n");
		return 1;
	}
	
	stats_file = fopen("stats.txt", "a");
	stats_open = 1;
	return 0;
}

int log_client_request(char *request)
{
	int request_length;
	int write_return_value;
	request_length = strlen(request);
	if (!stats_open)
	{
		printf("Attempted to log a client request when stats.txt was not open.\n");
		return 1;
	}
	
	pthread_mutex_lock(&mutex);
	write_return_value = fwrite(request, sizeof(char), request_length, stats_file);
	if (write_return_value != request_length)
	{
		printf("\tError when attempting to write client request to stats.txt.\n");
		return 1;
	}
	if (fflush(stats_file) == EOF)
	{
		printf("\tError when attempting to flush stats.txt.\n");
		return 1;
	}
	pthread_mutex_unlock(&mutex);
	printf("\tClient request logged to stats.txt.\n");
	return 0;
}
