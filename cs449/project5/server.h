#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include "server_stats.h"
#include "server_headers.h"

#define CLIENT_REQUEST_INCREMENT 10
#define MIN(a, b) (((a) <= (b)) ? (a) : (b))

void *process_client(void *client);
void print_request(char *request);
int send_file(int client_file_descriptor, char *filename, int filename_length);
int socket_is_not_empty(int client_file_descriptor);
