#include "server_headers.h"

int send_error_400(int client_file_descriptor)
{
	static const char *bad_request_header = "HTTP/1.1 400 Bad Request\r\n\r\n";
	static const int bad_request_header_length = 28;
	char *header_pointer = bad_request_header;
	int send_amount_remaining = bad_request_header_length;
	int send_return_value;
	while (send_amount_remaining > 0)
	{
		send_return_value = send(client_file_descriptor, header_pointer, send_amount_remaining, 0);
		if (send_return_value < 0)
		{
			printf("\tError attempting to send the HTTP/1.1 400 Bad Request header.\n");
			return 1;
		}
		send_amount_remaining -= send_return_value;
		header_pointer += send_return_value;
	}
	printf("\tSent HTTP/1.1 400 Bad Request header.\n");
	return 0;
}

int send_error_404(int client_file_descriptor)
{
	static const char *file_not_found_header = "HTTP/1.1 404 Not Found\r\n\r\n";
	static const int file_not_found_header_length = 26;
	char *header_pointer = file_not_found_header;
	int send_amount_remaining = file_not_found_header_length;
	int send_return_value;
	while (send_amount_remaining > 0)
	{
		send_return_value = send(client_file_descriptor, header_pointer, send_amount_remaining, 0);
		if (send_return_value < 0)
		{
			printf("\tError attempting to send the HTTP/1.1 404 Not Found header.\n");
			return 1;
		}
		send_amount_remaining -= send_return_value;
		header_pointer += send_return_value;
	}
	printf("\tSent HTTP/1.1 404 Not Found header.\n");
	return 0;
}

int send_header_200(int client_file_descriptor, int file_size)
{
	time_t rawtime;
	struct tm *timeinfo;
	int header_length;
	char *header;
	char *header_pointer;
	char time_buffer[30];
	char time_string[29];
	int temp_return_value;
	int send_amount_remaining;
	
	time(&rawtime);
	timeinfo = gmtime(&rawtime);
	temp_return_value = strftime(time_buffer, 30, "%a, %d %b %Y %H:%M:%S GMT", timeinfo);
	if (!temp_return_value)
	{
		printf("\t\tError attempting to create the Date portion of the HTTP/1.1 200 OK header.\n");
		return 1;
	}
	strncpy(time_string, time_buffer, 29);
	
	header_length = 118 + NUMBER_DIGITS(file_size);
	header = (char *)malloc(header_length);
	
	temp_return_value = sprintf(header, "HTTP/1.1 200 OK\r\nDate: %s\r\nContent-Length: %d\r\n"
				"Connction: close\r\nContent-Type: text/html\r\n\r\n", time_string, file_size);
	if (temp_return_value < 0)
	{
		free(header);
		printf("\t\tError attempting to create the HTTP/1.1 200 OK header.\n");
		return 1;
	}
	
	send_amount_remaining = header_length - 1;
	header_pointer = header;
	while (send_amount_remaining > 0)
	{
		temp_return_value = send(client_file_descriptor, header_pointer, send_amount_remaining, 0);
		if (temp_return_value < 0)
		{
			free(header);
			printf("\t\tError attempting to send the HTTP/1.1 200 OK header.\n");
			return 1;
		}
		send_amount_remaining -= temp_return_value;
		header_pointer += temp_return_value;
	}
	
	free(header);
	printf("\tSent the HTTP/1.1 200 OK header.\n");
	return 0;
}
