#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

int send_error_400(int client_file_descriptor);
int send_error_404(int client_file_descriptor);
int send_header_200(int client_file_descriptor, int file_size);

#define NUMBER_DIGITS(number) (log10(number) + 1)
