#include <linux/fs.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <asm/uaccess.h>
#include "e.h"

static ssize_t e_driver_read(struct file * file, char * buf, size_t count, loff_t *ppos)
{
	e(buf, count + *ppos);
	*ppos = count;
	return count;
}

static const struct file_operations e_driver_fops = {
	.owner		= THIS_MODULE,
	.read		= e_driver_read,
};

static struct miscdevice e_driver_dev = {
	MISC_DYNAMIC_MINOR,
	"e_driver",
	&e_driver_fops
};

static int __init
e_driver_init(void)
{
	int return_value;

	return_value = misc_register(&e_driver_dev);
	if (return_value)
	{
		printk(KERN_ERR
		       "Unable to register the \"e_driver\" misc device.\n");
	}

	return return_value;
}
module_init(e_driver_init);

static void __exit
e_driver_exit(void)
{
	misc_deregister(&e_driver_dev);
}
module_exit(e_driver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jacob Deitloff <jbd19@pitt.edu, jacob@deitloff.com>");
MODULE_DESCRIPTION("\"e_driver\" minimal module");
MODULE_VERSION("dev");
