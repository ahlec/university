#include <stdio.h>

int main(int argc, char *argv[])
{
	char *digits_read_buffer;
	char *digits_read_head;
	int start_position;
	int end_position;
	FILE *driver_stream;
	
	// begin processing command line arguments
	if (argc != 3) // where the first parameter is the program name, second is start index, third is end index
	{
		printf("Invalid number of parameters provided. Must provide two parameters, the start position and the end position.\n");
		return 1;
	}
	start_position = atoi(argv[1]);
	end_position = atoi(argv[2]);
	// finish processing command line arguments	

	// command line argument constraints	
	if (end_position < start_position) // if the end position is less than start position, we can't read anything
	{
		printf("Cannot provide a start position that exists after the end position. Aborting.\n");
		return 1;
	}

	// begin driver+buffer setup
	driver_stream = fopen("/dev/e_driver", "r"); // opening the driver as a FILE *, so that we can perform fread on it.
	if (driver_stream < 0) // If driver_stream has returned a negative number, we have a problem here, and couldn't open it.
	{
		printf("Unable to open device driver. Aborting.\n");
		return 1;
	}

	digits_read_buffer = (char *)malloc(end_position + 1); // allocate the buffer that we will read the driver's contents
		// into. Because with fread we aren't able to specify where to begin reading, we cut out losses and make a
		// buffer that is able to hold the digits from e[0] to e[end_position], inclusive, because start_index cannot
		// be less than zero, so we will only ever have as many as end_position + 1 characters to read.
	digits_read_head = digits_read_buffer; // When we are printing the buffer later, we advance the pointer for the buffer
		// by start_position, to begin the output at the start_position that the command line arguments specified.
		// Therefore, in order to be able to call free() on our malloc'd buffer, we need to save the original pointer
		// to the head of that buffer.
	// finish driver+buffer setup

	fread(digits_read_buffer, sizeof(char), end_position + 1, driver_stream); // read from the driver_stream into the digits_read_buffer,
		// end_position + 1 number of characters. It would perhaps be a better idea to be checking the return value of this
		// operation.

	if (!start_position && !end_position) // If both start_position and end_position are equal to 0, that means we are only
		// displaying the first digit of e. This is a bit of a special case, as experimentation with the e function provided in
		// e.h does not return just the first digit, but requires at minimum the first two digits to be returned; asking for
		// only the first digit returns nothing. We print out only the first char in the digits_read_buffer here, then, to
		// correctly display the first digit of e and give proper output, as expected, across the board.
	{
		printf("%c\n", digits_read_buffer[0]);
	}
	else
	{
		if (start_position) // If start position isn't zero, we need to advance the buffer, so that we begin our printing from
			// the buffer at the index which the command line arguments specify.
		{
			digits_read_buffer += start_position; // Because we are dealing with chars for our malloc'd region, we can
				// simply add start_position to the pointer for digits_read_buffer, as that will advance us the
				// necessary amount of characters;
		}
	
		printf("%s\n", digits_read_buffer);
	}

	// close up shop
	free(digits_read_head);
	close(driver_stream);
	
	return 0;
}
