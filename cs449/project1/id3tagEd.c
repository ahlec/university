#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct tag
{
	char identifier[3];
	char title[30];
	char artist[30];
	char album[30];
	char year[4];
	char comment[28];
	char separator_byte;
	char track_number;
	char genre;
};

enum tag_locations
{
	none,
	start,
	end
};

void reset_tag(struct tag *);

int main(int argc, char *argv[])
{
	struct tag id3tag;
	enum tag_locations location;
	FILE *file_stream;
	int arg_index;
	//char read_buffer[30];
	int tag_updated = 0;
	if (argc < 2)
	{
		printf("The filename must be provided at minimum to the program in order to execute.\n");
		return 1;
	}

	// Fill id3tag with default values
	reset_tag(&id3tag);
	// End filling with default values

	file_stream = fopen(argv[1], "r+b");
	
	// attempt to put the file pointer at the proper location within the file stream
	if (fread(&id3tag, 1, 128, file_stream) != 128)
	{
		printf("Error attempting to read from the file, but the file appears to be empty.\n");
		fclose(file_stream);
		return 1;
	}

	if (strncmp(id3tag.identifier, "TAG", 3) == 0)
	{
		location = start;
	}
	else
	{
		fseek(file_stream, -128, SEEK_END);
		if (fread(&id3tag, 1, 128, file_stream) != 128)
		{
			printf("Error reading from the end of the file, as the file is less than 128 bytes.\n");
			fclose(file_stream);
			return 1;
		}

		if (strncmp(id3tag.identifier, "TAG", 3) == 0)
		{
			location = end;
		}
		else
		{
			location = none;
		}
	}
	// end moving the file pointer to the proper location

	// make sure that the identifier is always "TAG"
	strncpy(id3tag.identifier, "TAG", 3);

	// reset to default values if we didn't read an actual tag (read as, remove music data)
	if (location == none)
	{
		reset_tag(&id3tag);
	}
	// finish reseting

	// Output the tag if it exists, or otherwise
	if (location == none)
	{
		printf("No tag was found.\n");
	}
	else
	{
		printf("The ID3 Tag is as follows:\n");
		printf("Title: %.30s\n", id3tag.title);
		printf("Artist: %.30s\n", id3tag.artist);
		printf("Album: %.30s\n", id3tag.album);
		printf("Track Number: %d\n", id3tag.track_number);
		printf("Year: %.4s\n", id3tag.year);
		printf("Comment: %.28s\n", id3tag.comment);
	}
	// Finish outputting

	// Update the tag (STRUCT) with command line values
	for (arg_index = 2; arg_index < argc; arg_index += 2)
	{
		if (strcmp(argv[arg_index], "-title") == 0)
		{
			tag_updated = 1;
			strncpy(id3tag.title, argv[arg_index + 1], 30);
		}
		else if (strcmp(argv[arg_index], "-artist") == 0)
		{
			tag_updated = 1;
			strncpy(id3tag.artist, argv[arg_index + 1], 30);
		}
		else if (strcmp(argv[arg_index], "-album") == 0)
		{
			tag_updated = 1;
			strncpy(id3tag.album, argv[arg_index + 1], 30);
		}
		else if (strcmp(argv[arg_index], "-tracknumber") == 0)
		{
			tag_updated = 1;
			id3tag.track_number = (char)atoi(argv[arg_index + 1]);
		}
		else if (strcmp(argv[arg_index], "-year") == 0)
		{
			tag_updated = 1;
			strncpy(id3tag.year, argv[arg_index + 1], 4);
		}
		else if (strcmp(argv[arg_index], "-comment") == 0)
		{
			tag_updated = 1;
			strncpy(id3tag.comment, argv[arg_index + 1], 28);
		}
	}
	// Finish updating the tag (STRUCT)
	
	// Output the new tag
	if (tag_updated)
	{
		printf("\nThe updated ID3 tag will be as follows:\n");
		printf("Title: %.30s\n", id3tag.title);
		printf("Artist: %.30s\n", id3tag.artist);
		printf("Album: %.30s\n", id3tag.album);
		printf("Track Number: %d\n", id3tag.track_number);
		printf("Year: %.4s\n", id3tag.year);
		printf("Comment: %.28s\n", id3tag.comment);
	}
	// Finish outputting

	// Write the new tag to the file, if we should
	if (tag_updated)
	{
		if (location == start)
		{
			fseek(file_stream, 0, SEEK_SET);
		}
		else if (location == end)
		{
			fseek(file_stream, -128, SEEK_END);
		}
		else
		{
			fseek(file_stream, 0, SEEK_END);
		}
		fwrite(&id3tag, 1, 128, file_stream);
	}
	// Finish writing

	fclose(file_stream);
	return 0;
}

void reset_tag(struct tag * id3tag)
{
	strncpy(id3tag->identifier, "TAG", 3);
	strncpy(id3tag->title, "", 1);
	strncpy(id3tag->artist, "", 1);
	strncpy(id3tag->album, "", 1);
	strncpy(id3tag->year, "", 1);
	strncpy(id3tag->comment, "", 1);
	id3tag->separator_byte = 0;
	id3tag->track_number = 0;
	id3tag->genre = 0;
}
