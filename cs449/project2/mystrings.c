#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE *file_stream;
  char four_consecutive[4];
  int current_index;
  char read;

  if (argc != 2)
  {
	  printf("Must provide one (and only one) command line argument -- the name of the file.\n");
	  return 1;
  }
  
  file_stream = fopen(argv[1], "r+b");
  current_index = 0;

  while (!feof(file_stream))
  {
	read = fgetc(file_stream);
	if (read == EOF || read < 32 || read == 127)
	{
		if (current_index == 4)
		{
			printf("\n");
		}
		current_index = 0;
		continue;
	}
	
	if (current_index < 4)
	{
		four_consecutive[current_index++] = read;
		if (current_index == 4)
		{
			printf("%c%c%c%c", four_consecutive[0], four_consecutive[1],
				four_consecutive[2], four_consecutive[3]);
		}
		continue;
	}

	printf("%c", read);
  }

  fclose(file_stream);
  return 0;
}
