#include <stdio.h>

typedef struct node
{
	int grade;
	struct node *next;
	struct node *previous;
} Node;

int main()
{
	Node *head;
	Node *next;
	Node *current;
	int gradeInput;
	int numberGrades = 0;
	double totalScore = 0;
	while (1)
	{
		scanf("%d", &gradeInput);
		if (gradeInput == -1)
		{
			break;
		}
		if (head == NULL)
		{
			head = malloc(sizeof(Node));
			current = head;
			head->grade = gradeInput;
		}
		else
		{
			current->next = malloc(sizeof(Node));
			current->next->grade = gradeInput;
			current->next->previous = current;
			current = current->next;
		}
	}

	current = head;
	while (1)
	{
		totalScore += current->grade;
		numberGrades++;
		
		if (current->next == NULL)
		{
			break;
		}
		current = current->next;
	}

	printf("\nAverage: %f\n", (numberGrades > 0 ? totalScore / numberGrades : 0));
	
	while (current != NULL)
	{
		if (current->previous == NULL)
		{
			free(current);
			break;
		}
		current = current->previous;
		free(current->next);
	}

	return 0;
}
