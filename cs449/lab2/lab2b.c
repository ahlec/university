#include <stdio.h>

int main(void)
{
	int weight;
	printf("Please enter the weight you'd like to convert: ");
	scanf("%d", &weight);
	printf("\n\nYou have entered: %d lbs\n\n", weight);
	printf("Here is your weight on other planets:\n");
	printf("Mercury		%.lf lbs\n", weight * 0.38);
	printf("Venus		%.lf lbs\n", weight * 0.91);
	printf("Mars		%.lf lbs\n", weight * 0.38);
	printf("Jupiter		%.lf lbs\n", weight * 2.54);
	printf("Saturn		%.lf lbs\n", weight * 1.08);
	printf("Uranus		%.lf lbs\n", weight * 0.91);
	printf("Neptune		%.lf lbs\n", weight * 1.19);
	printf("Pluto		%.lf lbs\n", weight * 0.08);
	return 0;
}
