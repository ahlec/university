void *my_worstfit_malloc(int);
void my_free(void *);

typedef struct malloc_node {
    int size;
    char is_empty;
    struct malloc_node *next;
    struct malloc_node *previous;
} mymalloc_node;
