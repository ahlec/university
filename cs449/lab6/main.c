#include <stdio.h>
#include <dlfcn.h>

int main()
{
	void *handle;
	void (*my_str_copy)(char *, char *);	
	char (*my_str_cat)(char *, char *);
	char *error;
	char dest[100];
	char a[] = " Goodbye";
	char src[] = "Hello World!";
	char b[] = " World!";
	handle = dlopen("mystr.so", RTLD_LAZY);
	if (!handle)
	{
		printf("%s\n", dlerror());
		exit(1);
	}
	dlerror();
	my_str_copy = dlsym(handle, "my_strcpy");
	if ((error = dlerror()) != NULL)
	{
		printf("%s\n", dlerror());
		exit(1);
	}
	dlerror();
	my_str_cat = dlsym(handle, "my_strcat");
	if ((error = dlerror()) != NULL)
	{
		printf("%s\n", dlerror());
		exit(1);
	}

	my_str_copy(dest, src);
	my_str_cat(dest, a);
	my_str_cat(dest, b);
	printf("'%s'\n", dest);
	dlclose(handle);
	return 0;
}
