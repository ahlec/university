// Project 1 - Rock, Paper, Scissors
// Jacob Deitloff (jbd19)

#include <stdio.h>
#include <string.h>
#include <ctype.h>

void to_lower(char str[], int length)
{
  int index = 0;
  for (index = 0; index < length; index++)
  {
    str[index] = tolower(str[index]);
  }
}

int main(void)
{
  int player_score = 0;
  int computer_score = 0;
  char confirmation[3];
  int round_score = 0;

  srand((unsigned int)time(NULL));
  printf("Welcome to Rock, Paper, Scissors\n\n");

  while (1)
  {
    printf("Would you like to play? ");
    scanf("%s", &confirmation);
    printf("\n");
    to_lower(confirmation, sizeof(confirmation) / sizeof(confirmation[0]));
    if (strcmp(confirmation, "no") == 0)
    {
      printf("Goodbye!\n");
      break;
    }
    
    player_score = 0;
    computer_score = 0;
    do
    {
      round_score = play_round();
      if (round_score == -1)
      {
        computer_score++;
      }
      else if (round_score == 1)
      {
        player_score++;
      }
      printf("The score is now you: %d computer: %d\n\n", player_score, computer_score);
    } while (player_score < 3 && computer_score < 3);

    if (player_score > computer_score)
    {
      printf("You have beaten the computer!\a\n\n");
    }
    else
    {
      printf("You have lost to the computer.\a\n\n");
    }
  }

  return 0;
}

int play_round(void)
{
  char playerTextInput[8];
  int computerInput;
  int playerInput;
  
  /* ROCK = 0
     PAPER = 1
     SCISSORS = 2 */

  computerInput = rand() % 3;
  printf("What is your choice? ");
  scanf("%s", &playerTextInput);
  to_lower(playerTextInput, sizeof(playerTextInput) / sizeof(playerTextInput[0]));
  playerInput = (strcmp(playerTextInput, "rock") == 0 ? 0 : (strcmp(playerTextInput, "paper") == 0 ? 1 : 2));
  printf("The computer chooses %s. You %s this game!\n", (computerInput == 0 ? "rock" : (computerInput == 1 ? "paper" :
	"scissors")), (computerInput == playerInput ? "tie" : ((computerInput == 0 && playerInput == 1) ||
	(computerInput == 1 && playerInput == 2) || (computerInput == 2 && playerInput == 0) ? "win" : "lose")));

  if (computerInput == playerInput)
  {
    return 0;
  }
  else if (computerInput == 0 && playerInput == 1 || computerInput == 1 && playerInput == 2 ||
	computerInput == 2 && playerInput == 0)
  {
    return 1;
  }

  return -1;
}
