import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.Collections;

public class omet_stats {
    private Connection _connection;
    
    public static void main(String[] args) {		
		// Create the omet_stats
		omet_stats omet = new omet_stats();
		
		// Parse Args
		if (args.length < 1) {
			System.err.println("Not enough arguments.");
			System.exit(1);
		}
		
		if (args[0].equals("term")) {
			enforceArgumentCount(args, 2);
			omet.listTerm(args[1]);
		}
		else if (args[0].equals("class")) {
			enforceArgumentCount(args, 3);
			omet.listClass(args[1], args[2]);
		}
		else if (args[0].equals("subject")) {
			enforceArgumentCount(args, 2);
			omet.listSubject(args[1]);
		}
		else if (args[0].equals("prof")) {
			enforceArgumentCount(args, 2);
			omet.listProf(args[1]);
		}
		else {
			System.err.println("Invalid method.");
			System.exit(1);
		}
    }
	
	public static void enforceArgumentCount(String[] args, int count) {
		if (args.length < count) {
			System.err.println("Not enough arguments.");
			System.exit(1);
		}
	}
	
    public omet_stats() {
		try {
			_connection = OmetHelper.openConnection();
		}
		catch(Exception e) {
			System.err.println("Error connecting to database.");
			System.err.println(e.toString());
		}
    }
	
	public void listTerm(String term) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, sum_q1, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE term = ?");
			statement.setString(1, term);
			
			ResultSet results = statement.executeQuery();
			if (!results.next()) {
				System.err.println("Invalid survey -- no such term: " + term);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void listClass(String subject, String courseNumber) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, sum_q1, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE lower(subject) = lower(?) AND course_number = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, 
  ResultSet.CONCUR_READ_ONLY);
			statement.setString(1, subject);
			statement.setString(2, courseNumber);
			
			ResultSet results = statement.executeQuery();

			if (!results.next()) {
				System.err.println("Invalid survey -- no such class: " + subject + " " + courseNumber);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void listSubject(String subject) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, sum_q1, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE lower(subject) = lower(?)");
			statement.setString(1, subject);
			
			ResultSet results = statement.executeQuery();
			if (!results.next()) {
				System.err.println("Invalid survey -- no such subject: " + subject);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void listProf(String lastName) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, sum_q1, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE lower(last_name) = lower(?)");
			statement.setString(1, lastName);
			
			ResultSet results = statement.executeQuery();
			if (!results.next()) {
				System.err.println("Invalid survey -- no such last name: " + lastName);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	private void outputResults(ResultSet results) {
		try {
			// Calculate all of the averages
			ArrayList<Double> averages = new ArrayList<Double>();
			do {
				if (results.getInt(3) > 0) {
					averages.add(results.getDouble(2) / results.getDouble(3));
				}
				else {
					averages.add(0.0);
				}
			} while (results.next());
			Collections.sort(averages);
			Collections.reverse(averages);
			
			// Output the statistics
			int nAverages = averages.size();
			System.out.println(Double.toString(averages.get(0)) + "\tMax");
			int top25Index = Math.min((int)Math.floor((double)nAverages / 4), nAverages - 1);
			System.out.println(Double.toString(averages.get(top25Index)) + "\tTop 25%");
			int medianIndex = Math.min((int)Math.floor((double)nAverages / 2), nAverages - 1);
			System.out.println(Double.toString(averages.get(medianIndex)) + "\tMedian");
			int bottom25Index = Math.min((int)Math.ceil((double)nAverages * 0.75), nAverages - 1);
			System.out.println(Double.toString(averages.get(bottom25Index)) + "\tBottom 25%");
			System.out.println(Double.toString(averages.get(nAverages - 1)) + "\tMin");
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);		
		}
	}
}
