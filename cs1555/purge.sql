set head off

set pagesize 0

set verify off

set feedback off

select 'drop '||object_type||' '|| object_name||  DECODE(OBJECT_TYPE,'TABLE',' CASCADE CONSTRAINTS;',';') from user_objects

spool drop_objects.sql

/

spool off