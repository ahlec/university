CS1555 - Spring 2014
19 April 2014
Alec Jacob Deitloff (jbd19)
Final Project

I. INCLUDED FILES
--------------------
data.sql (self-created testing dataset)
data2.sql (class-provided testing dataset)
omet_list.java (Problem #3; lists all of the surveys based on search criteria)
omet_show.java (Problem #2; shows the summary of a given survey)
omet_stats.java (Problem #4; shows the breakdown of q1 scores for a given survey)
omet_submit.java (Problem #1; submits an individual student's survey response)
OmetHelper.java (library for connecting to the database)
README.txt (this file)
schema.sql (class-provided schema file)
tw.sql (Problem #1; trigger creation script to power the sum fields in the database)

II. PREPARATION
-------------------
First, when compiling on unixs.cssd.pitt.edu, we need to set up the database.

$ source ~labrinid/1555/bash.env
$ sqlplus
// Enter username:
// Enter password:
$ sqlplus> @schema
$ sqlplus> @tw
$ sqlplus> @data2

data2 is the class-provided dataset that we will work with; data.sql is a self-created dataset used for testing. Either of them are fine, but data2.sql has more entries to work with. Before this, however, we need to create the schema for the database. The provided file (schema.sql) will create the tables and constraints. However, tw.sql will create the assignment trigger (the part not provided by the assignment schema) that powers the sum_q1, sum_q2, sum_s3 and sum_q4 fields.

Next, we need to compile the java programs (and helper library)

$ sqlplus> exit
$ javac omet_list.java omet_show.java omet_stats.java omet_submit.java OmetHelper.java

III. RUNNING
-----------------
See project PDF file for specifications on how to run the individual programs.

IV. NOTES
-----------------
Everything has been tested on unix.cssd.pitt.edu and runs correctly. No abnormal bugs or otherwise have been detected so far.