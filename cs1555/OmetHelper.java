import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class OmetHelper {

	public static Connection openConnection() throws SQLException {
		DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
		try {
			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@class3.cs.pitt.edu:1521:dbclass",
				"jbd19", "7046");
			
			return connection;
		}
		catch (Exception e) {
			System.err.println("Unable to open connection.");
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	public static void validateSurveyId(int surveyId, Connection dbConnection) {
		try {
			PreparedStatement statement = dbConnection.prepareStatement("SELECT count(*) FROM Surveys WHERE survey_id = ?");
			statement.setInt(1, surveyId);
			
			ResultSet results = statement.executeQuery();
			results.next();
			
			if (results.getInt(1) == 0) {
				System.err.println("Invalid survey -- no such survey id: " + Integer.toString(surveyId));
				System.exit(1);
			}
		}
		catch (Exception e) {
			System.err.println("Encountered an error");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
}