import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.CallableStatement;

public class omet_show {
    private Connection _connection;
    
    public static void main(String[] args) {
		// PARSE ARGS
		if (args.length != 1) {
			System.err.println("Must provide one argument to this function.");
			System.err.println("<survey id>");
			System.exit(1);
		}
		
		int surveyId = 0;
		try {
			surveyId = Integer.parseInt(args[0]);
		}
		catch (Exception e) {
			System.err.println("<survey id> must be an integer.");
			System.exit(1);
		}
		
		omet_show omet = new omet_show();
		omet.perform(surveyId);
    }
	
    public omet_show() {
		try {
			_connection = OmetHelper.openConnection();
		}
		catch(Exception e) {
			System.err.println("Error connecting to database.");
			System.err.println(e.toString());
		}
    }
	
    public void perform(int surveyId) {
		// Validate parameters
		OmetHelper.validateSurveyId(surveyId, _connection);
		
		// Query the database for survey metadata
		ResultSet metadataResults = null;
		try {
			PreparedStatement metadataStatement = _connection.prepareStatement("SELECT subject, course_number, last_name, sum_q1, sum_q2, sum_q3, sum_q4, num_submitted, enrollment FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE survey_id = ?");
			metadataStatement.setInt(1, surveyId);
			
			metadataResults = metadataStatement.executeQuery();
			metadataResults.next();
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		// Print the metadata header
		try {
			System.out.println("Survey " + Integer.toString(surveyId) + " for class " + metadataResults.getString(1) + " " +
				metadataResults.getString(2) + " (Prof. " + metadataResults.getString(3) + ")");
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);		
		}
				
		// Query and print all of the survey results
		int numberResults = 0;
		try {
			PreparedStatement resultsStatement = _connection.prepareStatement("SELECT q1, q2, q3, q4, q5_str FROM Surveydata WHERE survey_id = ?");
			resultsStatement.setInt(1, surveyId);
			
			ResultSet resultsResults = resultsStatement.executeQuery();
			while (resultsResults.next()) {
				System.out.println(Integer.toString(++numberResults) + ". " + resultsResults.getString(1) + " " + resultsResults.getString(2) + " " + resultsResults.getString(3) + " " + resultsResults.getString(4) + " \"" + resultsResults.getString(5) + "\"");
			}
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		// Print metadata footer
		System.out.println("-----------------------------------");
		System.out.println("SURVEY SUMMARY:");
		try {
			double averageQ1 = (numberResults > 0 ? metadataResults.getDouble(4) / metadataResults.getDouble(8) : 0);
			double averageQ2 = (numberResults > 0 ? metadataResults.getDouble(5) / metadataResults.getDouble(8) : 0);
			double averageQ3 = (numberResults > 0 ? metadataResults.getDouble(6) / metadataResults.getDouble(8) : 0);
			double averageQ4 = (numberResults > 0 ? metadataResults.getDouble(7) / metadataResults.getDouble(8) : 0);
			System.out.println("Average Q1: " + Double.toString(averageQ1));
			System.out.println("Average Q2: " + Double.toString(averageQ2));
			System.out.println("Average Q3: " + Double.toString(averageQ3));
			System.out.println("Average Q4: " + Double.toString(averageQ4));
			System.out.println("Submitted: " + metadataResults.getString(8));
			System.out.println("Enrollment: " + metadataResults.getString(9));
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		// Close the connection
		try {
			_connection.close();
		}
		catch(Exception e) {
			System.err.println("Error disconnecting from database.");
			System.err.println(e.toString());
		}
    }
}
