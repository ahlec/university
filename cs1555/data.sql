drop table School;
create table School ( 
	school_id int,  
	school_name varchar(50),
	constraint pk_school primary key (school_id)
);

drop table Dept;
create table Dept (
	dept_id int, 
	dept_name varchar(30), 
	school_id int, 
	constraint pk_dept primary key (dept_id),
	constraint fk_dept_school foreign key (school_id) 
		references School(school_id)
);

drop table Instructors;
create table Instructors (
	fid int, 
	first_name varchar(32), 
	last_name varchar(32), 
	pitt_account varchar(10), 
	dept_id int, 
	constraint pk_instructor primary key (fid),
	constraint fk_instructor_dept foreign key (dept_id)
		references Dept(dept_id)
);

drop table Subjects;
create table Subjects (
	subject varchar(10), 
	dept_id int, 
	constraint pk_subjects primary key (subject),
	constraint fk_subject_dept foreign key (dept_id) 
		references Dept(dept_id)
);

drop table Students;
create table Students ( 
	sid int, 
	first_name varchar(32), 
	last_name varchar(32), 
	pitt_account varchar(10), 
	major varchar(10), 
	constraint pk_student primary key (sid),
	constraint fk_student_subject foreign key (major) 
		references Subjects(subject)
);

drop table Courses;
create table Courses (
	cid int, 
	subject varchar(10), 
	course_number int, 
	name varchar(30), 
	enrollment int, 
	term int, 
	instructor_id int, 
	constraint pk_courses primary key (cid),
	constraint fk_courses_instructor foreign key (instructor_id) 
		references Instructors(fid)
);

drop table Surveys;
create table Surveys ( 
	survey_id int, 
	cid int, 
	num_submitted int, 
	sum_q1 int, 
	sum_q2 int, 
	sum_q3 int, 
	sum_q4 int,
	constraint pk_survey primary key (survey_id),
	constraint fk_courses foreign key (cid) 
		references Courses(cid)
);

drop table Surveydata;
create table Surveydata ( 
	survey_id int,  
	sid int,
	submit_time date,
	q1 int, 
	q2 int, 
	q3 int, 
	q4 int, 
	q5_str varchar(250),
	constraint pk_survey_data primary key (survey_id, sid),
	constraint fk_sdata_surveys foreign key (survey_id) 
		references Surveys(survey_id),
	constraint fk_sdata_students foreign key (sid) 
		references Students(sid)
);

INSERT INTO School VALUES(1, 'University of Pittsburgh');
INSERT INTO School VALUES(2, 'Carnegie Mellon University');

INSERT INTO Dept VALUES(1, 'Computer Science', 1);
INSERT INTO Dept VALUES(2, 'Math', 1);
INSERT INTO Dept VALUES(3, 'Computer Science', 2);
INSERT INTO Dept VALUES(4, 'Math', 2);

INSERT INTO Instructors VALUES(1, 'Alec', 'Deitloff', 'jbd19', 1);
INSERT INTO Instructors VALUES(2, 'Jackson', 'Overland', 'jfo17', 1);

INSERT INTO Subjects VALUES('CS', 1);
INSERT INTO Subjects VALUES('Math', 1);
INSERT INTO Subjects VALUES('COE', 1);

INSERT INTO Students VALUES(1, 'Alec', 'Deitloff', 'jbd19', 'CS');
INSERT INTO Students VALUES(2, 'Emma', 'Overland', 'eso20', 'Math');
INSERT INTO Students VALUES(3, 'Harald', 'Haddock', 'hhh1', 'Math');

INSERT INTO Courses VALUES(1, 'CS', 1550, 'Operating Systems', 20, 1, 1);
INSERT INTO Courses VALUES(2, 'CS', 1675, 'Machine Learning', 48, 1, 2);
INSERT INTO Courses VALUES(3, 'CS', 1000, 'Algorithms', 60, 1, 2);

INSERT INTO Surveys VALUES(1, 1, 0, 0, 0, 0, 0);
INSERT INTO Surveys VALUES(2, 2, 0, 0, 0, 0, 0);
INSERT INTO Surveys VALUES(3, 3, 0, 0, 0, 0, 0);