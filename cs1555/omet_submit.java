import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.CallableStatement;

public class omet_submit {
    private Connection _connection;
    
    public static void main(String[] args) {
		// PARSE ARGS
		if (args.length != 7) {
			System.err.println("Must provide seven arguments to this function.");
			System.err.println("<survey id> <sid> <q1> <q2> <q3> <q4> <q5 str>");
			System.exit(1);
		}
		
		int surveyId = 0;
		try {
			surveyId = Integer.parseInt(args[0]);
		}
		catch (Exception e) {
			System.err.println("<survey id> must be an integer.");
			System.exit(1);
		}
		
		int sid = 0;
		try {
			sid = Integer.parseInt(args[1]);
		}
		catch (Exception e) {
			System.err.println("<sid> must be an integer.");
			System.exit(1);
		}
		
		int q1 = omet_submit.parseQNumber(args[2], 1);
		int q2 = omet_submit.parseQNumber(args[3], 2);
		int q3 = omet_submit.parseQNumber(args[4], 3);
		int q4 = omet_submit.parseQNumber(args[5], 4);
	
		omet_submit omet = new omet_submit();
		omet.perform(surveyId, sid, q1, q2, q3, q4, args[6]);
    }
	
	private static int parseQNumber(String strValue, int fieldNumber) {
	
		// PARSE IT AS AN INTEGER
		int intValue = 0;
		try {
			intValue = Integer.parseInt(strValue);
		} catch (Exception e) {
			System.err.println("<q" + Integer.toString(fieldNumber) + "> must be an integer.");
			System.exit(1);
		}
		
		// CHECK TO MAKE SURE THAT IT IS WITHIN BOUNDS
		if (intValue <= 0 || intValue >= 6) {
			System.err.println("<q" + Integer.toString(fieldNumber) + "> must be within [1,5].");
			System.exit(1);
		}
		
		// RETURN
		return intValue;
	}

    public omet_submit() {
		try {
			_connection = OmetHelper.openConnection();
		}
		catch(Exception e) {
			System.err.println("Error connecting to database.");
			System.err.println(e.toString());
		}
    }
	
    public void perform(int surveyId, int sid, int q1, int q2, int q3, int q4, String q5) {
		// Validate parameters
		OmetHelper.validateSurveyId(surveyId, _connection);
		try {
			PreparedStatement sidStatement = _connection.prepareStatement("SELECT count(*) FROM Students WHERE sid = ?");
			sidStatement.setInt(1, sid);
			
			ResultSet results = sidStatement.executeQuery();
			results.next();
			
			if (results.getInt(1) == 0) {
				System.err.println("Invalid survey -- no such student id: " + Integer.toString(sid));
				System.exit(1);
			}
		}
		catch (Exception e) {
			System.err.println("Encountered an error");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		// Add the survey to the database
		try {
			CallableStatement insertStatement = _connection.prepareCall("INSERT INTO Surveydata VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
			insertStatement.setInt(1, surveyId);
			insertStatement.setInt(2, sid);
			insertStatement.setDate(3,new java.sql.Date(System.currentTimeMillis()));  
			insertStatement.setInt(4, q1);
			insertStatement.setInt(5, q2);
			insertStatement.setInt(6, q3);
			insertStatement.setInt(7, q4);
			insertStatement.setString(8, q5);
			
			insertStatement.execute();
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		// Print out the success message
		try {
			PreparedStatement courseStatement = _connection.prepareStatement("SELECT subject, course_number FROM Surveys s JOIN Courses c ON s.cid = c.cid WHERE survey_id = ?");
			courseStatement.setInt(1, surveyId);
			
			ResultSet courseResults = courseStatement.executeQuery();
			courseResults.next();
			
			System.out.println("Survey " + Integer.toString(surveyId) + " for class " + courseResults.getString(1) + " " +
				courseResults.getString(2) + " recorded.");
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		// Close the connection
		try {
			_connection.close();
		}
		catch(Exception e) {
			System.err.println("Error disconnecting from database.");
			System.err.println(e.toString());
		}
    }
}
