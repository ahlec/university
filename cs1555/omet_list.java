import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.CallableStatement;

public class omet_list {
    private Connection _connection;
    
    public static void main(String[] args) {		
		// Create the omet_list
		omet_list omet = new omet_list();
		
		// Parse Args
		if (args.length < 1) {
			System.err.println("Not enough arguments.");
			System.exit(1);
		}
		
		if (args[0].equals("term")) {
			enforceArgumentCount(args, 2);
			omet.listTerm(args[1]);
		}
		else if (args[0].equals("class")) {
			enforceArgumentCount(args, 3);
			omet.listClass(args[1], args[2]);
		}
		else if (args[0].equals("subject")) {
			enforceArgumentCount(args, 2);
			omet.listSubject(args[1]);
		}
		else if (args[0].equals("prof")) {
			enforceArgumentCount(args, 2);
			omet.listProf(args[1]);
		}
		else {
			System.err.println("Invalid method.");
			System.exit(1);
		}
    }
	
	public static void enforceArgumentCount(String[] args, int count) {
		if (args.length < count) {
			System.err.println("Not enough arguments.");
			System.exit(1);
		}
	}
	
    public omet_list() {
		try {
			_connection = OmetHelper.openConnection();
		}
		catch(Exception e) {
			System.err.println("Error connecting to database.");
			System.err.println(e.toString());
		}
    }
	
	public void listTerm(String term) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, term, subject, course_number, last_name, sum_q1, sum_q2, sum_q3, sum_q4, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE term = ?");
			statement.setString(1, term);
			
			ResultSet results = statement.executeQuery();
			if (!results.next()) {
				System.err.println("Invalid survey -- no such term: " + term);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void listClass(String subject, String courseNumber) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, term, subject, course_number, last_name, sum_q1, sum_q2, sum_q3, sum_q4, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE lower(subject) = lower(?) AND course_number = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, 
  ResultSet.CONCUR_READ_ONLY);
			statement.setString(1, subject);
			statement.setString(2, courseNumber);
			
			ResultSet results = statement.executeQuery();

			if (!results.next()) {
				System.err.println("Invalid survey -- no such class: " + subject + " " + courseNumber);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void listSubject(String subject) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, term, subject, course_number, last_name, sum_q1, sum_q2, sum_q3, sum_q4, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE lower(subject) = lower(?)");
			statement.setString(1, subject);
			
			ResultSet results = statement.executeQuery();
			if (!results.next()) {
				System.err.println("Invalid survey -- no such subject: " + subject);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public void listProf(String lastName) {
		try {
			PreparedStatement statement = _connection.prepareStatement("SELECT survey_id, term, subject, course_number, last_name, sum_q1, sum_q2, sum_q3, sum_q4, num_submitted FROM Surveys s JOIN Courses c ON s.cid = c.cid JOIN Instructors i ON i.fid = c.instructor_id WHERE lower(last_name) = lower(?)");
			statement.setString(1, lastName);
			
			ResultSet results = statement.executeQuery();
			if (!results.next()) {
				System.err.println("Invalid survey -- no such last name: " + lastName);
				System.exit(1);
			}
			
			outputResults(results);
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	private void outputResults(ResultSet results) {
		try {
			double averageQ1 = 0;
			double averageQ2 = 0;
			double averageQ3 = 0;
			double averageQ4 = 0;		
			do {
				if (results.getInt(10) > 0) {
					averageQ1 = (results.getDouble(6) / results.getDouble(10));
					averageQ2 = (results.getDouble(7) / results.getDouble(10));
					averageQ3 = (results.getDouble(8) / results.getDouble(10));
					averageQ4 = (results.getDouble(9) / results.getDouble(10));	
				}
				else {
					averageQ1 = 0;
					averageQ2 = 0;
					averageQ3 = 0;
					averageQ4 = 0;
				}
				System.out.println(results.getString(1) + "\t" + results.getString(2) + "\t" + results.getString(3) + "\t" + results.getString(4) + "\t" + results.getString(5) + "\t" + Double.toString(averageQ1) + "\t" + Double.toString(averageQ2) + "\t" + Double.toString(averageQ3) + "\t" + Double.toString(averageQ4));
			} while (results.next());
		}
		catch (Exception e) {
			System.err.println("Encountered an error.");
			System.err.println(e.getMessage());
			System.exit(1);		
		}
	}
}
