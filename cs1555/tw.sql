CREATE OR REPLACE TRIGGER multiple_captains_trigger AFTER INSERT ON Surveydata
referencing new as nrow
for each row
begin
	UPDATE Surveys
	SET num_submitted = num_submitted + 1,
		sum_q1 = sum_q1 + :nrow.q1,
		sum_q2 = sum_q2 + :nrow.q2,
		sum_q3 = sum_q3 + :nrow.q3,
		sum_q4 = sum_q4 + :nrow.q4
	WHERE 
		survey_id = :nrow.survey_id;
end;
/