<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template match="book1">
        <html>
            <head>
                <title>Calvino</title>
                <style>
                    p {
                        margin : 0px;
                    }
                    div.tocContainer {
                        cursor : crosshair;
                    }
                    div.tocContainer span {
                        font-size : 90%;
                    }
                    div.tocContainer a {
                        display : block;
                    }
                    div.tocContainer.collapsed a {
                        display : none;
                    }</style>
                <script>
                    function toggleTOC(chapterElement)
                    {
                    chapterElement.className = (chapterElement.className ==
                                "tocContainer collapsed" ? "tocContainer" : "tocContainer collapsed");
                                chapterElement.getElementsByTagName("span")[0].innerHTML =
                                    (chapterElement.className == "tocContainer" ? "[-]" : "[+]");
                    }
                </script>
            </head>
            <body style="background-color:#5C5C5C;margin:0px;">
                <div>
                    <xsl:attribute name="style"
                        >position:fixed;top:10px;right:10px;background-color:white;border:1px solid
                        black;padding:5px 15px 5px 15px;</xsl:attribute>
                    <h2>Table of Contents</h2>
                    <xsl:for-each-group group-by="@chapter" select="//city">
                        <div>
                            <xsl:attribute name="class">tocContainer collapsed</xsl:attribute>
                            <b>
                                <xsl:attribute name="onclick">toggleTOC(this.parentNode);</xsl:attribute>
                                <span>[+]</span> Chapter <xsl:value-of
                                    select="current-grouping-key()"/></b>
                            <xsl:for-each select="current-group()">
                                <a>
                                    <xsl:attribute name="style"
                                        >text-indent:10px;text-decoration:none;</xsl:attribute>
                                    <xsl:attribute name="href">#<xsl:value-of
                                            select="current()/@name"/></xsl:attribute>
                                    <text>› </text>
                                    <xsl:value-of select="current()/@title"/>
                                    <text> </text>
                                    <xsl:value-of select="current()/@index"/>
                                </a>
                            </xsl:for-each>
                        </div>
                    </xsl:for-each-group>
                    <!--<ul>
                        <xsl:apply-templates select="//city" mode="toc"/>
                    </ul>-->
                </div>
                <div>
                    <xsl:attribute name="style">padding-left:40px;margin-left:10px;border-left:1px
                        solid
                        black;background-color:white;width:400px;padding-right:30px;border-right:1px
                        solid black;</xsl:attribute>
                    <xsl:apply-templates select="content/pages/city"/>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="city">
        <a>
            <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
            </xsl:attribute>
        </a>
        <h2>
            <xsl:apply-templates select="@title"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="@index"/>
            <!--<a>
                <xsl:attribute name="style"
                    >font-size:12px;text-decoration:none;float:right;</xsl:attribute>
                <xsl:attribute name="href">#toc</xsl:attribute> Top </a>-->
        </h2>
        <xsl:apply-templates select="paragraph"/>
        <xsl:if test="position() &lt; last()">
            <hr/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="city" mode="toc">
        <li>
            <a>
                <xsl:attribute name="href">#<xsl:value-of select="current()/@name"/></xsl:attribute>
                <xsl:value-of select="current()/@title"/>
                <text> </text>
                <xsl:value-of select="current()/@index"/>
            </a>
        </li>
    </xsl:template>
    <xsl:template match="paragraph">
        <p>
            <xsl:if test="position() &gt; 1">
                <xsl:attribute name="style">text-indent:20px;</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="mention">
        <span>
            <xsl:attribute name="style">font-weight:bold;color:#4B0082;</xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="anachronism">
        <span>
            <xsl:attribute name="style">color:red;cursor:help;</xsl:attribute>
            <xsl:attribute name="title">(<xsl:value-of select="./@object"/>)</xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="character">
        <span>
            <xsl:choose>
                <xsl:when test="./@tag eq 'polo'">
                    <xsl:attribute name="style">color:green;</xsl:attribute>
                </xsl:when>
                <xsl:when test="./@tag eq 'kublai'">
                    <xsl:attribute name="style">color:orange;</xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:variable name="characterHandle">
                <xsl:value-of select="./@tag"/>
            </xsl:variable>
            <xsl:attribute name="title">
                <xsl:value-of select="//information/characters/character[current()/@tag eq ./@tag]"
                />
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
</xsl:stylesheet>
