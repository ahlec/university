.data
	input_prompt:	.asciiz "Enter your string?\n"
	output_header:	.asciiz "Here is your output?\n"
	input_string:	.space 64
.text
	#	load string
	li $v0, 4
	la $a0, input_prompt
	syscall
	li $v0, 8
	la $a0, input_string
	li $a1, 64
	syscall
	
	#	get length of string
	la $a0, input_string
	jal GET_STRING_LENGTH
	move $s0, $v0
	
	#	seed random
	li $v0, 30
	syscall
	move $a1, $a0
	li $a0, 1
	li $v0, 40
	syscall
	
	#	replace random characters with asterisks
	li $t0, 3
	la $t4, input_string
	replaceLoop:
		
		#	get random index
		li $a0, 1
		move $a1, $s0
		li $v0, 42
		syscall
		move $t2, $a0
		add $t2, $t4, $t2
		
		#	check to see if character is already an asterisk
		lb $t1, 0($t2)
		li $t3, 0x2A
		beq $t1, $t3, replaceLoop
		
		#	replace the character
		sb $t3, 0($t2)
		subi $t0, $t0, 1
		
		beq $t0, $zero, finishLoop
		j replaceLoop
	finishLoop:
	
	#	output string
	la $a0, output_header
	li $v0, 4
	syscall
	la $a0, input_string
	syscall
	
	li $v0, 10
	syscall
	
GET_STRING_LENGTH:
	li $t0, 0
	move $t1, $a0
	li $t3, 0xA
	getStringLengthLoop:
		lb $t2, 0($t1)
		beq $t2, $zero, finishStringLength
		beq $t2, $t3, finishStringLength
		addi $t0, $t0, 1
		addi $t1, $t1, 1
		j getStringLengthLoop
	finishStringLength:
	move $v0, $t0
	jr $ra