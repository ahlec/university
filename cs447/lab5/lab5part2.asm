.data
	numberDishesPrompt:	.asciiz "Enter the number of dishes?\n"
	minimumNumberHeader:	.asciiz "The minimum number of moves?\n"
.text
	#	output prompt
	la $a0, numberDishesPrompt
	li $v0, 4
	syscall
	
	#	load number of dishes
	li $v0, 5
	syscall
	
	#	get minimum number of moves
	move $a0, $v0
	jal GET_HANOI_MOVES
	move $s0, $v0
	
	#	output minimum number
	la $a0, minimumNumberHeader
	li $v0, 4
	syscall
	move $a0, $s0
	li $v0, 1
	syscall
	
	#	quit
	li $v0, 10
	syscall
	
GET_HANOI_MOVES:
	#	base case
	li $t0, 1
	bne $a0, $t0, noBaseCase
		li $v0, 1
		jr $ra
	noBaseCase:
	
	#	store to the stack
	subi $sp, $sp, 4
	sw $ra, 0($sp)
	
	subi $a0, $a0, 1
	jal GET_HANOI_MOVES
	li $t1, 2
	mul $v0, $v0, $t1
	addi $v0, $v0, 1
	
	#	retrieve from stack
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	
	jr $ra
