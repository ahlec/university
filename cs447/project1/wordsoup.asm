################
# Variable Definitions
################
# $s0 : game score
# $s1 : round score
# $s2 : the character read from the console
# $s3 : round word address
# $s4 : round word game progress address (ie _ _ * _ _)
# $s5 : number of skips used this round
# $s6 : round word true game progress address ( ie _ _ _ _ _ ). Identical to $s4 except without the stars.
#	Used to check if the word has been completely guessed.
# $s7 : number of rounds played (includes in this count the current round)

.data
	introText:	.asciiz "Welcome to WordSoup!\n"
	startRoundText:	.asciiz "I am thinking of a word. "
	theWordIs:	.asciiz "The word is "
	roundScore:	.asciiz ". Round score is "
	guessPrompt:	.asciiz "Guess a letter?\n"
	goodbyePrompt1:	.asciiz "Your final game tally is "
	goodbyePrompt2:	.asciiz ". Goodbye!"
	playAgainPrompt:	.asciiz "\nDo you want to play again? (y/n)\n"
	yes:	.asciiz "Yes! "
	no:	.asciiz "No! "
	lostRound:	.asciiz "You lost the round!"
	wonRound:	.asciiz "You won the round!"
	finalGuessOutput:	.asciiz " Your final guess was:\n"
	correctWordOutput:	.asciiz "\nCorrect word was:\n"
	yourRoundScore:	.asciiz "\nYour round score was "
	gameTallyOutput:	.asciiz ". The game tally is "
	incorrectInput:	.asciiz "Your choice of input is invalid."
	forfeited:	.asciiz "\nYou have forfeited the round. You will receive 0 points for this round."
	guessedWordIncorrect:	.asciiz "\nYour word guess was incorrect."
	guessedWordCorrect:	.asciiz "\nYour word guess was correct!"
	guessWordPrompt:	.asciiz "Guess the word of the round, double or nothing.\n"
	noMoreRevealsOutput:	.asciiz	"You have no more reveals for this round.\n"
	revealOutput:	.asciiz "The next character has been revealed.\n"
		
	roundWord:	.space 32
	roundWordTrue:	.space 32
	wordInputSpace:	.space 32
	
	wordCount:	.word 10
	word1:	.asciiz	"computer"
	word2:	.asciiz	"jacob"
	word3:	.asciiz	"saie"
	word4:	.asciiz	"japanese"
	word5:	.asciiz	"german"
	word6:	.asciiz	"english"
	word7:	.asciiz "programming"
	word8:	.asciiz "organization"
	word9:	.asciiz "assembly"
	word10:	.asciiz "kanji"
.align 2
	vocabulary:	.word word1, word2, word3, word4, word5, word6, word7, word8, word9, word10
.text
jal INITIALIZE_RANDOM
move $s0, $zero
move $s7, $zero
la $a0, introText
li $v0, 4
syscall
startRound:
	la $a0, startRoundText
	li $v0, 4
	syscall
	beq $s7, $zero, startFirstRound	# because $s7 is incremented at the end of startRound, if it is zero, this is
					# our first round, and the word should be "computer"
	jal GET_RANDOM_WORD_ADDRESS
	j processStartRound
	startFirstRound:
		la $v0, word1
	processStartRound:
	move $s3, $v0
	move $a0, $v0
	jal GET_WORD_LENGTH
	move $s1, $v0
	move $a0, $s3
	jal HIDE_WORD
	move $s4, $v0
	move $s6, $v1
	li $s5, 3	#	reset skips for this round
	addi $s7, $s7, 1
guess:
	la $a0, theWordIs
	li $v0, 4
	syscall
	move $a0, $s4
	syscall
	move $a0, $s3
	syscall
	la $a0, roundScore
	syscall
	move $a0, $s1
	li $v0, 1
	syscall
	li $a0, 0x2e
	li $v0, 11
	syscall
	li $a0, 0xA
	syscall
	la $a0, guessPrompt
	li $v0, 4
	syscall
	li $v0, 12	# read character
	syscall
	move $s2, $v0	#	$s2 : the character just read from the console
	li $a0, 0xA
	li $v0, 11
	syscall # output a new line after the input has been gathered
	addi $t1, $s2, -46
	beq $t1, $zero, forfeitRound
	addi $t1, $s2, -33
	beq $t1, $zero, guessWord
	addi $t1, $s2, -63
	beq $t1, $zero, revealNextLetter
	move $a0, $s2
	move $a1, $s3
	jal WORD_CONTAINS_CHARACTER
	bne $v0, $zero, word_does_contain_character
	word_doesnt_contain_character:
		subi $s1, $s1, 1
		li $v1, 0	#	set $v1 to "lost." Used by roundOver. If round is over, it will be properly set;
				#	if not, it will ultimately end up with the proper value it needs
		beq $s1, $zero, roundOver	#	if $s1 (round score) is zero ($zero), go to roundOver
		la $a0, no
		li $v0, 4
		syscall	# output the prompt "No!"
		j rejoin_guess_processing
	word_does_contain_character:
		move $a0, $s2
		move $a1, $s3
		move $a2, $s4
		move $a3, $s6
		jal REVEAL_CHARACTER
		move $a0, $s6
		jal IS_WORD_REVEALED
		li $v1, 1	#	set $v1 to "won." Used by roundOver
		bne $v0, $zero, roundOver	#	if IS_WORD_REVEALED returns !0, the word has been revealed.
		la $a0, yes
		li $v0, 4
		syscall # output the prompt "Yes!"
	rejoin_guess_processing:
	j guess	#	the round hasn't been ended elsewhere, so continue to guess
forfeitRound:
	move $s1, $zero
	la $a0, forfeited
	li $v0, 4
	syscall
	move $v1, $zero
	j forfeitRound
guessWord:
	la $a0, guessWordPrompt
	li $v0, 4
	syscall
	li $v0, 8
	la $a0, wordInputSpace
	li $a1, 32
	syscall
	la $t0, wordInputSpace	#	$t0 - address to the word input space
	move $t1, $s3		#	$t1 - address of the round word (changed during the searching)
	checkGuessedWordCorrect:
		lb $t2, 0($t0)
		lb $t3, 0($t1)
		addi $t4, $t2, -10 # when an input string has run out, it fills with 0xA. Let's standardize this to 0x0.
		bne $t4, $zero, inputStandardized
		li $t2, 0x0
		inputStandardized:
		bne $t2, $t3, guessedWordIsIncorrect
		beq $t2, $zero, guessedWordIsCorrect	# from this point on, $t2 == $t3 has been proven
		addi $t0, $t0, 1
		addi $t1, $t1, 1
		j checkGuessedWordCorrect
	guessedWordIsCorrect:
		sll $s1, $s1, 1
		la $a0, guessedWordCorrect
		li $v0, 4
		syscall
		li $v1, 1
		j roundOver
	guessedWordIsIncorrect:
		li $s1, 0
		li $v1, 0
		la $a0, guessedWordIncorrect
		li $v0, 4
		syscall
revealNextLetter:
	beq $s5, $zero, noMoreReveals
	subi $s5, $s5, 1
	subi $s1, $s1, 1
	li $v1, 0	#	set $v1 to "lost." Used by roundOver.
	beq $s1, $zero, roundOver	#	if $s1 (round score) is zero ($zero), go to roundOver
	move $a0, $s3
	move $a1, $s4
	move $a2, $s6
	jal REVEAL_NEXT_CHARACTER
	la $a0, revealOutput
	li $v0, 4
	syscall
	move $a0, $s6
	jal IS_WORD_REVEALED
	li $v1, 1	#	set $v1 to "won." Used by roundOver
	bne $v0, $zero, roundOver	#	if IS_WORD_REVEALED returns !0, the word has been revealed.
	j guess
noMoreReveals:
	la $a0, noMoreRevealsOutput
	li $v0, 4
	syscall
	j guess
roundOver:
	li $a0, 0xA
	li $v0, 11
	syscall # output a new line
	bne $v1, $0, round_over_won
		la $a0, lostRound
		j round_over_rejoin
	round_over_won:
		la $a0, wonRound
	round_over_rejoin:
	li $v0, 4
	syscall	# output "You (won/lost) the round!"
	la $a0, finalGuessOutput
	syscall	# output "Your final guess was:"
	move $a0, $s4
	syscall # output the round word buffer (ie _ _ * _ _)
	la $a0, correctWordOutput
	syscall # output "Correct word was:"
	move $a0, $s3
	syscall # output the round word (ie "computer")
	la $a0, yourRoundScore
	syscall # output "Your round score was "
	move $a0, $s1
	li $v0, 1
	syscall # output round score
	la $a0, gameTallyOutput
	li $v0, 4
	syscall # output "The game tally is"
	add $s0, $s0, $s1 # add the round score to the game tally (note: game tally will always be >= 0, never negative)
	move $a0, $s0
	li $v0, 1
	syscall # output the game tally
	li $a0, 0x2E
	li $v0, 11
	syscall # output a period
	getContinueInput:
	la $a0, playAgainPrompt
	li $v0, 4
	syscall
	li $v0, 12
	syscall
	addi $t0, $v0, -121 # is the character that has just been put in equal to "y" ?
	addi $t1, $v0, -110 # is the character that has just been put in equal to "n" ?
	li $a0, 0xA
	li $v0, 11
	syscall
	syscall # output a second new line
	beq $t0, $zero, startRound
	beq $t1, $zero, endGame
	la $a0, incorrectInput
	li $v0, 4
	syscall
	j getContinueInput
endGame:
	la $a0, goodbyePrompt1
	li $v0, 4
	syscall
	move $a0, $s0
	li $v0, 1
	syscall
	la $a0, goodbyePrompt2
	li $v0, 4
	syscall
	li $v0, 10	# exit program
	syscall

# Function Definitions Below
INITIALIZE_RANDOM:
	li $v0, 30
	syscall
	move $a1, $a0
	li $a0, 1
	li $v0, 40
	syscall
	jr $ra
GET_RANDOM_WORD_ADDRESS:
	li $a0, 1
	la $t8, wordCount
	lw $a1, 0($t8)
	li $v0, 42
	syscall
	la $t8, vocabulary
	sll $t9, $a0, 2
	add $v0, $t8, $t9
	lw $v0, 0($v0)
	jr $ra
GET_WORD_LENGTH:
	move $t0, $a0	#	$t0 : word address
	li $t1, 0	#	$t1 : word length (string length)
	find_length:
		lb $t2, 0($t0)
		beq $t2, $zero, length_found
		addi $t1, $t1, 1
		addi $t0, $t0, 1
		j find_length
	length_found:
	move $v0, $t1
	jr $ra
HIDE_WORD:
	subi $sp, $sp, 4
	sw $ra, 0($sp)
	jal GET_WORD_LENGTH
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	move $t1, $v0	#		$t1 : word length, as gotten from GET_WORD_LENGTH
	la $t0, roundWord	#	$t0 : the output buffer address, used to iterate and hide
	la $t5, roundWordTrue
	li $t2, 0	# 		$t2 : 0x0 (used when reseting roundWord)
	li $t3, 32	#		$t3 : number of bytes left to reset in roundWord
	li $a0, 1
	srl $a1, $t1, 1
	li $v0, 42
	syscall
	move $t6, $a0	#		$t6 : number of characters to hide (number of "*"s) left
	reset_round_word:
		sb $t2, 0($t0)
		sb $t2, 0($t5)
		addi $t0, $t0, 1
		addi $t5, $t5, 1
		subi $t3, $t3, 1
		beq $t3, $0, round_word_reset
		j reset_round_word
	round_word_reset:
	la $t0, roundWord
	la $t5, roundWordTrue
	li $t3, 0x5f	#	$t3 : the ASCII "_"
	li $t4, 0x20	#	$t4 : the ASCII " "
	move $t8, $t1	# (temporary hold) $t8 : preserved length of the string (does not change)
	hide_word_loop:
		beq $t1, $zero, finished_hiding
		sb $t3, 0($t0)
		sb $t4, 1($t0)
		sb $t3, 0($t5) # store a "_ " to roundWordTrue. roundWordTrue will never contain the * character
		sb $t4, 1($t5)
		subi $t1, $t1, 1
		addi $t0, $t0, 2
		addi $t5, $t5, 2
		j hide_word_loop
	finished_hiding:
	li $t7, 0x2A	#	$t7 : the ASCII "*"
	move $t1, $t8	# restore $t1 to the length of the string; $t8 IS NOW FREE
	la $t8, roundWord
	li $v0, 42
	move $a1, $t1
	create_astrisks:
		beq $t6, $zero, word_hiding_completed
		li $a0, 1
		syscall
		sll $a0, $a0, 1
		add $t9, $t8, $a0
		lb $t3, 0($t9)	# repurpose $t3 to hold the temporary 
		beq $t3, $t7, create_astrisks	# if the location we've randomly pulled is already filled by an astrisk,
						# ignore this position and retry
		subi $t6, $t6, 1	# recall that $t6 is the number of astrisks still to put into the word
		sb $t7, 0($t9)
		j create_astrisks
	word_hiding_completed:
	la $v0, roundWord
	la $v1, roundWordTrue
	jr $ra
WORD_CONTAINS_CHARACTER:	#	$a0 - character, $a1 - address of word
	move $t2, $a0	#	$t2 - character to be changed
	move $t0, $a1	#	$t0 - address of word (modified while searching)
	search_for_char_in_word:
		lb $t1, 0($t0)
		sub $t8, $t2, $t1
		beq $t8, $zero, char_found
		beq $t1, $zero, char_not_found
		addi $t0, $t0, 1
		j search_for_char_in_word
	char_found:
		li $v0, 1
		jr $ra
	char_not_found:
	li $v0, 0
	jr $ra
IS_WORD_REVEALED:	#	$a0 - the address of the round word true buffer (usually $s6)
	move $t0, $a0
	li $t2, 0x5F	#	$t2 - the ASCII code for an underscore ("_")
	search_for_an_underscore:
		lb $t1, 0($t0)
		beq $t1, $t2, underscore_found
		beq $t1, $zero, underscore_not_found
		addi $t0, $t0, 1
		j search_for_an_underscore
	underscore_found:
		li $v0, 0
		jr $ra
	underscore_not_found:
	li $v0, 1
	jr $ra
REVEAL_CHARACTER:	#	$a0 - character to revel, $a1 - round word, $a2 - round word buffer (ie _ _ * _ _),
			#	$a3 - round word true buffer (ie _ _ _ _ _)
	move $t0, $a1
	move $t2, $a2
	move $t3, $a3
	li $t5, 0x2A	#	$t5 - the ASCII character "*"
	traverse_word:
		lb $t1, 0($t0)	#	$t1 - character in the round word
		beq $t1, $zero, word_traversed
		bne $t1, $a0, character_replaced
		sb $t1, 0($t3) # store the revealed character into the true round word buffer
		lb $t4, 0($t2)
		beq $t4, $t5, character_replaced
		sb $t1, 0($t2)
		character_replaced:
			addi $t0, $t0, 1
			addi $t2, $t2, 2 # increment for a character ("_"/"*") and a space (" ")
			addi $t3, $t3, 2 # do the same here
			j traverse_word
	word_traversed:
	jr $ra
REVEAL_NEXT_CHARACTER:	#	$a0 - round word, $a1 - round word buffer (ie _ _ * _ _),
			#	$a2 - round word true buffer (ie _ _ _ _ _)
	move $a3, $a2
	move $a2, $a1
	move $a1, $a0
	subi $sp, $sp, 4
	sw $ra, 0($sp)
	move $t1, $a2	#	address to the round buffer (ie _ _ * _ _)
	move $t2, $a3 	#	address to the true round word buffer (ie _ _ _ _ _)
	move $t5, $a0
	find_character_to_reveal:
		lb $t3, 0($t1)
		lb $t4, 0($t2)
		addi $t3, $t3, -42
		beq $t3, $zero, not_a_find_character_match
		beq $t4, $zero, finish_revealing_character # if we've reached the end, because all that's left are "*" characters
		addi $t4, $t4, -95
		beq $t4, $zero, reveal_the_next_character
		not_a_find_character_match:
		addi $t2, $t2, 2
		addi $t1, $t1, 2
		addi $t5, $t5, 1
		j find_character_to_reveal
	reveal_the_next_character:
	lb $a0, 0($t5)	# load the proper next character to reveal
	jal REVEAL_CHARACTER
	finish_revealing_character:
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra