Question 1: What is the address of the byte with value 0x19?
0x10010003

Question 2: What is now the address of the byte with value 0x19?
0x10010000

Question 3: Is the simulator little endian or big endian? How can you tell?
The simulator is a bid-endian system. In the initial definition of a, the fourth byte input (0x19) is in the fourth position (relative to the start of the data segment). During the second definition of a, the byte representing 0x19 is in the first position even though it "comes last" in the MIPS definition of a. This is how a big-endian system works.
