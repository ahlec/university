.data
	prompt:	.asciiz	"Please enter your string:"
	output:	.asciiz "Here is the output: "
	buffer1:	.space	64
	buffer2:	.space	64
.text
	la $a0, prompt
	li $v0, 4
	syscall
	li $v0, 8
	la $a0, buffer1
	li $a1, 64
	syscall
	la $a1, buffer1		#	$a1 = buffer1
	la $a2, buffer2		#	$a2 = buffer2
	li $a3, 0		#	$a3 = length of word in buffer1 (before next space/termination)
	wordLoop:
		li $a3, 0
		li $t4, 0x20
		move $t0, $a1
		findCharacter:
			lb $t1, 0($t0)
			beq $t1, $t4, foundSpace
			beq $t1, $0, foundEnd
			addi $t0, $t0, 1
			addi $a3, $a3, 1
			j findCharacter
		foundEnd:
			subi $a3, $a3, 1
		foundSpace:
		jal reverseWord
		add $a1, $a1, $a3
		addi $a1, $a1, 1
		beq $a1, $a2, quit
		bne $t1, $0, wordLoop
	quit:
	la $a0, output
	li $v0, 4
	syscall
	la $a0, buffer2
	syscall
	li $v0, 10
	syscall

reverseWord:
	li $t4, 0x20	#	$t4 = space ASCII hex
	subi $sp, $sp, 8
	sw $t1, 0($sp)
	sw $a3, 4($sp)
	reverse_loop:
		subi $a3, $a3, 1
		slt $t1, $a3, $0
		bne $t1, $0, finishedReversing
		add $t3, $a1, $a3
		lb $t2, 0($t3)
		sb $t2, 0($a2)
		addi $a2, $a2, 1
		j reverse_loop
	finishedReversing:
	lw $t1, 0($sp)
	lw $a3, 4($sp)
	addi $sp, $sp, 8
	jr $ra
