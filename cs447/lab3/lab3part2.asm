.data
	prompt:	.asciiz "Please enter your string:\n"
	output:	.asciiz "Here is the output: "
	theString:	.space	64
.text
	la $a0, prompt
	li $v0, 4
	syscall
	la $a0, theString
	li $a1, 64
	li $v0, 8
	syscall
	la $a0, output
	li $v0, 4
	syscall
	la $a1, theString
	li $t2, 0xA
	loop:
		lb $t0, 0($a1)
		beq $t0, $zero, exit
		beq $t0, $t2, exit
		xor $t1, $t0, 0x20
		move $a0, $t1
		li $v0, 11
		syscall
		addi $a1, $a1, 1
		j loop
	exit:
		li $v0, 10
		syscall