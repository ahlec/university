
	#	$a0 - x Coordinate
	#	$a1 - y Coordinate
	#	$a2 - color
	#
	#	trashes: $t0	
drawLED:
	sll $a1, $a1, 5
	addi $a1, $a1, 0xFFFF0008
	move $t0, $a0
	srl $a0, $a0, 2
	add $a0, $a0, $a1
	
	
	
	srl $a0, $a0, 2
	sll $a0, $a0, 2	# set $a0 equal to the x coordinate on the row, where each coord is two bits long
	addi $a0, $a0, 0xFFFF0000	# add to $a0 the start memory position of the LED grid
	sll $a1, $a1, 5	# set $a1 equal to the proper line in memory, where each y coord is 32 bits long
	add $a0, $a0, $a1	# set $a0 equal to the memory address that we're looking for in the LED grid
	sb $a2, 0($a0)
	jr $ra