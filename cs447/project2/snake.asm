.data
	maxNumberFrogs:	.half 32
	initialSnakeLength:	.byte	8
	gameOver_text:	.asciiz "Game Over.\n"
	playTime_text_a:	.asciiz "The playing time was "
	playTime_text_b:	.asciiz " ms.\n"
	gameScore_text_a:	.asciiz "The game score was "
	gameScore_text_b:	.asciiz " frogs.\n"
.text
	
	# initial game score
	li $s4, 0
	
	# initialize the snake -- create the buffer
	la $t0, maxNumberFrogs
	lh $a0, 0($t0)
	la $t0, initialSnakeLength
	lb $t0, 0($t0)
	add $a0, $a0, $t0
	jal snakeInitialize
	
	# create the snake -- add the initial body
	la $t6, initialSnakeLength
	lb $t6, 0($t6)
	
	li $a0, 8
	sub $a0, $a0, $t6
	li $a1, 31
	_createSnake:
		subi $t6, $t6, 1
		jal snakeAdd
		li $a2, 2
		jal setLED
		addi $a0, $a0, 1
		bne $t6, $zero, _createSnake
	
	# place the frogs randomly
	li $v0, 30
	syscall
	move $a1, $a0
	li $a0, 1
	li $v0, 40
	syscall
	la $a0, maxNumberFrogs
	lh $t4, 0($a0)
	li $s5, 0
	createFrog:
		beq $t4, $zero, _createFrog_finished
		li $v0, 42
		li $a0, 1
		li $a1, 64
		syscall
		move $t5, $a0
		li $a0, 1
		syscall
		move $a1, $a0
		move $a0, $t5
		jal getLED
		subi $t4, $t4, 1
		bne $v0, $zero, createFrog
		li $a2, 3
		jal setLED
		addi $s5, $s5, 1
		j createFrog
	_createFrog_finished:
	
	# place walls
	li $t4, 20
	placeWall:
		beq $t4, $zero, _placeWall_finished
		li $v0, 42
		li $a0, 1
		li $a1, 64
		syscall
		move $t5, $a0
		li $a0, 1
		syscall
		move $a1, $a0
		move $a0, $t5
		jal getLED
		subi $t4, $t4, 1
		bne $v0, $zero, placeWall
		li $a2, 1
		jal setLED
		j placeWall
	_placeWall_finished:
	
	# initialize game time
	li $v0, 30
	syscall
	move $s6, $a0
	
	li $t7, 1 # deltaX for movement
	li $t8, 0 # deltaY for movement
	li $t9, 64
	loop:
		# is input?
		la $t0, 0xFFFF0000
		lw $t0, 0($t0)
		beq $t0, $zero, _loop_moveSnake
		la $t0, 0xFFFF0004
		lw $t0, 0($t0)
		subi $t1, $t0, 0xE0
		beq $t1, $zero, _loop_inputUp
		subi $t1, $t0, 0xE1
		beq $t1, $zero, _loop_inputDown
		subi $t1, $t0, 0xE2
		beq $t1, $zero, _loop_inputLeft
		subi $t1, $t0, 0xE3
		beq $t1, $zero, _loop_inputRight
		subi $t1, $t0, 0x42
		beq $t1, $zero, _gameOver
		
		_loop_inputUp:
			beq $t7, $zero, _loop_moveSnake	# if we're already moving vertically, don't process
			li $t7, 0
			li $t8, -1
			j _loop_moveSnake
		_loop_inputDown:
			beq $t7, $zero, _loop_moveSnake	# if we're already moving vertically, don't process
			li $t7, 0
			li $t8, 1
			j _loop_moveSnake
		_loop_inputRight:
			beq $t8, $zero, _loop_moveSnake	# if we're already moving horizontally, don't process
			li $t7, 1
			li $t8, 0
			j _loop_moveSnake
		_loop_inputLeft:
			beq $t8, $zero, _loop_moveSnake	# if we're already moving horizontally, don't process
			li $t7, -1
			li $t8, 0
			j _loop_moveSnake
		
		# move snake
		_loop_moveSnake:
			jal snakePeek
			add $a0, $v0, $t7	# set $a0 equal to the x-position of the head + deltaX
			add $a1, $v1, $t8	# set $a1 equal to the y-position of the head + deltaY
			_loop_snakeOverflowX:
				bne $a0, $t9, _loop_snakeUnderflowX
				li $a0, 0
			_loop_snakeUnderflowX:
				slt $t0, $a0, $zero
				beq $t0, $zero, _loop_snakeOverflowY
				li $a0, 63
			_loop_snakeOverflowY:
				bne $a1, $t9, _loop_snakeUnderflowY
				li $a1, 0
			_loop_snakeUnderflowY:
				slt $t0, $a1, $zero
				beq $t0, $zero, _loop_putHead
				li $a1, 63
			_loop_putHead:
				# is there something already at this new location?
				jal getLED				
			
				# is nothing at all here?
				_loop_putHead_add:
					bne $v0, $zero, _loop_putHead_checkFrog
					jal snakeAdd
					li $a2, 2
					jal setLED
					jal snakeRemove
					move $a0, $v0
					move $a1, $v1
					li $a2, 0
					jal setLED
					j _loop_snakeMoved

				# is there a frog here?
				_loop_putHead_checkFrog:
					subi $t0, $v0, 3
					bne $t0, $zero, _loop_putHead_checkWall
					addi $s4, $s4, 1	#	$s4 is the game score
					subi $s5, $s5, 1	#	$s5 is the number of frogs remaining
					jal snakeAdd
					li $a2, 2
					jal setLED
					beq $s5, $zero, _gameOver	# if $s5 equals 0, that means there are no more frogs; we're done
					j _loop_snakeMoved					
				
				# is there a wall here?
				_loop_putHead_checkWall:
					subi $t0, $v0, 1
					bne $t0, $zero, _loop_putHead_checkSnake
					jal snakePeek
					move $t5, $v0
					move $t6, $v1
					add $a0, $t5, $t8
					add $a1, $t6, $t7
					jal getLED
					subi $t0, $v0, 1
					bne $t0, $zero, _loop_putHead_checkWall_turnA
					
					sub $a0, $t5, $t8
					sub $a1, $t6, $t7
					jal getLED
					subi $t0, $v0, 1
					bne $t0, $zero, _loop_putHead_checkWall_turnB
					
					j _gameOver
					_loop_putHead_checkWall_turnA:
						move $t0, $t8
						move $t8, $t7
						move $t7, $t0
						j loop
					_loop_putHead_checkWall_turnB:
						move $t0, $t8
						move $t8, $t7
						move $t7, $t0
						neg $t8, $t8
						neg $t7, $t7
						j loop
				
				# the snake is here!
				_loop_putHead_checkSnake:
					j _gameOver
		_loop_snakeMoved:
		
		_loop_wait:
			li $v0, 32
			li $a0, 200
			syscall
		
		j loop
		
	_gameOver:
		li $v0, 4
		la $a0, gameOver_text
		syscall
		la $a0, playTime_text_a
		syscall
		li $v0, 30
		syscall
		sub $a0, $a0, $s6
		li $v0, 1
		syscall
		li $v0, 4
		la $a0, playTime_text_b
		syscall
		la $a0, gameScore_text_a
		li $v0, 4
		syscall
		move $a0, $s4
		li $v0, 1
		syscall
		li $v0, 4
		la $a0, gameScore_text_b
		syscall
		li $v0, 10
		syscall
	
	#  $s0 - start of memory buffer
	#  $s1 - end of memory buffer
	#  $s2 - head
	#  $s3 - tail
	#  $s4 - game score
	#  $s5 - number of frogs remaining
	#  $s6 - game time started

#-----------------------------------------------------------
# Snake data structure
#    -----
#  (Note that this has nothing to do with LED manipulation)
#-----------------------------------------------------------

# void snakeInitialize(int maxLength)
#    initializes the data queue that stores the snake, using the max length the snake can obtain
#    arguments: $a0 is the maximum number of digits that the snake can obtain
#    returns: nothing
#    warnings: reserves the registers $s0-$s3 for use solely within this data structure (see $s0-$s7 mapping, earlier documented)
snakeInitialize:
	sll $t0, $a0, 4
	move $s1, $sp
	sub $sp, $sp, $t0
	move $s0, $sp
	move $s2, $s0
	move $s3, $s0
	jr $ra
# void snakeAdd(int x, int y)
#    adds the x, y coordinates supplied as the latest head location for the snake
#    arguments: $a0 is the x coordinate, $a1 is the y coordinate
#    returns: nothing
#    warnings: assumes no one has messed with the reserved registers $s0 - $s3
snakeAdd:
	bne $s3, $s1, _snakeAdd_add
	move $s3, $s0
	_snakeAdd_add:
		sh $a0, 0($s3)
		sh $a1, 2($s3)
	addi $s3, $s3, 4
	jr $ra
# int, int snakeRemove()
#  removes the last LED of the snake's tail from the memory queue, and returns the location (x,y) that was this LED
#  returns: $v0 holds the x coordinate, $v1 holds the y coordinate
#  trashes: $t0
#  warnings: assumes no one has messed with the reserved registers $s0 - $s3
snakeRemove:
	bne $s2, $s1, _snakeRemove_remove
	move $s2, $s0
	_snakeRemove_remove:
		lh $v0, 0($s2)
		sh $zero, 0($s2)
		lh $v1, 2($s2)
		sh $zero, 2($s2)
	addi $s2, $s2, 4
	jr $ra
	
# int, int snakePeek()
#  returns the coordinates of the snake's head position
#  returns: $v0 holds the x coordinate, $v1 holds the y coordinate
#  warnings: assumes no one has messed with the reserved registers $s0 - $s3
snakePeek:
	move $t0, $s3
	bne $t0, $s0, _snakePeek_peek
	move $t0, $s1
	_snakePeek_peek:
		subi $t0, $t0, 4
		lh $v0, 0($t0)
		lh $v1, 2($t0)
	jr $ra
	
#-----------------------------------------------------------#
# LED manipulation                                                                             #
#-----------------------------------------------------------#
# void setLED(int x, int y, int color)
# 03/11/2012: this version is for the 64x64 LED
#   sets the LED at (x,y) to color
#   color: 0=off, 1=red, 2=orange, 3=green
#
# warning:   x, y and color are assumed to be legal values (0-63,0-63,0-3)
# arguments: $a0 is x, $a1 is y, $a2 is color 
# trashes:   $t0-$t3
# returns:   none
setLED:
	# byte offset into display = y * 16 bytes + (x / 4)
	sll	$t0,$a1,4      # y * 16 bytes
	srl	$t1,$a0,2      # x / 4
	add	$t0,$t0,$t1    # byte offset into display
	li	$t2,0xffff0008	# base address of LED display
	add	$t0,$t2,$t0    # address of byte with the LED
	# now, compute led position in the byte and the mask for it
	andi	$t1,$a0,0x3    # remainder is led position in byte
	neg	$t1,$t1        # negate position for subtraction
	addi	$t1,$t1,3      # bit positions in reverse order
	sll	$t1,$t1,1      # led is 2 bits
	# compute two masks: one to clear field, one to set new color
	li	$t2,3		
	sllv	$t2,$t2,$t1
	not	$t2,$t2        # bit mask for clearing current color
	sllv	$t1,$a2,$t1    # bit mask for setting color
	# get current LED value, set the new field, store it back to LED
	lbu	$t3,0($t0)     # read current LED value	
	and	$t3,$t3,$t2    # clear the field for the color
	or	$t3,$t3,$t1    # set color field
	sb	$t3,0($t0)     # update display
	jr	$ra
# int getLED(int x, int y)
# 03/11/2012: this version is for the 64x64 LED
#   returns the value of the LED at position (x,y)
#
#  warning:   x and y are assumed to be legal values (0-63,0-63)
#  arguments: $a0 holds x, $a1 holds y
#  trashes:   $t0-$t2
#  returns:   $v0 holds the value of the LED (0, 1, 2, 3)
getLED:
	# byte offset into display = y * 16 bytes + (x / 4)
	sll  $t0,$a1,4      # y * 16 bytes
	srl  $t1,$a0,2      # x / 4
	add  $t0,$t0,$t1    # byte offset into display
	la   $t2,0xffff0008
	add  $t0,$t2,$t0    # address of byte with the LED
	# now, compute bit position in the byte and the mask for it
	andi $t1,$a0,0x3    # remainder is bit position in byte
	neg  $t1,$t1        # negate position for subtraction
	addi $t1,$t1,3      # bit positions in reverse order
    	sll  $t1,$t1,1      # led is 2 bits
	# load LED value, get the desired bit in the loaded byte
	lbu  $t2,0($t0)
	srlv $t2,$t2,$t1    # shift LED value to lsb position
	andi $v0,$t2,0x3    # mask off any remaining upper bits
	jr   $ra
