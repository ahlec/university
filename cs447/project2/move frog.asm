# move a random frog!
		_loop_moveFrog:
			# which row?
			li $v0, 42
			li $a0, 1
			li $a1, 64
			syscall
			move $t5, $a0
			
			# go from left->right or right->left?
			li $v0, 42
			li $a0, 1
			li $a1, 1
			syscall
			beq $a0, $zero, _loop_moveFrog_leftRight
				li $t6, 63
				li $t4, -1
				j _loop_moveFrog_find
			_loop_moveFrog_leftRight:
				li $t6, 0
				li $t4, 1
			_loop_moveFrog_find:
				move $a0, $t5
				move $a1, $t6
				jal getLED
				subi $t0, $v0, 3
				beq $t0, $zero, _loop_moveFrog_frogFound
				add $t6, $t6, $t4
				li $t0, 63
				beq $t6, $zero, _loop_moveFrog_finished
				beq $t6, $t0, _loop_moveFrog_finished
				j _loop_moveFrog_find
		_loop_moveFrog_frogFound:
			li $v0, 42
			li $a0, 1
			li $a1, 2
			syscall
			subi $t4, $a0, 1
			li $v0, 42
			li $a0, 1
			li $a1, 2
			subi $t9, $a0, 1
			add $a0, $t5, $t4
			add $a1, $t6, $t9
			jal getLED
			bne $v0, $zero, _loop_moveFrog_finished
			move $a0, $t5
			move $a1, $t6
			li $a2, 0
			jal setLED
			add $a0, $t5, $t4
			add $a1, $t6, $t9
			li $a2, 3
			jal setLED
		_loop_moveFrog_finished:
			li $t9, 64