# frog
#	$a0 - number of max frogs
frogsInitialize:
	sll $t0, $a0, 8
	sub $sp, $sp, $t0
	move $s7, $sp
	li $t2, -1
	_frogsInitialize_loop:
		subi $t0, $t0, 8
		sh $t2, 0($t0)
		sh $t2, 4($t0)
		bne $t0, $s7, _frogsInitialize_loop
	jr $ra
frogAdd:
	move $t0, $s7
	li $t2, -1
	frogAdd_location_loop:
		lh $t1, 0($t0)
		beq $t1, $t2, frogAdd_location_found
		addi $t0, $t0, 8
		j frogAdd_location_loop
	frogAdd_location_found:
		sh $a0, 0($t0)
		sh $a1, 4($t1)
	jr $ra
frogRemove:
	move $t0, $s7
	frogRemove_location_loop:
		lh $t1, 0($t0)
		lh $t2, 4($t0)
		addi $t0, $t0, 8
		bne $t1, $a0, frogRemove_location_loop
		bne $t2, $a1, frogRemove_location_loop
	subi $t0, $t0, 8
	li $t2, -1
	sh $t2, 0($t0)
	sh $t2, 4($t0)
	jr $ra