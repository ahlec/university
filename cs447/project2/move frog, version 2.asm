		_loop_findRandomFrog:
			li $v0, 42
			li $a0, 1
			li $a1, 64
			syscall
			move $t5, $a0
			li $a0, 1
			syscall
			move $t6, $a0
			
			li $t9, 3
			move $a0, $t5
			move $a1, $t6
			li $a2, 1
			jal setLED
			j _loop_finishedFrogging
			#jal getLED
			#beq $v0, $t9, _loop_frogFound
			
			_loop_findFrog_goUp:
				beq $t6, $zero, _loop_findFrog_goLeft
				move $a0, $t5
				subi $a1, $t6, 1
				jal getLED
				bne $v0, $t9, _loop_findFrog_goLeft
				move $t3, $t5
				move $t4, $t6
				subi $t6, $t6, 1
				j _loop_frogFound
			_loop_findFrog_goLeft:
				beq $t5, $zero, _loop_findFrog_goDown
				subi $a0, $t5, 1
				move $a1, $t6
				jal getLED
				bne $v0, $t9, _loop_findFrog_goDown
				move $t3, $t5
				move $t4, $t6
				subi $t5, $t5, 1
				j _loop_frogFound
			_loop_findFrog_goDown:
				li $t9, 63
				beq $t6, $t9, _loop_findFrog_goRight
				move $a0, $t5
				addi $a1, $t6, 1
				jal getLED
				li $t9, 3
				bne $v0, $t9, _loop_findFrog_goRight
				move $t3, $t5
				move $t4, $t6
				addi $t6, $t6, 1
				j _loop_frogFound
			_loop_findFrog_goRight:
				li $t9, 63
				beq $t5, $t9, _loop_finishedFrogging
				addi $a0, $t5, 1
				move $a1, $t6
				jal getLED
				li $t9, 3
				bne $v0, $t9, _loop_finishedFrogging
				move $t3, $t5
				move $t4, $t6
				addi $t5, $t5, 1
				j _loop_frogFound
			
		_loop_frogFound:	# old location at $t3, $t4; new location at $t5, $t6
			move $a0, $t3
			move $a1, $t4
			li $a2, 0
			jal setLED
			move $a0, $t5
			move $a1, $t6
			li $a2, 3
			jal setLED
			j _loop_finishedFrogging
			
		_loop_finishedFrogging:
			li $t9, 63