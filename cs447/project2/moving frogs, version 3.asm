		_loop_moveFrog:
			jal frogGetRandom
			li $t0, -1
			beq $v0, $t0, _loop_wait
			move $t5, $v0
			move $t6, $v1

			_loop_findFrog_goUp:
				beq $t6, $zero, _loop_findFrog_goLeft
				move $a0, $t5
				subi $a1, $t6, 1
				jal getLED
				bne $v0, $t9, _loop_findFrog_goLeft
				move $t3, $t5
				move $t4, $t6
				subi $t6, $t6, 1
				j _loop_frogFound
			_loop_findFrog_goLeft:
				beq $t5, $zero, _loop_findFrog_goDown
				subi $a0, $t5, 1
				move $a1, $t6
				jal getLED
				bne $v0, $t9, _loop_findFrog_goDown
				move $t3, $t5
				move $t4, $t6
				subi $t5, $t5, 1
				j _loop_frogFound
			_loop_findFrog_goDown:
				li $t9, 63
				beq $t6, $t9, _loop_findFrog_goRight
				move $a0, $t5
				addi $a1, $t6, 1
				jal getLED
				li $t9, 3
				bne $v0, $t9, _loop_findFrog_goRight
				move $t3, $t5
				move $t4, $t6
				addi $t6, $t6, 1
				j _loop_frogFound
			_loop_findFrog_goRight:
				li $t9, 63
				beq $t5, $t9, _loop_finishedFrogging
				addi $a0, $t5, 1
				move $a1, $t6
				jal getLED
				li $t9, 3
				bne $v0, $t9, _loop_finishedFrogging
				move $t3, $t5
				move $t4, $t6
				addi $t5, $t5, 1
				j _loop_frogFound
			
		_loop_frogFound:	# old location at $t3, $t4; new location at $t5, $t6
			move $a0, $t3
			move $a1, $t4
			jal frogRemove
			move $a0, $t5
			move $a1, $t6
			jal frogAdd
			move $a0, $t3
			move $a1, $t4
			li $a2, 0
			jal setLED
			move $a0, $t5
			move $a1, $t6
			li $a2, 3
			jal setLED
			j _loop_finishedFrogging
			
		_loop_finishedFrogging:
			li $t9, 63
			
# frog
#	$a0 - number of max frogs
frogsInitialize:
	sll $t0, $a0, 4
	sub $sp, $sp, $t0
	move $s7, $sp
	li $t2, -1
	move $t0, $s0
	_frogsInitialize_loop:
		subi $t0, $t0, 4
		sh $t2, 0($t0)
		sh $t2, 2($t0)
		bne $t0, $s7, _frogsInitialize_loop
	jr $ra
frogAdd:
	move $t0, $s7
	li $t2, -1
	frogAdd_location_loop:
		lh $t1, 0($t0)
		beq $t1, $t2, frogAdd_location_found
		addi $t0, $t0, 4
		j frogAdd_location_loop
	frogAdd_location_found:
		sh $a0, 0($t0)
		sh $a1, 2($t0)
	jr $ra
frogRemove:
	move $t0, $s7
	frogRemove_location_loop:
		lh $t1, 0($t0)
		lh $t2, 2($t0)
		addi $t0, $t0, 4
		bne $t1, $a0, frogRemove_location_loop
		bne $t2, $a1, frogRemove_location_loop
	subi $t0, $t0, 4
	li $t2, -1
	sh $t2, 0($t0)
	sh $t2, 2($t0)
	jr $ra
frogGetRandom:
	li $v0, 42
	li $a0, 1
	li $a1, 32
	syscall
	move $t0, $s7
	sll $t1, $a0, 4
	add $t0, $t0, $t1
	lh $t1, 0($t0)
	li $t2, -1
	beq $t1, $t2, _frogGetRandom_noReturn
	move $v0, $t1
	lh $v1, 2($t0)
	j _frogGetRandom_return
	_frogGetRandom_noReturn:
		li $v0, -1
		li $v1, -1
	_frogGetRandom_return:
	jr $ra