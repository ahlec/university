.data
	numberA:	.asciiz "1000000000000011"
	numberB:	.asciiz "0000000000001011"
	numberC:	.space 64
	
	sum:	.asciiz "Sum is: "
	overflow:	.asciiz "\nOverflow is: "
.text
la $a0, numberA
la $a1, numberB
la $a2, numberC
jal AddNumbers
move $t0, $v0

la $a0, sum
li $v0, 4
syscall

la $a0, numberC
li $v0, 4
syscall

la $a0, overflow
li $v0, 4
syscall

move $a0, $t0
li $v0, 1
syscall

li $v0, 10
syscall

# BitAdder
#	adds two bits with the carry in and outputs the 1-bit sum and carry out for the next step
# INPUT:
# 	BitAdder expects arguments in $a0, $a1, $a2
# 	$a0 = specific bit (of values either 0 or 1 in decimal) from A, do not pass character '0' or '1'
# 	$a1 = specific bit (of values either 0 or 1 in decimal) from B, do not pass character '0' or '1'
# 	$a2 = carry in (of values either 0 or 1 in decimal) from previous step
# OUTPUT: 
# 	$v0 = 1-bit sum in $v0 
#	$v1 = carry out for the next stage
BitAdder:
	li $t0, 0 # number of "1"s that we have in all of the incoming arguments
	beq $a0, $zero, BitAdder_a1
	addi $t0, $t0, 1
	BitAdder_a1:
		beq $a1, $zero, BitAdder_a2
		addi $t0, $t0, 1
	BitAdder_a2:
		beq $a2, $zero, BitAdder_body
		addi $t0, $t0, 1
	BitAdder_body:
	
	# carry if the number of "1"s brought in is 2+
	slti $t1, $t0, 2
	xori $t1, $t1, 1
	li $v1, 0
	beq $t1, $zero, BitAdder_sum
	li $v1, 1
	
	# set the value of the addition to 1 if the number of "1"s brought in is an odd number
	BitAdder_sum:
	andi $t2, $t0, 1
	li $v0, 0
	beq $t2, $zero, BitAdder_return
	li $v0, 1
	
	# return
	BitAdder_return:
        jr $ra
        
AddNumbers:
	# prologue
	subi $sp, $sp, 20
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)

	li $s0, 16 # iterations left
	addi $s1, $a0, 15
	addi $s2, $a1, 15
	addi $s3, $a2, 15
	
	# body
	addNumbers_Loop:
		beq $s0, $zero, addNumbers_Finished
		lb $t4, 0($s1)
		lb $t5, 0($s2)
		subi $t4, $t4, 0x30
		subi $t5, $t5, 0x30
		move $a0, $t4
		move $a1, $t5
		move $a2, $v1
		jal BitAdder
		addi $v0, $v0, 0x30
		sb $v0, 0($s3)
		subi $s0, $s0, 1
		subi $s1, $s1, 1
		subi $s2, $s2, 1
		subi $s3, $s3, 1
		j addNumbers_Loop

	# loop ends, set $v0 with the overflow
	addNumbers_Finished:
		move $v0, $v1

	# epilogue
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	addi $sp, $sp, 20
	
	# return
	jr $ra