.data
	victory_message:	.asciiz "You have won!"
.text
	# set start location
	li $s0, 64
	li $s1, 3

	# initialize random
	li $v0, 30
	syscall
	move $a1, $a0
	li $a0, 1
	li $v0, 40
	syscall
	
	# place five random green dots
	li $t5, 5
	placeDots:
		beq $t5, $zero, dotsPlaced
		
		# generate random location
		li $v0, 42
		li $a0, 1
		li $a1, 128
		syscall
		move $t1, $a0
		li $v0, 42
		li $a0, 1
		li $a1, 8
		syscall
		move $t2, $a0
		
		# if the random location equals the start location of the red dot, regenerate
		sub $t3, $t1, $s0
		sub $t4, $t2, $s1
		add $t3, $t3, $t4
		beq $t3, $zero, placeDots
		
		
		# is there already a dot here? (if there is, regenerate a new location)
			# first, move our position somewhere safe
			move $t8, $t1
			move $t9, $t2
			
		move $a0, $t8
		move $a1, $t9
		jal _getLED
		bne $v0, $zero, placeDots
		
		# dot is a valid location, so let's draw it
		move $a0, $t8
		move $a1, $t9
		li $a2, 3
		jal _setLED
		
		subi $t5, $t5, 1
		j placeDots
	dotsPlaced:
	
	# place the red dot at the starting location
	move $a0, $s0
	move $a1, $s1
	li $a2, 1
	jal _setLED
	
	# initialize $s2 ("am I currently obscuring a green dot that should come back?")
	li $s2, 0	# we know we can't already be, because the random loop ensures so
	
	# initialize $s3 ("how many dots are left on the board, including obscured?")
	li $s3, 5	# again, our loop ensures it
	
	gameLoop:
		
		#	is there input?
		la $a0, 0xFFFF0000
		lw $t0, 0($a0)
		andi $t0, $t0, 0x1
		subi $t0, $t0, 1
		bne $t0, $zero, inputProcessed
		
			#	there is input, so what is it?
			la $a0, 0xFFFF0004
			lw $t2, 0($a0)
			
			li $t4, 224	# ^
			li $t5, 225	# v
			li $t6, 226	# <
			li $t7, 227	# >
			li $t8, 0x42	# center

			beq $t2, $t8, centerKey

			#	we're moving, so let's set $t0, $t1 equal to our current x, y (for temp working)
			#	and then branch out from there
			move $t0, $s0
			move $t1, $s1
			beq $t2, $t4, upKey
			beq $t2, $t5, downKey
			beq $t2, $t6, leftKey
			beq $t2, $t7, rightKey
									
			upKey:
				beq $t1, $zero, upKey_underflow
				subi $t1, $t1, 1
				j processMoving
				upKey_underflow:
					li $t1, 7
					j processMoving
			downKey:
				slti $t2, $t1, 7
				beq $t2, $zero, downKey_overflow
				addi $t1, $t1, 1
				j processMoving
				downKey_overflow:
					li $t1, 0
					j processMoving
			leftKey:
				beq $t0, $zero, leftKey_underflow
				subi $t0, $t0, 1
				j processMoving
				leftKey_underflow:
					li $t0, 127
					j processMoving
			rightKey:
				slti $t2, $t0, 127
				beq $t2, $zero, rightKey_overflow
				addi $t0, $t0, 1
				j processMoving
				rightKey_overflow:
					li $t0, 0
					j processMoving
			processMoving:
				# let's move our new position to safer registers
				move $t8, $t0
				move $t9, $t1
				
				# what color should we restore to our current position? put that in $t2
				beq $s2, $zero, processMoving_nothingUnder
				li $t2, 3
				j processMoving_colorFound
				processMoving_nothingUnder:
					li $t2, 0
					j processMoving_colorFound
				processMoving_colorFound:
				
				# set the old position to this newfound color
				move $a0, $s0
				move $a1, $s1
				move $a2, $t2
				jal _setLED

				# will we now be obscuring an LED?
				move $a0, $t8
				move $a1, $t9
				jal _getLED
				beq $v0, $zero, processMoving_noObscuration
				li $s2, 1
				j processMoving_obscurationComplete
				processMoving_noObscuration:
					li $s2, 0
					j processMoving_obscurationComplete
				processMoving_obscurationComplete:
				
				# let's move ourselves into this new location!
				move $s0, $t8
				move $s1, $t9
				move $a0, $t8
				move $a1, $t9
				li $a2, 1
				jal _setLED
																								
				j inputProcessed
			centerKey:
				beq $s2, $zero, inputProcessed
				
				li $s2, 0
				subi $s3, $s3, 1
				beq $s3, $zero, gameWon
				
				j inputProcessed
		#
		inputProcessed:
		
		j gameLoop
	
	gameWon:
		la $a0, victory_message
		li $v0, 4
		syscall
	
	li $v0, 10
	syscall	

	
#	the following functions have been adapted for used with the current board	
	

	# void _setLED(int x, int y, int color)
	# 03/11/2012: this version is for the 64x64 LED
	#   sets the LED at (x,y) to color
	#   color: 0=off, 1=red, 2=orange, 3=green
	#
	# warning:   x, y and color are assumed to be legal values (0-63,0-63,0-3)
	# arguments: $a0 is x, $a1 is y, $a2 is color 
	# trashes:   $t0-$t3
	# returns:   none
	#
_setLED:
	# byte offset into display = y * 32 bytes + (x / 4)
	sll	$t0,$a1,5      # y * 32 bytes
	srl	$t1,$a0,2      # x / 4
	add	$t0,$t0,$t1    # byte offset into display
	li	$t2,0xffff0008	# base address of LED display
	add	$t0,$t2,$t0    # address of byte with the LED
	# now, compute led position in the byte and the mask for it
	andi	$t1,$a0,0x3    # remainder is led position in byte
	neg	$t1,$t1        # negate position for subtraction
	addi	$t1,$t1,3      # bit positions in reverse order
	sll	$t1,$t1,1      # led is 2 bits
	# compute two masks: one to clear field, one to set new color
	li	$t2,3		
	sllv	$t2,$t2,$t1
	not	$t2,$t2        # bit mask for clearing current color
	sllv	$t1,$a2,$t1    # bit mask for setting color
	# get current LED value, set the new field, store it back to LED
	lbu	$t3,0($t0)     # read current LED value	
	and	$t3,$t3,$t2    # clear the field for the color
	or	$t3,$t3,$t1    # set color field
	sb	$t3,0($t0)     # update display
	jr	$ra

	# int _getLED(int x, int y)
	# 03/11/2012: this version is for the 64x64 LED
	#   returns the value of the LED at position (x,y)
	#
	#  warning:   x and y are assumed to be legal values (0-63,0-63)
	#  arguments: $a0 holds x, $a1 holds y
	#  trashes:   $t0-$t2
	#  returns:   $v0 holds the value of the LED (0, 1, 2, 3)
	#
_getLED:
	# byte offset into display = y * 32 bytes + (x / 4)
	sll  $t0,$a1,5      # y * 32 bytes
	srl  $t1,$a0,2      # x / 4
	add  $t0,$t0,$t1    # byte offset into display
	la   $t2,0xffff0008
	add  $t0,$t2,$t0    # address of byte with the LED
	# now, compute bit position in the byte and the mask for it
	andi $t1,$a0,0x3    # remainder is bit position in byte
	neg  $t1,$t1        # negate position for subtraction
	addi $t1,$t1,3      # bit positions in reverse order
    	sll  $t1,$t1,1      # led is 2 bits
	# load LED value, get the desired bit in the loaded byte
	lbu  $t2,0($t0)
	srlv $t2,$t2,$t1    # shift LED value to lsb position
	andi $v0,$t2,0x3    # mask off any remaining upper bits
	jr   $ra