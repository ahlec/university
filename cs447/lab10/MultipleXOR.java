public class MultipleXOR
{
  public static void main(String[] args)
  {
     for (int a = 0; a < 2; a++)
       for (int b = 0; b < 2; b++)
         for (int c = 0; c < 2; c++)
           for (int d = 0; d < 2; d++)
           {
              System.out.print(a);
              System.out.print(b);
              System.out.print(c);
              System.out.print(d);
              int p = a ^ b ^ c ^ d;
              int s = p + a + b + c + d;
              System.out.print(" - {P: ");
              System.out.print(p);
              System.out.println(", S: " + s + "}");
           }
  }
}
