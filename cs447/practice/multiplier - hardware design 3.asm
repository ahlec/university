.data
	multiplicand:	.byte 165
	multiplier:	.byte 183
	product:	.space 16
.text
	la $s0, multiplicand # multiplicand
	la $s1, multiplier # multiplier
	la $s2, product
	lb $t0, 0($s0)
	andi $t0, $t0, 255
	lb $t1, 0($s1)
	andi $t1, $t1, 255
	sb $t1, 8($s2)
	
	li $v0, 35
	move $a0, $t0
	syscall
	
	li $v0, 11
	li $a0, 0xA
	syscall
	
	li $v0, 35
	move $a0, $t1
	syscall
	
	li $v0, 11
	li $a0, 0xA
	syscall
	
	lh $a0, 0($s2)
	li $v0, 35
	andi $a0, $a0, 255
	syscall