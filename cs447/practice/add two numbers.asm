# 16. Write MIPS code that prompts the user to enter 
# two numbers, adds the numbers, and prints the result. 
# The prompts should be "Number 1?" and "Number 2?". 
# When the code prints the number, it should print 
# "The result is X", where "X" is the sum. 

.data
	prompt1:	.asciiz "Number 1?\n"
	prompt2:	.asciiz "Number 2?\n"
	resultText:	.asciiz "The result is "
.text
	la $a0, prompt1
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t0, $v0
	la $a0, prompt2
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t1, $v0
	add $t2, $t1, $t0
	la $a0, resultText
	li $v0, 4
	syscall
	move $a0, $t2
	li $v0, 1
	syscall