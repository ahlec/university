# 18. Write MIPS code that finds and prints the 
# smallest number in the integer array "arry".  The 
# declaration of the array is shown below. 
.data
	smallestText:	.asciiz	"The smallest value in the array is "
	arrayLen:	.word	10
.align 2
	arry:	.word	100,10,3,-2,110,-100,-50,150,-17,8
.text

#	$t0 - array length
#	$t1 - smallest word
#	$t2 - temp loaded word
#	$t3 - index
#	$t4 - conditional checking
	la $a1, arry
	la $a2, arrayLen
	lw $t0, 0($a2)
	lw $t1, 0($a2)
	move $t3, $0
	findLoop:
		addi $t3, $t3, 1
		slt $t4, $t3, $t0
		beq $t4, $0, endLoop
		addi $a1, $a1, 4
		lw $t2, 0($a1)
		slt $t4, $t1, $t2
		bne $t4, $0, findLoop
		move $t1, $t2
		j findLoop
	endLoop:
		la $a0, smallestText
		li $v0, 4
		syscall
		move $a0, $t1
		li $v0, 1
		syscall