#	Which of the following accesses cause an error due to misalignment?
.data
.align 2
	var:	.word	0x01020304
.text
la $t0, var
lw $t1,0($t0)
lw $t1,1($t0) #
lw $t3,2($t0) #
lb $t2,0($t0)
lb $t2,1($t0)
lb $t2,2($t0)
lh $t2,0($t0)
lh $t2,1($t0) #
lh $t2,2($t0)