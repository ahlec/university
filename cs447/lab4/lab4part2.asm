#CS/COE 447 Lab 4 Part 2 Template

#This template includes testing code, but also has some support code to check
#for a common error.

.text:
        #This is the beginning of the testing code. 

		# you may put additional instructions to calculate the addresses and bit patterns
	li $t0, 0xFFFF0008
	addi $t0, $t0, 8
	
	move $a0, $t0	# replace your_address with the actual address
        li $a1, 0x7EF965BD    # replace your_pattern with the actual pattern
        li $a2, 7               #Draw the pattern 7 times vertically.
        jal drawRepeatedPattern #Jump and link to drawRepeatedPattern.


	addi $t1, $t0, 48
	move $a0, $t1	# replace your_address with the actual address
	li $t2, 0x7EF965BD
	xori $t2, $t2, 0xF0F009F0
        move $a1, $t2    # replace your_pattern with the actual pattern
        li $a2, 7               #Draw the pattern 7 times vertically.
        jal drawRepeatedPattern #Jump and link to drawRepeatedPattern.

		# do not alter 
        la $a0, successfulQuitMessage
        li $v0, 4
        syscall

        li $v0, 10             #Exit syscall
        syscall

        #This is the end of the testing code.

#========================================
# * Place your drawPattern code here    *
#========================================
    drawPattern:
	sw $a1, 0($a0)
	jr $ra

#========================================
# * DO NOT ALTER THIS NEXT LINE         *
j returnErrorHappened
#========================================




#========================================
# * Place drawRepeatedPattern code here *
#========================================
    drawRepeatedPattern:
    	subi $sp, $sp, 4
    	sw $ra, 0($sp)
    	drawRepeatedPatternLoop:
    		beq $a2, $zero, loopFinished
    		jal drawPattern
    		addi $a0, $a0, 32
    		subi $a2, $a2, 1
    		j drawRepeatedPatternLoop
    	loopFinished:
    	lw $ra, 0($sp)
    	addi $sp, $sp, 4
        jr $ra

#========================================
# * DO NOT ALTER THIS NEXT LINE         *
j returnErrorHappened
#========================================


returnErrorHappened:
    #If this code is executed, your function did not properly return.
    la $a0, badReturnMessage
    li $v0, 4
    syscall
    li $v0, 10
    syscall

.data:
    badReturnMessage:       .asciiz "A function did not properly return!"
    successfulQuitMessage:  .asciiz "The program has finished."
