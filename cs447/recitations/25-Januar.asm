.data
	a:	.word 4
	b:	.word 10
	prompt1:	.asciiz "a is "
	prompt2:	.asciiz "b is "
	newline:	.asciiz "\n"
		
.text

la $t0, a  # load address
la $t1, b

lw $s0, ($t0)	# load word using the addresses already loaded
lw $s1, ($t1)

# store information in b in address a, store information in a in address b
sw $s1, ($t0)
sw $s0, ($t1)

# output
la $a0, prompt1 #  $a0 is the argument register
li $v0, 4       #  printing a string
syscall

move $a0, $s0
li $v0, 1
syscall

la $a0, newline
li $v0, 4
syscall

la $a0, prompt2
li $v0, 4
syscall

move $a0, $s1
li $v0, 1
syscall
