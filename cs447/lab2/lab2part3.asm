.data
prompt1:	.asciiz "What is the first value?\n"
prompt2:	.asciiz "What is the second value?\n"
outputA:	.asciiz "The difference of "
outputB:	.asciiz " and "
outputC:	.asciiz " is "
.text
la $a0, prompt1
li $v0, 4
syscall
li $v0, 5
syscall
move $t0, $v0
la $a0, prompt2
li $v0, 4
syscall
li $v0, 5
syscall
move $t1, $v0
la $a0, outputA
li $v0, 4
syscall
move $a0, $t0
li $v0, 1
syscall
la $a0, outputB
li $v0, 4
syscall
move $a0, $t1
li $v0, 1
syscall
la $a0, outputC
li $v0, 4
syscall
sub $t2, $t0, $t1
move $a0, $t2
li $v0, 1
syscall