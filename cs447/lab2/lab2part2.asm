.data
x:	.half 15
y:	.half 8
z:	.half 0
.text
la $a0, z
la $a1, x
la $a2, y
lh $t1, -2($a0)
lh $t2, -4($a0)
add $t3, $zero, $t1
add $t3, $t3, $t2
sh $t3, 0($a0)
sh $t3, -2($a0)
sh $t3, -4($a0)
