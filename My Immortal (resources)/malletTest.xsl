<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    <xsl:template match="/">
        <xsl:apply-templates select="//story"/>
    </xsl:template>
    <xsl:template match="story">
        <xsl:for-each select="chapter">
            <xsl:variable name="filename" select="concat('chapter','-',@number)"/>
            <xsl:result-document href="malletChapters/{$filename}.txt">
                <xsl:apply-templates/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="poc"/>
    <xsl:template match="sp | grammar">
        <xsl:choose>
            <xsl:when test="@intended">
                <xsl:apply-templates select="@intended"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="separation"/>
    <xsl:template match="redact">
        <xsl:apply-templates select="proposed"/>
    </xsl:template>
    <xsl:template match="omitted">
        <xsl:apply-templates select="@content"/>
    </xsl:template>
</xsl:stylesheet>
