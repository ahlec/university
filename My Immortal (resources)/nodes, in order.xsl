<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    <xsl:template match="/">
        <xsl:variable name="immortal" select="."/>
        <xsl:for-each select="distinct-values(//*/name())">
            <xsl:sort select="count($immortal//*[name() = current()])" order="descending" />
            <xsl:value-of select="current()"/> - <xsl:value-of select="count($immortal//*[name() = current()])"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>