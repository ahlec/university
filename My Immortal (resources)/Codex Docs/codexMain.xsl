<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:param name="urlPath"/>
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <div class="codexList1">
            <xsl:apply-templates select="codexData/characterList"/>
            <xsl:apply-templates select="codexData/locationList"/>
            <xsl:apply-templates select="codexData/referenceList"/>
            <xsl:apply-templates select="codexData/spellList"/>
        </div>
    </xsl:template>
    <xsl:template match="characterList">
        <div class="codexDropButton">
            <span class="codexDropButtonLabel"><xsl:text>Characters</xsl:text></span>
            <xsl:for-each select="character">
                <div class="codexItem"><a href="{$urlPath}/codex/characters/{./@lowerCaseHandle}/"><xsl:value-of select="name"/></a></div>
            </xsl:for-each>
        </div>
    </xsl:template>
    <xsl:template match="locationList">
        <div class="codexDropButton">
            <span class="codexDropButtonLabel"><xsl:text>Locations</xsl:text></span>
            <xsl:for-each select="location">
                <div class="codexItem"><a href="{$urlPath}/codex/locations/{./@lowerCaseHandle}"><xsl:value-of select="name"/></a></div>
            </xsl:for-each>
        </div>
    </xsl:template>
    <xsl:template match="referenceList">
        <div class="codexDropButton">
            <span class="codexDropButtonLabel"><xsl:text>References</xsl:text></span>
            <xsl:for-each select="reference">
                <div class="codexItem"><a href="{$urlPath}/codex/references/{./@lowerCaseHandle}"><xsl:value-of select="name"/></a></div>
            </xsl:for-each>
        </div>
    </xsl:template>
    <xsl:template match="spellList">
        <div class="codexDropButton">
            <span class="codexDropButtonLabel"><xsl:text>Spells</xsl:text></span>
            <xsl:for-each select="spell">
                <div class="codexItem"><a href="{$urlPath}/codex/spells/{./@lowerCaseHandle}"><xsl:value-of select="name"/></a></div>
            </xsl:for-each>
        </div>
    </xsl:template>
</xsl:stylesheet>