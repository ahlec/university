<html>
<head><title>XQuery Assignment #3</title></head>
<body>
<h1>The Many Names of Professor Albert Dumblydore</h1>
<h6>(In Order of Frequency)</h6>
<ul>
{
let $names := (for $reference in doc('/db/test/immortal.xml')//story//char[./@handle and contains(./@handle, 'dumblydore')]
where not(@type)
return string-join($reference//text(), " "))

let $lowerCaseNames := (for $name in $names return lower-case($name))

let $sorted := (
for $name in distinct-values($lowerCaseNames)
let $frequency := count(index-of($lowerCaseNames, $name))
order by $frequency
return <result><name>{$name}</name><frequency>{$frequency}</frequency></result>
)

for $item in reverse($sorted)
let $originalCasing := (for $name in $names where lower-case($name) = $item/name/text() return $name)
return <li><b>{$originalCasing[1]}</b> &amp;mdash; {$item/frequency/text()} {if (count($originalCasing) - count(index-of($originalCasing, $originalCasing[1])) > 0) then (<ul><li><b>Also written as:</b></li>{for $alternateCase in distinct-values($originalCasing)
where not($alternateCase = $originalCasing[1])
return <li>{$alternateCase}</li>}</ul>) else()}</li>
}
</ul>
</body>
</html>
