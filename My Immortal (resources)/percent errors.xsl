<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    <xsl:template match="/">
        <table border="1" cellpadding="2" cellspacing="1">
            <tr>
                <th><xsl:text>Chapter</xsl:text></th>
                <th><xsl:text>Number of Errors</xsl:text></th>
                <th><xsl:text>Number of Words</xsl:text></th>
                <th><xsl:text>Percentage of Errors</xsl:text></th>
            </tr>
<xsl:for-each select="//chapter">
    <xsl:variable name="chapter" select="."/>
    <xsl:variable name="error" select="(count($chapter//sp)+count($chapter//omitted)+count($chapter//grammar)+count($chapter//redact)+count($chapter//remove)+count($chapter//paradox)+count($chapter//fwb))"/>
    <xsl:variable name="word" select="count(tokenize(.,'\s'))"/>
    <xsl:variable name="percent" select="round(($error div $word)*100)"/>
    <xsl:variable name="number" select="./@number"/>

<tr>
<td><xsl:value-of select="data($number)"/></td>
<td><xsl:value-of select="$error"/></td>
<td><xsl:value-of select="$word"/></td>
<td><xsl:value-of select="$percent"/></td>
</tr>


</xsl:for-each>
            <!--<tr>
                <th><xsl:text>Total Errors:</xsl:text></th>
                <td colspan="3"><xsl:value-of select="sum($error)"/></td>
            </tr>-->
        </table>
    </xsl:template>
</xsl:stylesheet>