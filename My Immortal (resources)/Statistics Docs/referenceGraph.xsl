<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <title>References by Chapter in My Immortal</title>
            </head>
            <body>
                <svg height="100%" width="100%" xmlns="http://www.w3.org/2000/svg">
                    <g id="chart" transform="translate(10,350)">
                        <text fill="black" transform="rotate(-90 5 -100)" x="5" y="-100">Number of
                            References</text>
                        <text fill="black" x="350" y="20">Chapter</text>
                        <g id="guides" transform="translate(20,0)">
                            <line stroke="black" stroke-width="2" x1="10" x2="10" y1="-350" y2="0"/>
                            <line stroke="black" stroke-width="2" x1="10" x2="700" y1="0" y2="0"/>
                            <text fill="black" x="-10" y="-35">10</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-40" y2="-40"/>
                            <text fill="black" x="-10" y="-75">20</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-80" y2="-80"/>
                            <text fill="black" x="-10" y="-115">30</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-120" y2="-120"/>
                            <text fill="black" x="-10" y="-155">40</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-160" y2="-160"/>
                            <text fill="black" x="-10" y="-195">50</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-200" y2="-200"/>
                            <text fill="black" x="-10" y="-235">60</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-240" y2="-240"/>
                            <text fill="black" x="-10" y="-275">70</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-280" y2="-280"/>
                            <text fill="black" x="-10" y="-315">80</text>
                            <line stroke="black" stroke-dasharray="5 3" stroke-width="1" x1="-5"
                                x2="700" y1="-320" y2="-320"/>
                            <xsl:for-each select="//chapter">
                                <xsl:variable name="xPos" select="position() * 15"/>
                                <xsl:variable name="yPos" select="count(descendant::ref) * 4"/>
                                <rect fill="purple" height="{$yPos}" stroke="white" stroke-width="1"
                                    width="15" x="{$xPos}" y="-{$yPos}">
                                    <title>
                                        <xsl:text>Chapter </xsl:text>
                                        <xsl:value-of select="@number"/>
                                        <xsl:text>, Number of References: </xsl:text>
                                        <xsl:value-of select="count(descendant::ref)"/>
                                    </title>
                                </rect>
                            </xsl:for-each>
                        </g>
                    </g>
                </svg>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
<!--
    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
-->
