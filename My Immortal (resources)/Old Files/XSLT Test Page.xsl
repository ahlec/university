<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    
    <!--You want to try applying templates within grammar to see how sp's are presented when nested within grammar elements!!!-->
    
    <xsl:template match="/">
        <html>
            <head>
                <title><xsl:value-of select="//meta/title"/></title>
                <style>
                    #maple
                    {
                        -webkit-border-image:url("rock-maple.png") 30 30 stretch;
                        border-image:url("border.png") 30 30 round;
                        -webkit-border-image:url("border.png") 30 30 round;
                        -moz-border-image:url("border.png") 30 30 round;
                    }
                </style>
            </head>
            <body bgcolor="#EEEEEE" style="border-width:15px;width:1000px;padding:10x 20px;font-family:Georgia, serif;" id="maple">
                <xsl:attribute name="id">style</xsl:attribute>
                <h1 style="border-style:ridge;color:#ABCDEF;text-align:center;text-decoration:underline">
                    <!--01AFDD-->
                    <xsl:value-of select="//meta/title"/>
                </h1>

                <xsl:apply-templates select="//chapter"/>
                
            </body>
        </html>
    </xsl:template>
    <xsl:template match="chapter">
        <div style="padding:.7em;max-width:40em">
            
            <h2>Chapter <xsl:value-of select="@number"/> <span style="font-size:.5em;color:777777;padding-left:.5em">Edited by <xsl:value-of select="@editor"/></span></h2>
            
            <xsl:apply-templates/>
            
        </div>
    </xsl:template>
    <xsl:template match="paragraph">
        <p style="max-width:40em;text-indent:1.5em">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <xsl:template match="separation">
        <div style="font-family:'Reprise Rehearsal';max-width:70em;">
            <xsl:value-of select="."/>
        </div>
    </xsl:template>
    
    <xsl:template match="an">
        <xsl:if test="parent::chapter">
            <div><xsl:value-of select="."/></div>
            <br/>
        </xsl:if>
        <xsl:if test="not(parent::chapter)">
            <xsl:value-of select="."/>
        </xsl:if>
        <xsl:apply-templates/>
        
    </xsl:template>
    
    <xsl:template match="grammar">
        <strike><span>
            <xsl:value-of select="."/>
        </span></strike>
    </xsl:template>
     
    <xsl:template match="charsp">
        <span><xsl:value-of select="//character[@handle eq current()/@handle]"/></span>
    </xsl:template>
    
    <xsl:template match="ref">
        
            <a>
                <xsl:attribute name="href">http://en.wikipedia.org/wiki/<xsl:value-of select="//reference[@handle eq current()/@handle]"/></xsl:attribute>
                <xsl:apply-templates/>
            </a>
    </xsl:template>
    
    <xsl:template match="sp">
        <span>
            <xsl:choose>
                <xsl:when test="@type = 'phonetic'">
                    <xsl:attribute name="style">color:red</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'keyboarding'">
                    <xsl:attribute name="style">color:green</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'hyperbole'">
                    <xsl:attribute name="style">color:blue</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'apocope'">
                    <xsl:attribute name="style">color:purple</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'chatspeak'">
                    <xsl:attribute name="style">color:pink</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'goffik'">
                    <xsl:attribute name="style">color:orange</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'metathesis'">
                    <xsl:attribute name="style">color:yellow</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = 'homophone'">
                    <xsl:attribute name="style">color:brown</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type = '???'">
                    <xsl:attribute name="style">color:teal</xsl:attribute>
                </xsl:when>
            </xsl:choose>
        [<xsl:value-of select="@intended"/>]
        </span>
    </xsl:template>
    
    <xsl:template match="poc">
        
    </xsl:template>
</xsl:stylesheet>