<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:apply-templates select="//meta/title"/>
                </title>
                <link rel="stylesheet" type="text/css" href="test.css"/>
            </head>
            <body>
                <a href="test.html"><h1>
                    <xsl:value-of select="//meta/title"/>
                </h1></a>
                <div>
                    <!--<table><tr><td>-->
                    <h2 style="text-align:center;text-decoration:underline;">Table of Contents</h2>
                    <!--<table>-->
                    <xsl:apply-templates select="//chapter" mode="toc"/>
                    <!--</table></td></tr></table>-->
                </div>
                <br/>
                <xsl:apply-templates select="//chapter"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="//chapter" mode="toc">
        <!--<xsl:if test="number(@number) lt number(26)">
            <tr>
                <td>
                    <a><xsl:attribute name="name">toc_<xsl:apply-templates select="@number"/></xsl:attribute><xsl:attribute name="href">#chapter_<xsl:apply-templates select="@number"/></xsl:attribute><xsl:text>Chapter </xsl:text>
                    <xsl:apply-templates select="@number"/></a>
                </td>
                <td>
                    <a><xsl:attribute name="name">toc_<xsl:apply-templates select="@number"/></xsl:attribute><xsl:attribute name="href">#chapter_<xsl:apply-templates select="@number"/></xsl:attribute><xsl:text>Edited by </xsl:text>
                    <xsl:apply-templates select="@editor"/></a>
                </td>
            </tr>
        </xsl:if>-->
        <xsl:if test="number(@number) lt number(30)"><a>
            <xsl:attribute name="name">toc_<xsl:apply-templates select="@number"/></xsl:attribute>
            <xsl:attribute name="href">#chapter_<xsl:apply-templates select="@number"
                /></xsl:attribute>
            <xsl:attribute name="title">
                <xsl:text>Edited by </xsl:text>
                <xsl:apply-templates select="@editor"/>
            </xsl:attribute>
            <xsl:text>Chapter </xsl:text>
            <xsl:apply-templates select="@number"/>
        </a>
        <xsl:text> | </xsl:text></xsl:if>
    </xsl:template>
    <xsl:template match="chapter">
        <xsl:if test="number(@number) lt number(30)">
            <div>
                <h2>
                    <a>
                        <xsl:attribute name="name">chapter_<xsl:apply-templates select="@number"
                            /></xsl:attribute>
                        <xsl:attribute name="href">#toc_<xsl:apply-templates select="@number"
                            /></xsl:attribute>
                        <xsl:text>Chapter </xsl:text>
                        <xsl:apply-templates select="@number"/>
                    </a>
                    <br/>
                </h2>
                <h3>
                    <xsl:text>Edited by </xsl:text>
                    <xsl:apply-templates select="@editor"/>
                </h3>
                <xsl:apply-templates/>
            </div>
            <br/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="paragraph">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="an | aside | fwb">
        <span class="aside">
            <xsl:apply-templates/>
        </span>
        <xsl:if test="self::an">
            <br/>
            <br/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="separation">
        <center>
            <xsl:apply-templates/>
        </center>
    </xsl:template>
    <xsl:template match="sp">
        <span class="error">
            <xsl:attribute name="title">
                <xsl:value-of select="."/>
            </xsl:attribute>
            <xsl:apply-templates select="@intended"/>
        </span>
    </xsl:template>
    <xsl:template match="poc"> </xsl:template>
    <xsl:template match="redact">
        <xsl:apply-templates select="proposed"/>
    </xsl:template>
    <xsl:template match="ref">
        <a>
            <xsl:attribute name="href">http://en.wikipedia.org/wiki/<xsl:value-of
                    select="normalize-space(//reference[current()/@handle = ./@handle])"
                /></xsl:attribute>
            <xsl:attribute name="title">
                <xsl:value-of select="normalize-space(//reference[current()/@handle = ./@handle])"/>
            </xsl:attribute>
            <xsl:text>[</xsl:text>
            <xsl:apply-templates/>
            <xsl:text>]</xsl:text>
        </a>
    </xsl:template>
    <xsl:template match="omitted">
        <span class="error">
            <xsl:apply-templates select="@content"/>
        </span>
    </xsl:template>
    <xsl:template match="grammar">
        <span class="error">
            <xsl:attribute name="title"/>
            <xsl:apply-templates select="@intended"/>
        </span>
    </xsl:template>
    <xsl:template match="remove"> </xsl:template>
</xsl:stylesheet>
