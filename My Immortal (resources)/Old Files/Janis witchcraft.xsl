<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:value-of select="//title"/>
                </title>
                <style>
                    text-align:justify;
                    font-family:'gill sans mt','tw cen mt';
                    margin:3em;
                    background-color:#d3d3d3;
                    color:purple;
                    link{color:#00cc00;text-decoration:none}
                    a:link{color:#00cc00;text-decoration:none;}
                    v:link{color:#00cc00;text-decoration:none;}
                    a:hover{color:#00cc00; font-variant:small-caps;text-decoration:none;}
                    a:active{#00cc00; font-variant:small-caps;text-decoration:none;}
</style>
            </head>
            <body
                style="
                    text-align:justify;
                    font-family:'gill sans mt','tw cen mt';
                    margin:3em;
                    background-color:#d3d3d3;
                    color:purple;">
                <h1 style="text-align:center; text-decoration:underline;">
                    <xsl:value-of select="//title"/>
                </h1>
                <xsl:apply-templates select="//chapter"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="chapter">
        <xsl:for-each select=".">
            <xsl:if test="number(@number) &lt; 20">
                <h2>
                    <xsl:text>Chapter </xsl:text>
                    <xsl:value-of select="@number"/>
                </h2>
                <h3>
                    <xsl:text>Edited by: </xsl:text>
                    <xsl:value-of select="@editor"/>
                </h3>
                <!--<xsl:apply-templates select="./an"/>-->
                <!--<xsl:apply-templates select="./paragraph"/>
                <br/>
                <center>
                    <xsl:value-of select="./separation"/>
                </center>-->
                <!-- <br/>-->
                <xsl:apply-templates/>
                <br/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="an | aside">
        <span>
            <xsl:attribute name="style">font-variant:small-caps;</xsl:attribute>
            <xsl:apply-templates/>
        </span>
        <xsl:if test="self::an">
            <br/>
            <br/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="paragraph">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="separation">
        <center>
            <xsl:apply-templates/>
        </center>
    </xsl:template>
    <xsl:template match="sp | grammar | charsp">
        <span>
            <xsl:attribute name="title">
                <xsl:value-of select="."/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@type='phonetic'">
                    <xsl:attribute name="style">color:black;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='keyboarding'">
                    <xsl:attribute name="style">color:blue;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='goffik'">
                    <xsl:attribute name="style">color:red;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='metathesis'">
                    <xsl:attribute name="style">color:brown;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='hyperbole'">
                    <xsl:attribute name="style">color:hotpink;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='apocope'">
                    <xsl:attribute name="style">color:green;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='chatspeak'">
                    <xsl:attribute name="style">color:orange;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='homophone'">
                    <xsl:attribute name="style">color:teal;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='???'">
                    <xsl:attribute name="style">color:#ff0099;</xsl:attribute>
                </xsl:when>
                <xsl:when test="self::grammar">
                    <xsl:attribute name="style">color:white;</xsl:attribute>
                </xsl:when>
                <xsl:when test="self::charsp">
                    <xsl:attribute name="style">color:#9900ff;</xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:text>[</xsl:text>
            <xsl:value-of select="./@intended"/>
            <xsl:text>]</xsl:text>
        </span>
    </xsl:template>
    <xsl:template match="omitted">
        <span>
            <xsl:attribute name="style">color:#0099ff;</xsl:attribute>
            <xsl:value-of select="./@content"/>
        </span>
    </xsl:template>
    <xsl:template match="ref">
        <!--<xsl:for-each select="ref">-->
        <a>
            <xsl:attribute name="href">http://en.wikipedia.org/wiki/<xsl:value-of
                    select="normalize-space(//reference[current()/@handle = ./@handle])"
                /></xsl:attribute>
            <xsl:attribute name="title">
                <xsl:value-of select="normalize-space(//reference[current()/@handle = ./@handle])"/>
            </xsl:attribute>
            <!--<xsl:value-of select="."/>-->
            <xsl:text>[</xsl:text><xsl:apply-templates/><xsl:text>]</xsl:text>
        </a>
        <!--</xsl:for-each>-->
    </xsl:template>
</xsl:stylesheet>
