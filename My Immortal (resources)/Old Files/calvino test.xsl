<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:value-of select="//information/title"/>
                </title>
                <link rel="stylesheet" type="text/css" href="test.css" />
            </head>
            <body style="/*font-family:'georgia','serif'; background-color:lightblue; margin:1em;*/">
                <h1 style="/*text-align:center; text-decoration:underline;*/">
                    <xsl:apply-templates select="//information/title"/>
                    <br/>
                    <span style="/*font-size:large; text-transform:capitalize;*/">
                        <xsl:text> By </xsl:text>
                        <xsl:apply-templates select="//information/author"/>
                    </span>
                </h1>
                <span style="">
                    <a>
                        <xsl:attribute name="name">TOC</xsl:attribute>
                        <h2> Table of Contents </h2>
                    </a>
                    <ul>
                        <xsl:apply-templates select="//city" mode="toc"/>
                    </ul>
                </span>
                <hr/>
                <xsl:apply-templates select="//pages"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="pages">
        <!--<p>
            <xsl:apply-templates select="//information"/>
        </p>-->
        <xsl:apply-templates select="city"/>
    </xsl:template>
    <xsl:template match="city">
        <h2 style="/*display:inline;*/">
            <xsl:text>Chapter </xsl:text>
            <xsl:apply-templates select="@chapter"/>
            <xsl:text>: </xsl:text>
            <xsl:apply-templates select="@title"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="@index"/>
            <br/>
        </h2>
        <h3 style="/*display:inline;*/">
            <a>
                <xsl:attribute name="name">
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:attribute name="href">#TOC</xsl:attribute>
                <xsl:apply-templates select="@name"/>
                <xsl:text>: Pages </xsl:text>
                <xsl:apply-templates select="@pageBegin"/>
                <xsl:text>-</xsl:text>
                <xsl:apply-templates select="@pageEnd"/>
            </a>
        </h3>
        <xsl:apply-templates select="./paragraph"/>
    </xsl:template>
    <xsl:template match="paragraph">
        <p style="/*margin-left:3em;margin-right:3em;text-align:justify;*/">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="information">
        <div
            style="/*border:1px black;border-style:dashed;padding:.1em;margin-left:4em;margin-right:4em;*/">
            <!--<h1 style="text-align:center;text-decoration:underline;font-size:x-large;"
                >Information</h1>-->
            <blockquote>
                <xsl:apply-templates select="inclusions"/>
            </blockquote>
        </div>
    </xsl:template>
    <xsl:template match="item[@tag='headline']">
        <h3>
            <xsl:apply-templates/>
        </h3>
    </xsl:template>
    <xsl:template match="item[@tag='description']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="character">
        <span style="*/font-weight:bold;*/">
            <xsl:attribute name="title">
                <xsl:value-of select="//characters/character[current()/@tag = ./@tag]"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="mention | anachronism">
        <span style="outline:black dotted thin;">
            <xsl:if test="self::anachronism">
                <xsl:attribute name="title">
                    <xsl:value-of select="@object"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <!--<xsl:template match="anachronism">
        <span style="outline:black dashed thin;">
            <xsl:attribute name="title">
                <xsl:value-of select="@object"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
        </xsl:template>-->
    <xsl:template match="city" mode="toc">

        <li>
            <a>
                <xsl:attribute name="href">#<xsl:value-of select="@name"/></xsl:attribute>
                <xsl:apply-templates select="@title"/>
                <xsl:text>, </xsl:text>
                <xsl:apply-templates select="@index"/>
            </a>
        </li>
    </xsl:template>
</xsl:stylesheet>
