<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:immortal="http://deitloff.com">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="//chapter[42]"/>
        <!--        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>-->
    </xsl:template>

    <xsl:template match="chapter[@editor]"/>
    <!-- remove this later -->

    <xsl:template match="chapter[not(@editor)]">
        <xsl:element name="chapter">
            <xsl:attribute name="number" select="@number"/>
            <xsl:variable name="expressionSplit">(\s+|(!|[0-9])+|!+|\.+)</xsl:variable>
            <!--<xsl:for-each select="tokenize(normalize-space(text()), $expressionSplit)">
                <xsl:element name="piece">
                    <xsl:value-of select="current()"/>
                </xsl:element>
            </xsl:for-each>-->

            <xsl:analyze-string select="normalize-space(.)" regex="{$expressionSplit}">
                <xsl:matching-substring>
                    <xsl:apply-templates select="immortal:makeNode(current())" mode="token"/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:apply-templates select="immortal:makeNode(current())" mode="token"/>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:element>
    </xsl:template>

    <xsl:function name="immortal:makeNode">
        <xsl:param name="inputText"/>
        <xsl:element name="textNode">
            <xsl:value-of select="$inputText"/>
        </xsl:element>
    </xsl:function>

    <xsl:variable name="count" select="0"/>
    <xsl:template match="*" mode="token">
        <xsl:variable name="count" select="$count + 1"/>
        <xsl:if test="$count &lt;= 1">
            <xsl:element name="pieceRender">
                <xsl:variable name="textNode" select="text()"/>
                <xsl:variable name="processed" select="false()"/>
                <xsl:variable name="selectStatementAll">doc('My Immortal.xml')//story/chapter/*[self::an | self::paragraph | self::sex]//*[not(self::sentence)]</xsl:variable>
                <xsl:for-each select="doc('My Immortal.xml')//sp[not(ancestor::grammar) and not(ancestor::sp)]">
                    <xsl:if test="(string(current()) eq $textNode) and not($processed)">
                        <xsl:copy>
                            <xsl:value-of select="$textNode"/>
                        </xsl:copy>
                    </xsl:if>
                    <xsl:element name="searchCriteria">
                        <xsl:attribute name="search" select="$textNode"/>
                        <xsl:value-of select="string(current())"/>
                    </xsl:element>
                </xsl:for-each>
                <xsl:if test="not($processed)">
                    <xsl:value-of select="$textNode"/>
                </xsl:if>
            </xsl:element>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
