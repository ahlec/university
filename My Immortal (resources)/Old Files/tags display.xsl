<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template match="troll">
        <html>
            <body style="background-color:#C3E4ED;margin:0px;padding:0px;">
                <table style="width:450px;margin-left:auto;border-left:1px solid black;border-right:1px solid black;background-color:white;margin-right:auto;">
                    <tr>
                        <td colspan="4" style="text-align:center;font-size:200%;font-weight:bold;"
                            >&lt;sp&gt;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align:center;font-weight:bold;width:150px;">Misspelling</td>
                        <td style="text-align:center;font-weight:bold;width:150px;">Intended</td>
                        <td style="text-align:center;font-weight:bold;width:150px;">Type</td>
                    </tr>
                    <xsl:for-each-group group-by="lower-case(text())" select="//sp">
                        <xsl:sort select="current-grouping-key()" order="ascending"/>
                        <tr>
                            <xsl:variable name="isEven" select="position() mod 2"/>
                            <xsl:variable name="cellStyle"
                                select="if ($isEven = 1) then 'background-color:#C6E2FF;' else
                                'background-color:#9AC0CD;'"/>
                            <td style="{$cellStyle}"></td>
                            <td style="text-align:center;width:150px;{$cellStyle}">
                                <xsl:value-of select="lower-case(.)"/>
                            </td>
                            <td colspan="2" style="{$cellStyle}">
                                <table cellpadding="0" cellspacing="0">
                                    <xsl:for-each-group select="current-group()"
                                        group-by="lower-case(@intended)">
                                        <xsl:variable name="uniqueTypes"
                                            select="count(distinct-values(current-group()/@type))"/>
                                            <xsl:for-each-group select="current-group()"
                                            group-by="@type">
                                                <xsl:variable name="instanceLocations"
                                                    select="string-join(for $instance in current-group() 
                                                    return concat('Chapter ',
                                                    $instance/ancestor::chapter/@number,
                                                    ', ',
                                                    if (.[ancestor::sentence]) then
                                                    concat('Paragraph ',
                                                    string(count($instance/ancestor::paragraph/preceding-sibling::paragraph) + 1),
                                                    ', Sentence ',
                                                    string(count($instance/ancestor::sentence/preceding-sibling::sentence) + 1))
                                                    else 'Authors Note'), '\n')" />
                                                <xsl:if test="position() = 1">
                                                    <tr>
                                                        <td style="text-align:center;width:150px;vertical-align:middle;"
                                                            rowspan="{$uniqueTypes}">
                                                            <xsl:value-of select="lower-case(./@intended)"/>
                                                        </td>
                                                        <td style="width:150px;text-align:center;cursor:pointer;"
                                                            onclick="alert('{$instanceLocations}');"><xsl:value-of select="count(current-group())"/>
                                                            [<xsl:value-of select="./@type"/>]</td>
                                                    </tr>
                                                </xsl:if>
                                                <xsl:if test="position() > 1">
                                                    <tr><td style="width:150px;text-align:center;cursor:pointer;"
                                                        onclick="alert('{$instanceLocations}');">
                                                        <xsl:value-of select="count(current-group())"/>
                                                        [<xsl:value-of select="./@type"/>]</td>
                                                    </tr>
                                                </xsl:if>
                                            </xsl:for-each-group>
                                        <xsl:if test="position() lt last()">
                                        <tr>
                                            <td colspan="2" style="border-bottom:2px solid white;"></td>
                                        </tr>
                                        </xsl:if>
                                    </xsl:for-each-group>
                                </table>
                            </td>
                            <td style="{$cellStyle}"> </td>
                        </tr>
                    </xsl:for-each-group>
                </table>
            </body>

        </html>
    </xsl:template>
</xsl:stylesheet>
