<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
    <xsl:template match="/">
        <characterList>
            <xsl:apply-templates select="//meta/characterList/*"/>
        </characterList>
    </xsl:template>
    <xsl:template match="character">
        <xsl:element name="character">
            <xsl:attribute name="handle">
                <xsl:value-of select="@handle"/>
            </xsl:attribute>
            <xsl:attribute name="lowerCaseHandle">
                <xsl:value-of select="lower-case(@handle)"/>
            </xsl:attribute>
            <xsl:element name="name">
                <xsl:value-of select="normalize-space(.)"/>
            </xsl:element>
            <xsl:element name="gender">
                <xsl:value-of select="@gender"/>
            </xsl:element>
            <xsl:if test="@hp">
                <xsl:element name="potterCharacter">
                    <xsl:value-of select="@hp"/>
                </xsl:element>
            </xsl:if>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
