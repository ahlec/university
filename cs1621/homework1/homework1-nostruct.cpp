#include <cstdio>

#define NUMBER_ELEMENTS 3

// Create a shared instance of our array. We're doing this here (even though shared variables are bad)
// because it will make it easier to directly compare this version to the version of the code that uses
// structs.
char** s_names;
int* s_ages;
float* s_gpas;
char** s_gradeLevels;

// Create an initialisation function for the data.
void initialize()
{
    s_names = new char*[NUMBER_ELEMENTS];
    s_ages = new int[NUMBER_ELEMENTS];
    s_gpas = new float[NUMBER_ELEMENTS];
    s_gradeLevels = new char*[NUMBER_ELEMENTS];

    s_names[0] = "Amanda";
    s_ages[0] = 19;
    s_gpas[0] = 3.2;
    s_gradeLevels[0] = "Sophomore";

    s_names[1] = "Braeden";
    s_ages[1] = 18;
    s_gpas[1] = 3.3;
    s_gradeLevels[1] = "Freshman";

    s_names[2] = "Charlie";
    s_ages[2] = 20;
    s_gpas[2] = 3.6;
    s_gradeLevels[2] = "Senior";
};

void deinitialize()
{
    delete[] s_names;
    delete[] s_ages;
    delete[] s_gpas;
    delete[] s_gradeLevels;
};

// Our main function, to demonstrate how to use the data.
int main(int argc, char** argv)
{
    initialize();

    for (int index = 0; index < NUMBER_ELEMENTS; ++index)
    {
        printf("[STUDENT] %s, age %d. (Year: %s, GPA: %f)\n", s_names[index], s_ages[index],
            s_gradeLevels[index], s_gpas[index]);
    }

    deinitialize();

    return 0;
}
