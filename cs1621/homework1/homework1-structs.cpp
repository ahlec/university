#include <cstdio>

#define NUMBER_ELEMENTS 3

// Define the struct
typedef struct {
    char* name;
    int age;
    float gpa;
    char* gradeLevel;
} Student;

// Create a shared instance of our array. We're doing this here (even though shared variables are bad)
// because it will make it easier to directly compare this version to the version of the code that doesn't
// use structs.
Student* s_students;

// Create an initialisation function for the data.
void initialize()
{
    s_students = new Student[NUMBER_ELEMENTS];

    s_students[0].name = "Amanda";
    s_students[0].age = 19;
    s_students[0].gpa = 3.2;
    s_students[0].gradeLevel = "Sophomore";

    s_students[1].name = "Braeden";
    s_students[1].age = 18;
    s_students[1].gpa = 3.3;
    s_students[1].gradeLevel = "Freshman";

    s_students[2].name = "Charlie";
    s_students[2].age = 20;
    s_students[2].gpa = 3.6;
    s_students[2].gradeLevel = "Senior";
};

void deinitialize()
{
    delete[] s_students;
};

// Our main function, to demonstrate how to use the data.
int main(int argc, char** argv)
{
    initialize();

    for (int index = 0; index < NUMBER_ELEMENTS; ++index)
    {
        printf("[STUDENT] %s, age %d. (Year: %s, GPA: %f)\n", s_students[index].name, s_students[index].age,
            s_students[index].gradeLevel, s_students[index].gpa);
    }

    deinitialize();

    return 0;
}
