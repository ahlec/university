#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "ArrayInfo.h"

/// ---------------------------
/// Array functions
/// ---------------------------
void readSequential()
{
    int storageVariable;
    int* currentPointer = s_array[0];
    size_t majorIndex;
    size_t minorIndex;

    for (majorIndex = 0; majorIndex < MAJOR_DIMENSION_SIZE; ++majorIndex)
    {
        for (minorIndex = 0; minorIndex < MINOR_DIMENSION_SIZE; ++minorIndex)
        {
            ++currentPointer;
            storageVariable = *currentPointer;
        }
    }
};

void readRandom()
{
    const static int TOTAL_DIMENSION_SPACE = MAJOR_DIMENSION_SIZE * MINOR_DIMENSION_SIZE;
    int storageVariable;
    int trialNo = 0;
    int* headPointer = s_array[0];

    while (trialNo < NUMBER_RANDOM_TRIALS_TO_PERFORM)
    {
        storageVariable = *(headPointer + (rand() % TOTAL_DIMENSION_SPACE));

        ++trialNo;
    }
};

/// ---------------------------
/// Utility function
/// ---------------------------
int measureFunction(void (*functionPointer)())
{
    static struct timeval s_begin;
    static struct timeval s_end;
    gettimeofday(&s_begin, NULL);
    functionPointer();
    gettimeofday(&s_end, NULL);

    static int timeDifference;
    timeDifference = (s_end.tv_sec - s_begin.tv_sec) * 1000 +
        (s_end.tv_usec - s_begin.tv_usec);
    return (timeDifference < 0 ? 0 : timeDifference);
};

void outputResults(TrialTimes results, const char* prefix)
{
    float sum = 0;
    int minTime = results[0];
    int maxTime = results[0];
    size_t index;
    float average;

    for (index = 0; index < NUMBER_DATA_TRIALS; ++index)
    {
        sum += results[index];
        if (results[index] < minTime)
        {
            minTime = results[index];
        }
        if (results[index] > maxTime)
        {
            maxTime = results[index];
        }
    }

    average = sum / NUMBER_RANDOM_TRIALS_TO_PERFORM;

    printf("%sAVG: %f\tMIN: %d\tMAX: %d\n", prefix, average, minTime, maxTime);
};

int main(int argc, char** argv)
{
    TrialTimes sequentialTimes;
    TrialTimes randomTimes;
    long trialNo;

    srand(time(NULL));

    for (trialNo = 0; trialNo < NUMBER_DATA_TRIALS; ++trialNo)
    {
        sequentialTimes[trialNo] = measureFunction(&readSequential);
        randomTimes[trialNo] = measureFunction(&readRandom);
    }

    outputResults(sequentialTimes, "Sequential reading results>\t\t");
    outputResults(randomTimes, "Random reading results>\t\t\t");

    return 0;
};
