#include <cstdio>
#include <cstdlib>
#include <sys/time.h>
#include <vector>
#include "ArrayInfo.h"

/// ---------------------------
/// Array functions
/// ---------------------------
void readSequential()
{
    int storageVariable;
    for (size_t majorIndex = 0; majorIndex < MAJOR_DIMENSION_SIZE; ++majorIndex)
    {
        for (size_t minorIndex = 0; minorIndex < MINOR_DIMENSION_SIZE; ++minorIndex)
        {
            storageVariable = s_array[majorIndex][minorIndex];
        }
    }
};

void readRandom()
{
    int storageVariable;
    int trialNo = 0;
    int randomMajor;
    int randomMinor;
    while (trialNo < NUMBER_RANDOM_TRIALS_TO_PERFORM)
    {
        randomMajor = rand() % MAJOR_DIMENSION_SIZE;
        randomMinor = rand() % MINOR_DIMENSION_SIZE;
        storageVariable = s_array[randomMajor][randomMinor];

        ++trialNo;
    }
};

/// ---------------------------
/// Utility function
/// ---------------------------
int measureFunction(void (*functionPointer)())
{
    static struct timeval s_begin;
    static struct timeval s_end;
    gettimeofday(&s_begin, NULL);
    functionPointer();
    gettimeofday(&s_end, NULL);

    static int timeDifference;
    timeDifference = (s_end.tv_sec - s_begin.tv_sec) * 1000 +
        (s_end.tv_usec - s_begin.tv_usec);
    return (timeDifference < 0 ? 0 : timeDifference);
};

void outputResults(const std::vector<int>& results, const char* prefix)
{
    float sum(0);
    int minTime(results[0]);
    int maxTime(results[0]);
    for (size_t index = 0; index < NUMBER_DATA_TRIALS; ++index)
    {
        sum += results[index];
        if (results[index] < minTime)
        {
            minTime = results[index];
        }
        if (results[index] > maxTime)
        {
            maxTime = results[index];
        }
    }

    float average(sum / (float)results.size());

    printf("%sAVG: %f\tMIN: %d\tMAX: %d\n", prefix, average, minTime, maxTime);
};

int main(int argc, char** argv)
{
    srand(time(NULL));

    std::vector<int> sequentialTimes;
    sequentialTimes.reserve(NUMBER_DATA_TRIALS);
    std::vector<int> randomTimes;
    randomTimes.reserve(NUMBER_DATA_TRIALS);

    for (long trialNo(0); trialNo < NUMBER_DATA_TRIALS; ++trialNo)
    {
        sequentialTimes.push_back(measureFunction(&readSequential));
        randomTimes.push_back(measureFunction(&readRandom));
    }

    outputResults(sequentialTimes, "Sequential reading results>\t\t");
    outputResults(randomTimes, "Random reading results>\t\t\t");

    return 0;
};
