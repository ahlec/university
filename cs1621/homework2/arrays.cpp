#include <cstdio>
#include <sys/time.h>
#include <vector>
#include <stdint.h>

#define ARRAY_SIZE 1000000
#define NUMBER_TRIALS 100000

/// -----------------------------
/// Array functions
/// -----------------------------
void createStaticArray()
{
    static char theArray[ARRAY_SIZE];
};

void createStackArray()
{
    char theArray[ARRAY_SIZE];
};

void createHeapArray()
{
    char* theArray = new char[ARRAY_SIZE];
    delete[] theArray;
};

/// ---------------------------
/// Utility function
/// ---------------------------
int32_t measureFunction(void (*functionPointer)())
{
    static struct timeval s_begin;
    static struct timeval s_end;
    gettimeofday(&s_begin, NULL);
    functionPointer();
    gettimeofday(&s_end, NULL);

    static int32_t timeDifference;
    timeDifference = (s_end.tv_sec - s_begin.tv_sec) * 1000 +
        (s_end.tv_usec - s_begin.tv_usec);
    return (timeDifference < 0 ? 0 : timeDifference);
};

void outputResults(const std::vector<int32_t>& results, const char* testName)
{
    uint64_t sum(0);
    int32_t minTime(results[0]);
    int32_t maxTime(results[0]);
    for (size_t index = 0; index < NUMBER_TRIALS; ++index)
    {
        sum += results[index];
        if (results[index] < minTime)
        {
            minTime = results[index];
        }
        if (results[index] > maxTime)
        {
            maxTime = results[index];
        }
    }

    float average((float)sum / (float)results.size());

    printf("%s results>\t\tAVG: %f\tMIN: %d\tMAX: %d\n", testName, average, minTime,
        maxTime);
};

int main(int argc, char** argv)
{
    std::vector<int> staticTimes;
    staticTimes.reserve(NUMBER_TRIALS);
    std::vector<int> stackTimes;
    stackTimes.reserve(NUMBER_TRIALS);
    std::vector<int> heapTimes;
    heapTimes.reserve(NUMBER_TRIALS);

    for (long trialNo(0); trialNo < NUMBER_TRIALS; ++trialNo)
    {
        staticTimes.push_back(measureFunction(&createStaticArray));
        stackTimes.push_back(measureFunction(&createStackArray));
        heapTimes.push_back(measureFunction(&createHeapArray));
    }

    outputResults(staticTimes, "Static array");
    outputResults(stackTimes, "Stack array");
    outputResults(heapTimes, "Heap array");

    return 0;
};
