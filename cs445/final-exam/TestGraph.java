///
/// Contents: Test Simple Graph Data Structure
/// Author:   John Aronis
/// Date:     April 2012

import java.util.ArrayList ;

public class TestGraph {

  public static boolean VERBOSE = true ;

  public static void main(String[] args) {
    System.out.println("========================= G1 =========================") ;
    Graph G1 = new Graph(10) ;
    G1.edge(0,1) ; G1.edge(1,2) ; G1.edge(2,3) ; G1.edge(3,4) ; G1.edge(1,7) ;
    G1.edge(3,5) ; G1.edge(0,6) ; G1.edge(6,7) ; G1.edge(7,8) ; G1.edge(8,5) ;
    G1.edge(6,9) ; G1.edge(9,5) ;
    if (VERBOSE) G1.print() ;
    System.out.println("CONNECTED: " + G1.connected()) ;
    System.out.println("PATH 0 TO 0: " + G1.path(0,0) ) ;
    System.out.println("PATH 0 TO 5: " + G1.path(0,5) ) ;
    System.out.println("PATH 3 TO 8: " + G1.path(3,8) ) ;
    
    System.out.println("========================= G2 =========================") ;
    Graph G2 = new Graph(14) ;
    G2.edge(0,1) ; G2.edge(1,2) ; G2.edge(2,3) ; G2.edge(3,4) ; G2.edge(1,7) ;
    G2.edge(3,5) ; G2.edge(0,6) ; G2.edge(6,7) ; G2.edge(7,8) ; G2.edge(8,5) ;
    G2.edge(6,9) ; G2.edge(9,5) ; G2.edge(10,11) ; G2.edge(11,12) ; G2.edge(11,13) ;
    G2.edge(10,13) ;
    if (VERBOSE) G2.print() ;
    System.out.println("CONNECTED: " + G2.connected()) ;
    System.out.println("PATH 0 TO 0: " + G2.path(0,0) ) ;
    System.out.println("PATH 0 TO 5: " + G1.path(0,5) ) ;
    System.out.println("PATH 3 TO 8: " + G2.path(3,8) ) ;
    System.out.println("PATH 0 TO 12: " + G2.path(0,12) ) ;

    System.out.println("========================= G3 =========================") ;
    Graph G3 = new Graph(8) ;
    G3.edge(0,1) ; G3.edge(0,4) ; G3.edge(1,2) ; G3.edge(2,3) ; G3.edge(2,4) ;
    G3.edge(3,7) ; G3.edge(4,5) ; G3.edge(5,6) ; G3.edge(6,7) ;
    if (VERBOSE) G3.print() ;
    System.out.println("TWO-COLOR: " + G3.twoColor()) ;
    System.out.println("THREE-COLOR: " + G3.threeColor()) ;

    System.out.println("========================= G4 =========================") ;
    Graph G4 = new Graph(7) ;
    G4.edge(0,1) ; G4.edge(0,5) ; G4.edge(1,2) ; G4.edge(2,3) ; G4.edge(2,5) ;
    G4.edge(3,4) ; G4.edge(5,6) ; G4.edge(4,6) ;
    if (VERBOSE) G4.print() ;
    System.out.println("TWO-COLOR: " + G4.twoColor()) ;
    System.out.println("THREE-COLOR: " + G4.threeColor()) ;

    System.out.println("========================= G5 =========================") ;
    Graph G5 = new Graph(5) ;
    G5.edge(0,1) ; G5.edge(1,2) ; G5.edge(2,3) ; G5.edge(3,4) ; G5.edge(4,0) ;
    G5.edge(1,3) ; G5.edge(1,4) ; G5.edge(2,4) ;
    if (VERBOSE) G5.print() ;
    System.out.println("TWO-COLOR: " + G5.twoColor()) ;
    System.out.println("THREE-COLOR: " + G5.threeColor()) ;

    System.out.println("========================= G6 =========================") ;
    Graph G6 = new Graph(6) ;
    G6.edge(0,1) ;
    G6.edge(1,2) ;
    G6.edge(2,0) ;
    G6.edge(0,3) ;
    G6.edge(1,3) ;
    G6.edge(1,4) ;
    G6.edge(2,4) ;
    G6.edge(0,5) ;
    G6.edge(2,5) ;
    if (VERBOSE) G6.print() ;
    System.out.println("TWO-COLOR: " + G6.twoColor()) ;
    System.out.println("THREE-COLOR: " + G6.threeColor()) ;

  }

}

/// End-of-File