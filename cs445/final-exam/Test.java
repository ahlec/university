public class Test
{
  public static void main(String[] args)
  {
    Graph graph = new Graph(5);
    graph.edge(0, 1);
    graph.edge(1, 4);
    graph.edge(4, 3);

    graph.print();
    System.out.println("Connected? " + graph.connected());
    System.out.println("0 -> 3 connected? " + graph.path(0, 3));
    System.out.println("0 -> 4 connected? " + graph.path(0, 4));
    System.out.println("0 -> 2 connected? " + graph.path(0, 2));
    System.out.println("Twocolor? " + graph.twoColor());
  }
}
