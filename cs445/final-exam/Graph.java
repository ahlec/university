///
/// Contents: Simple Graph Data Structure
/// Author:   Jacob Deitloff (jbd19@pitt.edu)
/// Date:     24. April 2012
///

import java.util.ArrayList;

public class Graph
{
  private int _capacity;
  private ArrayList<ArrayList<Integer>> _vertices;

  public Graph(int s)
  {
    _capacity = s;
    _vertices = new ArrayList<ArrayList<Integer>>(s);
    for (int vertex = 0; vertex < _capacity; vertex++)
    {
      _vertices.add(new ArrayList<Integer>());
    }
  }

  public void edge(int a, int b)
  {
    _vertices.get(a).add(b);
    _vertices.get(b).add(a);
  }

  /*public ArrayList<Integer> path(int start, int goal)
  {
    return path_recursor(start, goal, new ArrayList<Integer>());
  }

  private ArrayList<Integer> path_recursor(int start, int goal, ArrayList<Integer> pathHere)
  {
    if (start == goal)
    {
      ArrayList<Integer> baseCase = new ArrayList<Integer>();
      baseCase.add(start);
      return baseCase;
    }

    ArrayList<Integer> _path = null;
    ArrayList<Integer> _pathHere_temp;
    for (int neighbor : _vertices.get(start))
    {
      if (pathHere.contains(neighbor))
      {
        continue;
      }
      _pathHere_temp = new ArrayList<Integer>(pathHere);
      _pathHere_temp.add(neighbor);
      _path = path_recursor(neighbor, goal, _pathHere_temp);
      if (_path != null)
      {
        _path.add(0, start);
        break;
      }
    }

    return _path;
  }*/

  public ArrayList<Integer> path(int start, int goal)
  {
    return path_recursor(start, goal, new ArrayList<Integer>());
  }
  private ArrayList<Integer> path_recursor(int start, int goal, ArrayList<Integer> progress)
  {  
    progress.add(start);
	
	if (start == goal)
	{
	  return progress;
	}
	
    ArrayList<Integer> _toReturn = null;
	ArrayList<Integer> _recursed = null;
	for (int neighbor : _vertices.get(start))
	{
	  if (progress.contains(neighbor))
	  {
	    continue;
	  }
	  
	  _recursed = path_recursor(neighbor, goal, new ArrayList<Integer>(progress));
	  if (_recursed != null)
	  {
	    if (_toReturn == null || _recursed.size() < _toReturn.size())
		{
		  _toReturn = new ArrayList<Integer>(_recursed);
		}
	  }
	  
	}
	
	return _toReturn;
  }

  public boolean connected()
  {
    for (int a = 0; a < _capacity - 1; a++)
    {
      for (int b = 1; b < _capacity; b++)
      {
        if (path(a, b) == null)
        {
          return false;
        }
      }
    }
    return true;
  } 

  public ArrayList<Character> twoColor()
  {
    ArrayList<Character> nodes = new ArrayList<Character>(_capacity);
    for (int vertex = 0; vertex < _capacity; vertex++)
    {
      nodes.add('0');
    }

    nodes.set(0, 'R');

    for (int vertex = 0; vertex < _capacity; vertex++)
    {
      for (int neighbor : _vertices.get(vertex))
      {
        if (nodes.get(vertex).equals('0') && nodes.get(neighbor).equals('R'))
        {
          nodes.set(vertex, 'B');
        }
        else if (nodes.get(vertex).equals('0') && nodes.get(neighbor).equals('B'))
        {
          nodes.set(vertex, 'R'); 
        }
        else if (nodes.get(vertex).equals('R') && nodes.get(neighbor).equals('0'))
        {
          nodes.set(neighbor, 'B');
        }
        else if (nodes.get(vertex).equals('B') && nodes.get(neighbor).equals('0'))
        {
          nodes.set(neighbor, 'R');
        } else if (!nodes.get(vertex).equals('0') && nodes.get(vertex).equals(nodes.get(neighbor)))
        {
          return null;
        }
      }
    }

    return nodes;
  }

  public ArrayList<Character> threeColor()
  {
    ArrayList<Character> nodes = new ArrayList<Character>(_capacity);
    for (int vertex = 0; vertex < _capacity; vertex++)
    {
       nodes.add('0');
    }
    nodes.set(0, 'R');
	
	for (int vertex = 1; vertex < _capacity; vertex++)
	{
	  boolean hasNeighborR = false;
	  boolean hasNeighborG = false;
	  boolean hasNeighborB = false;
	  for (int neighbor : _vertices.get(vertex))
	  {
	    switch (nodes.get(neighbor))
		{
		  case 'R': hasNeighborR = true; break;
		  case 'G': hasNeighborG = true; break;
		  case 'B': hasNeighborB = true; break;
		}
	  }
		
	  if (hasNeighborR && hasNeighborG && hasNeighborB)
	  {
	    return null;
	  }
	
	  if (!hasNeighborR && !nodes.get(vertex).equals('R'))
	  {
	    nodes.set(vertex, 'R');
	  }
	  else if (!hasNeighborB && !nodes.get(vertex).equals('B'))
	  {
	    nodes.set(vertex, 'B');
	  }
	  else if (!hasNeighborG && !nodes.get(vertex).equals('G'))
	  {
	    nodes.set(vertex, 'G');
	  }
	}

    /*for (int vertex = 0; vertex < _capacity; vertex++)
    {
      for (int neighbor : _vertices.get(vertex))
      {
        if (nodes.get(vertex).equals('0') && nodes.get(neighbor).equals('R'))
        {
          nodes.set(vertex, 'B');
        }
        else if (nodes.get(vertex).equals('0') && nodes.get(neighbor).equals('B'))
        {
          nodes.set(vertex, 'R'); 
        }
        else if (nodes.get(vertex).equals('R') && nodes.get(neighbor).equals('0'))
        {
          nodes.set(neighbor, 'B');
        }
        else if (nodes.get(vertex).equals('B') && nodes.get(neighbor).equals('0'))
        {
          nodes.set(neighbor, 'R');
        } else if (!nodes.get(vertex).equals('0') && nodes.get(vertex).equals(nodes.get(neighbor)))
        {
			boolean vertexR = false;
			boolean vertexG = false;
			boolean vertexB = false;
			switch (nodes.get(vertex))
			{
			    case 'R': vertexR = true; break;
				case 'G': vertexG = true; break;
				case 'B': vertexB = true; break;
			}
			for (int vertexNeighbor : _vertices.get(vertex))
			{
			  if (vertexNeighbor == neighbor)
			  {
			    continue;
			  }
			  switch (nodes.get(vertexNeighbor))
			  {
			    case 'R': vertexR = true; break;
				case 'G': vertexG = true; break;
				case 'B': vertexB = true; break;
			  }
			}
			if (vertexR && vertexG && vertexB)
			{
			  return null;
			}
			
			boolean neighborR = false;
			boolean neighborG = false;
			boolean neighborB = false;
			switch (nodes.get(neighbor))
			{
			    case 'R': neighborR = true; break;
				case 'G': neighborG = true; break;
				case 'B': neighborB = true; break;
			}
			for (int neighborNeighbor : _vertices.get(neighbor))
			{
			    if (neighborNeighbor == vertex)
				{
				  continue;
				}
				switch (nodes.get(neighborNeighbor))
				{
					case 'R': neighborR = true; break;
					case 'G': neighborG = true; break;
					case 'B': neighborB = true; break;
				}
			}
			if (neighborR && neighborG && neighborB)
			{
			  return null;
			}
			
			if (!vertexR && !neighborB)
			{
			   nodes.set(vertex, 
			}
        }
      }
    }

    return nodes;*/
	return nodes;
  }

  public void print()
  {
    System.out.println("VERTICES:");
    System.out.println(" 0..." + (_capacity - 1));
    System.out.println("NEIGHBORS:");
    for (int vertex = 0; vertex < _capacity; vertex++)
    {
      System.out.print(" " + vertex + ": ");
      if (_vertices.get(vertex).size() == 0)
      {
        System.out.print("(none)");
      }
      for (int index = 0; index < _vertices.get(vertex).size(); index++)
      {
        System.out.print(_vertices.get(vertex).get(index) + (index < _vertices.get(vertex).size() - 1 ? " " : ""));
      }
      System.out.println("");
    }
  }
  
}

/// End-of-File

