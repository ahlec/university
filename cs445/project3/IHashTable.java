public interface IHashTable
{
  void add(String word) throws Exception;
  boolean contains(String word);
  boolean containsPrefix(String prefix);
  void tellLongestWord(String word);
  int hash(String word);
}
