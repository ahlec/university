import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Test
{
  public static void main(String[] args)
  {
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    BetterHashTable hashTable = new BetterHashTable();
    while(true)
    {
      System.out.print("Input word:");
      String input;
      try
      {
        input = bufferedReader.readLine().toLowerCase();
      } catch(IOException exception)
      {
        System.err.println("Reading exception. Abort.");
        return;
      }
      System.out.println("  HashCode: " + hash(input));
    }
  }

  public static int hash(String word)
  {
    int hashCode = 0;
    for (int index = 0; index < word.length(); index++)
      hashCode += Math.pow(26, word.length() - index - 1) * (word.charAt(index) - 96);
    hashCode -= 1; // set start of hash function at index = 0;
    return hashCode;
  }
}
