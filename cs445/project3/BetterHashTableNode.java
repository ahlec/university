public class BetterHashTableNode
{
  String value;
  boolean isAWord;
  public BetterHashTableNode(String value)
  {
    this.value = value;
  }
  
  public String getValue() { return value; }
  public boolean isAWord() { return isAWord; }

  public void setIsWord(boolean value) { isAWord = value; }

  public String toString() { return "[" + value + ", " + (isAWord ? "word" : "prefix") + "]"; }

  public BetterHashTableNode Next;
}
