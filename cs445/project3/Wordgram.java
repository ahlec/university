import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Wordgram
{
  static Dictionary dictionary;

  public static void main(String[] args)
  {
    IHashTable hashTable = null;
    String dictionaryFile = null;
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equals("-h") || args[index].equals("--help"))
      {
        System.out.println("Wordgram (CS0445 Project 3)");
        System.out.println("  Designed by Jacob Deitloff (jbd19@pitt.edu)");
        System.out.println("");
        System.out.println("  Running the program normally, input strings of characters (latin alphabet characters only). Input of an empty string (an input of no characters) will result in closure of the program.");
        System.out.println("");
        System.out.println("  -h");
        System.out.println("  --help    Brings up this help menu.");
        return;
      } else if (args[index].equals("-1"))
        hashTable = new HashTable();
      else if (args[index].equals("-2"))
        hashTable = new BetterHashTable();
      else if (args[index].endsWith(".txt"))
        dictionaryFile = args[index];
    }
    if (hashTable == null)
      hashTable = new HashTable();
    if (dictionaryFile == null)
      dictionaryFile = "Words.txt";

    System.out.println("Welcome to the Wordgram program.");
    try
    {
      dictionary = new Dictionary(dictionaryFile, hashTable);
    } catch (IOException exception)
    {
      System.err.println("Could not load the dictionary file '" + dictionaryFile + "'. Program is aborting.");
      return;
    }

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    while (true)
    {
      System.out.print("Enter Wordgram: ");

      /* Get input */
        String inputWordgram;
        try
        {
          inputWordgram = bufferedReader.readLine().toLowerCase();
        } catch (IOException exception)
        {
          System.err.println("There was a problem with reading from the input line. Program is aborting.");
          return;
        }
      
      /* Abort the program? */
        if (inputWordgram.equals(""))
        {
          System.out.println("Goodbye.");
          return;
        }

      if (inputWordgram.matches("^[a-z]*$"))
        System.out.println(dictionary.getWordgrams(inputWordgram));
      else
        System.err.println("Invalid input string ('" + inputWordgram + "'). Input must be only latin alphabet characters.");
    } 
  }
}
