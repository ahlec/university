import java.util.ArrayList;

public class HashTable implements IHashTable
{
  HashTable[] hashTables;
  boolean isAWord = false;

  public HashTable()
  {
    hashTables = new HashTable[26];
  }

  public void tellLongestWord(String word)
  {
    // we don't need any functionality like this here
  }
  public int hash(String word)
  {
    return word.length(); // just anything; this functionality isn't really needed here either
  }
  public void add(String word)
  {
    add(word, word); 
  }
  public void add(String word, String remainingWord)
  {
    /* Base case */
    if (remainingWord.length() == 0)
    {
      isAWord = true;
      return;
    }

    if (hashTables[remainingWord.charAt(0) - 97] == null)
      hashTables[remainingWord.charAt(0) - 97] = new HashTable();
    hashTables[remainingWord.charAt(0) - 97].add(word, remainingWord.substring(1));
  }

  public boolean contains(String word)
  {
    return contains(word, word, false);
  }
  public boolean containsPrefix(String prefix)
  {
    return contains(prefix, prefix, true);
  }
  public boolean contains(String word, String remainingWord, boolean lookingForPrefix)
  {
    /* Base case */
    if (remainingWord.length() == 0)
    {
      if (lookingForPrefix)
        return true; // we got here, eh?
      return isAWord;
    }

    if (hashTables[remainingWord.charAt(0) - 97] == null)
      return false;
    return hashTables[remainingWord.charAt(0) - 97].contains(word, remainingWord.substring(1), lookingForPrefix);
  }

  public String toString()
  {
    return "[HashTable]";
  }
}
