import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Dictionary
{
  private IHashTable hashTable;

  public Dictionary(String dictionaryFilename, IHashTable hashTable) throws IOException
  {
    BufferedReader bufferedReader = new BufferedReader(new FileReader(dictionaryFilename));
    this.hashTable = hashTable;

    ArrayList<String> tempList = new ArrayList<String>();
    String inputLine;
    String longestWord = "";
    while ((inputLine = bufferedReader.readLine()) != null)
    {
      tempList.add(inputLine);
      if (inputLine.length() >= longestWord.length() && this.hashTable.hash(inputLine) > this.hashTable.hash(longestWord))
        longestWord = inputLine;
    }
    this.hashTable.tellLongestWord(longestWord);
    for (String str : tempList)
    {
      try
      {
        this.hashTable.add(str);
      } catch (Exception e)
      {
        System.err.println(str.toUpperCase());
        System.err.println(e);
        e.printStackTrace();
        System.exit(1);
        return;
      }
    }

    bufferedReader.close();
    System.out.println(this.hashTable);
  }

  public ArrayList<String> getWordgrams(String inputString)
  {
    ArrayList<String> results = getWordgram("", inputString.toCharArray());
    if (results.size() == 0)
      return results;
    ArrayList<String> distinctResults = new ArrayList<String>(results.size());
    for (int index = 0; index < results.size(); index++)
      if (!distinctResults.contains(results.get(index)))
        distinctResults.add(results.get(index));
    return distinctResults;
  }
  
  private ArrayList<String> getWordgram(String prefix, char[] remainingCharacters)
  {
    ArrayList<String> arrayList = new ArrayList<String>();

    /* Base case */
    if (remainingCharacters.length == 0)
    {
      if (hashTable.contains(prefix))
        arrayList.add(prefix);
      return arrayList;
    }

    /* Recursion */
    for (int index = 0; index < remainingCharacters.length; index++)
    {
      if (!hashTable.containsPrefix(prefix + remainingCharacters[index]))
        continue;
      char[] otherCharacters = new char[remainingCharacters.length - 1];
      int otherCharIndex = 0;
      for (int charIndex = 0; charIndex < remainingCharacters.length; charIndex++)
      {
        if (charIndex == index)
          continue;
        otherCharacters[otherCharIndex++] = remainingCharacters[charIndex];
      }
      arrayList.addAll(getWordgram(prefix + remainingCharacters[index], otherCharacters));
    }
    return arrayList;
  }

  public boolean prefixValid(String prefix)
  {
    return hashTable.containsPrefix(prefix);
  }
}
