import java.util.ArrayList;
import java.util.Collections;

public class BetterHashTable implements IHashTable
{
  private int HASH_TABLE_SIZE = 4999;
  BetterHashTableNode[] hashTable;
  //ArrayList<BetterHashTableNode> hashTable;

  public BetterHashTable()
  {
    hashTable = new BetterHashTableNode[HASH_TABLE_SIZE];
    //hashTable = new ArrayList<BetterHashTableNode>();
  }

  public int hash(String word)
  {
    int hashCode = 0;
    for (int index = 0; index < word.length(); index++)
      hashCode += Math.pow(26, word.length() - index - 1) * (word.charAt(index) - 96);
    hashCode -= 1; // set start of hash function at index = 0;
    return hashCode % HASH_TABLE_SIZE;
  }
  public void tellLongestWord(String word)
  {
    /*int hashCode = hash(word);
    hashTable.addAll(Collections.<BetterHashTableNode>nCopies(hashCode + 1, null));
    System.out.println(word);
    System.out.println(hashCode);
    System.out.println("------------------");*/
  }

  public void add(String word) throws Exception
  {
   int hashCode = hash(word);
   if (hashTable[hashCode] == null)
   {
     hashTable[hashCode] = new BetterHashTableNode(word)
   }
   // add(word, "", word); 
  }
  void add(String word, String wordSoFar, String remainingWord) throws Exception
  {


    /* Base case */
    if (remainingWord.length() == 0 && wordSoFar.equals(word))
    {
      int hashCode = hash(word);
      while (hashTable[hashCode] != null)
      {
        if (hashTable.get(hashCode).getValue().equals(word))
          break;
        hashCode++;
      }
      if (hashTable.get(hashCode) == null)
      {
        if (hashTable.get(hashCode) == null)
          hashTable.set(hashCode, new BetterHashTableNode(word));
      }
      hashTable.get(hashCode).setIsWord(true);
      return;
    } else if (remainingWord.length() == 0)
      throw new Exception("Error has arisen while attempting to add the word.");

    wordSoFar += remainingWord.substring(0, 1);
    remainingWord = remainingWord.substring(1);
    int hashCode = hash(wordSoFar);
    
    while (hashTable.get(hashCode) != null && !hashTable.get(hashCode).getValue().equals(wordSoFar))
      hashCode++;
    if (hashTable.get(hashCode) == null)
      hashTable.set(hashCode, new BetterHashTableNode(wordSoFar));//hashCode, new BetterHashTableNode(wordSoFar));
    add(word, wordSoFar, remainingWord);    
  }

  public boolean contains(String word)
  {
    return contains(word, "", word, false);
  }
  public boolean containsPrefix(String prefix)
  {
    return contains(prefix, "", prefix, true);
  }
  boolean contains(String word, String wordSoFar, String remainingWord, boolean lookingForPrefix)
  {


 /*  for (int index = 0; index < hashTable.size(); index++)
    {
       System.out.println("Index: " + index + ", " + hashTable.get(index));
    }
   System.exit(1);
*/
    /* Base case */
    if (remainingWord.length() == 0)
    {
      int hashCode = hash(word);
      if (hashCode >= hashTable.size())
      {
        System.out.println(wordSoFar + ", Base Case, False 1");
        return false;
      }
      while (hashTable.get(hashCode) != null && !hashTable.get(hashCode).getValue().equals(word))
      {
        hashCode++;
        if (hashCode >= hashTable.size())
        {
          System.out.println(wordSoFar + ", Base Case, False 2");
          return false;
        }
      }
      if (hashTable.get(hashCode) == null)
      {
        System.out.println(wordSoFar + ", Base Case, " + hashCode + ", False 3");
        return false; // nothing is here
      }
      if (lookingForPrefix)
        return true; // we got here, eh?
      return hashTable.get(hashCode).isAWord();
    }

    wordSoFar += remainingWord.substring(0, 1);
    remainingWord = remainingWord.substring(1);
    int hashCode = hash(wordSoFar);

    if (hashCode >= hashTable.size())
    {
      System.out.println(wordSoFar + ", " + hashCode + ", Abort 1");
      return false;
    }
    while (hashTable.get(hashCode) != null && !hashTable.get(hashCode).getValue().equals(wordSoFar))
    {
      hashCode++;
      if (hashCode >= hashTable.size())
      {
        System.out.println(wordSoFar + ", Abort 2");
        return false;
      }
    }
    return contains(word, wordSoFar, remainingWord, lookingForPrefix);
  }

  public String toString()
  {
    return "[BetterHashTable. Size: " + hashTable.size() + "]";
  }
}
