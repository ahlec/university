import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Random;

public class Generator
{
  static Dictionary dictionary;
  static Random random;

  public static void main(String[] args)
  {
    IHashTable hashTable = new HashTable();
    String dictionaryFile = null;
    random = new Random();
    for (int index = 0; index < args.length; index++)
      if (args[index].endsWith(".txt"))
        dictionaryFile = args[index];
    if (dictionaryFile == null)
      dictionaryFile = "Words.txt";

    System.out.println("Welcome to the Wordgram word generator program.");
    System.out.println("Please be nice. The generator at least tries to get it right.");
    System.out.println("  (it just doesn't always get it right)");
    try
    {
      dictionary = new Dictionary(dictionaryFile, hashTable);
    } catch (IOException exception)
    {
      System.err.println("Could not load the dictionary file '" + dictionaryFile + "'. Program is aborting.");
      return;
    }

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    String generatedWord = null;
    boolean wordGenerated = false;
    while (true)
    {
      wordGenerated = false;
      while(!wordGenerated)
      {
        try
        {
          generatedWord = generateWord();
          wordGenerated = true;
        } catch (Exception e)
        {
          System.err.println("  > Timed out when generating word. Trying again.");
        }
      }
      System.out.println(generatedWord);

      /* Get input */
        String inputWordgram = "";
        do
        {
          try
          {
            System.out.print("Generate another word? (y/N): ");
            inputWordgram = bufferedReader.readLine();
          } catch (IOException exception)
          {
            System.err.println("There was a problem with reading from the input line. Program is aborting.");
            return;
          }
        } while (!inputWordgram.equals("y") && !inputWordgram.equals("N"));

      /* Abort the program? */
        if (inputWordgram.equals("N"))
        {
          System.out.println("Goodbye.");
          return;
        }
    } 
  }

  public static String generateWord() throws Exception
  {
    return generateWord("", 0);
  }
  static String generateWord(String wordSoFar, int lengthSoFar) throws Exception
  {
    if (lengthSoFar > 3 && random.nextBoolean())
      return wordSoFar;
    String lastCharacter = (wordSoFar.length() == 0 ? "" : wordSoFar.substring(wordSoFar.length() - 1));
    char newCharacter = '1'; // no prefix in here will match anything containing a '1', so we can use this as an empty character
    int generationCounter = 0;
    do
    {
      generationCounter++;
      if (generationCounter > 50)
        throw new Exception();
      newCharacter = (char)(random.nextInt(26) + 97);
    } while (!dictionary.prefixValid(lastCharacter + Character.toString(newCharacter)));

    return generateWord(wordSoFar + Character.toString(newCharacter), lengthSoFar + 1);
  }
}
