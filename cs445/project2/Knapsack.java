public class Knapsack
{
  private Item[] items;
  private int weight, value;
  public Knapsack()
  {
    items = new Item[0];
  }
  public Knapsack(Item[] items)
  {
    this.items = items;
    for (int index = 0; index < items.length; index++)
    {
      weight += items[index].getWeight();
      value += items[index].getValue();
    }
  }
  public Knapsack(Knapsack prior, Item newItem)
  {
    this.items = new Item[prior.items.length + 1];
    for (int index = 0; index < prior.items.length; index++)
    {
      items[index] = prior.items[index];
      weight += items[index].getWeight();
      value += items[index].getValue();
    }
    items[items.length - 1] = newItem;
    weight += newItem.getWeight();
    value += newItem.getValue();
  }
  public int getWeight() { return weight; }
  public int getValue() { return value; }
  public String toString()
  {
    String output = "";
    for (int index = 0; index < items.length; index++)
      output += items[index].getHandle() + " ";
    return output + " (" + value + ")";
  }
}
