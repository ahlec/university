public class Item
{
  private String handle;
  private int weight, value;
  public Item(String handle, int weight, int value)
  {
    this.handle = handle;
    this.weight = weight;
    this.value = value;
  }
  public String getHandle() { return handle; }
  public int getWeight() { return weight; }
  public int getValue() { return value; }
}
