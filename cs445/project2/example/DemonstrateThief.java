/// 
/// Contents: Demonstrate solution to Knapsack Problem.
/// Author:   John Aronis
/// Date:     January 2012
///

public class DemonstrateThief {

  public static void main(String[] args) {
    ///
    /// Set up experiment:
    ///
    int INITIAL_CAPACITY = 1 ;
    int INCREMENT = 1 ;
    int MAX_CAPACITY = 100 ;

    ///
    /// Create objects with add(NAME,SIZE,VALUE):
    ///
    Thief.initialize() ;
    Thief.add("A",3,4) ;
    Thief.add("B",4,5) ;
    Thief.add("C",7,10) ;
    Thief.add("D",8,11) ;
    Thief.add("E",9,13) ;

    ///
    /// Run experiment:
    ///
    Thief.Knapsack knapsack ;
    for (int capacity=INITIAL_CAPACITY ; capacity<=MAX_CAPACITY ; capacity+=INCREMENT) {
      System.out.println("Capacity: " + capacity) ;
      Thief.MEMOIZE = true ;
      Thief.CALLS = 0 ;
      knapsack = Thief.pack(capacity) ;
      System.out.println( "  Memoized recursive calls: " + Thief.CALLS ) ;
      Thief.MEMOIZE = false ;
      Thief.CALLS = 0 ;
      knapsack = Thief.pack(capacity) ;
      System.out.println( "  Nonmemoized recursive calls: " + Thief.CALLS ) ;
      System.out.println( "  Knapsack: " + knapsack ) ;
    }
  }

}

/// End-of-File
