import java.util.ArrayList;
public class Thief
{
  Knapsack[] memoized = new Knapsack[0];
  ArrayList<Item> availableItems = new ArrayList<Item>();
  public void addItem(Item item)
  {
    availableItems.add(item);
  }
  public Knapsack pack(int capacity)
  {
    if (MEMOIZE && memoized.length > capacity && memoized[capacity - 1] != null)
      return memoized[capacity - 1];
    CALLS++;
    ArrayList<Knapsack> knapsacks = new ArrayList<Knapsack>();
    for (int index = 0; index < availableItems.size(); index++)
    {
      if (capacity - availableItems.get(index).getWeight() == 0)
      {
        Item[] soleItem = new Item[1];
        soleItem[0] = availableItems.get(index);
        knapsacks.add(new Knapsack(soleItem));
      } else if (capacity - availableItems.get(index).getWeight() > 0)
      {
        knapsacks.add(new Knapsack(pack(capacity - availableItems.get(index).getWeight()),
		availableItems.get(index)));
      }
    }
    if (knapsacks.size() == 0)
    {
      if (MEMOIZE)
        memoize(capacity, new Knapsack());
      return new Knapsack();
    }
    Knapsack bestKnapsack = knapsacks.get(0);
    for (int index = 1; index < knapsacks.size(); index++)
    {
      if (knapsacks.get(index).getValue() > bestKnapsack.getValue())
        bestKnapsack = knapsacks.get(index);
    }
    if (MEMOIZE)
      memoize(capacity, bestKnapsack);
    return bestKnapsack;
  }
  public void clearCache() { memoized = new Knapsack[0]; }
  private void memoize(int capacity, Knapsack knapsack)
  {
    if (capacity < memoized.length)
    {
      memoized[capacity - 1] = knapsack;
      return;
    }
    Knapsack[] newMemoized = new Knapsack[capacity];
    for (int index = 0; index < memoized.length; index++)
      newMemoized[index] = memoized[index];
    newMemoized[capacity - 1] = knapsack;
    memoized = newMemoized;
  }
  public boolean MEMOIZE;
  public int CALLS;
}
