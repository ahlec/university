public class project2
{
  public static void main(String[] args)
  {
    int startCapacity, endCapacity;
    if (args.length != 2 || !isInteger(args[0]) || !isInteger(args[1]) ||
	Integer.parseInt(args[0]) > Integer.parseInt(args[1]) ||
	Integer.parseInt(args[0]) <= 0 || Integer.parseInt(args[1]) <= 0)
    {
      System.err.println("Requires two parameters to be run: the capacity at which to start, and the capacity at which to end. Both of these must be nonzero, nonnegative integers, and the first parameter (start capacity) must be less than or equal to the second parameter (end capacity).");
      return;
    }
    startCapacity = Integer.parseInt(args[0]);
    endCapacity = Integer.parseInt(args[1]);
    Thief thief = new Thief();
    thief.addItem(new Item("A", 3, 4));
    thief.addItem(new Item("B", 4, 5));
    thief.addItem(new Item("C", 7, 10));
    thief.addItem(new Item("D", 8, 11));
    thief.addItem(new Item("E", 9, 13));
    
    for (int index = startCapacity; index <= endCapacity; index++)
    {
      thief.clearCache();
      System.out.println("Capacity: " + index);
      thief.MEMOIZE = true;
      thief.CALLS = 0;
      Knapsack capacityKnapsack = thief.pack(index);
      System.out.println("  - Memoized calls:     " + thief.CALLS);
      thief.MEMOIZE = false;
      thief.CALLS = 0;
      thief.pack(index);
      System.out.println("  - Nonmemoized calls:  " + thief.CALLS);
      System.out.println("  - Knapsack:           " + capacityKnapsack);
    }
  }
  public static boolean isInteger(String input)
  {
    try
    {
      Integer.parseInt(input);
    } catch(NumberFormatException e)
    {
      return false;
    }
    return true;
  }
}
