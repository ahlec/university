import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DisplayPanel extends JPanel
{
  private Location _current;
  private Maze _maze;
  private int _tileSize = 30;
  private ArrayList<Location> _visited;

  public DisplayPanel(Maze maze)
  {
    _maze = maze;
    setPreferredSize(new Dimension(_tileSize * _maze.getWidth(),
	_tileSize * _maze.getHeight()));
    _visited = new ArrayList<Location>();
  }

  public void setCurrent(Location current, ArrayList<Location> visited)
  {
    _current = current;
    _visited = visited;
  }

  public void paintComponent(Graphics g)
  {
    for (int y = 0; y < _maze.getHeight(); y++)
      for (int x = 0; x < _maze.getWidth(); x++)
      {
        if (_visited.contains(new Location(x, y)))
        {
          g.setColor(Color.cyan);
          g.fillRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
        }
        switch (_maze.getTile(x, y))
        {
          case Goal:
          {
            g.setColor(Color.yellow);
            g.fillRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
            break;
          }
          case Wall:
          {
            g.setColor(Color.black);
            g.fillRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
            break;
          }
          case Start:
          {
            g.setColor(Color.green);
            g.fillRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
            break;
          }
        }
        if (_current != null && x == _current.getX() && y == _current.getY())
        {
          g.setColor(Color.blue);
          g.fillOval(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
        }
        if (_maze.getTile(x, y) != MazeTiles.Wall)
        {
          g.setColor(Color.black);
          g.drawRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
        }
      }
  }

}
