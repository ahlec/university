import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Maze
{
  private int _width;
  private int _height;
  private MazeTiles[][] _tiles;
  private Location _start;
  private Location _goal;

  public static Maze Load(String filepath) throws IOException
  {
    Maze parsed = new Maze();
    BufferedReader file = new BufferedReader(new FileReader(filepath));
    String fileLine = file.readLine();
    ArrayList<MazeTiles> tiles = new ArrayList<MazeTiles>();
    parsed._width = fileLine.length();
    do
    {
      for (Character tile : fileLine.toCharArray())
      {
        switch (tile)
        {
          case '*': tiles.add(MazeTiles.Wall); break;
          case ' ': tiles.add(MazeTiles.Empty); break;
          case 'G': tiles.add(MazeTiles.Goal); break;
          case 'S': tiles.add(MazeTiles.Start); break;
          default: throw new IOException();
        }
      }
    } while ((fileLine = file.readLine()) != null);
    parsed._height = tiles.size() / parsed._width;  

    parsed._tiles = new MazeTiles[parsed._width][parsed._height];
    for (int y = 0; y < parsed._height; y++)
      for (int x = 0; x < parsed._width; x++)
      {
        parsed._tiles[x][y] = tiles.get(y * parsed._width + x);
        if (parsed._tiles[x][y] == MazeTiles.Start)
        {
          parsed._start = new Location(x, y);
        }
        if (parsed._tiles[x][y] == MazeTiles.Goal)
        {
          parsed._goal = new Location(x, y);
        }
      }

    return parsed;
  }

  public int getWidth() { return _width; }
  public int getHeight() { return _height; }
  public MazeTiles getTile(int x, int y) { return _tiles[x][y]; }
  public Location getStart() { return _start; }
  public Location getGoal() { return _goal; }

  private ArrayList<Location> _getNext_arrays = new ArrayList<Location>();
  public Location[] getNext(int x, int y, ArrayList<Location> previouslyVisited)
  {
    _getNext_arrays.clear();
    
    if (x + 1 < _width && _tiles[x + 1][y] != MazeTiles.Wall && !previouslyVisited.contains(new Location(x + 1, y)))
      _getNext_arrays.add(new Location(x + 1, y, this));
    if (x - 1 >= 0 && _tiles[x - 1][y] != MazeTiles.Wall && !previouslyVisited.contains(new Location(x - 1, y)))
      _getNext_arrays.add(new Location(x - 1, y, this));
    if (y + 1 < _height && _tiles[x][y + 1] != MazeTiles.Wall && !previouslyVisited.contains(new Location(x, y + 1)))
      _getNext_arrays.add(new Location(x, y + 1, this));
    if (y - 1 >= 0 && _tiles[x][y - 1] != MazeTiles.Wall && !previouslyVisited.contains(new Location(x, y - 1)))
      _getNext_arrays.add(new Location(x, y - 1, this));

    Location[] returnArray = new Location[_getNext_arrays.size()];
    _getNext_arrays.toArray(returnArray);
    return returnArray;
  }
}
