public class Location implements Comparable<Location>
{
  private int _x;
  private int _y;
  private int _manhattanDistance;

  public Location(int x, int y)
  {
    _x = x;
    _y = y;
  }
  public Location(int x, int y, Maze maze)
  {
    _x = x;
    _y = y;
    _manhattanDistance = Math.abs(maze.getGoal().getX() - x) + Math.abs(maze.getGoal().getY() - y);
  }
  public int getX() { return _x; }
  public int getY() { return _y; }
  public boolean equals(Object o)
  {
    Location loc = (Location)o;
    return (loc._x == _x && loc._y == _y);
  }

  public int compareTo(Location loc)
  {
    if (loc._manhattanDistance == _manhattanDistance)
      return 0;
    return (_manhattanDistance < loc._manhattanDistance ? 1 : -1); 
  }
}
