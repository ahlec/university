public class Test
{
  public static void main(String[] args)
  {
    Stack<String> stack = new Stack<String>();
    Queue<String> queue = new Queue<String>();
    Heap<String> heap = new Heap<String>(10);
    for (String str : args)
    {
      stack.push(str);
      queue.enqueue(str);
      heap.add(str);
    }

    System.out.println("Heap:");
    while (true)
    {
      System.out.println(heap.remove());
      if (heap.isEmpty())
        break;
    }

    System.out.println("Stack:");
    while (true)
    {
      System.out.println(stack.pop());
      if (stack.isEmpty())
        break;
    }

    System.out.println("Queue:");
    while (true)
    {
      System.out.println(queue.dequeue());
      if (queue.isEmpty())
        break;
    }
  }
}
