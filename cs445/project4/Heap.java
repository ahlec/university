public class Heap<T extends Comparable<? super T>>
{
  private T[] _array;
  private int _lastIndex;

  public Heap(int initialCapacity)
  {
    @SuppressWarnings("unchecked")
    T[] heapArray = (T[])(new Comparable[initialCapacity + 1]);
    _array = heapArray;
    _lastIndex = 0;    
  }

  public void add(T value)
  {
    _lastIndex++;
    if (_lastIndex == 1)
    {
      _array[_lastIndex] = value;
      return;
    }

    int newIndex = _lastIndex;
    int parentIndex = newIndex / 2;
    while (parentIndex > 0 && value.compareTo(_array[parentIndex]) > 0)
    {
      _array[newIndex] = _array[parentIndex];
      newIndex = parentIndex;
      parentIndex = newIndex / 2;
    }
    _array[newIndex] = value;
  }

  public T remove()
  {
    if (_lastIndex == 0)
      return null;

    T returnValue = _array[1];
    int currentIndex = 1;
    T sinker = _array[_lastIndex];

    while (currentIndex * 2 <= _lastIndex)
    {

      int largerChildIndex = (currentIndex * 2 == _lastIndex ? currentIndex * 2 : (_array[currentIndex * 2].compareTo(_array[currentIndex * 2 + 1]) > 0 ? currentIndex * 2 :
		currentIndex * 2 + 1));

      if (sinker.compareTo(_array[largerChildIndex]) < 0)
      {
        _array[currentIndex] = _array[largerChildIndex];
        currentIndex = largerChildIndex;
      } else
        break;
    }
 
    _array[currentIndex] = sinker;
    _lastIndex--;
    
    return returnValue;
  }

  public T preview()
  {
    if (_lastIndex == 0)
      return null;
    return _array[1];
  }

  public boolean isEmpty()
  {
    return (_lastIndex == 0);
  }

  public void clear()
  {
    for (int index = 0; index <= _lastIndex; index++)
      _array[index] = null;
    _lastIndex = 0;
  }
}
