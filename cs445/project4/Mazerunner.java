import javax.swing.JFrame;
import java.io.IOException;
import java.util.Date;
import javax.swing.border.EmptyBorder;
import java.util.ArrayList;

public class Mazerunner
{
  public static void main(String[] args)
  {
    if (args.length < 1)
    {
      System.err.println("Mazerunner must be run with only the filename " +
	"of the maze that should be loaded, and the one word string for " +
	"the type of search to perform.");
      System.exit(1);
    }
    
    Maze maze = null;
    try
    {
      maze = Maze.Load(args[0]);
    } catch (IOException e)
    {
      System.err.println("The maze file provided ('" + args[0] + "') " +
	"could not be loaded. The program is aborting.");
      System.exit(1);
    }

    if (args.length < 2 || (!args[1].equals("depth") && !args[1].equals("bredth") && !args[1].equals("best")))
    {
      System.err.println("The second parameter provided to Mazerunner should " +
	"be the one word string for the type of search to be performed.");
      System.err.println("  \"depth\": depth-first search");
      System.err.println("  \"bredth\": bredth-first search");
      System.err.println("  \"best\": best-first search");
      System.exit(1);
    }
    String typeOfSearch = args[1];

    ArrayList<Location> locationsVisitedAlready = new ArrayList<Location>();
    Stack<Location> depthFirstSearch = new Stack<Location>();
    Queue<Location> bredthFirstSearch = new Queue<Location>();
    Heap<Location> bestFirstSearch = new Heap<Location>(50); // <-- come back and fix this later

    JFrame frame = new JFrame("Mazerunner: " + args[0]);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    DisplayPanel displayPanel = new DisplayPanel(maze);
    displayPanel.setBorder(new EmptyBorder(0,0,0,0));
    frame.getContentPane().add(displayPanel);
    frame.pack();
    frame.setResizable(false);
    frame.setVisible(true);
    Location currentLocation;
    switch (typeOfSearch)
    {
      case "depth": depthFirstSearch.push(maze.getStart()); break;
      case "bredth": bredthFirstSearch.enqueue(maze.getStart()); break;
      case "best": bestFirstSearch.add(maze.getStart()); break;
    }

    while (true)
    {
      currentLocation = null;
      switch (typeOfSearch)
      {
        case "depth": currentLocation = depthFirstSearch.pop(); break;
        case "bredth": currentLocation = bredthFirstSearch.dequeue(); break;
        case "best": currentLocation = bestFirstSearch.remove(); break;
      }
      locationsVisitedAlready.add(currentLocation);

      if (maze.getTile(currentLocation.getX(), currentLocation.getY()) == MazeTiles.Goal)
      {
        displayPanel.setCurrent(currentLocation, locationsVisitedAlready);
        frame.repaint();
        System.out.println("Found the goal!");
        break;
      }

      Location[] next = maze.getNext(currentLocation.getX(), currentLocation.getY(), locationsVisitedAlready);
      boolean empty = false;
      switch (typeOfSearch)
      {
        case "depth": empty = depthFirstSearch.isEmpty(); break;
        case "bredth": empty = bredthFirstSearch.isEmpty(); break;
        case "best": empty = bestFirstSearch.isEmpty(); break;
      }
      if (next.length == 0 && empty)
      {
        System.err.println("Unable to reach the goal location.");
        displayPanel.setCurrent(currentLocation, locationsVisitedAlready);
        frame.repaint();
        break;
      }
      for (Location nextLocation : next)
      {
        switch (typeOfSearch)
        {
          case "depth": depthFirstSearch.push(nextLocation); break;
          case "bredth": bredthFirstSearch.enqueue(nextLocation); break;
          case "best": bestFirstSearch.add(nextLocation); break;
        }
      }

      displayPanel.setCurrent(currentLocation, locationsVisitedAlready);

      frame.repaint();
      sleep(200);
    }

  }

  public static void sleep(long milliseconds) {
    Date d ;
    long start, now ;
    d = new Date() ;
    start = d.getTime() ;
    do { d = new Date() ; now = d.getTime() ; } while ( (now - start) < milliseconds ) ;
  }
}
