public class Queue<T>
{
  private class QueueNode
  {
    T _contents;
    QueueNode _next;
    public QueueNode(T contents)
    {
      _contents = contents;
      _next = null;
    }
  }

  QueueNode _head, _tail;

  public Queue()
  {
    _head = null;
    _tail = null;
  }

  public void enqueue(T value)
  {
    QueueNode newNode = new QueueNode(value);
    if (_head == null)
    {
      _head = newNode;
      _tail = newNode;
      return;
    }

    _tail._next = newNode;
    _tail = newNode;
  }

  public T dequeue()
  {
    if (_head == null)
    {
      return null;
    }

    T returnValue = _head._contents;

    if (_head == _tail)
    {
      _head = null;
      _tail = null;
    } else
    {
      _head = _head._next;
    }

    return returnValue;
  }

  public T getFront()
  {
    return (_head != null ? _head._contents : null);
  }

  public boolean isEmpty()
  {
    return (_head == null);
  }

  public void clear()
  {
    _head = null;
    _tail = null;
  }
}
