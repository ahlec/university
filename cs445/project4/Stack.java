import java.util.ArrayList;

public class Stack<T>
{
  ArrayList<T> _arrayList = new ArrayList<T>();
  
  public void push(T value)
  {
    _arrayList.add(0, value);
  }
  public T pop()
  {
    if (_arrayList.size() == 0)
    {
      return null;
    }

    T returnValue = _arrayList.get(0);
    _arrayList.remove(0);
    return returnValue;
  }
  public T peek()
  {
    if (_arrayList.size() == 0)
    {
      return null;
    }
    return _arrayList.get(0);
  }
  public boolean isEmpty()
  {
    return (_arrayList.size() == 0);
  }
  public void clear()
  {
    _arrayList.clear();
  }
}
