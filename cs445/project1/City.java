/*
  CS0445 - Spring 2011-2012
  Jacob Deitloff
  jbd19 [at] pitt [dot] edu
  29. Januar 2012
*/
public class City
{
  public City(int x, int y)
  {
    this.x = x;
    this.y = y;
  }
  public City(int x, int y, String identifier)
  {
    this.x = x;
    this.y = y;
    this.identifier = identifier;
  }
  private int x, y;
  private String identifier = null;
  public int getX() { return x; }
  public int getY() { return y; }
  public String getIdentifier() { return identifier; }
  public String toString()
  {
    return "[" + (identifier != null ? identifier + " - " : "") + x + ", " + y + "]"; 
  }
}
