Project 1: The Traveling Salesman
Jacob Deitloff
  jbd19 [at] pitt [dot] edu
Submitted: Sonntag, 29. Januar 2012
------------

The Traveling Salesman Problem is very easy to state: Given a set of 
cities (points in the plane), find a shortest tour that visits each city 
once and returns to the starting city. Despite its simple statement, and 
practical importance, the problem has eluded an efficient exact 
solution. However, we do have several good approximation algorithms, 
including the 2-Opt algorithm described in class.

===================================================
Due: Montag, 30. Januar 2012
===================================================

Compiled originally in Java 1.7
To run the program, requires one command line parameter, being the number of points to generate.
Once the program completes, the right-side tree pane allows you to view back on all of the steps
the program took in order to reach this point. The two blue lines on a tour drawing are the changes
that were made to that tour over the previous tour (hence why the first tour has no blue lines).
The numbers on the map are put above the dots they represent; these numbers represent the order in
which the dots were first generated (0 being the first dot, 1 being the dot that was originally
after 1, so on so forth).

===================================================
Files Listing:
 - City.class
 - City.java
 - Map.class
 - Map.java
 - Map.java~
 - MapWindow.class
 - MapWindow.java
 - MapWindowTourHistory$1.class
 - MapWindowTourHistory$TourSelectionListener.class
 - MapWindowTourHistory.class
 - MapWindowTourHistory.java
 - MapWindowTourHistory.java~
 - README
 - README.txt (clone of README)
 - TSP.class
 - TSP.java
 - TSP.java~
 - Timing.class
 - Timing.java
 - Tour.class
 - Tour$TourSuggestion.class
 - Tour.java
 - Tour.java~

===================================================
Known problems:

Despite the fact that the implementation logic employed here was later checked against a number
of other 2-opt implementations (including a textual graduate paper), the implementation here doesn't
run faithfully always. That said, the majority of the time, the program will run and complete
properly, but under certain circumstances (noted from ten cities and up, but most commonly found
in larger city populations such as 20+), there is a chance that the program will begin to loop
and will not finish the loop. From looking at the data produced, it seems that the code will reach
a point where the optimize() function in Tour begins to cycle through certain sets of Tours
repeatedly, seen thus far as being on a consistent interval of 4 between repetition or 8 between
repetition, but always that same number during the specific instance of the problem.

After several hours of intense work, it has been declared "annoying."
