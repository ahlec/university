import javax.swing.*;
import java.awt.*;
import javax.swing.tree.*;
import java.util.ArrayList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

public class MapWindowTourHistory extends JScrollPane
{
  ArrayList<Tour> tours = new ArrayList<Tour>();
  JTree tree;
  DefaultMutableTreeNode treeNode;
  int numberToursAdded = 0;
  MapWindow window;
  public MapWindowTourHistory(MapWindow mapWindow)
  {
    super();
    this.window = mapWindow;
    setEnabled(false);
    treeNode = new DefaultMutableTreeNode("Step History");
    tree = new JTree(treeNode);
    getViewport().add(tree, null);
    setPreferredSize(new Dimension(250, Map.HEIGHT + Map.SIZE_OF_DOTS));
  }
  public void optimizationComplete()
  {
    for (int index = 0; index < tree.getRowCount(); index++)
      tree.expandRow(index);
    setEnabled(true);
    tree.addTreeSelectionListener(new TourSelectionListener(tours, window));
  }
  private class TourSelectionListener implements TreeSelectionListener
  {
    public TourSelectionListener(ArrayList<Tour> tours, MapWindow window)
    {
      this.tours = tours;
      this.window = window;
    }
    private ArrayList<Tour> tours;
    private MapWindow window;
    public void valueChanged(TreeSelectionEvent selectionEvent)
    {
      JTree tree = (JTree)selectionEvent.getSource();
      DefaultMutableTreeNode targetItem = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
      if (targetItem.getParent() == null)
        return;
      int index = targetItem.getParent().getIndex(targetItem);
      window.setTour(tours.get(index), index + 1);
    }
  }
  public void addTour(Tour tour)
  {
    numberToursAdded++;
    tours.add(tour.getCopy());
    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode("Step " + numberToursAdded);
    ((DefaultTreeModel)tree.getModel()).insertNodeInto(newNode, treeNode, treeNode.getChildCount());
    tree.expandPath(new TreePath(newNode.getPath()));
  }
}
