import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.FlowLayout;

public class MapWindow extends JFrame
{
  public MapWindow()
  {
    super("Project 1 - The Traveling Salesperson");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
  }
  Map mapComponent = null;
  public void setMap(Map map)
  {
    mapComponent = map;
  }
  public void setTour(Tour tour, int stepNumber)
  {
    mapComponent.setTour(tour, stepNumber);
    mapComponent.repaint();
  }
}
