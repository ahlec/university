import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;

public class Map extends JPanel
{
  public Map()
  {
    setPreferredSize(new Dimension(WIDTH + SIZE_OF_DOTS,
    	HEIGHT + SIZE_OF_DOTS));
  }
  public static final int WIDTH = 500;
  public static final int HEIGHT = 500;
  public static final int SIZE_OF_DOTS = 12;
  private Tour tour = null;
  private boolean optimized = false;
  private Color brown = new Color(139, 69, 19);
  private int finalSteps = 0;
  private int currentStepNumber = 0;
  public void setTour(Tour tour)
  {
    this.tour = tour;
    currentStepNumber = 0;
  }
  public void setTour(Tour tour, int stepNumber)
  {
    currentStepNumber = stepNumber;
    this.tour = tour;
  }
  public void setOptimized(boolean value, int totalSteps)
  {
    optimized = value;
    finalSteps = totalSteps;
    currentStepNumber = finalSteps;
  }
  private boolean abort = false;
  public void abort(boolean value) { abort = value; }
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    g.setColor(Color.GREEN);
    g.fillRect(0,0,WIDTH + SIZE_OF_DOTS, HEIGHT + SIZE_OF_DOTS);
    g.setColor(brown);
    if (tour != null)
    {
      for (int index = 0; index < tour.size(); index++)
      {
        if (tour.wasChanged(index))
          g.setColor(Color.BLUE);
        else
          g.setColor(brown);
        g.fillOval(tour.get(index).getX(),
		tour.get(index).getY(),
		SIZE_OF_DOTS, SIZE_OF_DOTS);
        g.drawString(tour.get(index).getIdentifier(), tour.get(index).getX() + SIZE_OF_DOTS / 2, tour.get(index).getY());
        if (tour.wasChanged(index) && tour.changeState(index) % 2 == 1)
          g.setColor(brown);
	g.drawLine(tour.get(index).getX() + SIZE_OF_DOTS / 2,
		tour.get(index).getY() + SIZE_OF_DOTS / 2,
		tour.get(index + 1 == tour.size() ? 0 : index + 1).getX() +
		SIZE_OF_DOTS / 2,
		tour.get(index + 1 == tour.size() ? 0 : index + 1).getY() +
		SIZE_OF_DOTS / 2);
      }
    }
    g.setColor(abort ? Color.RED : Color.BLACK);
    g.drawString(optimized ? (abort ? "Aborted." : "Tour optimized (Display step " + currentStepNumber + " of " + finalSteps + ").")  : "Optimizing" +
    	new String(new char[optimizingPeriods]).replace("\0", "."), 10, 20);
    optimizingPeriods = (optimizingPeriods == 3 ? 1 : optimizingPeriods + 1);
    g.drawString("Tour length: " + Double.toString(tour.getLength()), 10, 35);
  }
  int optimizingPeriods = 1;
}
