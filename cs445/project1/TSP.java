import javax.swing.*;
import java.util.Random;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.ArrayList;

public class TSP
{
  public static void main(String[] args)
  {
    if (args.length == 0)
    {
      System.err.println("First parameter is required; a number signifying how many cities should be generated for the process.");
      return;
    }
    int numberOfCities = Integer.parseInt(args[0]);

    MapWindow mapWindow = new MapWindow();
    Map map = new Map();
    mapWindow.setMap(map);
    MapWindowTourHistory tourHistory = new MapWindowTourHistory(mapWindow);
    mapWindow.getContentPane().add(map);
    mapWindow.getContentPane().add(tourHistory);
    mapWindow.pack();
    mapWindow.setLocationRelativeTo(null);
    mapWindow.setVisible(true);

    Random randomGenerator = new Random();
    Tour tour = new Tour();
    for (int iteration = 0; iteration < numberOfCities; iteration++)
      tour.add(new City(randomGenerator.nextInt(Map.WIDTH),
      	randomGenerator.nextInt(Map.HEIGHT), Integer.toString(iteration)));
    map.setTour(tour);

    int steps = 1;
    tourHistory.addTour(tour);
    tour = tour.optimize();
    ArrayList<Tour> previousTours = new ArrayList<Tour>();
    while (tour != null)
    {
      steps++;
      map.setTour(tour);
      map.repaint();
      try { tourHistory.addTour(tour); }
      catch (java.lang.OutOfMemoryError e)
      {
        System.err.println("Program has exceeded memory space and is crashing. Please stand back.");
        System.exit(0);
      }
      if (steps > 5000)
      {
        map.abort(true);
        break;
      }
      //if (steps > 200)
      //  Timing.sleep(1000);
      previousTours.add(tour.getCopy());
      tour = tour.optimize(previousTours);
    }
    map.setOptimized(true, steps);
    tourHistory.optimizationComplete();
    map.repaint();
  }
}
