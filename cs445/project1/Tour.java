import java.util.ArrayList;
import java.lang.Math;
import java.awt.geom.Line2D;

public class Tour extends ArrayList<City>
{
  int[] changes = new int[0];
  public Tour optimize() { return optimize(new ArrayList<Tour>()); }
  public Tour optimize(ArrayList<Tour> ignore)
  {
    ArrayList<TourSuggestion> suggestions = new ArrayList<TourSuggestion>();
    for (int index = 0; index < size() - 2; index++)
      for (int newB = index + 2; newB < size(); newB++)
      {
        double currentLength = getDistanceBetween(get(index), get(index + 1 == size() ? 0 : index + 1)) +
		getDistanceBetween(get(newB), get(newB + 1 == size() ? 0 : newB + 1));
	if (getDistanceBetween(get(index), get(newB)) + getDistanceBetween(get(index + 1 == size() ? 0 : index + 1),
		get(newB + 1 == size() ? 0 : newB + 1)) < currentLength)
        {
          int[] newChanges = new int[4];
          newChanges[0] = index;
          newChanges[1] = index + 1;
          newChanges[2] = newB;
          newChanges[3] = newB + 1;
          int distanceToPreviousRepetition = -1;
          for (int retrib = 1; retrib <= ignore.size(); retrib++)
            if (ignore.get(ignore.size() - retrib).changes == newChanges)
            {
              distanceToPreviousRepetition = retrib;
              break;
            }
          if (distanceToPreviousRepetition >= 0)
          {
            System.out.println("repetition detected.");
            continue;
          }
          suggestions.add(new TourSuggestion(index, newB, getDistanceBetween(get(index), get(newB)) +
		getDistanceBetween(get(index + 1 == size() ? 0 : index + 1), get(newB + 1 == size() ? 0 : newB + 1))));
        }
      }
      int bestTourIndex = -1;
      double bestTourLength = 0.0;
      for (int index = 0; index < suggestions.size(); index++)
        if (bestTourIndex == -1 || suggestions.get(index).getChangeLength() < bestTourLength)
        {
          bestTourIndex = index;
          bestTourLength = suggestions.get(index).getChangeLength();
        }
      if (bestTourIndex == -1)
        return null;
      Tour newTour = this;
      newTour.swapCities(suggestions.get(bestTourIndex).getIndexA() + 1, suggestions.get(bestTourIndex).getIndexB());
      changes = new int[4];
      changes[0] = suggestions.get(bestTourIndex).getIndexA();
      changes[1] = suggestions.get(bestTourIndex).getIndexA() + 1;
      changes[2] = suggestions.get(bestTourIndex).getIndexB();
      changes[3] = suggestions.get(bestTourIndex).getIndexB() + 1;
      return newTour;
  }
  private class TourSuggestion
  {
    int indexA, indexB;
    double changeLength;
    public TourSuggestion(int indexA, int indexB, double changeLength)
    {
      this.indexA = indexA;
      this.indexB = indexB;
      this.changeLength = changeLength;
    }
    public int getIndexA() { return indexA; }
    public int getIndexB() { return indexB; }
    public double getChangeLength() { return changeLength; }
  }
  public Tour getCopy()
  {
    Tour newTour = new Tour();
    for (int index = 0; index < size(); index++)
      newTour.add(get(index));
    newTour.changes = changes;
    return newTour;
  }
  public boolean wasChanged(int index)
  {
    for (int search = 0; search < changes.length; search++)
      if (changes[search] == index)
        return true;
    return false;
  }
  public int changeState(int index)
  {
    for (int search = 0; search < changes.length; search++)
      if (changes[search] == index)
        return search;
    return -1;
  }
  public double getLength()
  {
    double length = 0;
    for (int index = 0; index < size(); index++)
    {
      double xCoords = Math.pow(get(index).getX() -
      	get(index + 1 == size() ? 0 : index + 1).getX(), 2);
      double yCoords = Math.pow(get(index).getY() -
      	get(index + 1 == size() ? 0 : index + 1).getY(), 2);
      length += Math.sqrt(xCoords + yCoords);
    }
    return length;
  }
  public double getDistanceBetween(City cityA, City cityB)
  {
    return Math.sqrt(Math.pow(cityA.getX() - cityB.getX(), 2) + Math.pow(cityA.getY() - cityB.getY(), 2));
  }
  private void swapCities(int indexA, int indexB)
  {
    City cityA = get(indexA);
    set(indexA, get(indexB));
    set(indexB, cityA);
  }
  private boolean linesIntersect(City a1, City a2, City b1, City b2)
  {
    Line2D lineA = new Line2D.Double(a1.getX(), a1.getY(), a2.getX(), a2.getY());
    Line2D lineB = new Line2D.Double(b1.getX(), b1.getY(), b2.getX(), b2.getY());
    return lineA.intersectsLine(lineB);
  }
  private boolean isUncrossedSolution(int focusIndex)
  {
    for (int searchIndex = focusIndex + 1; searchIndex < size(); searchIndex++)
    {
      if (linesIntersect(get(focusIndex), get(focusIndex + 1), get(searchIndex), get(searchIndex + 1 == size() ? 0 : searchIndex + 1)))
	return false;
    }
    return true;
  }
  public String toString()
  {
    String str = "[TOUR - " + getLength() + "]\n";
    for (int index = 0; index < size(); index++)
      str += "  - " + get(index) + "\n";
    return str;
  }
}


