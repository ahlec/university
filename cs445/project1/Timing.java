import java.util.Date;

public class Timing
{
  public static void sleep(long milliseconds)
  {
    Date date = new Date();
    long start = date.getTime();
    long now;
    do
    {
      date = new Date();
      now = date.getTime();
    } while ((now - start) < milliseconds);
  }
}
