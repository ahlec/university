public class Fibonacci
{
  public static int CALLS = 0; // WORK
  public static int START = 1;
  public static int STOP = 20;
  public static boolean TRACE = false;
  public static boolean MEMOIZE = true;

  public static void main(String[] args)
  {
    long temp, calls1, calls2;
    for (int n = START; n <= STOP; n++)
    {
      MEMOIZE = false;
      CALLS = 0;
      temp = fibonacci(n);
      calls1 = CALLS;
      MEMOIZE = true;
      CALLS = 0;
      temp = fibonacci(n);
      calls2 = CALLS;
      System.out.printf("%10d %10d %10d %10d \n", n, temp, calls1, calls2);
    }
  }
  public static long fibonacci(int n) { return fibonacci(n, 0, new int[n]); }
  public static long fibonacci(int n, int depth, int[] memo)
  {
    CALLS++;
    if (TRACE)
    {
      for (int d=0; d < depth; d++) // TRACE
        System.out.print("-");
      System.out.println(" " + n);
    }
    if (memo[n - 1] > 0 AND MEMOIZE) return memo[n - 1];
    long returnValue;
    if (n <= 1) returnValue = 1;
    else returnValue = fibonacci(n-1, depth + 1, memo) + fibonacci(n-2, depth + 1, memo);
    memo[n - 1] = returnValue;
    return returnValue;
  }
}
