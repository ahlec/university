public class LinkedList
{
  private class Node
  {
    Object value;
    Node next;
    public Node()
    {
      value = 0;
      next = null;
    }
    public Node(Object value)
    {
      this.value = value;
      next = null;
    }
  }
  
  Node head;

  public LinkedList()
  {
    head = null;
  }

  public void printList()
  {
    Node current;
    for (current = head; current != null; current = current.next)
    {
      System.out.println(current.value + " ");
    }
  }

  public void printListReverse()
  {
    printListReverseHelp(head);
  }

  public void printListReverseHelp(Node h)
  {
    // Base case
    if (h.next == null)
      System.out.println(h.value + " ");
    else
    {
      printListReverseHelp(h.next);
      System.out.println(h.value + " ");
    }
  }

  public void insertFront(Object item)
  {
    Node new_node = new Node(item);
    new_node.next = head;
    head = new_node;
  }

  public static void main(String[] args)
  {
    LinkedList ll = new LinkedList();
    ll.insertFront(4);
    ll.insertFront(3);
    ll.insertFront(2);
    ll.insertFront(1);
    ll.insertFront(0);
    ll.printList();
    ll.printListReverse();
  }
}
