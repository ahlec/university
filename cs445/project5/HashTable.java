public class HashTable
{
  private class Entry
  {
    String _key;
    Object _contents;
    public Entry(String key, Object value)
    {
      _key = key;
      _contents = value;
    }
  }
  
  Entry[] _entries;
  int _capacity;

  public HashTable(int capacity)
  {
    _entries = new Entry[capacity]; 
    _capacity = capacity;
  }

  public void add(String key, Object value)
  {
    Entry newEntry = new Entry(key, value);
    int destinationIndex = hashKey(key);
    int initialIndex = destinationIndex;
    while (_entries[destinationIndex] != null)
    {
      if (_entries[destinationIndex]._key.equals(key))
      {
        // throw new Exception(); /* Not supported by the project driver, so this won't work */
        return;
      }
      destinationIndex++;
      if (_entries.length <= destinationIndex)
      {
        destinationIndex = 0;
      }
      if (destinationIndex == initialIndex) // We've made a full circle
      {
        return;
        // throw new OverflowException(); /* Not supported by the project driver, so this won't work */
      }
    }
    _entries[destinationIndex] = newEntry;
  }

  private int hashKey(String key)
  {
    return Math.abs(key.hashCode()) % _capacity;
  }

  public Object getValue(String key)
  {
    int destinationIndex = hashKey(key);
    int initialIndex = destinationIndex;
    while (_entries[destinationIndex] != null && !_entries[destinationIndex]._key.equals(key))
    {
      destinationIndex++;
      if (_entries.length <= destinationIndex)
      {
        destinationIndex = 0;
      }
      if (destinationIndex == initialIndex)
      {
        return null;
      }
    }
    return (_entries[destinationIndex] != null ? _entries[destinationIndex]._contents : null);
  }
}
