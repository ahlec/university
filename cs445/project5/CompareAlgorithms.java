///
///  Contents: Compare hash table to binary search tree.
///  Author:   John Aronis
///  Date:     January 2012
///

import java.util.ArrayList ;
import java.util.Date ;
import java.util.Random ;

public class CompareAlgorithms {

  public static int[] SIZES       = { 10000, 20000, 30000, 40000, 50000, 100000 } ;
  public static int   PROBES      = 100000 ;
  public static int   KEY_LENGTH  = 20 ;
  public static float LOAD_FACTOR = 0.5f ;

  public static Random R = new Random() ;

  public static void main(String[] args) {

    HashTable HT ;
    BinarySearchTree BST ;
    String[] keys ;

    System.out.println("           SIZE         HT Add      HT Search        BST Add     BST Search") ;
    for (int size : SIZES) {

      // Create keys:
      keys = new String[size] ;
      for (int s=0 ; s<size ; s++) { keys[s]=key(KEY_LENGTH) ; }
      System.out.printf("%15d",size) ;

      // Hash keys (and dummy values) into hash table:
      HT = new HashTable( (int)(1.0/LOAD_FACTOR*size) ) ;
      startTimer() ;
      for (int s=0 ; s<size ; s++) { HT.add(keys[s],"foo") ; }
      System.out.printf("%15d",getElapsedTime()) ;

      // Search (misses) hash table PROBES number of times:
      startTimer() ;
      for (int p=0 ; p<PROBES ; p++) { HT.getValue("miss") ; }
      System.out.printf("%15d",getElapsedTime()) ;

      // Add keys (and dummy values) to hash table:
      BST = new BinarySearchTree() ;
      startTimer() ;
      for (int s=0 ; s<size ; s++) { BST.add(keys[s],"foo") ; }
      System.out.printf("%15d",getElapsedTime()) ;

      // Search (misses) tree PROBES number of times:
      startTimer() ;
      for (int p=0 ; p<PROBES ; p++) { BST.getValue("miss") ; }
      System.out.printf("%15d",getElapsedTime()) ;

      System.out.println() ;

    }
  }

  public static String key(int length) {
    String result = "key" ;
    for (int i = 0 ; i < length-3 ; i++) { result = result + R.nextInt(10) ; }
    return result ;
  }

  public static long startTime ;
  public static void startTimer() { startTime = ( (new Date()).getTime() ) ; }
  public static long getElapsedTime() { return ( (new Date()).getTime() - startTime ) ; }

}

/// End-of-File

