public class Test
{
  public static void main(String[] args)
  {
    HashTable hashTable = new HashTable(100);
    BinarySearchTree searchTree = new BinarySearchTree();
    for (int index = 0; index < args.length; index += 2)
    {
      System.out.println("{" + args[index] + "} => '" + args[index + 1] + "'");
      searchTree.add(args[index], args[index + 1]);
      hashTable.add(args[index], args[index + 1]);
    }

    System.out.println("");

    for (int index = 0; index < args.length; index += 2)
    {
      
      System.out.println("{" + args[index] + "} = " + searchTree.getValue(args[index]));
      System.out.println("{" + args[index] + "} = " + hashTable.getValue(args[index]));
      System.out.println("");
    }

  }
}
