public class BinarySearchTree
{
  private class Node
  {
    String _key;
    Object _contents;
    Node _left;
    Node _right;
    public Node(String key, Object value)
    {
      _key = key;
      _contents = value;
      _left = null;
      _right = null;
    }
  }

  private Node _root;

  public BinarySearchTree()
  {
    _root = null;
  }

  public void add(String key, Object value)
  {
    Node newNode = new Node(key, value);
    if (_root == null)
    {
      _root = newNode;
      return;
    }

    Node current = _root;
    int comparedValue;
    while (true)
    {
      comparedValue = key.compareTo(current._key);
      if (comparedValue < 0)
      {
        if (current._right == null)
        {
          current._right = newNode;
          break;
        } else
        {
          current = current._right;
        }
      } else// if (comparedValue > 0)
      {
        if (current._left == null)
        {
          current._left = newNode;
          break;
        } else
        {
          current = current._left;
        }
      }
    }
  }

  public Object getValue(String key)
  {
    Node current = _root;
    int comparedValue;
    while (current != null)
    {
      comparedValue = key.compareTo(current._key);
      if (comparedValue == 0)
      {
        return current._contents;
      } else if (comparedValue < 0)
      {
        current = current._right;
      } else
      {
        current = current._left;
      }
    }

    return null; // Nothing found, return null
  }

}
